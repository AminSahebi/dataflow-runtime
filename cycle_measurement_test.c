#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdint.h>
#ifdef PAPI
#include <papi.h>
#endif
uint64_t xfib(int n) { return n < 2 ? (uint64_t)n : xfib(n-1) + xfib(n-2); }
#define rdtscll(val) {                                           \
	unsigned int __a,__d;                                        \
	asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
	(val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}

int main(int argc, char **argv)
{
	struct timespec tv;
	long long start,stop;
#ifdef PAPI
	//  gcc fib-tsu4.c -I/${PAPI_DIR}/include -L/${PAPI_DIR}/lib -O3 -o papi-test -lpapi
	if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT)
		exit(1);
	start = PAPI_get_real_cyc();
	#ifdef hello
	printf("hello_world\n");	
	#endif
	#ifdef math
	xfib(10);
	#endif 	
	stop = PAPI_get_real_cyc();
	printf("total cycles : %lld\n",stop-start);
#endif	
#ifdef clk
	start = clock();
	#ifdef hello
	printf("hello_world\n");	
	#endif
	#ifdef math
	xfib(10);
	#endif 	
	stop = clock();
	printf("total cycles : %lld\n",stop-start);
#endif
#ifdef Wtime
	clock_gettime(CLOCK_REALTIME, &tv);
	start= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	#ifdef hello
	printf("hello_world\n");	
	#endif
	#ifdef math
	xfib(10);
	#endif 	
	clock_gettime(CLOCK_REALTIME, &tv);
	stop= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	printf("total time : %lld\n",stop-start);
#endif
#ifdef hardware_counter
	rdtscll(start);
	#ifdef hello
	printf("hello_world\n");	
	#endif
	#ifdef math
	xfib(10);
	#endif 	
	rdtscll(stop);	
	printf("total cycles : %lld\n",stop-start);
#endif
	return 0;
}
