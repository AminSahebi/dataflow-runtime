#!/bin/bash

if [ "$1" = "" ]; then echo "You must specify a Fibonacci index (n)"; exit 1; fi

i=$1
DRT_DEBUG=0 DRT_FSIZE=10000000 ./fib-tsu4 $i
