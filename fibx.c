#include "tsu.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "time.h"


#define DEBUG_FIB

uint64_t serialfib(int n)
{
#ifdef RECFIB
	if (n==-2) return -1;
	if (n==-1) return 1;
	return n < 2 ? n : serialfib(n-1) + serialfib(n-2);
#else
	uint64_t a=-1,b=1; 
	int i=-2;
	for(; i < n; ++i) {
		uint64_t t = a + b; 
		a = b;
		b = t; 
	}
	return a;
#endif
}

typedef struct { xtid_t tloc; uint64_t n1; uint64_t n2; } adder_s;
void adder(void)
{
	const adder_s* fp=xpreload();
	xtid_t xloc = fp->tloc;
	uint64_t *xp = xpoststor(xloc);
	xp[_X_OFF(xloc)] = fp->n1 + fp->n2;

	xdecrease(xloc,1);
	xdestroy();
}

typedef struct { xtid_t tloc; uint64_t n; uint64_t t; } fib_s;
void fib(void)
{
	const fib_s* fp=xpreload();
	uint64_t n  = fp->n; // receive n
	uint64_t t  = fp->t; // receive t (threshold)
	uint64_t t_lim = t + 1; 
	xtid_t xloc = fp->tloc; // target location
#ifdef DEBUG_FIB
	printf("FIB: fp 0x%lx n %ld t %ld tloc 0x%lx\n",fp,n,t,xloc);
	fflush(stdout);
#endif
	if (n <= t_lim || n < 2 ) {
		uint64_t *tp = xpoststor(xloc);
		tp[_X_OFF(xloc)] = serialfib(n);
#ifdef DEBUG_FIB  
		printf("FIB UNDER THREASHOLD: serial count of n=%d is: %d. TLOC: %lx\n",n,*tp,tp);
		fflush(stdout);
#endif
		xdecrease(xloc,1);
	}
	else {
		xtid_t xadd = xschedule64(&adder,3); // spawn adder
		adder_s* tadd = xpoststor(xadd);
		tadd->tloc = xloc;  // add.dst is this.dst
		xdecrease(xadd,1);

		xtid_t xfib1 = xschedule64(&fib,3);  // spawn fib1
		fib_s* tfib1 = xpoststor(xfib1);
		tfib1->tloc  = _XREF(xadd,1); // fib1.tloc is add[1]
		tfib1->n     = n-1;           // send fib1, n-1
		tfib1->t     = t;             // send fib1, threshold
		xdecrease(xfib1,3);

		xtid_t xfib2 = xschedule64(&fib,3);  // spawn fib2
		fib_s* tfib2 = xpoststor(xfib2);
		tfib2->tloc  = _XREF(xadd,2); // fib2.tloc is add[2]
		tfib2->n     = n-2;           // send fib2, n-2
		tfib2->t     = t;             // send fib2, threshold
		xdecrease(xfib2,3);
	}
	xdestroy();
}


uint64_t nn=0; // input, for checking purposes
uint64_t th=0; // input, for checking purposes

typedef struct { uint64_t res; } report_s;
void report(void)
{
	const report_s* fp=xpreload();
	uint64_t res=fp->res;
	xzonestop(1);
	xdestroy();
	printf("\t++report\n");
	printf("\txsm fib= %ld\n",res);
	uint64_t ser_res = serialfib(nn);

	printf(res==ser_res?"\t*** SUCCESS\n":"\t***FAILURE\n");
	fflush(stdout);
	//xdestroy();
}

int main(int argc, char **argv)
{	
	int retval;
	nn = 16;//default
	th = 9;
	if (argc > 1)
		nn = atoi(argv[1]);
	if (argc > 2)
		th = atoi(argv[2]);
	printf("Fibonacci %lu,%lu started\n",nn,th);
	xzonestart(1);
	
	xtid_t xrep = xschedule64(&report,1); // spawn reporter
	xconstrain(xrep,0x1); // constrain execution on node 1
	
	xtid_t xfib = xschedule64(&fib, 3);  // spawn fib
	fib_s* tfib = (fib_s*)xpoststor(xfib);
	tfib->tloc = _XREF(xrep,0); // edge: fib.out -> report[0]
	tfib->n    = nn;
	tfib->t    = th;
	xdecrease(xfib,3);

	//xdestroy();
	//
	return 0;
}
