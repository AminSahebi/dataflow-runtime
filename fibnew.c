#include "tsu.h"
#include <stdio.h>

//#define THRESHOLD 24
#define THRESHOLD 2
#define RECFIB

int nn=0; // input, for checking purposes
int th=0; // threshold, to adjust granularity

// reference version
uint64_t fastfib(int n)
{
#ifdef RECFIB
	return n < 2 ? n : fastfib(n-1) + fastfib(n-2);
#else
	uint64_t a=0,b=1; 
	int i=0;
	for(; i < n; ++i) {
		uint64_t t = a + b; 
		a = b;
		b = t; 
	}
	return a;
#endif
}

void adder(void)
{
	const uint64_t* fp=xpreload();
	xtid_t xloc = fp[0];
	uint64_t *xp = xpoststor(xloc);
	xp[_TLOC_OFF(xloc)] = fp[1]+fp[2];
	xdecrease(xloc,1);
	//xdestroy();
}

void fib(void)
{
	const uint64_t* fp=xpreload();
	int n = fp[1]; // receive n
	xtid_t xloc = fp[0]; // target location
	int64_t t_lim = th + 1;
#ifdef DEBUG
	printf("\tFIB:  n-> %ld t-> %ld tloc-> 0x%x\n",n, th , xloc);
	fflush(stdout);
#endif
	if (n <= t_lim || n < 2) {
		uint64_t *tp = xpoststor(xloc);
		tp[_TLOC_OFF(xloc)] = fastfib(n);
		xdecrease(xloc,1);
	}
	else {
		xtid_t xadd = xschedule64(&adder,3); // spawn adder
		uint64_t* tadd = xpoststor(xadd);
		tadd[0]=xloc; // add.dst is this.dst
		xdecrease(xadd,1);

		xtid_t xfib1 = xschedule64(&fib,2);  // spawn fib1
		uint64_t* tfib1 = xpoststor(xfib1);
		tfib1[0]=_TLOC(xadd,1); // fib1.dst is add[1]
		tfib1[1]= n-1; // send fib1, n-1
		xdecrease(xfib1,2);

		xtid_t xfib2 = xschedule64(&fib,2);  // spawn fib2
		uint64_t* tfib2 = xpoststor(xfib2);
		tfib2[0]=_TLOC(xadd,2); // fib2.dst is add[2]
		tfib2[1]= n-2; // send fib2, n-2
		xdecrease(xfib2,2);
	}
	xdestroy();
}


// stat reporting
uint64_t tt;
uint64_t ts0[100],ts1[100];

void report(void)
{
	const uint64_t* fp=xpreload();
	printf("\t++report\n");
	uint64_t n=fp[0];
	xzonestop(1);
	printf("\tDataflow fib= %lu\n",n);
	df_exit();
	uint64_t nx = fastfib(nn);

	printf("\tReference fib= %lu\n",nx);

	printf(n==nx?"\t*** SUCCESS\n":"\t***FAILURE\n");
	//xdestroy();
}

int main(int argc, char **argv)
{
	nn = 10;
	th = 1;
	if (argc > 1)
		nn = atoi(argv[1]);
	if (argc > 2) 
		th = atoi(argv[2]);	
	printf("\tstarting main...\n");
	printf("\tcomputing fibonacci(%d ,%d)\n",nn, th);
	tt = df_tstamp(ts0); //Initial the allocation memory
	xzonestart(1);
	xtid_t xrep = xschedule64(&report,1); // spawn reporter
	xconstrain(xrep,0x1); // constrain execution on node 1
	xtid_t xfib = xschedule64(&fib, 2);  // spawn fib
	uint64_t* tfib = xpoststor(xfib);
	tfib[0] = _TLOC(xrep,0); // edge: fib.out -> report[0]
	tfib[1] = nn;
	xdecrease(xfib,2);
	xdestroy(); 
	return 0;
}

