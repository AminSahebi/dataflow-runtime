/*
   Copyright (c) 2015-2018 Roberto Giorgi - University of Siena
   Copyright (c) 2015-2018 Hewlett-Packard Development Company, L.P.

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/
//$Id: xlog.h 13 2015-05-22 08:55:25Z giorgi $

#ifndef XLOG_H_INCLUDED
#define XLOG_H_INCLUDED
#include <stdio.h>

#ifdef _DEBUG_THIS_
#define XLOG(...) printf(__VA_ARGS__),fflush(stdout)
#else
#define XLOG(...)  /**/
#endif
#define XERR(...) fprintf(stderr,__VA_ARGS__),fflush(stderr)

/* Note: the following flag WILL increase the simulation time */
#ifdef _DEBUG_VERBOSE_
#define XLOG_VERBOSE(...) printf(__VA_ARGS__)
#else
#define XLOG_VERBOSE(...)  /**/
#endif

#if defined _XLOG_XSM_CLASS_ || defined _XLOG_XSM_CLASS_VERBOSE_
#define XLOG4(...) fprintf(stderr,"[XSM] "),fprintf(stderr,__VA_ARGS__),fflush(stderr)
#else
#define XLOG4(...)
#endif
#ifdef _XLOG_XSM_CLASS_VERBOSE_
#define XLOG4V(...) fprintf(stderr,"[XSM_V] "),fprintf(stderr,__VA_ARGS__),fflush(stderr)
#else
#define XLOG4V(...)
#endif

#endif

