#!/bin/bash
# 210121 Roberto Giorgi - University of Siena, Italy
##########################################################################
# MIT LICENSE:
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERi
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
##########################################################################
# $id$
VERSION="210121"
AMY_ENV="amyenv.sh"
CFGDEFAULT="amy.cfg"
MYSCRIPTNAME=`basename $0`
pushd `dirname $0` > /dev/null; AMY_SCRIPTPATH=`pwd -P`; popd > /dev/null
CFGFILE="$AMY_SCRIPTPATH/$CFGDEFAULT"
THISSCRIPT="$AMY_SCRIPTPATH/$MYSCRIPTNAME"
md5sig=`md5sum $0|awk '{print $1}'`
MYSIG="${md5sig:0:2}${md5sig:(-2)}" # reduced-hash: first 2 char and last two char
VERSION1="v${VERSION}-${MYSIG}"
a=`pwd`
BDIR=`basename "$a"`
MYBASEDIR=`echo "$BDIR"|awk '{print tolower($0)}'`
AMY_ENVPATH=${AMY_SCRIPTPATH}/${AMY_ENV}

usage() {
   echo "Usage: $0 [<options>] <SCRIPT>"
   echo "   where:"
   echo "   <SCRIPT>  is a **full-path** SCRIPT"
   echo ""
   echo "   and <options> can be:"
   echo "   -v           verbose mode (more '-v': more details)"
   echo "   -d           debug mode   (more '-d': more details)"
   echo "   -q           quiet mode"
   echo "   --cmd <COMMAND> give a specific command-word with the following syntax (see amy.cfg)"
   echo "         <COMMAND>::=<command_acronym_on_two_letters><machine|machine list|machine_set>['Q']"
   echo "   --stop       this should be the last check"
   echo "   --tag <tag>  operate with this tag"
   echo "   --atj <id>   id of the at-job used for timout case"
   echo "   --next <tag> next operate with this tag"
   echo "   --timeout <minutes> | -t <minutes>"
   echo "   --repeat <minutes> | -r <minutes>"
   echo "   --mesg <mesg> | -m <mesg>"
   echo "   --version   print version"
   echo "   --help | -h for help"
   echo ""
}

printversion() {
   echo "$0: version $VERSION1";
}

#####################################################
function cecho ()            # Color-echo.
                             # Argument $1 = message
                             # Argument $2 = color
{
local default_msg="No message passed."
                             # Doesn't really need to be a local variable.

message=${1:-$default_msg}   # Defaults to default message.
color=${2:-$white}           # Defaults to white, if not specified.

  echo -en "$color"
  echo -n "$message"
  tput sgr0             # reset to normal
}

#####################################################
function comment() {
   if [ "$QUIET" = "0" ]; then echo "C: $1"; fi
}

#####################################################
function debug2() {
   if [ "$DEBUG" -gt "1" ]; then echo "D2: $1"; fi
}

#####################################################
function debug1() {
   if [ "$DEBUG" -gt "0" ]; then echo "D1: $1"; fi
}

#####################################################
function verbose2() {
   if [ "$VERBOSE" -gt "1" ]; then echo "V: $1"; fi
}

#####################################################
function verbose1() {
   if [ "$VERBOSE" -gt "0" ]; then echo "V: $1"; fi
}


shopt -s extglob
declare -A longoptspec
amc_commandline() {
   #-----------------------------------------------------------------------------
   # Colors
   black='\E[30;40m'
   red='\E[31;40m'
   green='\E[32;40m'
   yellow='\E[33;40m'
   blue='\E[1;34;40m'
   magenta='\E[35;40m'
   cyan='\E[36;40m'
   white='\E[37;40m'

   # set defaults
   VERBOSE="0"
   DEBUG="0"
   QUIET="0"
   AMYCMD=""
   STOP="0"
   DRYRUN="0"
   TIMEREP="5"
   MYTIMEMAX=""
   MYMESG=""
   MYTAG=""
   MYNEXTTAG=""
   ATJID=""
   local printusage="0"

   #Note: the firs colon ':'means taht I manage the errors myself
   optspec=":c:t:r:m:vhdqV-:"

   # Use associative array to declare how many arguments a long option
   # expects. In this case we declare that loglevel expects/has one
   # argument and range has two. Long options that aren't listed in this
   # way will have zero arguments by default.
   longoptspec=( [timeout]=1 [repeat]=1 [mesg]=1 [tag]=1 [next]=1 [atj]=1 [dryrun]=0 [cmd]=1 )

   i=$(($# + 1)) # index of the first non-existing argument
   while [ $# -gt 0 ]; do
      # Resetting OPTIND is necessary if getopts was used previously in the script.
      # It is a good idea to make OPTIND local if you process options in a function.
      OPTIND=1
#      echo "STARTING: $@"
#      echo "OPTIND: $OPTIND"
#      echo "OPTARG: $OPTARG"
#      echo "\$#: $#"
      while getopts "$optspec" opt; do
      while true; do
#      echo "opt=$opt   OPTARG='$OPTARG'  OPTIND=$OPTIND"
      case "${opt}" in # all argument that start with a '-' .........
         -)#OPTARG is name-of-long-option or name-of-long-option=value
           if [[ ${OPTARG} =~ .*=.* ]]; then # with this --key=value format only one argument is possible
              opt=${OPTARG/=*/}
              ((${#opt} <= 1)) && {
                 echo "Syntax error: Invalid long option '$opt'" >&2; exit 2;
              }
              if (($((longoptspec[$opt])) != 1)); then
                 echo "Syntax error: Option '$opt' does not support this syntax." >&2; exit 2;
              fi
              OPTARG=${OPTARG#*=}
           else if [ "${longoptspec[$OPTARG]}" = "1"  ]; then
              opt="$OPTARG"
              OPTARG=${@:OPTIND:1}
              ((OPTIND+=1))
           else #with this --key value1 value2 format multiple arguments are possible
              opt="$OPTARG"
              ((${#opt} <= 1)) && {
                 echo "Syntax error: Invalid long option '$opt'" >&2; exit 2;
              }
              OPTARG=(${@:OPTIND:$((longoptspec[$opt]))})
              ((OPTIND+=longoptspec[$opt]))
              #echo $OPTIND
              ((OPTIND > i)) && {
                 echo "Syntax error: Not all required arguments for option '$opt' are given." >&2; exit 3;
              }
           fi fi
           continue #now that opt/OPTARG are set we can process them as
           # if getopts would've given us long options
           ;;
         cmd)       AMYCMD=$OPTARG ;;
         t|timeout) MYTIMEMAX=$OPTARG ;;
         r|repeat)  TIMEREP=$OPTARG ;;
         m|mesg)    MYMESG="$OPTARG" ;;
         dryrun)    DRYRUN="1";;
         tag)       MYTAG=$OPTARG ;;
         next)      MYNEXTTAG=$OPTARG ;;
         atj)       ATJID=$OPTARG ;;
         h|help)    usage; exit 0 ;;
         v|verbose) VERBOSE=$((VERBOSE+1)) ;;
         d|debug)   DEBUG=$((DEBUG+1)) ;;
         q|quiet)   QUIET="1" ;;
         stop)      STOP="1" ;;
         V|version) printversion; exit 0 ;;
         ?) echo "Syntax error: Unknown short option '$OPTARG'" >&2; exit 2; ;;
         *) echo "Syntax error: Unknown long option '$opt'" >&2; exit 2; ;;
      esac
      break; done
      done
      shift $((OPTIND-1)) # Shift off the options and optional --.
#      echo "NON-OPTIONS: $@"
      case "$1" in
      !(-)*)
##            if [ "$SCRIPT" = "" ]; then
##            SCRIPT="$1";
##            else
               if [ $# -gt 0 ]; then echo "Syntax error: unknow parameter '$1'."; printusage="1"; fi
##            fi
         shift;
            ;;
      esac
      #echo "REMAINING: $@"
   done

#   # Everything that's left in "$@" is a non-option.  In our case, a FILE to process.
#   cmd1="$SCRIPT"

   verbose1 "* STARTING $MYSCRIPTNAME"
   # Check
   debug1 "VERBOSE=$VERBOSE"
   debug1 "DEBUG=$DEBUG"
   debug1 "QUIET=$QUIET"
   debug1 "AMYCMD=$AMYCMD"
   debug1 "DRYRUN=$DRYRUN"
   debug1 "timeout=$MYTIMEMAX"
   debug1 "repeat=$TIMEREP"
   debug1 "MYMESG='$MYMESG'"
   debug1 "MYTAG='$MYTAG'"
   debug1 "MYNEXTTAG='$MYNEXTTAG'"
   debug1 "STOP='$STOP'"
   debug1 "ATJID='$ATJID'"
   debug1 "cmd1='$cmd1'"
#   [ -z "$cmd1" ] && { echo "Syntax error: No <script> was specified."; printusage="1"; }
   [ "$printusage" = "1" ] && { usage; exit 1; }
}

#-----------------------------------------------------------------------------------
amc_commandline "$@"
# ASSERT: AMYCMD should be set here

# Sourcing
[ -s "$AMY_ENVPATH" ] || { echo "ERROR: cannot find environment file '$AMY_ENVPATH'"; exit 1; }
source $AMY_ENVPATH

#------
debug1 "AMYOPT='$AMYOPT' AMYCTIMEMAX='$AMYCTIMEMAX' AMYMACHINES='$AMYMACHINES' AMYCMD0='$AMYCMD0' AMYCHECKCMD1='$AMYCHECKCMD1' AMYCMD1='$AMYCMD1'"

#echo "* CMD: $cmd1"
debug1 "MYMESG='$MYMESG'"
debug1 "MYTAG='$MYTAG'"
debug1 "MYNEXTTAG='$MYNEXTTAG'"
debug1 "ATJID='$ATJID'"
debug1 "MYMACHINES='$MYMACHINES'"
debug1 "AMYMACHINES='$AMYMACHINES'"
debug1 "AMYCHECKCMD1='$AMYCHECKCMD1'"
[ -z "$MYMACHINES" ] || AMYMACHINES="$MYMACHINES"
AMYMACHINES=`echo "$AMYMACHINES"|tr ',' ' '`
[ -z "$MYTIMEMAX" ] || { AMYCTIMEMAX=$TIMEMAX; }
[ -z "$MYMESG" ] && { subj=""; optmesg=""; } || { subj="$MYMESG "; optmesg=" -m $MYMESG"; }
[ -z "$MYTAG" ] && optmytag="" || optmytag=" --tag $MYTAG"
[ -z "$MYNEXTTAG" ] && optmynexttag="" || optmynexttag=" --next $MYNEXTTAG"
[ -z "$ATJID" ] && optatjid="" || optatjid=" --atj $ATJID"
[ -z "$ATJID" ] && msgatjid="" || msgatjid=" job $ATJID"
#extraopts1="$optmytag$optmycfg"
extraopts1="$optmytag$optmesg$optrep$optmax$optmynexttag$optatjid --cmd $AMYCMD"
extraopts="$optmytag$optmesg"
#echo "extraopts1=$extraopts1"

# Do the Check
echo "* Launching: $AMY_LAUNCHERPATH \"$AMYMACHINES\" -e \"$AMYCHECKCMD1\" -verify \"$AMYGOODBYE\""
fppreptmp2=`mktemp ${AMY_MYHOME}/tmpamiXXXXXX` # Saving output to a tmp file (later will go to log)
debug1 "fppreptmp2=$fppreptmp2"

$AMY_LAUNCHERPATH "$AMYMACHINES" -e "$AMYCHECKCMD1" -verify "$AMYGOODBYE"|tee $fppreptmp2
#echo "GOODBYE.">$fppreptmp2

ret=${PIPESTATUS[0]} # get the exit code of the first piped command above
#echo "ret=$ret $fppreptmp2"
a=`cat $fppreptmp2`
rm -f $fppreptmp2
b=`echo "$a"|tail -1`

# Choose the a final message
if [ "$b" = "GOODBYE." ]; then
   FINALMSG="OK"
else
   if [ "$STOP" = "1" ]; then
      FINALMSG="FAILED"
   else
#   echo "$a"
      FINALMSG="NOT_READY"
   fi
fi
[ "$DRYRUN" = "1" ] && { echo "***DRYRUN*** exiting here!"; exit; }

#echo "FINALMSG='$FINALMSG'"
#echo "subj='$subj'"
#echo "a='$a'"
#echo "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-"
if [ "$FINALMSG" != "NOT_READY" ]; then
   if [ "$subj" != "" ]; then
#   echo "$a"

      # send an email
      echo "* Sending an email to '$AMY_MAILADDR' with subj '[$FINALMSG] ${subj}$msgatjid `/bin/date`'"
      echo "$a"| /usr/bin/mail -s "[$FINALMSG] ${subj}$msgatjid `/bin/date`" $AMY_MAILADDR

      # deschedule myself
#      echo "* Descheduling myself: $AMY_STOPPERPATH $THISSCRIPT$extraopts1"
      echo "* Descheduling myself: $AMY_STOPPERPATH $THISSCRIPT"
      $AMY_STOPPERPATH $THISSCRIPT $extraopts1

      if [ "$STOP" = "0" ]; then
         # Remove AT-JOB since everything went well
         if [ ! -z "$ATJID" ]; then
            echo "* Removing an at-job used for timeout (id='$ATJID')"
            atrm $ATJID
         fi

         # schedule next installer
         [ ! -z "$MYNEXTTAG" ] && {
            echo "* Scheduling the next installer: $AMY_STARTERPATH --tag ${MYNEXTTAG} --cmd $AMYCMD"
            $AMY_STARTERPATH --tag ${MYNEXTTAG} --cmd $AMYCMD
         }
      fi
   fi
fi

# Print the a final message
echo "$FINALMSG"
