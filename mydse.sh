#!/bin/bash
VERSION="210430"
# Design Space Exploration script for COTSon
# 150201 Roberto Giorgi - University of Siena, Italy
##########################################################################
# MIT LICENSE:
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERi
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
##########################################################################
# $id$
DSEDRIVERPREF="dsedriver"
LOCALMNTFORREMOTESIMCLIENTS="/mnt"
DEFHOME="/home"
DEFLOCALINST="bin"
DEFROOT="/SPACE"
PCTMINFREESPACE="5"
FREE10GIGA="10737418240"
MYCOLLECTSCRIPT="gtcollect"
MYGRAPHSCRIPT="gtgraph"
#----------------------------------------------------------------------
# CUSTOMIZATION
SIMULATOR="COTSON"
DEFSIMDIR="$SIMULATOR"
NOMEDIATOR="0"
SIMPIDDETECT="(Generating|Empty) control script"
SIMPIDDETECT2="MYCPID"
VIRTUALIZER="simnow"
SFWPSAXUEXE="cotson" # Simulation FrameWork PS AXU EXEcutable
DEFAULTRUNID="mytsu"
DEFAULTLIBC="2.13"

DEFAULTMEMTRFILE="mytrace.txt.gz"
DEFAULTCPUTRFILE="mycputrace.txt.gz"
DEFAULTMEDTRFILE="mymedtrace.txt.gz"
DEFAULTGUESTSETUPSCRIPT="guestsetup.sh"
DEFAULTINPUTFILTERSCRIPT="infilter.sh"
logrepdir="report"
#DEFAULTDSEPARAMS="model hd appi size[appi] nodes cores workers timing l1c l2c l3c tsulatency cpu nic"
#NOTE: THE FIRST TWO LETTERS OF EACH PARAMETER NAME **MUST BE** BE UNIQ:
DEFAULTDSEPARAMS="model appi[model] size[appi] nodes cores workers timing hd l1c l2c l3c tsulatency cpu nic ntopology narch mem freq"
#DEFAULTDSEBINGEN="model appi size[appi]"
DEFAULTDSEBINGEN="model appi[model] size[appi]"
DEFAULTDEPPARAMS="infilter[appi] stringok[appi] workingsubdir[model] guestsetup[appi]"
SIMSTARTEDOLD="prepareToRun"
SIMSTARTEDOLDREV="722"
SIMSTARTED="COTSON STARTED"
SIMFINISHED="COTSON FINISHED"
SIMCOMPLETED="EXIT TRIGGER: terminate"
NODCOMPLETED="Sending terminate message"
INPUTFILTERSUFF="input.sh"
SIMDEFHD="karmic64"
# The following can be inserted in the DSECONFIGFILE, if needed
# SIMSERVER="bs1.promana.unisi.it"
goodstring="All workers done, goodbye"
MSGSIMRUN="run simulation"
DEFAULTSIMBRANCH="trunk"
DEFAULTSIMLIB="sbin/abaeterno.so"
DEFAULTSIMPATH="bin/cotson"
DEFAULTBSDPATH="data"
DEFAULTEXENAME="make"
DEFAULTSIMVER="726"
ROISTART="COTSON_INTERNAL(10,1,0)"
ROIEND="COTSON_INTERNAL(10,1,1)"
DEFAULTHD=`[ -z "$LOCALHD" ] && echo -n "$SIMDEFHD" || echo -n "$LOCALHD"`
DEFAULTMODEL="std"
DEFAULTWSUBDIR="."
DEFAULTAPPI="a"
DEFVERIFICATIONSTRING="SUCCESS"
DEFAULTINPUT="0"
DEFAULTNODES="1"
DEFAULTCORES="1"
DEFAULTTIMING="simple+3M"
DEFAULTNETTOPOLOGY="star"
DEFAULTNETARCH="simple+1000+20"
DEFAULTOWMSIZE="32000000"
DEFAULTXSMSIZE="128"
DEFAULTCPU="timer0"
DEFAULTTSUL="1+6+3+100+10"               # defl+rdl+wrl+subl+schl+snd+rcv
DEFAULTNIC="10000M+0+0"                  # throughput+latency+setup
DEFAULTL1IC="32kB+64+4+WT+false+2+l2"    # size+blks+ways+whpo+wmpo+late
DEFAULTL1DC="32kB+64+2+WT+false+3+l2"    # size+blks+ways+whpo+wmpo+late
DEFAULTL2UC="512kB+64+2+WB+true+5+bus"   # size+blks+ways+whpo+wmpo+late
DEFAULTL3UC="4096kB+64+4+WB+true+20+mem" # size+blks+ways+whpo+wmpo+late
DEFAULTT1I="4kB+64+64+WT+false+1+t2"     # pgsz+entr+ways+whpo+wmpo+late
DEFAULTT1D="4kB+64+64+WT+false+1+t2"     # pgsz+entr+ways+whpo+wmpo+late
DEFAULTT2S="4kB+512+512+WB+true+1+busT"  # pgsz+entr+ways+whpo+wmpo+late
DEFAULTLMEM="100"                        # memory latency
DEFAULTFREQ="3000"                       # core frequncy
DEFAULTMBUS="MOESI+25+4+l3"              # memory bus
DEFAULTTBUS="MOESI+25+4+l3"              # TLB bus
DEFAULTARCH="mem-l3+l3-bus+l3-busT.ic-cpu+bus-l2+busT-t2+l2-ic+l2-dc+t2-it+t2-dt" # mem-architecture
LIMITTONODELECPUS="1"
SIMLOOPTHRESHOLD="3"
DEFAULTMKFILE="Makefile"
SIMSMALL=`echo $SIMULATOR|tr '[:upper:]' '[:lower:]'` # SIMULATOR NAME


#----------------------------------------------------------------------
LISTOPARAMS="timer.cycles+m+i+cy timer.cycles+c+i+tc timer.instructions+c+i+ic timer.icache_ccc.read_miss+a+i+icrm timer.icache_ccc.write_miss+a+i+icwm timer.icache_ccc.read+a+f+icr timer.icache_ccc.write+a+f+icw timer.icache_ccc.read_miss_rate+a+f+icmr timer.dcache_ccc.read+a+i+dcr timer.dcache_ccc.write+a+i+dcw timer.dcache_ccc.read_miss+a+i+dcrm timer.dcache_ccc.write_miss+a+i+dcwm timer.dcache_ccc.l2cache_ccc.read+a+i+l2r timer.dcache_ccc.l2cache_ccc.write+a+i+l2w timer.dcache_ccc.l2cache_ccc.read_miss+a+i+l2rm timer.dcache_ccc.l2cache_ccc.write_miss+a+i+l2wm timer.dcache_ccc.l2cache_ccc.bus.l3cache_ccc.read+a+i+l3r timer.dcache_ccc.l2cache_ccc.bus.l3cache_ccc.write+a+i+l3w timer.dcache_ccc.l2cache_ccc.bus.l3cache_ccc.read_miss+a+i+l3rm timer.dcache_ccc.l2cache_ccc.bus.l3cache_ccc.write_miss+a+i+l3wm ^sim_system_time+a+i+sims ^sim_user_time+a+i+simu timer.kcycles+m+i+kcy timer.kcycles+c+i+tkc timer.kinstructions+c+i+kic timer.uinstructions+c+i+tuic timer.ucycles+m+i+tucy ^icache_ccc.latency+a+i+tih1 ^dcache_ccc.latency+a+i+tdh1 ^l2cache_ccc.latency+a+i+th2 ^l3cache_ccc.latency+a+i+th3 ^main.latency+a+i+tmem timer.kcycles+a+i+akcy idlecount+a+i+aidc cycles+m+i+ncy ^cycles_per_usec+a+i+mhz timer.idlecycles+a+i+idlcc ^cpu_ccc.total_power+c+f+powto ^cpu_ccc.total_leakage_power+c+f+powtl ^cpu_ccc.dynamic_power+c+f+powrd ^l2cache_ccc.total_power+c+f+powl2to"
LISTOFORMULAS="(dcrm+dcwm)/(dcr+dcw+0.00000000001),dcmr,a,f (l2rm+l2wm)/(l2r+l2w+0.00000000001),l2mr,a,f (l3rm+l3wm)/(l3r+l3w+0.00000000001),l3mr,a,f,1 simu+sims,simt,a,i dcrm+dcwm,dcm,a,i icrm+icwm,icm,a,i l2rm+l2wm,l2m,a,i l3rm+l3wm,l3m,a,i cy-kcy,ucy,m,i ic-kic,uic,c,i tmem*l3mr+th3,tpen3,a,f (tmem*l3mr+th3)*l2mr+th2,tpen2,a,f ((tmem*l3mr+th3)*l2mr+th2)*dcmr+tdh1,dlat,a,f ((tmem*l3mr+th3)*l2mr+th2)*icmr+tih1,ilat,a,f kcy/(cy+0.00000000001)*100,pkcy,m,f akcy/(cy+0.00000000001)*100,apkcy,m,f tkc/(tc+0.0000000001)*100,tpkc,m,f cy-akcy,u0cy,m,i cy-aidc,u1cy,m,i cy-aidc-akcy,u2cy,m,i (cy/mhz)/1000000,tx,m,f (u0cy/mhz)/1000000,ux0,m,f powto*tx,ent,c,f ent*tx,edpt,c,f"
LISTOSUMMARY="tx cy+ ic icmr+ dcmr+ l2mr+ l3mr+ simt+ icm dcm l2m l3m icrm icwm icr icw dcrm dcwm dcr dcw l2rm l2wm l2r l2w l3rm l3wm l3r l3w kcy tucy kic tuic ucy uic tih1 tdh1 th2 th3 tmem tpen3 tpen2 ilat+ dlat+ pkcy apkcy tpkc akcy aidc u0cy u1cy u2cy ncy mhz ux0 idlcc powto powtl powrd ent edpt"
LISTOMISSINGPARAMS="timer.idlecycles"

#----------------------------------------------------------------------
msgBINGENSTART="START GENERATING BINARIES"
msgBINGEN__END="END GENERATING BINARIES"
msgEXPSTART="STARTING EXPERIMENT"
msgEXP__END="ENDING EXPERIMENT"
msgSIMSTART="********* SIMULATION STARTED:"
msgSIM__END="********* SIMULATION ENDED:"
#PSSTRING="ps axo stat,tpgid,sess,pgrp,ppid,pid,user"
PSSTRING="ps axo stat,tpgid,sess,pgrp,ppid,pid,user,comm"
SHMDEV="/dev/shm"
nfsport="2049"
portmapperport="111"
grep=`which grep`; if [ "$grep" = "" ]; then echo "Cannot find 'grep'"; exit 105; fi
awk=`which awk`; if [ "$awk" = "" ]; then echo "Cannot find 'awk'"; exit 107; fi
tail=`which tail`; if [ "$tail" = "" ]; then echo "Cannot find 'tail'"; exit 116; fi

TAKETIME="/usr/bin/time -v --output="
md5sig=`md5sum $0|awk '{print $1}'`
MYSIG="${md5sig:0:2}${md5sig:(-2)}" # reduced-hash: first 2 char and last two char
VERSION1="v${VERSION}-${MYSIG}"
DSE_THISSCRIPT=`basename ${BASH_SOURCE[0]}`
DSE_THISTOOL="${DSE_THISSCRIPT%%.*}"; DSE_THISTOOLCAP="${DSE_THISTOOL^^}"
DSECONFIGFILE="${DSE_THISTOOL}.cfg"

CURPATH=`pwd -P` # Save current path

DSUFFBIN="BIN"
DSUFFINFO="INFO"
DSUFFEXP="EXP"
DSUFFQUEUE="QUEUE"
   # If I am the sim master: save the DONE-Q and FAILED-Q too in the OUTDIR
MYQRUNNING="running"
MYQFAILED="failed"
MYQWAITING="waiting"
MYQDONE="done"
INDENTLEV0="0"
INDENTLEV1="1"


MAXRETRYEXP="4"
CRITICALDF="500" # critical MB of disk free to raise a warning condition

declare -A XFP # associative array for execution full path for each app
declare -A MODELDIR # associative array for MODELDIR full path for each model
declare -A EXEDIR # associative array for EXDIR full path for each model
declare -A GSFP # associative array for execution full path of guestsetup.sh
declare -A IFFP # associative array for execution full path of infilter.sh
declare -A ARCELMNAME # associative array with the architecture element names
ARCELMNAME=( [cpu]=cpu [trace]=trace [mem]=main [l3]=l3cache [bus]=bus [busT]=tlb_bus \
             [l2]=l2cache [t2]=l2tlb [ic]=icache [dc]=dcache [it]=itlb [dt]=dtlb )

MYPID="$$" # MY PID

# Set $SCRIPTPATH to the same directory where this script is
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

#-----------------------------------------------------------------------------
CODEFUN="fun"
# Colors
black='\E[30;40m'
red='\E[31;40m'
green='\E[32;40m'
yellow='\E[33;40m'
blue='\E[1;34;40m'
magenta='\E[35;40m'
cyan='\E[36;40m'
white='\E[37;40m'

#####################################################
function cecho ()            # Color-echo.
                             # Argument $1 = message
                             # Argument $2 = color
{
   local default_msg="No message passed."
                             # Doesn't really need to be a local variable.
   local nopt=""
   local message=""
   local color=""
   local force="0"

   while [ $# -gt 0 ]; do
      case "$1" in
         -n) nopt="-n "; shift;;
         -f) force="1"; shift;;
         *) if [ -z "$message" ]; then message=${1:-$default_msg}; shift;   # Defaults to default message.
            elif [ -z "$color" ]; then  color=${1:-$white}; shift;          # Defaults to white, if not specified.
            fi ;;
      esac
   done

   [ "$quiet" = "0" -o "$force" = "1" ] && echo -en "$color"
   [ "$quiet" = "0" -o "$force" = "1" ] && echo -n "$message"
   [ "$nopt" = "" -a \( "$quiet" = "0" -o "$force" = "1" \) ] && echo ""
   tput sgr0             # reset to normal
}

#####################################################
function comment() {
   local nopt=""
   local str=""
   if [ "$quiet" = "0" ]; then
      while [ $# -gt 0 ]; do
         case "$1" in
            -n) nopt="-n "; shift;;
            *)  str="$1"; shift;;
         esac
      done
      echo $nopt "$str"
   fi
}

#####################################################
function verbose4() {
   if [ "$verbose" -gt "3" ]; then echo "$1"; fi
}

#####################################################
function verbose3() {
   if [ "$verbose" -gt "2" ]; then echo "$1"; fi
}

#####################################################
function verbose2() {
   if [ "$verbose" -gt "1" ]; then echo "$1"; fi
}

#####################################################
function verbose1() {
   if [ "$verbose" -gt "0" ]; then echo "$1"; fi
}

#####################################################
function debug5() {
   if [ "$debug" -gt "4" ]; then echo "D5: $1"; fi
}

#####################################################
function debug4() {
   if [ "$debug" -gt "3" ]; then echo "D4: $1"; fi
}

#####################################################
function debug3() {
   if [ "$debug" -gt "2" ]; then echo "D3: $1"; fi
}

#####################################################
function debug2() {
   if [ "$debug" -gt "1" ]; then echo "D2: $1"; fi
}

#####################################################
function debug1() {
   if [ "$debug" -gt "0" ]; then echo "D1: $1"; fi
}

#####################################################
function setsize()  # NEEDS $model
{
   local lappi="$1"
   # if no listsize for this appi, then assign whatever was preset on $listsize
   if [ -z "${listsize[$lappi]}" ]; then
       listsize[$lappi]="$listsize"
       if [ "$quiet" = "0" ]; then
          echo "WARNING(setsize): listsize[$lappi] was empty! It gets then the default value '$listsize'."
       fi
   fi
   # command line overrides INFOFILE
   [ -z "$MYINPUT" ] || listsize[$lappi]="$MYINPUT"
   # ASSERT: listsize[$lappi] is now anyway set (listsize was already preset)

   #setup default values for GUESTSETUPSCRIPT
   debug1 "guestsetup[$lappi]='${guestsetup[$lappi]}'"
   [ -z "${guestsetup[$lappi]}" ] && {
       [ "$GUESTSCRIPTTYPE" = "builtin" ] && guestsetup[$lappi]="builtin" \
          || guestsetup[$lappi]="$DEFAULTGUESTSETUPSCRIPT";
   }
   [ -z "$MYGUESTSETUPSH" ] || guestsetup[$lappi]="$MYGUESTSETUPSH"
   #
   GSFP[$lappi]=""
   if [ "${guestsetup[$lappi]}" != "builtin" ]; then
      debug1 "Checking GSFP ${EXEDIR[$model]}/${guestsetup[$lappi]}"
      if [ -s "${EXEDIR[$model]}/${guestsetup[$lappi]}" ]; then
         GSFP[$lappi]="${EXEDIR[$model]}/${guestsetup[$lappi]}"
      else
         debug2 "No GSFP in ${EXEDIR[$model]}"
      fi
      debug1 "Checking GSFP ${MODELDIR[$model]}/${guestsetup[$lappi]}"
      if [ -s "${MODELDIR[$model]}/${guestsetup[$lappi]}" ]; then
         GSFP[$lappi]="${MODELDIR[$model]}/${guestsetup[$lappi]}"
      else
         debug2 "No GSFP in ${MODELDIR[$model]}"
      fi
      debug1 "Checking GSFP ./${guestsetup[$lappi]}"
      if [ -s "./${guestsetup[$lappi]}" ]; then
         GSFP[$lappi]="./${guestsetup[$lappi]}"
      else
         debug2 "No GSFP in current directory `pwd`"
      fi
   else
      GSFP[$lappi]="builtin"
   fi
   debug1 "GSFP[$lappi]='${GSFP[$lappi]}'"


   #setup default values for INPUTFILTERSCRIPT
   debug1 "infilter[$lappi]='${infilter[$lappi]}'"
   [ -z "${infilter[lappi]}" ] && {
      [ "$INPUTFILTERTYPE" = "builtin" ] && infilter[$lappi]="builtin" \
         || infilter[$lappi]="$DEFAULTINPUTFILTERSCRIPT";
   }
   [ -z "$MYINPUTFILTERSH" ] || infilter[$lappi]="$MYINPUTFILTERSH"
   #
   IFFP[$lappi]=""
   if [ "${infilter[$lappi]}" != "builtin" ]; then
      debug1 "Checking IFFP ${EXEDIR[$model]}/${infilter[$lappi]}"
      if [ -s "${EXEDIR[$model]}/${infilter[$lappi]}" ]; then
         IFFP[$lappi]="${EXEDIR[$model]}/${infilter[$lappi]}"
      else
         debug2 "No IFFP in ${EXEDIR[$model]}"
      fi
      debug1 "Checking IFFP ${MODELDIR[$model]}/${infilter[$lappi]}"
      if [ -s "${MODELDIR[$model]}/${infilter[$lappi]}" ]; then
         IFFP[$lappi]="${MODELDIR[$model]}/${infilter[$lappi]}"
      else
         debug2 "No IFFP in ${MODELDIR[$model]}"
      fi
      debug1 "Checking IFFP ./${infilter[$lappi]}"
      if [ -s "./${infilter[$lappi]}" ]; then
         IFFP[$lappi]="./${infilter[$lappi]}"
      else
         debug2 "No IFFP in current directory `pwd`"
      fi
   else
      IFFP[$lappi]="builtin"
   fi
   debug1 "IFFP[$lappi]='${IFFP[$lappi]}'"
}

#####################################################
function setappi()
{
   local lmodel
   lmodel=$1
   # if no listappi for this model, then assign whatever was preset on $listappi
   debug4 "listappi[$lmodel]='${listappi[$lmodel]}'"
   if [ -z "${listappi[$lmodel]}" ]; then
       listappi[$lmodel]="$listappi"
       if [ "$quiet" = "0" ]; then
          echo "WARNING(setappi): listappi[$lmodel] was empty! It gets then the default value '$listappi'."
       fi
   fi
   # command line overrides INFOFILE
   [ -z "$MYAPPI" ] || listappi[$lmodel]="$MYAPPI"
   # ASSERT: listappi[$lmodel] is now anyway set (listappi was already preset)

}

#####################################################
function funmodel()
{
   debug2 "---------- STARTING funmodel '$1'"
   local lmodel="$1"
#   local lwdiru=`echo $lmodel|tr '[:lower:]' '[:upper:]'`
#   local lwdirs=`echo $lmodel|tr '[:upper:]' '[:lower:]'`
#   local direxists="0" 
   RETVAL="$lmodel"
#
#   # Sanity check
#   if [ ! -d "$MYBASEDIR" ]; then return; fi
#
#   debug3 "1: EXEDIR[$model]=${EXEDIR[$model]}"
#   debug3 "Checking $MYBASEDIR/$lwdiru"
#   if [ -d "$MYBASEDIR/$lwdiru" ]; then
#      EXEDIR[$model]="$MYBASEDIR/$lwdiru"
##      MODELDIR[$lmodel]="${EXEDIR[$model]}"
#      MODELDIR[$lmodel]="$MYBASEDIR/$lwdiru"
#      direxists="1" 
#   fi
#   debug3 "2: EXEDIR[$model]=${EXEDIR[$model]}"
#   debug3 "Checking $MYBASEDIR/$lwdirs"
#   if [ -d "$MYBASEDIR/$lwdirs" ]; then
#      EXEDIR[$model]="$MYBASEDIR/$lwdirs"
##      MODELDIR[$lmodel]="${EXEDIR[$model]}"
#      MODELDIR[$lmodel]="$MYBASEDIR/$lwdirs"
#      direxists="1" 
#   fi
#   debug3 "3: {EXEDIR[$model]}=${EXEDIR[$model]}"
#   debug3 "Checking $MYBASEDIR/${lmodel}-${MYSIMVER}"
#   if [ -d "$MYBASEDIR/${lmodel}-${MYSIMVER}" ]; then
#      EXEDIR[$model]="$MYBASEDIR/${lmodel}-$MYSIMVER"
##      MODELDIR[$lmodel]="${EXEDIR[$model]}"
#      MODELDIR[$lmodel]="$MYBASEDIR/${lmodel}-$MYSIMVER"
#      direxists="1" 
#   fi
#   debug3 "4: EXEDIR[$model]=${EXEDIR[$model]}"
   #setup default values
#echo "workingsubdir[$lmodel]=${workingsubdir[$lmodel]}"
#   if [ -z "${workingsubdir[$lmodel]}" ]; then workingsubdir[$lmodel]="$MYWSUBDIR"; fi
#echo "workingsubdir[$lmodel]=${workingsubdir[$lmodel]}"
#   #
#   debug3 "Checking ${EXEDIR[$lmodel]}/${workingsubdir[$lmodel]}"
#   if [ -d "${EXEDIR[$lmodel]}/${workingsubdir[$lmodel]}" ]; then
#      if [ "${workingsubdir[$lmodel]}" != "." ]; then
#         [ -z "${workingsubdir[$lmodel]}" ] || EXEDIR[$lmodel]="${EXEDIR[$lmodel]}/${workingsubdir[$lmodel]}"
#      fi
#      direxists="1" 
#   fi
#   debug3 "5: EXEDIR[$lmodel]=${EXEDIR[$lmodel]}"

   
   verbose2 "   cd ${EXEDIR[$model]}"
   if [ "${EXEDIR[$model]}" != "" ]; then # EXEDIR[$model] may remain unassigned
      if [ -d "${EXEDIR[$model]}" ]; then
        cd ${EXEDIR[$model]}  #existence of EXEDIR[$model] is already checked above
      fi
   fi
   verbose2 "   PWD=$PWD"

#   # ERROR CHECKING
#   debug1 "model=$lmodel"
#   debug1 "EXEDIR[$model]=${EXEDIR[$model]}"
#   debug1 "MODELDIR[$lmodel]=${MODELDIR[$lmodel]}"
#   debug1 "direxists=$direxists"
#   if [ "$direxists" = "0" -a "$NORUN" = "0" ]; then
#      echo "ERROR: cannot find working dir for model '$lmodel'."
#      [ -z "${EXEDIR[$model]}" -o -d "${EXEDIR[$model]}" ] || echo "  ERROR: EXEDIR[$model] '${EXEDIR[$model]}' not found."
#      [ -z "${MODELDIR[$lmodel]}" -o -d "${MODELDIR[$lmodel]}" ] \
#         || echo "  ERROR: MODELDIR[$lmodel]='${MODELDIR[$lmodel]}' not found."
#      [ "$lmodel" != "$DEFAULTMODEL" ] \
#         && echo "You may miss the DSEDRIVER for '$lmodel' model or '$lmodel' model is not installed."
#      exit 2
#   fi
   debug2 "---------- ENDING funmodel '$1'"
   RETVAL="$lmodel"
#   echo $lmodel
#   return 1
}

#####################################################
function funworkers()
{
   local workers
#echo "WORKERS='$1'"
#echo "HI: workers=$1"
   [ -z "$1" -o "$1" = "_" ] && workers="-" || workers="$1"
#echo "WORKERS2='$workers'"
   #  if workers unspecified set equal to cores
#echo "  :='$workers'   cores=$cores"
   [ "$workers" = "-" ] && workers=$cores || workers="$1"
#   [ "$workers" = "-" ] && workers=$nodes || workers="$1"
#echo "  :='$workers'   cores=$cores"
   workers=`echo "$workers"|awk '{printf("%02d", $0)}'`
#echo "\$1=$1 workers=$workers cores=$cores"
#echo "RV:=$workers"
   RETVAL="$workers"
#   echo $workers
#   return 0
}

#####################################################
function funcores()
{
   local cores
   cores=`echo "$1"|awk '{printf("%02d", $0)}'`
   [ "$workers" = "-" -o "$workers" = "" -o "$listworkers" = "-" -o "$listworkers" = "" ] && workers=$cores
   workers=`echo "$workers"|awk '{printf("%02d", $0)}'`
   debug4 "\$1=$1 cores=$cores  workers=$workers"
   RETVAL="$cores"
#   echo "$cores"
#   return 0
}

#####################################################
function funnodes()
{
   local nodes=`echo "$1"|awk '{printf("%02d", $0)}'`
#echo "\$1=$1 nodes=$nodes"
   RETVAL="$nodes"
#   echo $nodes
#   return 0
}

#####################################################
function funntopology()
{
   RETVAL="$1"
}

#####################################################
function funnarch()
{
   RETVAL="$1"
}

#####################################################
function funcpu()
{
   RETVAL="$1"
}

#####################################################
function funl1c()
{
   RETVAL="$1"
}

#####################################################
function funl2c()
{
   RETVAL="$1"
}

#####################################################
function funl3c()
{
   RETVAL="$1"
}

#####################################################
function funmem()
{
   RETVAL="$1"
}

#####################################################
function funfreq()
{
   RETVAL="$1"
}

#####################################################
function setrpcinfo () {
   #rpcinfo=`sudo which rpcinfo`; if [ "$rpcinfo" = "" ]; then echo "Cannot find 'rpcinfo'"; exit 104; fi
   rpcinfo=`which rpcinfo 2>/dev/null`
   if [ "$rpcinfo" = "" ]; then
#      rpcinfo=`sudo which rpcinfo 2>/dev/null`;
#      if [ "$rpcinfo" = "" ]; then echo "Cannot find 'rpcinfo'"; exit 104; fi
      if [ ! -x /usr/sbin/rpcinfo ]; then
         echo "Cannot find 'rpcinfo' (missing package 'rpcbind' ?)"; exit 104;
      else
         rpcinfo="/usr/sbin/rpcinfo"
      fi
   fi
}

#####################################################
function detectshmpids () {
   local fl
   local f
   local fver0
   local fver1
   local fpid
   local fpal
   local fplist
   local fplalive

   # Build list of shm pids
   fplist=""; fplalive=""; fpldead=""
   fl=`ls $SHMDEV/${SHMID}-* 2>/dev/null`
   debug2 "fl=$fl"
   if [ ! -z "$fl" ]; then for f in $fl; do
      fpid=""
      if [ -f $f ]; then
         fver0=${f##*\/}
         fver1=${fver0#${SHMID}-}
         if [[ "$fver1" =~ ^[0-9]+$ ]] ; then
            fpid="$fver1"
         fi
      fi
      if [ "$fpid" != "" ]; then
         [ -z "$fplist" ] && fplist="$fpid" || fplist="$fplist $fpid"
         fpal=`kill -0 $fpid 2>&1 >/dev/null; echo $?`
         if [ "$fpal" = "0" ]; then
            [ -z "$fplalive" ] && fplalive="$fpid" || fplalive="$fplalive $fpid"
         else
            [ -z "$fpldead" ] && fpldead="$fpid" || fpldead="$fpldead $fpid"
         fi
      fi
   done; fi
   debug2 "fplist=$fplist"; debug2 "fplalive=$fplalive"; debug2 "fpldead=$fpldead"
}

#####################################################
function cleanupshm () {
   local p
   local fn

   detectshmpids
   debug2 "fpldead=$fpldead"

   # Try to remove the shm files which are belonging to dead pids
   if [ ! -z "$fpldead" ]; then for p in $fpldead; do
      fn="${SHMDEV}/${SHMID}-${p}"
      rm -f $fn 2>&1 >/dev/null
   done; fi
}


#####################################################
function function_exists() {
   type -t $1 2>/dev/null >/dev/null
   return $?
}

#####################################################
function splitp() {
#####################################################
   local p
   local listpref
   local pvcheck
   local pf pr pq
   local plpram
   local pparampre
   local pparam
   local vv

   p="$1"
   listpref="$2"
#echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
#echo "p=$p"
#echo "listpref=$listpref"

   #debrace:  ${xx} --> xx
#echo "1:p=$p"
   if [ "${p:0:2}" = "\${" ]; then p=${p:2}; p=${p%\}}; fi #}
#echo "2:p=$p"

   pvcheck=${p/[^\[]*\[/\[}
   pvcheck=${pvcheck/\[[^\]]*\]/\[\]}
#echo "pvcheck=$pvcheck"
#   pf=${p%\[[^\]]*\]}   #delete from back
   pf=${p%%\[[^\]]*\]}   #delete from back
   pq=${p#[^\[]*\[} #delete from front
#   pq=${p##[^\[]*\[} #delete from front
#   pr=${pq%%\]*} #delete from back
   pr=${pq%\]*} #delete from back

   #debrace again (pr):  ${xx} --> xx
   pr=${pr#\$}
   pr=${pr#{}
   pr=${pr%\}}  #}

#echo "pf='$pf'  pq='$pq'  pr='$pr'  "
   if [ "$pvcheck" = "[]" ]; then
      plparam="$listpref${pf}"
#echo "plparam=$plparam"
      eval plist=\${$plparam}
#echo "plist=$plist"
      eval pval=\${$pr}
#echo "pval=$pval"
#      pparampre="plparam[$pval]"
      pparampre="$plparam[$pval]"
      if [ ! -z "$pval" ]; then
         eval pparam="\${$pparampre}" #e.g. listsize[fibf]
#         eval vv="\${$pparam}"
         eval vv="\${pparam}"
#echo "pparam=$pparam   vv=$vv"
      else
         pparam=""
         vv=""
      fi
#            eval vv="\${$pf}"
      p="$pf"
      outlist="$vv"
      outindex="$pr"
      outlistp="$plparam"
   else
      plparam="$listpref$pf"
      plist=""
      pparampre="plparam"
      eval pparam="\$$pparampre"
#            eval vv=\$v$pf
      eval vv="\$$pf"
      eval outlist="\$$plparam"
      outindex=""
      outlistp="$plparam"
   fi
   if [ "$outlist" = "" ]; then outlist="_"; fi
   outp="$p"
   debug5 "p=$p (outlist=$outlist) -- pvcheck=$pvcheck pf=$pf pr=$pr -- plist=$plist pl=$plparam ppp=$pparampre pp=$pparam vv=$vv outp=$outp"
#echo "p=$p (outlist=$outlist) -- pvcheck=$pvcheck pf=$pf pr=$pr -- plist=$plist pl=$plparam ppp=$pparampre pp=$pparam vv=$vv outp=$outp"
}

#####################################################
function calculate_simcost() {
#####################################################
# INPUT: simstring
# OUTPUT: mycost
# GLOBAL: costparam1
# GLOBAL: costparam2
#
# Cost model: mycost=size*nodes
#####################################################
   local simstring="$1"
   local ssv
   local dimssv
   local i
   local v
   local v1
   local v2
   local v2v
   local v3
   local costsi
   local costno

   IFS='_' read -a ssv <<< "$simstring"
   dimssv=`expr ${#ssv[@]} - 1`
   for i in $(seq 0 $dimssv); do
      v=${ssv[$i]}
      v1=${v:0:2}
      v2=${v:2}
      IFS='+' read -a v2v <<< "$v2"
      v3="${v2v[0]}"
      if [ "$v1" = "si" ]; then costsi=$v3; fi
      if [ "$v1" = "no" ]; then costno=$v3; fi
   done
   [ -z "$costsi" -o "$costsi" = "0" ] && costsi="10" # default value
   [ -z "$costno" ] && costno="10" # default value
   mycost=`expr $costsi \* $costno`
   [ -z "$mycost" ] && mycost="100" # default value
#echo "mycost=$mycost"

#   return $mycost # doesn't work ? ## THE RETURN VALUE IS ONLY 0..255
}

#####################################################
function buildexpstring() {
#####################################################
   local px
   local x
   local vv
   local pshort
   local doprint="0"
   local xecho
   x=""
   for px in $DSEPARAMS; do
      splitp $px list
      eval vv="\$$outp" 
      pshort=${outp:0:2}
      [ -z "$x" ] && x="${pshort}${vv}" || x="${x}_${pshort}${vv}"
   done

   if [ -z "$x" ]; then
      comment "WARNING(buildexpstring): buildexpstring generated empty element for queue"
   else
#         echo "$x"
      if [ "$nodistrib" = "1" -o "$POPULATE" = "1" -o "$COUNTONLY" = "1" -o "$CLOVERRIDE" = "1" ]; then
         GENCOUNT=`expr $GENCOUNT + 1`
         [ "`expr $GENCOUNT \% 100`" = "0" ] && verbose1 "    processed $GENCOUNT elements"
         if [ "$COUNTONLY" = "0" ]; then
            local eq
            local er
            local ef
            local ed
            xecho=`echo "$x" | awk '{printf("%2d=> %s\n",c,$0)}' c=$GENCOUNT`
            checkqelem "$MYQWAITING" "$x"; eq=$retcode
            checkqelem "$MYQRUNNING" "$x"; er=$retcode
            checkqelem "$MYQFAILED" "$x"; ef=$retcode
            checkqelem "$MYQDONE" "$x"; ed=$retcode

            # SIMULATION COST MODEL
            calculate_simcost $x

            insertc="1"
            if [ "$eq" = "1" -a "$er" = "1" -a "$ef" = "1" -a "$ed" = "1" ]; then
               insertq $MYQWAITING "$x" "$mycost 0 0 0"; # elem cost retry tstart tend machines
               insertec=$?
               doprint="1"
            fi
            if [ "$eq" = "0" ]; then # already present in waiting queue
               cleaninserttopq $MYQWAITING "$x" $mycost 0 0 0
               insertec=$?
               doprint="1"
            fi
            if [ "$insertec" = "0" ]; then
               SIM_COUNT=`expr $SIM_COUNT + 1`
               comment "$xecho"
            fi
         fi
      fi
   fi
}

#####################################################
function iterator() {
#####################################################
   local v
   local lastlev
   local paaa
   local funex
   local ifun="$1"
   [ "$2" = "" ] && ITNESTING="0"

   ITNESTING=`expr $ITNESTING + 1`
   IFS=' ' read -a paaa <<< "$ITPARAMS"
   lastlev=${#paaa[@]}
   debug5 "----"; debug5 "*$ITNESTING (ITNESTING=$ITNESTING lastlev=$lastlev)"
#   IFS=' ' read -a paaa <<< "$ITPARAMS"
   p1=${paaa[$ITNESTING-1]}
   splitp $p1 list
   p="$outp"
   list="$outlist"
   ppppp="$p"
   debug5 "p=$p  list='$list'"
   for v in $list; do
      splitp ${paaa[$ITNESTING-1]} list
      p="$outp"
      v1=""
      fun="$CODEFUN$p"
      function_exists $fun
      funex="$?"
#   if [ $? = 0 ]; then v1=$($fun $v); else v1="$v"; fi
      if [ $funex = 0 ]; then $fun $v; v1="$RETVAL"; else v1="$v"; fi
      debug5 "*$ITNESTING v=$v    p=$p   v1=$v1   funex=$funex"
      eval $p="$v1"
      if [ "${p:0:1}" != "\$" ]; then
         eval vv1=\${$p}
      else
         eval vv1=$p
      fi
      debug5 "*$ITNESTING $p=$v1  \$p=$vv1"
      if [ "$ITNESTING" -lt "$lastlev" ]; then iterator $ifun $ITNESTING; else
         function_exists $ifun
         if [ $? = 0 ]; then $ifun; fi
      fi
   done
   ITNESTING=`expr $ITNESTING - 1`
}

#####################################################
function checkconnection {
#####################################################
   local myservice="$1"
   local myhost="$2"
   local rc="1"
   local d
   local c
   local r
   local rpm
#   ch=""
   if [ "$myhost" != "" -a "$myservice" != "" ]; then
#      ch=`$sss ping -c 1 -W 1 $1| egrep "icmp_req|icmp_seq"`
      if [ "$verbose" -gt "1" ]; then
         echo "       Checking '$myservice' service..."
      fi
      case $myservice in
         web)
            wget --spider --timeout=5 --tries=3 ${myhost}/ >/dev/null 2>/dev/null
            rc=$?
            if [ "$verbose" -gt "1" ]; then
               if [ "$rc" = "0" ]; then  echo "       Machine $myhost has web service active."; fi
            fi
            ;;
         nfs)
            if [ "$nodistrib" = "0" ]; then
#               d=`echo "send escape"|telnet $myhost $nfsport 2>&1|$grep Connected |$awk '{print $1}'`
#               c=`echo "send escape"|telnet $myhost $portmapperport 2>&1|$grep Connected |$awk '{print $1}'`
               d=`echo "send escape"|nc -vw 5 $myhost $nfsport 2>&1|egrep "(succeeded|Connected to|open)"`
               c=`echo "send escape"|nc -vw 5 $myhost $portmapperport 2>&1|egrep "(succeeded|Connected to|open)"`
               if [ "$verbose" -gt "1" ]; then
                  if [ "$c" != "" ]; then  echo "       Machine $myhost has PORTMAPPER port $portmapperport open.";
                  else echo "Machine $myhost has PORTMAPPER port ($portmapperport) CLOSED."; fi
                  if [ "$d" != "" ]; then  echo "       Machine $myhost has NFS port $nfsport open.";
                  else echo "Machine $myhost has NFS port $nfsport CLOSED."; fi
               fi
               rpm=""
#               if [ "$d" = "Connected" -a "$c" = "Connected" ]; then
               if [ "$d" != "" -a "$c" != "" ]; then
                  r=`${rpcinfo} -p $myhost 2>/dev/null`
                  rpm=`echo "$r"|$grep $nfsport|$awk '{print $4}'|$tail -1`
                  if [ "$verbose" -gt "1" ]; then
                     if [ "$rpm" != "" ]; then  echo "       Machine $myhost has NFS server OK.";
                     else echo "Machine $myhost has no NFS service running."; fi
                  fi
               fi
#               if [ "$d" = "Connected" -a "$c" = "Connected" -a "$rpm" != "" ]; then # portcheck
               if [ "$d" != "" -a "$c" != "" -a "$rpm" != "" ]; then
                  rc="0"
               fi
            else
               echo "Local Simulation (non NFS check)"
            fi
            ;;
         *)
            ping -c 2 -W 3 $myhost >/dev/null 2>/dev/null ### sometime ping is filtered out and doesn't work: better:
            rc=$?
            if [ "$verbose" -gt "1" ]; then
               if [ "$rc" = "0" ]; then  echo "       Machine $myhost responded to ping."; fi
            fi
            ;;
      esac
   fi
#   if [ "$ch" != "" ]; then noconnection="0"; else noconnection="1"; fi
   if [ "$rc" = "0" ]; then noconnection="0"; else noconnection="1"; fi

   if [ "$noconnection" != "0" ]; then
      echo ""
      echo "(TEMPORARY) ERROR: No $myservice connection with host '$myhost'"
      cecho "IF you want to run the simulations only locally" "$yellow"
      cecho "THEN specify the command line option --nonet" "$yellow"
      exit 1
   fi
}

#####################################################
function md_usage () {
#####################################################
# Command line option
#####################################################
   libverdetect
   echo "Usage: $THISSCRIPT [<options>] <INFOFILE>"
   echo "   where:"
   echo "   <INFOFILE>  e.g. T000 is mandatory (file describing the experiment)"
   echo ""
   echo "   and <options> can be:"
   echo "   -h           show this help"
   echo "   -v           verbose mode (more '-v': more details)"
   echo "   -d           debug mode   (more '-d': more details)"
   echo "   -q           quiet mode"
#   echo "   -f          overwrite previous experiment results"
#   echo "   -c          clean existing simulation queue with same tag"
#   echo "   -r          resume last existing simulation queue with same tag"
   echo "   -i           interactive (ask what to do in case of execution error)"
   echo "   -u           update the infofile and the binfile copies"
   echo "   -e <tag>     execute and tag experiment "
   echo "   -o <dir>     output directory for LOGS"
   echo "   -t <tag>     use this specific <tag> instead of 'date/time'"
   echo "   --layout <filename>  file which specifies the layout (e.g. layout001)"
   echo "   --bingen     generate binaries only"
   echo "   --bingenrun  generate binaries and then run the experiment"
   echo "   --infogen    automatically create a infofile based on defaults and command line"
   echo "   --printmodeldir print the pathname of the MODELDIR"
#   echo "   --screen    show the full screen output as it is generated"
   echo "   --new        new experiment"
   echo "   --dbsqlite   produce also an sqlite-DB file"
   echo "   --mdbsqlite  produce also an sqlite-DB file (that includes also mediator statistics)"
   echo "   --dir        simulator directory (default '$DEFSIMDIR')"
#   echo "   --user      current user (default '$DEFUSER')"
#   echo "   --mach      current machine (default '$DEFMACH')"
#   echo "   --simver    simulator revision (default '$DEFAULTSIMVER')"
   echo "   --simver     simulator revision (default: locally detected (if not '$DEFAULTSIMVER'))"
   echo "   --memtrfile  filename of the memory trace file (default '$DEFAULTMEMTRFILE')"
   echo "   --cputrfile  filename of the cpu trace file (default '$DEFAULTCPUTRFILE')"
   echo "   --medtrfile  filename of the mediator trace file (default '$DEFAULTMEDTRFILE')"
#   echo "   --glibc     glibc version (default '$DEFAULTLIBC')"
   echo "   --glibc      glibc version (default: locally detected (if not '$LIBVERSION'))"
   echo "   --workdir    working subdirectory* (default '$DEFAULTWSUBDIR')"
   echo "   --model      simulator model(s)* (default '$DEFAULTMODEL')"
   echo "   --appi       application(s)* (default '$DEFAULTAPPI')"
   echo "   --size       input(s)* (default '$DEFAULTINPUT')"
   echo "   --cores      core(s)* (default '$DEFAULTCORES')"
   echo "   --nodes      node(s)* (default '$DEFAULTNODES')"
   echo "   --frequency  frequency* (default '$DEFAULTFREQ')"
   echo "   --memlat     memory latency* (default '$DEFAULTLMEM')"
   echo "   --memarch    memory architecture* (default '$DEFAULTARCH')"
   echo "   --timing     timing(s)* (default '$DEFAULTTIMING')"
   echo "   --nettype    network topology (default '$DEFAULTNETTOPOLOGY')"
   echo "   --nettime    network timing (default '$DEFAULTNETARCH')"
   echo "   --hd         hd(s)* (default '$DEFAULTHD')"
   echo "   --xsmsize    xsmsize(s)* (default '$DEFAULTXSMSIZE')"
   echo "   --cpu        cpu timer * (default '$DEFAULTCPU')"
   echo "   --nic        nic(s)* (default '$DEFAULTNIC')"
   echo "   --tsulat     tsu latency* (default '$DEFAULTTSUL')"
   echo "   --guestsetup guest setup script* (default '$DEFAULTGUESTSETUPSCRIPT')"
   echo "   --infilter   input-filter script* (default '$DEFAULTINPUTFILTERSCRIPT')"
   echo "   --nobin      don't use common binaries (compile locally for testing purpose)"
   echo "   --nodistrib  don't use distributed experiment modality"
   echo "   --populate   re-add missing tests"
   echo "   --nosim      don't actually start simulation (useful with --populate)"
   echo "   --nonet      run the simulation locally without using the host network"
   echo "   --noretry    don't try to re-execute failed experiments"
   echo "   --nokill     don't kill or cleanup simulation temporary files (before and after)"
   echo "   --copy       copy sandbox log files into current directory and update makefile/luafile"
   echo "   --fast       reduce waiting and timeout (may risk to get incorrect results)"
   echo "   --display    show displays when appliable (two times: persistent)"
   echo "   --noguestout disable the guest-stdout visualization"
   echo "   --erroparam  enable the error checking for not installed parameters"
   echo "   --power      enable power estimation"
   echo "   --version    print version"
   echo "   --gtc        (if successful) launch $MYCOLLECTSCRIPT+$MYGRAPHSCRIPT at the end"
   echo "   "
   echo "   * --> Multiple value are allowed as a comma separated list (no whitespaces)"
   exit 1
}

#####################################################
function md_commandline () {
#####################################################
   mypid="0"
   interactive="0"
   overwrite="0"
   cleanqueue="0"
#   resumequeue="0"
   resumequeue="1"
   newexp="0"
   oldexp=""
   outdir=""
   mmytag=""
   EXPTAG=""
   verbose="0"
   debug="0"
   quiet="0"
   DBMODE="0"
   nobin="0"
   nodistrib="0"
   POPULATE="0"
   NOSIM="0"
   NONET="0"
   noretry="0"
   nokill="0"
   BINMAKE="0"
   bingenrun="0"
   SHOWSCREEN="0"
   INFOGEN="0"
   docopy="0"
   GOSLOW="1"
   DISPLAYON="0"
   GUESTOUT="1"
   SUPPRESSERROPARAMNOVALUE="1"
   CHECKINGPIDS="1"
   printversion="0"
   IAMSIMMASTER="0"
   NORUN="0"
   ERRCOUNT="0"
   NEWCOMMANDLINE=""
   INFOFILEUPD="0"
   BINFILEUPD="0"
   PRINTMODELDIR="0"
   CLOVERRIDE="0"
   NOINFO="0"
   AUTOGTCOLLECT="0"
   POWER="0"
   OPT_Q=""

DEFHBASE="$(getent passwd `whoami`| cut -d: -f6)" # DEFAULT HOME DIRECTORY
DEFUSER="$(id -u -n)" #; echo $DEFUSER
h=`hostname`; mach=${h%%.*}
DEFMACH="$mach"; [ -z "$DEFMACH" ] && (echo "ERROR: cannot detect current machine name"; exit 1)

   # Cleanup log
   rm -f ${DSE_THISTOOL}.log

   while [ $# -gt 0 ]; do
      if [ "$1" != "--bingenrun" ]; then
         [ -z "$NEWCOMMANDLINE" ] && NEWCOMMANDLINE="$1" || NEWCOMMANDLINE="$NEWCOMMANDLINE $1"
      fi
      case "$1" in
	 --version) printversion="1"; shift;;
         -v) verbose=`expr $verbose + 1`; strverb="$strverb -v"; shift;;
         -d) debug=`expr $debug + 1`; strdebu="$strdebu -d"; shift;;
         -q) quiet="1"; OPT_Q="-q "; shift;;
         -i) interactive="1"; shift;;
         -u) INFOFILEUPD="1"; BINFILEUPD="1"; shift;;
#         -f) overwrite="1"; shift;;
#         -c) cleanqueue="1"; IAMSIMMASTER="1"; shift;;
#         -r) resumequeue="1"; shift;;
         -e) if [ "$2" != "" ]; then oldexp="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
         -o) if [ "$2" != "" ]; then outdir="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
         -t) if [ "$2" != "" ]; then mmytag="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --nobin) nobin="1"; shift;;
	 --nodistrib) nodistrib="1"; shift;;
	 --populate) POPULATE="1"; shift;;
	 --nosim) NOSIM="1"; shift;;
	 --nonet) NONET="1"; shift;;
	 --noretry) noretry="1"; shift;;
	 --nokill) nokill="1"; shift;;
	 --copy) docopy="1"; shift;;
	 --dbsqlite) DBMODE="1"; shift;;
	 --mdbsqlite) DBMODE="2"; shift;;
	 --fast) GOSLOW="0"; shift;;
	 --gtc) AUTOGTCOLLECT="1"; shift;;
	 --bingen) BINMAKE="1"; shift;;
	 --bingenrun) bingenrun="1"; BINMAKE="1"; shift;;
         --infogen) INFOGEN="1"; BINMAKE="0"; shift;;
         --printmodeldir) NORUM="1"; NOSIM="1"; noretry="1"; nokill="1"; quiet="1"; PRINTMODELDIR="1"; shift;;
         --screen) SHOWSCREEN="1"; shift;;
	 --display) DISPLAYON=`expr $DISPLAYON + 1`; shift;;
	 --noguestout) GUESTOUT="0"; shift;;
	 --erroparam) SUPPRESSERROPARAMNOVALUE="0"; shift;;
	 --power) POWER="1"; DBMODE="1"; shift;;
	 --norun) NORUN="1"; NOSIM="1"; noretry="1"; nokill="1"; shift;;
#         --new) newexp="1"; shift;;
         --layout) if [ "$2" != "" ]; then MYLAYOUT="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
         --new) newexp="1"; cleanqueue="1"; resumequeue="0"; shift;;
	 --dir) if [ "$2" != "" ]; then CLSIMDIR="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --user) if [ "$2" != "" ]; then CLUSER="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --mach) if [ "$2" != "" ]; then CLMACH="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --simver) if [ "$2" != "" ]; then CLMYSIMVER="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --memtrfile) if [ "$2" != "" ]; then MYMEMTRFILE="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --cputrfile) if [ "$2" != "" ]; then MYCPUTRFILE="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --medtrfile) if [ "$2" != "" ]; then MYMEDTRFILE="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --guestsetup) if [ "$2" != "" ]; then MYGUESTSETUPSH="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --infilter) if [ "$2" != "" ]; then MYINPUTFILTERSH="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --glibc) if [ "$2" != "" ]; then CLMYLIBC="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --workdir) if [ "$2" != "" ]; then MYWSUBDIR="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --model) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYMODEL="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --appi) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYAPPI="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --size) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYINPUT="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --nodes) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYNODES="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --cores) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYCORES="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --frequency) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYFREQ="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --memlat) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYLMEM="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --memarch) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYARCH="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --timing) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYTIMING="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
         --nettype) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYNETTOPOLOGY="$2"; shift; shift; else md_usage; fi;;
         --nettime) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYNETARCH="$2"; shift; shift; else md_usage; fi;;
	 --hd) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYHD="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --xsmsize) if [ "$2" != "" ]; then MYXSMSIZE="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --cpu) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYCPU="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --nic) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYNIC="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
	 --tsulat) if [ "$2" != "" ]; then CLOVERRIDE="1"; MYTSUL="$2"; NEWCOMMANDLINE="$NEWCOMMANDLINE $2"; shift; shift; else md_usage; fi;;
         -*) md_usage;;
         *)  if [ "$EXPTAG" = "" ]; then EXPTAG="$1"; shift; continue; fi
             if [ "$EXPTAG" != "" -a $# -gt 0 ]; then md_usage; fi
      esac
   done
   # PRINT VERSION
   if [ "$printversion" != "0" ]; then echo "$0: version $VERSION1"; exit 0; fi

   #
   if [ "$PRINTMODELDIR" = "0" -a -z "$EXPTAG" ]; then
      cecho "ERROR: You must specify the tag file (e.g. T000): type '$THISSCRIPT -h' for more help." "$red"
      exit 10
   fi

   #
   if [ "$BINMAKE" = "1" ]; then
      GUESTOUT="0"
      CHECKINGPIDS="0"
      GOSLOW="0"; POPULATE="1"; noretry="1"; nokill="1"
   fi

   # allows comma separated lists as parameters
   MYMODEL=`echo $MYMODEL|tr ',' ' '`
   MYAPPI=`echo $MYAPPI|tr ',' ' '`
   MYINPUT=`echo $MYINPUT|tr ',' ' '`
   MYNODES=`echo $MYNODES|tr ',' ' '`
   MYCORES=`echo $MYCORES|tr ',' ' '`
   MYTIMING=`echo $MYTIMING|tr ',' ' '`
   MYLMEM=`echo $MYLMEM|tr ',' ' '`
   MYFREQ=`echo $MYFREQ|tr ',' ' '`
   MYARCH=`echo $MYARCH|tr ',' ' '`
   MYNETARCH=`echo $MYNETARCH|tr ',' ' '`
   MYNETTOPOLOGY=`echo $MYNETTOPOLOGY|tr ',' ' '`
   MYHD=`echo $MYHD|tr ',' ' '`
   MYXSMSIZE=`echo $MYXSMSIZE|tr ',' ' '`
   MYCPU=`echo $MYCPU|tr ',' ' '`
   MYNIC=`echo $MYNIC|tr ',' ' '`
   MYTSUL=`echo $MYTSUL|tr ',' ' '`
   debug1 "MYXSMSIZE=$MYXSMSIZE"

   #verbose1 "* Verbose mode."
   verbose2 "   VERSION=$VERSION"
   verbose2 "   MYSIG=$MYSIG"
   verbose2 "   EXPTAG=$EXPTAG"

   # sanity checks
   if [ "$cleanqueue" = "1" -a "$resumequeue" = "1" ]; then echo "ERROR: cannot specify both -c and -r together"; exit 1; fi


   # External scripts/dependencies that are needed:
   # lockfile
#   requireddeps="lockfile isoinfo rpcinfo" # lockfile-->procmail rpcinfo-->rpcbind isoinfo-->genisoimage
# rpcinfo checks are done already separately
   requireddeps="isoinfo gawk" # lockfile-->procmail rpcinfo-->rpcbind isoinfo-->genisoimage
   [ "$nodistrib" = "0" ] && requireddeps="$requireddeps lockfile"
   missingdeps=""
   if [ ! -z "$requireddeps" ]; then
      for p in $requireddeps; do
         ispkgavail=`which $p`
         [ -z "$ispkgavail" ] && missingdeps="$missingdeps $p"
      done
   fi
   if [ ! -z "$missingdeps" ]; then
      echo "This script needs the following packages:"
      for p in $missingdeps; do
         echo "   $p"
      done
      exit 1
   fi

   if [ "$BINMAKE" != "0" ]; then
      DSEPARAMS="$DEFAULTDSEBINGEN"
   fi
   debug2 "md_commandline: DSEPARAMS=$DSEPARAMS"

   # MYUSER: CURRENT USER
   [ -z "$MYUSER" ] && MYUSER="$DEFUSER"
   [ -z "$CLUSER" ] || MYUSER="$CLUSER"
   debug1 "MYUSER=$MYUSER"

   # MYMACH
   debug1 "DEFMACH=$DEFMACH"
   [ -z "$MYMACH" ] && MYMACH="$DEFMACH"
   [ "$CLMACH" != "" -a "$CLMACH" != "'*'" ] && MYMACH="$CLMACH"
   if [ "$CLMACH" = "'*'" ]; then
      for dddd in $LOCALMNTFORREMOTESIMCLIENTS/*; do
         debug2 "$dddd$DEFHOME/$MYUSER/$DEFLOCALINST/$SIMULATOR/$DSUFFINFO/$EXPTAG"
         if [ -s $dddd$DEFHOME/$MYUSER/$DEFLOCALINST/$SIMULATOR/$DSUFFINFO/$EXPTAG ]; then
             MYMACH=`basename $dddd`
             break
         fi
      done
   fi
   debug1 "MYMACH=$MYMACH"

   # LOCAL SIMULATOR MACHINE
   if [ "$MYMACH" != "$DEFMACH" ]; then
      DIRMACH="$LOCALMNTFORREMOTESIMCLIENTS/$MYMACH"
   else
      DIRMACH=""
   fi
   debug1 "DIRMACH='$DIRMACH'  (can be empty)"

   # Define HOMBASE
   [ "$MYUSER" = "$DEFUSER" ] && HOMEBASE="$DEFHBASE" || HOMEBASE="$DIRMACH$DEFHOME/$MYUSER"

   # Define EXPUSER
   EXPUSER="${MYUSER:0:3}" # just 3 chars from user, to avoid too long paths

   # Define EXPBUFF (LOCAL or PER-USER SIMULATOR DIRECORY)
   EXPBUFF="$HOMEBASE/$DEFLOCALINST"

   # Create EXPBUFF
   [ -d "$EXPBUFF" -o "$NORUN" = "1" ] || debug1 "Creating directory: EXPBUFF=$EXPBUFF"
   [ -d "$EXPBUFF" -o "$NORUN" = "1" ] || mkdir "$EXPBUFF" 2>/dev/null

   # Source DSEBASECFG
   DSEBASECFG="$EXPBUFF/$DSECONFIGFILE"
   [ -s "$DSEBASECFG" ] && source "$DSEBASECFG"

   #
   LOCALSIMROOT="$EXPBUFF/$SIMULATOR"

   # LOCAL USER  DIRECTORIES
   LOCALCBIN="$LOCALSIMROOT/$DSUFFBIN"
   LOCALCINFO="$LOCALSIMROOT/$DSUFFINFO"
   LOCALCEXP="$LOCALSIMROOT/$DSUFFEXP"
   LOCALCQUEUE="$LOCALSIMROOT/$DSUFFQUEUE"

   # SERVER DIRECTORIES
   SERVSIMROOT="$DEFROOT/$MYUSER/$SIMULATOR"
   SERVCBIN="$SERVSIMROOT/$DSUFFBIN"
   SERVCINFO="$SERVSIMROOT/$DSUFFINFO"
#   SERVCEXP="$SERVSIMROOT/$DSUFFEXP"
   SERVCEXP="$SERVSIMROOT"
   SERVCQUEUE="$SERVSIMROOT/$DSUFFQUEUE"
   debug1 "SERVCEXP=$SERVCEXP"

} # END function md_commandline

#####################################################
function checkfree () {
#####################################################
   # Checking free space
   freebytes=$(($(stat -f --format="%a*%S" .)))
   totabytes=$(($(stat -f --format="%b*%S" .)))
   freespace=`echo "scale=0; $freebytes * 100 / $totabytes"|bc -l`  
   if [ "$freespace" -lt "$PCTMINFREESPACE" -a "$freebytes" -lt "$FREE10GIGA" ]; then
      cecho "ERROR: you need at least ${PCTMINFREESPACE}% of free disk space (current=${freespace}%)." "$red"
      exit 60
   fi
}

#####################################################
function setuplocalconfigfile () {
#####################################################
   local itag="$1" # e.g. 'EXPTAG'
   local otag="$2" # e.g. 'INFOFILE'
   local checkcf="$3" # e.g. 'CHECK' or 'NOCHECK'
#   tfile  # tag file (from itag)
#   cfile  # config file (from (otag) e.g. INFOFILE
   eval tfile=\$$itag # e.g. T1000
   eval cfile=\$$otag # e.g. complete path of T1000

   debug3 "itag=$itag"
   debug3 "otag=$otag"
   debug3 "tfile=$tfile"
   debug3 "cfile=$cfile"
   debug3 "checkcf=$checkcf"

   if [ ! -z "$tfile" ]; then # can be empty if I just want to print the defaults

      # Check if we are using COMMON INFOFILEs
      #if [ "$NORUN" = "0" ]; then
         verbose1 "* Checking and copying the $otag '$cfile'..."
         copyinf2locdir $tfile;   #sets NOINFO, input: $tfile
         debug3 "NOINFO=$NOINFO"
      #fi

      if [ "$NOINFO" = "0" -a "NONET" = "1" ]; then
         EXPINFO="$LOCALCINFO"; SERVSIMROOT="$LOCALSIMROOT";
         verbose2 "* No $otag found and no network:"
         verbose2 "  --> Changing EXPINFO to '$EXPINFO' and SERVSIMROOT to '$LOCALSIMROOT'"
      fi

      debug1 "$itag(1)=$tfile"
      eval $itag=`basename $tfile`
      debug1 "$itag(2)=$tfile"
      eval $otag="$EXPINFO/$tfile"
      debug1 "$otag=$cfile"
      eval cfile=\$$otag # e.g. complete path of T1000

      # sanity check
      if [ ! -s $cfile -a "$checkcf" = "CHECK" ]; then
         cecho "ERROR: Cannot find the $otag '$cfile'" "$red"
         exit 40
      fi
      if [ -d $cfile ]; then
         cecho "ERROR: The tag you specified is not valid (same as a dir name): change it" "$red"
         exit 40
      fi
   fi
}

#####################################################
function startup1 () {
#####################################################
   debug1 "---------- STARTING startup1"
   debug1 "LOCALSIMROOT='$LOCALSIMROOT'"
   [ "$nodistrib" = "0" ] && setrpcinfo # set $rpcinfo
   [ "$nodistrib" = "0" ] && MYLOCKF1="lockfile -r 3" || MYLOCKF1="touch"
   [ "$nodistrib" = "0" ] && MYLOCKF2="lockfile -l 3" || MYLOCKF2="touch"
   [ "$nodistrib" = "0" ] && MYLOCKF3="lockfile -l"   || MYLOCKF3="touch"
   [ "$nodistrib" = "0" ] && MYLF3TIMEOUT="\$timeout"  || MYLF3TIMEOUT=""
   # Check if the machine is part of a distributed simulation pool with a common SIMSERVER
   if [ -z "$SIMSERVER" -o "$NONET" = "1" ]; then
      verbose1 "* This machine ($MYMACH) is used as a standalone simulation host"
      NONET="1"
   else
      verbose1 "* This machine ($MYMACH) is part of a distributed simulation pool having SIMSERVER as NFS SERVER"
      verbose1 "* SIMSERVER='$SIMSERVER'"
   fi

   # Check if there is enough disk space
   checkfree

   # check connection to the server first of all
   NOSERVERSIMROOT="1"
   if [ "$NONET" = "0" ]; then
      if [ "$quiet" = "0" ]; then verbose1 "* Checking connection with simulation server '$SIMSERVER'..."; fi
      checkconnection nfs $SIMSERVER
      #ASSERT: the connection is up
      if [ "$quiet" = "0" ]; then verbose1 "  ...connection OK."; fi
      NOSERVERSIMROOT="0"

      #checks
      verbose1 "* Checking availability of NFS repo '$SERVSIMROOT'..."
      if [ ! -d $SERVSIMROOT ]; then
         NOSERVERSIMROOT="1"
         if [ "$quiet" = "0" ]; then
            cecho "WARNING(startup1): cannot find nfs repo SERVSIMROOT '$SERVSIMROOT'." "$yellow"
            echo "If you want to run in standalone mode (no network), specify the option --nonet"
            exit 21
#            echo "WARNING(startup1): switching to 'local mode'."
         fi
#         NONET="1"
#         SERVSIMROOT="$LOCALSIMROOT"
#         echo "WARNING: trying to use LOCALSIMROOT '$SERVSIMROOT'."
#   mkdir -p "$SERVSIMROOT" >/dev/null 2>/dev/null
      else
         verbose1 "  ...NFS server OK."
      fi
   fi

   if [ "$NOSERVERSIMROOT" = "1" ]; then
      SERVSIMROOT="$LOCALSIMROOT"
      [ -d $SERVSIMROOT ] && mkdir -p "$SERVSIMROOT" >/dev/null 2>/dev/null
   fi
   # ASSERT: a directory for simulation inputs is available
   # ASSERT: connection is working or user doesn't need the network
   verbose1 "* Location for simulator inputs/results: SERVSIMROOT='$SERVSIMROOT'"

#if [ "$PRINTMODELDIR" = "0" ]; then
   # Assign EXPINFO
   EXPINFO="$SERVSIMROOT/$DSUFFINFO"
   debug1 "EXPINFO='$EXPINFO'"

   # Sanity checks
   # Create server INFO directory if missing
   if [ ! -d $EXPINFO ]; then
      mkdir -p "$EXPINFO" >/dev/null 2>/dev/null
   fi
   #recheck:
   if [ ! -d $EXPINFO ]; then
      cecho "ERROR: couldn't create info directory '$EXPINFO'." "$red"
      exit 7
   fi

   # Create local BIN directory
   if [ ! -d $LOCALCBIN -a "$NORUN" = "0" ]; then
      verbose1 "   Creating LOCAL '$LOCALCBIN' folder"
      mkdir -p $LOCALCBIN 2>/dev/null
   fi
   #recheck:
   if [ ! -d $LOCALCBIN -a "$NORUN" = "0" ]; then
      cecho "ERROR: couldn't create info directory '$LOCALCBIN'." "$red"
      exit 7
   fi

   # Create local INFO directory
   if [ ! -d $LOCALCINFO -a "$NORUN" = "0" ]; then
      verbose1 "   Creating LOCAL '$LOCALCINFO' folder"
      mkdir -p $LOCALCINFO 2>/dev/null
   fi
   #recheck:
   if [ ! -d $LOCALCINFO -a "$NORUN" = "0" ]; then
      cecho "ERROR: couldn't create info directory '$LOCALCINFO'." "$red"
      exit 7
   fi

   # setup the local configuration file (EXPTAG-->INFOFILE)
   local checkinfofile="CHECK"
   [ "$INFOGEN" = "1" ] && { NOINFO="1"; checkinfofile=NOCHECK; }
   setuplocalconfigfile EXPTAG INFOFILE $checkinfofile


   if [ "$PRINTMODELDIR" = "0" ]; then
   #   # sanity check
   #   if [ -d $INFOFILE ]; then
   #      echo "ERROR: The tag you specified is not valid (same as a dir name): change it"
   #      exit 40
   #   fi

      EXPBASE="$SERVSIMROOT"
      debug1 "EXPBASE='$EXPBASE'"
      [ "$BINMAKE" = "1" ] && EXPBASE="${EXPBASE}/$DSUFFBIN"
      QUEUEDIR=${SERVSIMROOT}/$DSUFFQUEUE
      debug1 "QUEUEDIR='$QUEUEDIR'"
      [ -d $QUEUEDIR -o "$NORUN" = "1" ] || mkdir $QUEUEDIR
      BINDIR=${SERVSIMROOT}/$DSUFFBIN
      debug1 "BINDIR='$BINDIR'"
      [ -d $BINDIR -o "$NORUN" = "1" ] || mkdir $BINDIR
      INFODIR=${SERVSIMROOT}/$DSUFFINFO
      debug1 "INFODIR='$INFODIR'"
      [ -d $INFODIR -o "$NORUN" = "1" ] || mkdir $INFODIRINFO

      # EXIT WITH ERROR IF INFOFILE IS NOT DEFINED AT THIS POINT
      debug1 "NOINFO=$NOINFO"
      debug1 "INFOGEN=$INFOGEN"
      if [ "$NOINFO" = "1" -a "$INFOGEN" = "0" ]; then
         cecho "ERROR: INFOFILE '$INFOFILE' not found!" "$red"
         cecho "  If you want to automatically create one with that name, issue (option '-h' for help):" "$red"
         cecho "  $0 $EXPTAG --infogen [<experiment_options>]" "$red"
         exit 50
      fi

      # Detecting processors
      CPUS=`cat /proc/cpuinfo|awk '/^processor/{a=$3}END{printf("%d\n", a + 1)}'`
      verbose1 "* Detected $CPUS processors."
   fi

   #
   EXECTRACING="0"
   CPUTRACING="0"
   MEMTRACING="0"
   STATRACING="0"

   debug1 "---------- ENDING startup1"
} # END startup1

#####################################################
function startup2 () {
#####################################################
   debug1 "---------- STARTING startup2"
   debug1 "listmodel=$listmodel"
   # Validation Code
#   if [ -z $VALIDATIONCODE ]; then
#      echo "ERROR: Validation code is missing."
#      exit 99
#   fi
   # Checks DEFAULTS

   # SIMULATOR DIRECTORY
   [ -z "$SIMDIR" ] && SIMDIR="$DEFSIMDIR"
   [ -z "$CLSIMDIR" ] || SIMDIR="$CLSIMDIR"

   # RULE FOR SIMULATOR BASEDIR (holding several versions of the simulator):
   # should be in the HOME directory of current user
   # Define: $MYBASEDIR
   MYBASEDIR="$HOMEBASE/$SIMDIR"
   #Sanity check:
   [ ! -d "$MYBASEDIR" -a "$NORUN" = "0" ] && { cecho "ERROR: cannot find simulator dir '$MYBASEDIR'" "$red"; exit 1; }
   debug1 "HOMEBASE=$HOMEBASE"
   debug1 "SIMDIR=$SIMDIR"
   debug1 "MYBASEDIR=$MYBASEDIR"


   # set RUNID if not specified in the infofile
   [ -z "$RUNID" ] && RUNID="$DEFAULTRUNID"

   # override memtrfile (infofile) if specified on the command line
   [ -z "$MYMEMTRFILE" ] || memtrfile="$MYMEMTRFILE"

   # override cputrfile (infofile) if specified on the command line
   [ -z "$MYCPUTRFILE" ] || cputrfile="$MYCPUTRFILE"

   # override medtrfile (infofile) if specified on the command line
   [ -z "$MYMEDTRFILE" ] || medtrfile="$MYMEDTRFILE"

   # set MYLIBC if not specified in the infofile
   [ -z "$MYLIBC" ] && MYLIBC="$DEFAULTLIBC"
   [ -z "$CLMYLIBC" ] || MYLIBC="$CLMYLIBC"
   LIBVTARGET="$MYLIBC"
   [ "$BINMAKE" = "1" ] && LIBVTARGET="$LIBVERSION"

   # set exename if not specified in the infofile
   [ -z "$exename" ] && exename="$DEFAULTEXENAME"

   # display
   if [ "$DISPLAYON" != "0" ]; then display="yes"; fi
   if [ "$DISPLAYON" -gt "1" ]; then displayhold="yes"; else displayhold="no";  fi

   # model
   [ -z "$listmodel" ] && listmodel="$DEFAULTMODEL"
   [ -z "$MYMODEL" ] || listmodel="$MYMODEL"	

   # appi
   [ -z "$listappi" ] && listappi="$DEFAULTAPPI"
   [ -z "$MYAPPI" ] || listappi="$MYAPPI"	
   debug4 "listappi=$listappi"

   # size
   [ -z "$listsize" ] && listsize="$DEFAULTINPUT"
   [ -z "$MYINPUT" ] || listsize="$MYINPUT"	

   # nodes
   [ -z "$listnodes" ] && listnodes="$DEFAULTNODES"
   [ -z "$MYNODES" ] || listnodes="$MYNODES"	

   # cores
   [ -z "$listcores" ] && listcores="$DEFAULTCORES"
   [ -z "$MYCORES" ] || listcores="$MYCORES"	

   # timing
   [ -z "$listtiming" ] && listtiming="$DEFAULTTIMING"
   [ -z "$MYTIMING" ] || listtiming="$MYTIMING"	

    # ntopology
#    [ -z "$listntopology" ] && listntopology="$DEFAULTNETTOPOLOGY"
   [ -z "$MYNETTOPOLOGY" ] || listntopology="$MYNETTOPOLOGY"
   [ -z "$listntopology" ] && DSEPARAMS=`echo "$DSEPARAMS"|sed 's/ ntopology//g'`

    # narch
#    [ -z "$listnarch" ] && listnarch="$DEFAULTNETARCH"
   [ -z "$MYNETARCH" ] || listnarch="$MYNETARCH"
   [ -z "$listnarch" ] && DSEPARAMS=`echo "$DSEPARAMS"|sed 's/ narch//g'`

   # hd
   [ -z "$listhd" ] && listhd="$DEFAULTHD"
   [ -z "$MYHD" ] || listhd="$MYHD"	

   # cpu
   [ -z "$listcpu" ] && listcpu="$DEFAULTCPU"
   [ -z "$MYCPU" ] || listcpu="$MYCPU"	
#echo "PIPPO listcpu=$listcpu"

   # NIC
   [ -z "$listnic" ] && listnic="$DEFAULTNIC"
   [ -z "$MYNIC" ] || listnic="$MYNIC"

   # memory latency
   [ -z "$listmem" ] && listmem="$DEFAULTLMEM"
   [ -z "$MYLMEM" ] || listmem="$MYLMEM"

   # core frequency
   [ -z "$listfreq" ] && listfreq="$DEFAULTFREQ"
   [ -z "$MYFREQ" ] || listfreq="$MYFREQ"

   # memory architecture
   [ -z "$listarch" ] && listarch="$DEFAULTARCH"
   [ -z "$MYARCH" ] || listarch="$MYARCH"

   # TSU
   [ -z "$listtsulatency" ] && listtsulatency="$DEFAULTTSUL"
   [ -z "$MYTSUL" ] || listtsulatency="$MYTSUL"	


   # set owmsize if not specified in the timing file
   debug1 "owmsize=$owmsize"
#   if [ -z "$owmsize" ]; then
   if [ -z "$owmsize" -o ! -z "$MYXSMSIZE" ]; then
   # and if listxsmsize is not set already either in the infofile or in the command line
      [ -z "$MYXSMSIZE" ] || listxsmsize="$MYXSMSIZE"
      if [ -z "$listxsmsize" ]; then
         owmsize="$DEFAULTOWMSIZE"
      else 
         owmsize=`echo "$listxsmsize * 1048576"|bc`
      fi
   fi
   
   # POWER OPTION DETECTION
   if [ ! -z "$POWERPERHBS" ]; then
      POWER="1"; DBMODE="1"
   fi

   debug1 "VALIDATIONCODE=$VALIDATIONCODE"
   debug1 "RUNID=$RUNID"
   debug1 "MYMEMTRFILE=$MYMEMTRFILE"
   debug1 "MYCPUTRFILE=$MYCPUTRFILE"
   debug1 "MYMEDTRFILE=$MYMEDTRFILE"
   debug1 "MYLIBC=$MYLIBC"
   debug1 "CLMYLIBC=$CLMYLIBC"
   debug1 "LIBVTARGET=$LIBVTARGET"
   debug1 "exename=$exename"
   debug1 "DISPLAYON=$DISPLAYON"
   debug1 "POWER=$POWER"
   debug1 "DBMODE=$DBMODE"
   debug1 "MYWSUBDIR=$MYWSUBDIR"
   debug1 "MYMODEL=$MYMODEL"
   debug1 "MYAPPI=$MYAPPI"
   debug1 "MYINPUT=$MYINPUT"
   debug1 "MYNODES=$MYNODES"
   debug1 "MYCORES=$MYCORES"
   debug1 "MYTIMING=$MYTIMING"
   debug1 "MYNETTOPOLOGY=$MYNETTOPOLOGY"
   debug1 "MYNETARCH=$MYNETARCH"
   debug1 "MYHD=$MYHD"
   debug1 "MYCPU=$MYCPU"
   debug1 "MYNIC=$MYNIC"
   debug1 "MYXSMSIZE=$MYXSMSIZE"
   debug1 "listxsmsize=$listxsmsize"
   debug1 "owmsize=$owmsize"

   # xsmsize
   [ -z "$listxsmsize" ] && listxsmsize="$DEFAULTXSMSIZE"
   [ -z "$MYXSMSIZE" ] || listxsmsize="$MYXSMSIZE"

   # SETTING $MYSIMVER... (if not specified in the timing file or cmdline)
   if [ -z "$MYSIMVER" ]; then # try the following..
      mysavedir=`pwd`
      cd $MYBASEDIR
#      lastverdir=`ls -trd $SIMSMALL-*/|egrep "$SIMSMALL-[0-9][0-9]*/"|tail -1`
      lastverdir=`ls -d $SIMSMALL-*/|egrep "$SIMSMALL-[0-9][0-9]*/"|tail -1`
      cd $lastverdir
      # 1) Check if it exists a '$SIMSMALL' directory --> $MYSIMVER
      curmoddir=`pwd -L|rev|cut -d/ -f1|cut -d- -f2|rev`
      curmodver=`pwd -L|rev|cut -d/ -f1|cut -d- -f1|rev`
      cd $mysavedir
      debug2 "1:curmoddir=$curmoddir"
      debug2 "1:curmodver=$curmodver"
      if [ "$curmoddir" = "$SIMSMALL" ]; then MYSIMVER="$curmodver"; fi
      debug2 "1:MYSIMVER=$MYSIMVER"

      # 2) Check if we are in a 'listmodel' directory --> $MYSIMVER
      curmoddir=`pwd -L|rev|cut -d/ -f1|cut -d- -f2|rev`
      curmodver=`pwd -L|rev|cut -d/ -f1|cut -d- -f1|rev`
      debug2 "2:curmoddir=$curmoddir"
      debug2 "2:curmodver=$curmodver"
      if [ "$curmoddir" = "$listmodel" ]; then MYSIMVER="$curmodver"; fi
      debug2 "2:MYSIMVER=$MYSIMVER"

      # 3) Check if there is a 'latest' link --> $MYSIMVER
      if [ -L $MYBASEDIR/latest ]; then
         curmoddir=`readlink $MYBASEDIR/latest|rev|cut -d/ -f1|cut -d- -f2|rev`
         curmodver=`readlink $MYBASEDIR/latest|rev|cut -d/ -f1|cut -d- -f1|rev`
         debug2 "3:curmoddir=$curmoddir"
         debug2 "3:curmodver=$curmodver"
         if [ "$curmoddir" = "$SIMSMALL" ]; then
            MYSIMVER="$curmodver"
         fi
         debug2 "3:MYSIMVER=$MYSIMVER"
      fi

      # 4) Set MYSIMVER or as above
      if [ -z "$MYSIMVER" ]; then
         MYSIMVER="$DEFAULTSIMVER"
         debug2 "4:MYSIMVER=$MYSIMVER"
      fi
   fi
   [ -z "$CLMYSIMVER" ] || MYSIMVER="$CLMYSIMVER"

   # SETTING $SIMBASE
   # RULE FOR SIMULATOR DIR: should be in the HOME directory of current user
   SIMBASE="$MYBASEDIR/${SIMSMALL}-$MYSIMVER"

   debug2 "MYSIMVER=$MYSIMVER"
   debug2 "SIMSMALL=$SIMSMALL"
   debug2 "SIMDIR=$SIMDIR"
   debug2 "listmodel=$listmodel"
   debug2 "listappi=$listappi"
   debug2 "listsize=$listsize"
   debug2 "SIMBASE=$SIMBASE"

   # SETTING EXEDIR[$model]
   # RULE FOR WORKING DIR: there is a different one for each value of the first DSEPARAMS
   debug2 "startup2: DSEPARAMS=$DSEPARAMS"
   IFS=' ' read -a dseparamsv <<< "$DSEPARAMS"
   firstparam=${dseparamsv[0]}
   debug1 "first DSE param: '$firstparam'"
   if [ "$firstparam" = "" ]; then
      cecho "ERROR: cannot identify first parameter for DSE." "$red"
      exit 2
   fi
   debug3 "firstparam=$firstparam"
   eval firstparamvals=\$list$firstparam  # indirect reference !
   debug2 "firstparamvals=$firstparamvals"

## UNNECESSARY:
##   # Expecting the model list in firstparamvals; if empty set to listmodel
##   if [ "$firstparamvals" = "" ]; then firstparamvals="$listmodel"; fi 
   # Sanity check
   if [ "$firstparamvals" = "" ]; then
      cecho "ERROR: cannot identify values for parameter '$firstparam'." "$red"
      exit 2
   fi

   # Check if the related function works well (e.g. if all the specified model-directories exist)
   fun="$CODEFUN$firstparam"
   debug2 "fun=$fun"
   for model in $firstparamvals; do
      EXEDIR[$model]=`pwd`; # Set default EXEDIR[$model]
      #############################################
      # Preliminary setup and check on model-driver
      selectmodeldriver QUIET
      #############################################

      debug2 "function '$fun'   value=$model"
      function_exists $fun
      if [ $? = 0 ]; then 
         $fun $model  # e.g.: funmodel $model # Set EXEDIR[$model],MODELDIR[$model]
      else
         echo "ERROR: cannot find the function '$fun' for parameter '$firstparam'."; exit 2
      fi

      # Assert: MODELDIR[$model] is defined
      if [ "$PRINTMODELDIR" = "1" ]; then
         echo "${MODELDIR[$model]}"
         exit 0
      fi

      #overwrite EXEDIR[$model] if the user wants so
      if [ -z "${EXEDIR[$model]}" -a "$NORUN" = "0" ]; then
         echo "ERROR: EXEDIR[$model] is undefined"
         exit 15
      fi
      # Assert: EXEDIR[$model] is defined
   done

   # Create the tag file if it was not read by a file
   if [ "$newexp" = "1" -a "$INFOGEN" = "1" ]; then
      if [ -s $INFOFILE ]; then 
         cecho "WARNING: replacing previous INFOFILE '$INFOFILE'" "$yellow"
      fi
      NOINFO="1"
   fi
   if [ "$NOINFO" = "1" -a "$INFOGEN" = "1" ]; then
      debug1 "Creating INFOFILE '$INFOFILE'"
      echo "#!/bin/bash" > $INFOFILE
      echo "MYLIBC=\"$LIBVERSION\"" >> $INFOFILE
      echo "SIMDIR=\"$SIMDIR\"" >> $INFOFILE
      echo "MYSIMVER=\"$MYSIMVER\"" >> $INFOFILE
      [ -z "$DYSPLAYON" ]  || echo "display=\"yes\"" >> $INFOFILE
      echo "listmodel=\"$listmodel\"" >> $INFOFILE
      for model in $firstparamvals; do
         setappi $model   # sets listappi[$model]
         echo "listappi[$model]=\"${listappi[$model]}\"" >> $INFOFILE
         echo "workingsubdir[$model]=\"${workingsubdir[$model]}\"" >> $INFOFILE
         for appi in ${listappi[$model]}; do
            setsize $appi;    # sets listsize[$appi] and GSFP[$appi] and IFFP[$appi] # NEEDS $model
            echo "listsize[$appi]=\"${listsize[$appi]}\"" >> $INFOFILE
#            echo "guestsetup[$appi]=\"${guestsetup[$appi]}\"" >> $INFOFILE
#            echo "guestsetup[$appi]=\"builtin\"" >> $INFOFILE
         done
      done
      [ -z "$listcores" ]  || echo "listcores=\"$listcores\"" >> $INFOFILE
      [ -z "$listnodes" ]  || echo "listnodes=\"$listnodes\"" >> $INFOFILE
      [ -z "$listtiming" ] || echo "listtiming=\"$listtiming\"" >> $INFOFILE
      [ -z "$listnic" ] || echo "listnic=\"$listnic\"" >> $INFOFILE
      [ -z "$listntopology" ] || echo "listntopology=\"$listntopology\"" >> $INFOFILE
      [ -z "$listnarch" ]  || echo "listnarch=\"$listnarch\"" >> $INFOFILE
      [ -z "$listhd" ]     || echo "listhd=\"$listhd\"" >> $INFOFILE
      [ -z "$owmsize" ]    || echo "owmsize=\"$owmsize\"" >> $INFOFILE
      cecho "* INFOFILE '$INFOFILE' generated. Here is its content:" "$green"
      echo "-----------------------------"
      cat $INFOFILE
      echo "-----------------------------"
      if [ "$BINMAKE" = "0" ]; then
         cecho "* Now you should generate the binaries by issuing:" "$green"
         cecho "  $0 $EXPTAG --bingen" "$green"; echo ""
         exit 51
      else
         echo "* Starting the generation of binary file(s)..."
      fi
   fi


   #
   if [ "$newexp" = "1" -a "$oldexp" != "" ]; then
      if [ "$quiet" = "0" ]; then
         echo "WARNING(startup2): you specified both old and new expriments"; md_usage;
      fi
   fi

   if [ "$outdir" != "" ]; then 
      if [ -d $outdir ]; then
         EXPBASE="$outdir"
      else
         if [ "$quiet" = "0" ]; then
            echo "WARNING(startup2): cannot find output directory '$outdir'."
         fi
         EXPBASE="$EXPINFO"
         if [ "$quiet" = "0" ]; then
            echo "WARNING(startup2): using directory '$EXPBASE' as output directory."
         fi
#         echo "ERROR: cannot find output directory '$outdir'"
#         exit 2
      fi
   fi
   # assert: EXPBASE is set

   if [ "$oldexp" != "" ]; then 
#      exlab=${oldexp%%_*}
#      PREF=${exlab%%[0-9]*}
#     EXPNUM=${exlab##*[a-zA-Z]}
#echo "PREF=$PREF"
#echo "EXPNUM=$EXPNUM"
      EXP="$oldexp"
   else
      newexp="1"
   fi

   #assign a date tag
   DSTART=`date +%y%m%d%H%M`
   if [ "$mmytag" != "" ]; then 
      thetag=$mmytag
   else
      thetag="$DSTART"
   fi


   # BUILD THE EXPERIMENT PREFIX
   if [ "$newexp" = "1" -o "$EXP" = "" ]; then 
      PREF="EXP"; EXPNUM="$thetag"; 
      EXPROOT="${PREF}_${EXPTAG}";
      EXPLONG="${EXPROOT}_${MYMACH}_${EXPUSER}_${EXPNUM}"
      if [ "$nodistrib" = "1" ]; then
#         EXP="${PREF}_${EXPTAG}_${MYMACH}_${EXPUSER}_${EXPNUM}"
         EXP="$EXPLONG"
         EXPPREF="$EXPLONG"
      else
#         EXP="${PREF}_${EXPTAG}"
         EXP="$EXPROOT"
#         EXPPREF="${PREF}_${EXPTAG}_${MYMACH}_${EXPUSER}_${EXPNUM}"
         EXPPREF="$EXPLONG"
      fi
   fi
   EDIR="${EXPBASE}/${EXP}"  # MAYBE is remote: use with caution
   debug1 "EDIR=$EDIR"

   # check if EXPBASE is writable
if [ "$NORUN" = "0" ]; then
   writable="0"
   tmp=`mktemp --tmpdir=$EXPBASE 2>/dev/null`
   if [ "$tmp" != "" ]; then
      if [ -f $tmp ]; then
         rm -f $tmp >/dev/null 2>/dev/null
         writable="1"
      fi
   fi
   if [ "$writable" = "0" -a "$NORUN" = "0" ]; then
      cecho "ERROR: cannot write in directory '$EXPBASE'" "$red"
      exit 3
   fi

#   if [ "$overwrite" = "1" ]; then
   if [ "$cleanqueue" = "1" ]; then
       cleanq $MYQWAITING
       cleanq $MYQRUNNING
       cleanq $MYQFAILED
       cleanq $MYQDONE
   fi
   # decide single/multiple sim-host
   checkq def $MYQWAITING; #QSIZE="$?"  ## PREREQUISIT: $EXP must be defined before
   if [ "$QSIZE" = "0" ]; then
      # assume single sim-host or first host
      POPULATE="1";
   else
      # assume multiple sim-host
      POPULATE="0";
   fi

   #
#   if [ "$overwrite" = "1" -a "$NONET" = "0" ]; then
#      echo "REPEATING (AND OVERWRITING) EXPERIMENT ${EXP}"
#   else
#      seconds=`date +%S`
#      deltat=`expr 60 - $seconds`
      if [ -d "${EDIR}" ]; then 
         if [ "$quiet" = "0" ]; then
            echo "* Using the  already existing Experiment Directory EDIR='$EDIR'."
         fi
#         echo "         * If you should want to overwrite the experiment, then write:"
#         echo "           $0 -o -e $EXP or delete directory $EDIR"
#         echo "         * you can otherwise restart after $deltat seconds to get a new tag"
#         exit 4
      fi
#   fi

# Create the experiment directory EDIR
   if [ "$newexp" = "1" ]; then
      if [ "$BINMAKE" = "0" ]; then
         if [ ! -d "$EDIR" ]; then
            verbose1 "* Creating EDIR='$EDIR'..."
            mkdir $EDIR >/dev/null 2>/dev/null
         fi
         [ ! -d "$EDIR" ] && echo "ERROR: Cannot create '$EDIR'" && exit 11
      fi
   fi

#
#if [ "$NOSERVERSIMROOT" = "0" ]; then
#   if [ "$nobin" = "0" ]; then
#      if [ ! -d $SERVCBIN ]; then
#         echo "ERROR: cannot copy from directory 'SERVCBIN'"
#         exit 3
#      fi
#   fi ## at this point the source COMMON BIN directory exists
#fi

   # setup LOCAL output dir
   OUTDIR="${EXEDIR[$model]}/$EXP"
   if [ ! -d "$OUTDIR" ]; then
      verbose1 "* Creating 'OUTDIR=$OUTDIR'..."
      rm -rf $OUTDIR 2>/dev/null >/dev/null
      mkdir -p $OUTDIR 2>/dev/null >/dev/null
      [ ! -d "$OUTDIR" ] && echo "ERROR: Cannot create '$OUTDIR'" && exit 12
   fi
fi # NORUN==0

   #
   debug1 "---------- ENDING startup2"
}

#####################################################
function myexpstart () {
#####################################################
   local msg1
   #
   EXPCOUNT="0"
   SIMOKCOUNT="0"
   SIMFAILCOUNT="0"
   SIMRETRY="0"
   T00000=`date +%s%N | cut -b1-13`
#   if [ "$debug" != "0" ]; then echo "   T00000=$T00000"; fi
#   echo "<<<<<<<<< EXPERIMENT START $EXP (DIR: $EDIR ) `date` $VERSION1"
   if [ "$BINMAKE" = "0" ]; then
      msg1="$msgEXPSTART"
   else
      msg1="$msgBINGENSTART"
   fi
   comment "<<<<<<<<< $msg1 $EXP (DIR: $OUTDIR ) `date` $VERSION1"
   comment ""

   # Print out some important variables for checking
   verbose1 "*  SUMMARY OF GLOBALS:"
   verbose1 "   SVER=$SVER"
   verbose1 "   LIBVERSION=$LIBVERSION"
   verbose1 "   LIBVTARGET=$LIBVTARGET"
   verbose1 "   VALIDATIONCODE=$VALIDATIONCODE"
   verbose1 "   EXPBASE=$EXPBASE"
   verbose1 "   EXPINFO=$EXPINFO"
   verbose1 "   EXP=$EXP"
   verbose1 "   EDIR=$EDIR"
   verbose1 "   OUTDIR=$OUTDIR"
   #verbose1 "   exedir=$exedir"
   verbose1 "   verbose=$verbose"
   #verbose1 "   overwrite=$overwrite"
   verbose1 "   cleanqueue=$cleanqueue"
   verbose1 "   writable=$writable"
   verbose1 "   nobin=$nobin"
   verbose1 "   NONET=$NONET"
   verbose1 "   display=$display"
   verbose1 "   displayhold=$displayhold"
   verbose1 "   BINMAKE=$BINMAKE"
   verbose1 "   DSEPARAMS=$DSEPARAMS"
   verbose1 "   POWER=$POWER"
}

#####################################################
function buildfn1 () {
#####################################################
# GLOBAL: nodebase
# GLOBAL: FPREF
# GLOBAL: nodes
# GLOBAL: *pref
# GLOBAL: *num
# GLOBAL: *suff
# GLOBAL: *osuff
#
# OUTPUT: fninn
# OUTPUT: fnout
# OUTPUT: fnoxt
# OUTPUT: fnnic
# OUTPUT: fncom
   local nick1
   local nnnnn
   local ok2
   local nnnnn1
   local a
   local b
   local c
   local d
   local e
   local f
   local nickn1
   local mid
   local max
   local fin
   nick1="$1"
   nnnnn="$2"
   fninn=""
   fnout=""
   fnnic=""
   fncom=""
   ok2="0"
   fin=""

   nnnnn1="$nnnnn"; if [ "$nodebase" = "0" -a "$nnnnn" != "" ]; then nnnnn1=`expr $nnnnn - 1`; fi

   [ -z "$nick1" ] && return 1
   case "$nick1" in
      EXECTRACE)        ok2="1"; a="$exectracepref"; b="$exectracesuff"; c="$exectracenum"; d="$nnnnn"; e="$exectraceosuff"; f=""; ;;
      
      CPTRACE)        ok2="1"; a="$cptracepref"; b="$cptracesuff"; c="$cptracenum"; d="$nnnnn"; e="$cptraceosuff"; f=""; ;;
      METRACE)        ok2="1"; a="$metracepref"; b="$metracesuff"; c="$metracenum"; d="$nnnnn"; e="$metraceosuff"; f=""; ;;
      STTRACE)        ok2="1"; a="$sttracepref"; b="$sttracesuff"; c="$sttracenum"; d="$nnnnn"; e="$sttraceosuff"; f=""; ;;
      XBINDIR)       ok2="1"; a="$LOCALCBIN"; b=""; c=""; d=""; e=""; f=""; ;;
      SIMEXE)        ok2="1"; a="$simsrcpref"; b="$simexesuff"; c=""; d=""; e=""; f=""; ;;
      SIMSRCDIR)     ok2="1"; a="$simsrcpref"; b="$simsrcbranch"; c=""; d=""; e=""; f=""; ;;
      SIMCBLIB)      ok2="1"; a="$CBACKPREF"; b="$CBACKLIB"; c=""; d=""; e=""; f=""; ;;
      SIMSTXTDB)     ok2="1"; a="$simstxtdbpref"; b="$simstxtdbsuff"; c="$simstxtdbnum"; d=""; e="$simstxtdbosuff"; f=""; ;;
      SIMMTXTDB)     ok2="1"; a="$simmtxtdbpref"; b="$simmtxtdbsuff"; c="$simmtxtdbnum"; d=""; e="$simmtxtdbosuff"; f=""; ;;
#      SIMOUT)        ok2="1"; a="SOUT"; b=""; c=""; d=""; e="out"; f=""; ;;
      SIMOUT)        ok2="1"; a="$soutpref"; b="$soutsuff"; c="$soutnum"; d=""; e="$soutosuff"; f=""; ;;
      JOBWATCH)      ok2="1"; a="$jobwpref"; b="$jobwsuff"; c="$jobwnum"; d="$nnnnn"; e=""; f="";
#echo "a='$a' b='$b' c='$c' d='$d'"
                     ;;
      SIMSWATCH)     ok2="1"; a="$simswpref"; b="$simswsuff"; c="$simswnum"; d=""; e=""; f="";
#                     aa="$a"; eval a="$aa"
                     ;;
#      SIMLOG)        ok2="1"; a="LOG"; b=""; c="$simlognum"; d=""; e="log"; f=""; ;;
      SIMLOG)        ok2="1"; a="$simlogpref"; b="$simlogsuff"; c=""; d=""; e="$simlogosuff"; f=""; ;;
#      SIMCONFIG)     ok2="1"; a="LUA"; b=""; c=""; d=""; e="lua"; f=""; ;;
      SIMCONFIG)     ok2="1"; a="$simcfgpref"; b="$simcfgsuff"; c="$simcfgnum"; d=""; e="$simcfgosuff"; f=""; ;;
      SIMMAKEF)      ok2="1"; a="$simmkfpref"; b="$simmkfsuff"; c="$simmkfnum"; d=""; e="$simmkfosuff"; f=""; ;;
      SIMDONE)       ok2="1"; a="SIMDONE"; b=""; c=""; d=""; e="done"; f=""; ;;
      SIMFAILED)     ok2="1"; a="SIMFAILED"; b=""; c=""; d=""; e="failed"; f=""; ;;
#      SIMCTRL)       ok2="1"; a="$ctrlpref"; b="$ctrlsuff"; c=""; d="$simpid"; e=""; f=""; ;;
      SIMMONITOR)    ok2="1"; a="$tsumonpref"; b="$tsumonsuff"; c=""; d=""; e="$tsumonosuff"; f=""; ;;
      NODEOUT)       ok2="1"; a="$stdoutpref"; b="$stdounewf"; c="$stdoutnum"; d="$nnnnn"; e="$stdoutosuff"; f=""; ;;
      NODESCRIPT)    ok2="1"; a="$nodescriptpref"; b="$nodescripnewf"; c="$nodescriptnum"; d="$nnnnn"; e="$nodescriptosuff"; f="$nodes"; ;;
      NODESCRIPTOUT) ok2="1"; a="$nodescriptoutpref"; b="$nodescriptounewf"; c="$nodescriptoutnum"; d="$nnnnn"; e="$nodescriptoutosuff"; f=""; ;;
      SIMNODTIMER)   ok2="1"; a="$timerpref"; b="$timersuff"; c="$timernum"; d="$nnnnn"; e="$timerosuff"; f=""; ;;
      SIMSCREEN)     ok2="1"; a="$screenpref"; b="$screensuff"; c="$screennum"; d="$nnnnn"; e="$screenosuff"; f=""; ;;
      SIMTSUSTATS)   ok2="1"; a="$threadstatpref"; b="$threadstanewf"; c="$threadstatnum"; d="$nnnnn"; e="$threadstatosuff"; f=""; ;;
      SIMTSUSTATS1)  ok2="1"; a="$threadstat1pref"; b="$threadstat1suff"; c="$threadstat1num"; d="$nnnnn"; e="$threadstat1osuff"; f=""; ;;
      SIMMEDTIMER)   ok2="1"; a="$medtimerpref"; b="$medtimersuff"; c=""; d=""; e="$medtimerosuff"; f=""; ;;
      SIMMEDOUT)     ok2="1"; a="$medoutpref"; b="$medounewf"; c=""; d=""; e="$medoutosuff"; f=""; ;;
      SIMDBFN)       ok2="1"; a="$simdbpref"; b="$simdbsuff"; c=""; d=""; e="$simdbosuff"; f=""; ;;
   esac
   aa="$a"; eval a="$aa"
   bb="$b"; eval b="$bb"
#echo ""
#echo "aa='$aa' a='$a'"

   # Sanity check on the above 'case'
   [ -z "$a" ] && return 1

   [ -z "$c" ] && mid="" || mid="$nnnnn1"
   [ "$c" = "c" ] && mid=""
   [ "$c" = "z" ] && mid="" && fin="$nnnnn1"
   [ "$c" = "0" -a "$nnnnn" = "1" ] && mid="" # || b="$nnnnn1"
   [ "$c" = "1" ] && mid="." && fin="$nnnnn1"
   [ "$c" = "0" -a "$nnnnn" != "1" ] && a="$stdoutpref" && fin="$stdounewf"
   max="$f"; [ -z "$f" ] && max="0"

   debug3 "nick1=$nick1 a=$a b=$b c=$c d=$d e=$e f=$f mid=$mid fin=$fin nnnnn=$nnnnn nnnnn1=$nnnnn1"
   #
   if [ "$ok2" = "1" ]; then
      if [ -z "$d" ]; then
         fnnic=`echo "$nick1"|awk '{printf("%-14s",$0)}'`
         mido=""
         d="0"
      else
         fnnic=`echo "$nick1"|awk '{a=sprintf("%s_%02d", $0, n); printf("%-14s",a)}' n=$d`
         mido="$d"
      fi

      #
      fninn="${a}${mid}${b}${fin}"
      fnoxt="${mido}${e}"
      fnout="${FPREF}${fnoxt}"
      fncom="mv"; [ "$c" = "c" -a "$d" -lt "$max" ] && fncom="cp"
   fi
   return 0
}

#####################################################
function savefile1 () {
#####################################################
# INPUT: $1   nickname
# INPUT: $2   number  [1..n]
# GLOBAL OUTDIR
# OUTPUT: 
   buildfn1 "$1" "$2"
   [ -z "$fncom" ] && return 1
#echo "fnnic=$fnnic  fninn=$fninn  fnout=$fnout  fncom=$fncom"
   if [ -f $fninn ]; then
      debug1   "   [found]     $fnnic $fninn --> #$fnoxt"
      rm -f $OUTDIR/$fnout >/dev/null 2>/dev/null
      $fncom $fninn $OUTDIR/$fnout
   else
      SAVFILEERR=`expr $SAVFILERR + 1`
      [ "$verbose" -gt "0" ] && cecho "   [not_found] $fnnic $fninn --> #$fnoxt" "$yellow"
      debug1   "   fnnic=$fnnic"
      debug1   "   fninn=$fninn"
      debug1   "   fnout=$fnout"
      debug1   "   fncom=$fncom"
   fi
   return 0
}

#####################################################
function prockill () {
#####################################################
# GLOBAL: PROCKILLED
   local p=$1
   local dodisown=$2
   local isrunning=""
   local isrunning2=""
   local firstchar
   local procnum
   if [ ! -z "$p" ]; then
      firstchar=${p:0:1}
      procnum=""
      if [ "$firstchar" = "-" ]; then
         procunum=${p:1}
         isrunning=`ps axuc|egrep "^${DEFUSER:0:7}"|awk '{if ($2 == p) printf("%s ",$2)}' p="$p"`
      else
         isrunning=`ps axuc|egrep "^${DEFUSER:0:7}"|awk '{if ($11 == p) printf("%s ",$2)}' p="$p"`
      fi
      if [ ! -z "$isrunning" ]; then
         PROCKILLED="1"
         comment "   Killing processes '$isrunning' of '$p'"
         if [ "$dodisown" = "disown" ] ; then
            kill -0 $isrunning 2>/dev/null >/dev/null
            [ "$?" = "0" ] && disown $isrunning 2>&1 >/dev/null # remove $mypid from bash;s job control !!
         fi
         kill -9 $isrunning >/dev/null 2>&1
         sleep 2
         # verify if killed
         if [ "$firstchar" = "-" ]; then
            isrunning2=`ps axuc|egrep "^${DEFUSER:0:7}"|awk '{if ($2 == p) printf("%s ",$2)}' p="$p"`
         else
            isrunning2=`ps axuc|egrep "^${DEFUSER:0:7}"|awk '{if ($11 == p) printf("%s ",$2)}' p="$p"`
         fi
      fi
   fi
}

#####################################################
function libverdetect()
#####################################################
# OUTPUT: livers -- the libc version
{
   local tmplibdet
   local tmpprlibc
   #
   tmplibdet="`mktemp /tmp/mylibdetXXXXXX`"
   tmpprlibc="`mktemp /tmp/myprlibcXXXXXX`.c"
   debug2 "tmplibdet=$tmplibdet"
   debug2 "tmpprlibc=$tmpprlibc"
cat <<EOF >$tmpprlibc
#include<unistd.h>
#include <gnu/libc-version.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[])
{
    printf("GNU libc version: %s\n", gnu_get_libc_version());
    printf("GNU libc release: %s\n", gnu_get_libc_release());
    exit(0);
}
EOF
   LIBVERSION="---"
   cc -o $tmplibdet $tmpprlibc
   chmod +x $tmplibdet
   [ -s $tmplibdet ] && LIBVERSION=`$tmplibdet|awk '/^GNU libc version/{n=split($0,a); for(i=0;i<n;++i) if(match(a[i],"version:")) print a[i+1] }'|tr -d ','`
   rm -f $tmpprlibc $tmplibdet 2>/dev/null
}

ctrlccount="0"

#####################################################
function myexpend()
#####################################################
{
   local msg1
   local tots

   T00001=`date +%s%N | cut -b1-13`
#   if [ "$debug" != "0" ]; then echo "   T00001=$T00001"; fi
   exptimems=`expr $T00001 - $T00000`
   exptimehh=`echo ""|awk 'END{v=exptimems/3600000;printf("%7.2f",v)}' exptimems=$exptimems`

   #
   checkq def "$MYQWAITING"; sizeq=$QSIZE
   checkq def "$MYQRUNNING"; sizer=$QSIZE
   checkq def "$MYQFAILED"; sizef=$QSIZE
   checkq def "$MYQDONE"; sized=$QSIZE

   verbose1 "---------------------------------------------------------------"
#   verbose1 "   exptimems=$exptimems   exptimehh=$exptimehh   exp=$EXPCOUNT   simok=$SIMOKCOUNT  expfail=$SIMFAILCOUNT";
   verbose1 "   exptimems=$exptimems   exptimehh=$exptimehh   simok0=$SIMOKCOUNT  simfailed0=$SIMFAILCOUNT";
   #eventually print global statistics
   if [ ! -z "$globalt00000" ]; then
      globalt00001=`date +%s%N | cut -b1-13`
      globaldtms=`expr $globalt00001 - $globalt00000`
      globaldthh=`echo ""|awk 'END{v=globaldtms/3600000;printf("%7.2f",v)}' globaldtms=$globaldtms`
      glbstats=" - glbtimems=$globaldtms   glbtimehh=$globaldthh"
   fi
   verbose1 "   SIMOK=$sized of $TOTEXPECTEDSIMS (SIMFAIL=$sizef SIMRETRY=$SIMRETRY)$globstats";
   if [ "$BINMAKE" = "0" ]; then
      msg1="$msgEXP__END"
   else
      msg1="$msgBINGEN__END"
   fi
   comment ">>>>>>>>> $msg1 $EXP (DIR: $OUTDIR ) `date` $VERSION1"
   comment ""

   # try to backup results on sim server
   if [ "$BINMAKE" = "0" -a "$NOSIM" = "0" ] ; then
      if [ -d "$EXPBASE" ]; then
         if [ "$verbose" != "0" ]; then echo "* Trying to backup the experiments results into '$EXPBASE'..."; fi
         cp -af $OUTDIR $EXPBASE
         ec=$?
         if [ "$verbose" != "0" ]; then echo "  ...done with exitcode=$ec"; fi
      fi
   fi
   if [ "$BINMAKE" = "1" -a "$NOSIM" = "0" ] ; then
      # Update the binary files if requested in all necesary locations (to overwrite old copies)
      if [ "$nobin" = "0" -a "$BINFILEUPD" = "1" -a "$NORUN" = "0" ]; then
         copybin2exedir   #sets NOAPP2
      fi
   fi


   # If I am the sim master: save the DONE-Q and FAILED-Q too in the OUTDIR
   if [ "$IAMSIMMASTER" = "1" ] ; then
      # Save SIMDONE file in $OUTDIR
      QFILE=${QUEUEDIR}/${EXP}.$MYQDONE
      [ -f $QFILE ] && mv $QFILE SIMDONE || touch SIMDONE
#      savefile1 "SIMDONE"
      fnout="$EPREF.$MYQDONE"
      rm -f $OUTDIR/$fnout >/dev/null 2>/dev/null
      mv SIMDONE $OUTDIR/$fnout

      # Save SIMFAILED file in $OUTDIR
      QFILE=${QUEUEDIR}/${EXP}.$MYQFAILED
      [ -f $QFILE ] && mv $QFILE SIMFAILED || touch SIMFAILED
      fnout="$EPREF.$MYQFAILED"
#      savefile1 "SIMFAILED"
      rm -f $OUTDIR/$fnout >/dev/null 2>/dev/null
      mv SIMFAILED $OUTDIR/$fnout
   fi
   
   #
   if [ "$BINMAKE" = "1" ] ; then
      if [ "$sized" = "$TOTEXPECTEDSIMS" ]; then
         cecho "* You can now start the experiment by issuing:" "$green"
         cecho "  $0 $EXPTAG" "$green"
      else
         cecho "* Some binaries were not generated: please check!" "$red"
      fi
   fi

   # a final sanity cleanup
   cleanup

   verbose1 "--------------------------------------------------------------- END MYDSE"
   tots=`expr $exptimems / 1000`
   dse_close_str="Goodbye. `cecho -f -n "${DSE_THISTOOLCAP}" "$green"` $tots secs. SIMOK=$sized of $TOTEXPECTEDSIMS (SIMFAIL=$sizef SIMRETRY=$SIMRETRY)$globstats $EXPTAG `date +%y%m%d%H%M%S`"

   # AUTOGTCOLLECT
   if [ "$AUTOGTCOLLECT" = "1" -a "$sized" = "$TOTEXPECTEDSIMS" ]; then
      echo -e "$dse_close_str"
      #start automatically $MYCOLLECTSCRIPT+$MYGRAPHSCRIPT
      comment "Starting $MYCOLLECTSCRIPT to collect results in tables and $MYGRAPHSCRIPT to plot them"
      cd $CURPATH # to leave the output files in the initial dir
      $MYCOLLECTSCRIPT.sh $OPT_Q $EXPTAG --gtg --gth
   else
      cd && rm -f ${DSE_THISTOOL}.log
      echo "--> to collect the results of the experiment: $MYCOLLECTSCRIPT.sh $EXPTAG"
      echo -e "$dse_close_str"
   fi
#   if [ "$totret" = "0" ]; then
#      exitcode="0"
#   fi

   exit 0
}

#####################################################
function cleanup() {
#####################################################
   local lmode="$1"
   local lpid

   # only execute this in case of simulation not in case of compilation
   if [ "$BINMAKE" = "1" ] ; then return; fi

   # moving running/interrupted simulation identifier into failed queue (if any)
   checkq def $MYQRUNNING "6-"; rsize=$QSIZE
   running="0"
   if [ ! -z "$QOUT" ]; then
      for kk in $QOUT; do
         if [ "$MYMACH" = "$kk" ]; then running="1"; fi
      done
   fi
   if [ "$running" = "1" -a "$lmode" = "INTERRUPTED" ]; then
      # assume only one sim-identifiers running on this machine
#echo "extract"
      extractq def $MYQRUNNING ${MYMACH}-$MYPID "" "NOWARNING"
      # -->$FIRSTELEM ...
      EXTRACTQ_FAILED=$?
#echo "extract2"
      if [ ! -z "$FIRSTELEM" ]; then
         calculate_simcost $FIRSTELEM
#echo "mycost=$mycost"
#         insertq $MYQFAILED "$FIRSTELEM" "$mycost 0 0 0"
         insertq $MYQFAILED "$FIRSTELEM" "$RESTOFELEMS"
         if [ "$quiet" = "0" ]; then
            echo "* Inserted interrupted sim in ${QUEUEDIR}/${EXP}.$MYQFAILED"
         fi
      fi
   fi

   #
   PROCKILLED="0"

   # Focused killing (if possible)
   if [ "$costonpid" != "" ]; then
      verbose2 "* Trying to kill pending processes..."
      for lpid in $SFWPIDS; do
         prockill -$lpid
      done
      for lpid in $SAVEDKILLPIDS; do
         prockill -$lpid
      done 
   fi
   if [ "$SIMVID" != "" ]; then
      for lpid in $SIMVID; do # e.g., lpid=X50 
         rm -f /tmp/.X11-unix/X$lpid 2>/dev/null
         rm -f /tmp/.X${lpid}-lock 2>/dev/null
      done
   fi

   # Try to cleanup shared memory files belnging to dead pids
   debug1 "  - trying to cleanup shared memorydead pids... (SHMID=$RUNID)"
   SHMID="$RUNID"
   cleanupshm

   if [ "$nokill" = "1" ] ; then return; fi
   # less focused killing (not killed above?)
   prockill $SIMSMALL
   rm -f /tmp/.X11-unix/* 2>/dev/null
   rm -f /tmp/.X*lock 2>/dev/null

   # less focused killing
   prockill simnow
   prockill tsumon
   prockill Xvnc4
   prockill Xvnc
   prockill mediator

   if [ "$verbose" != "0" ]; then
      if [ "$PROCKILLED" = "0" ]; then verbose2 "   No pending process to kill."; else verbose2 "   Killed finished."; fi
   fi
#  rm -f /tmp/tempfile

   #cleanup timptime files
#   rm -f /tmp/time* 2>/dev/null

   #cleanup OUTFILE
   rm -f $OUTFILE 2>/dev/null

   return $PROCKILLED
}

#####################################################
function control_q() {
#####################################################
# run if user hits control-\
#####################################################
#   keypressed="1"
   keypressed="0"
   echo ""
   echo "--- CTRL-\ pressed: continuing simulation (pid=$mypid)"
   echo "--- ALIVE=$CURRENTALIVE/$CURRENTSIMS DETECTEDPIDS=$DETECTEDPIDS"
   # if I'm here I want to continue the simulation
   ctrlccount="0"
}

#####################################################
function single_process_kill() {
#####################################################
   local eck
   local pid
   pid="$1"
   kill -0 $pid 2>/dev/null >/dev/null
#   [ "$?" = "0" ] && disown $pid  2>&1 >/dev/null # remove $mypid from bash;s job control !!
   [ "$?" = "0" ] && disown $pid  &>/dev/null # remove $mypid from bash;s job control !!
   kill -9 $pid 2>/dev/null >/dev/null
   eck=$?
   KILLED="1"
   return $eck
}
#####################################################
function control_c() {
#####################################################
# run if user hits control-c
#####################################################
   debug1 ""
   debug1 "CTRL-C"
   keypressed="1"
   ctrlccount=`expr $ctrlccount + 1`
   if [ "$mypid" = "0" ]; then
      ctrlccount="3"
   fi
   debug1 "mypid=$mypid"
   sleep 1
trap control_c SIGINT
trap control_q SIGQUIT

   if [ "$ctrlccount" = "1" ]; then
      echo ""
      echo "--- CTRL-C pressed: hit once more to stop current simulation (pid=$mypid) or press CTRL-\ to continue."
      sleep 1
      return 0
   fi

   if [ "$ctrlccount" = "2" ]; then
      # if I'm here I want to kill the current simulation
      echo ""
      echo "--- DOUBLE CTRL-C pressed: killing process $mypid ..."
      single_process_kill $mypid
      eckill=$?
      echo "   - Process kill returned $eckill"
      echo "   - If you want to kill the whole DSE, press CTRL-C again, or CTRL-\ to go to next simulation."
      sleep 0.4
#      cleanup
#      echo ""
      return 0
   fi


   if [ "$ctrlccount" -ge "3" ]; then
      echo "--- DSE termination requested by user ---"
      cleanup INTERRUPTED
      exit $?
   fi
}

#####################################################
function checksimproc() {
#####################################################
# GLOBALS:
#    EXPUSER
#    SOMEPIDHASDIED
#    SFWPIDS
#####################################################
   # sending the signal 0 to a given PID just checks if any process with
   # the given PID is running and you have the permission 
   # to send a signal to it
   local procnames
#   local DETECTEDPIDS
   local detectedmypid
   local curdf # current disk free size (MB)
   local pid
   local retpid
   local diedpids
   local diedpidcount
   local expectedpids alivepidcount
   local timecounter
   local timeinterval
   local maxnumattempts
   local previousalivepids
   local somepidhasdied
   local timesomepidhasdied
   local parentpid
   local pidisstarted
   local nnn
   local nnnn
   local tosomepiddiedmaxw
   local tostarv2
   local tonoalive
   local numempty
   local timenumempty
   local numthreashold
   local myec
   local timeexeok
   local tc1
   local tne
   local tsp
   local myls
   local mysz
   local myszv
   local mylsv
   local mylsw
   local pidstatus
   local pidstatus1
   local pidstatus2
   local detailpids
   local simstarted
   local pidsup
   local nreflist
   local b
   local a
   local vid1
   local vidcount
#   local simscreenfiles
   local lastline
   local lastinfo
   local starvinfo
   local fcmdwatch
   local nochange
   local starvingcount
   local lasttime
   local lasttime1
   local lastprogress
   local lastprogress1
   local lastsynctimo1
   local starvingpcount
   local nopchange
   local starvtimeout
   local monitoringsfwpids
#   local checkingpids
   local timeoutcotsonpid
   local okterm
   local mypidstat
   local starvkill
   local starvreason=""
   local monitorjobs
   local monitorloopexitcode
   local lastfatal
   local nodcomplok dontwarnme
   declare -a nodeerrprinted
   declare -a nodecompleted

   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   local ncores=`echo "$cores"|sed 's/^0*//g'`
   for ((nnn=1;nnn<=nnodes;nnn++)); do nodeerrprinted[$nnn]="0"; done
   procnames="$1"
   expectedpids="$2"
   parentpid="$3"
   somepidhasdied="0"
   timesomepidhasdied="0"
   timecounter="0"
#   timeinterval="15"
#   timeinterval="10"
   timeinterval="5"
   timeinterval="0.5"
   timeinterval1="1"
   timeinterval2="5"
   timegrainthr="20"
   timegrainthr1="2"
   timegrainthr="30"
   timegrainthr="120"
   timeminwaitthr="15"
   timeminwaitthr="30"
   timeoutcotsonpid="30"
   [ -z "$EXTRACTEDCOST" ] && EXTRACTEDCOST="10" # Sanity check
   [ -z "${listsize[$appi]}" ] && myls="${listsize}" ||  myls="${listsize[$appi]}"
#echo "myls=$myls"
   IFS=' ' read -a mylsv <<< "$myls"
#echo "mylsv[0]=${mylsv[0]}"
   IFS='+' read -a mylsw <<< "${mylsv[0]}"
   local mylswfirst="${mylsw[0]}"
   # Sanity check:
   [ "$mylswfirst" = "0" ] && mylswfirst="1" # avoids to have mylswfirst==0
#echo "mylsw[0]=${mylsw[0]}"
   IFS='+' read -a myszv <<< "${size}"
   mysz="${myszv[0]}"
#echo "mysz=$mysz"
   tosomepiddiedmaxw="50"
   tosomepiddiedmaxw="75"
   tosomepiddiedmaxw=`expr 50 + 5 \* $expectedpids`
#   minwait=`expr 5 + 2 \* $expectedpids`
   minwait=`expr $timeminwaitthr + 3 \* $expectedpids`
   [ "$GOSLOW" = "0" ] && minwait="0"
   timeexeok="0"
#   maxnumattempts="20"
#   maxnumattempts="20"
#   maxnumattempts="10"
#   maxnumattempts="5"
#   maxnumattempts="7"
#   maxnumattempts=`expr $tosomepiddiedmaxw / $timeinterval`
   previousalivepids="$expectedpids"
   pidisstarted="0"
   starvingcount="0"
   starvinfo=""
   simstarted="0"
   lasttime=""
   lasttime1=""
   lastprogress=""
   lastprogress1=""
   lastsynctimo1=""
   starvingpcount="0"
   nopchange="0"
   pidstatus=""
   pidstatus1=""
   pidstatus2=""
   lastline=""
   lastinfo=""
   starvtimeout=`expr $EXTRACTEDCOST \* 100`
#   tonoalive=`expr 5 + $expectedpids`
#   tonoalive=`expr 30 + \( 5 + $ncores / 5 \) \* $expectedpids`
#   tonoalive=`expr 30 + \( 8 + $ncores / 1 \) \* $expectedpids`
#   tonoalive=`expr 30 + $EXTRACTEDCOST + \( 8 + $ncores / 1 \) \* $expectedpids + $mysz / $mylswfirst \* 50`
#   tonoalive=`expr 30 + \( 8 + $ncores / 1 \) \* $expectedpids + $mysz / $mylswfirst \* 50`
#   tonoalive=`expr \( 30 + \( 8 + $ncores / 1 \) \* $expectedpids + $mysz / $mylswfirst \* 50 \) \* $nnodes`
#   numthreashold=`expr $tonoalive / $timeinterval`

   # starvptimout is the timout when the simulation process is starving 
   # and printing "No progress"...
#   tostarv2="900" #900 seconds - fixed period
#   tostarv2="600" #600 seconds - fixed period
#   tostarv2="300" #600 seconds - fixed period
   if [ "$GLOBFAILCNT" -le "4" ]; then \
      tostarv2=`expr 300 \* \( $GLOBFAILCNT + 1 \)`; \
      tospdmaxw=`expr $tosomepiddiedmaxw \* \( $GLOBFAILCNT + 1 \)`; \
      tonoalive=`expr \( 500 + \( 8 + $ncores / 1 \) \* $expectedpids + $mysz / $mylswfirst \* 50 \) \* $nnodes \* \( $GLOBFAILCNT + 1 \)`
   else
      tostarv2=`expr 300 \* 4`; \
      tospdmaxw=`expr $tosomepiddiedmaxw \* 4`;
      tonoalive=`expr \( 500 + \( 8 + $ncores / 1 \) \* $expectedpids + $mysz / $mylswfirst \* 50 \) \* $nnodes \* 4`
   fi

   starvkill="0"
   waitingstr="\r   Waiting to monitor $SIMSMALL "
   [ "$BINMAKE" = "0" ] && monitoringsfwpids="1" || monitoringsfwpids="0"

   debug2 "monitoringsfwpids=$monitoringsfwpids"
   debug2 "procnames=$procnames   expectedpids=$expectedpids  user=$DEFUSER"
   verbose1 "   tona=$tonoalive   minw=$minwait   tospdmaxw=$tospdmaxw   cost=$EXTRACTEDCOST   stim=$starvtimeout  tostarv2=$tostarv2"

   #disable SIGINT propagation to PPID by using different process groups
   set +m

   nlist=""; nlist2=""
   detpidsprev=""
   CURRENTSIMS="$expectedpids"
   numempty="0"
   timenumempty="0"

#      buildfn1 SIMSCREEN 1; fn1="$fninn"
#      echo "$fn1"
#   simscreenfiles=""

#   # Initial choice for files to monitor in order to examine the Job status
   sleep 1
#   buildfn1 "JOBWATCH" 1; fn="$fninn"
#   verbose2 "   JOBWATCH[1]=$fn"
#   buildfn1 SIMSWATCH 1; fn="$fninn"
#   verbose2 "   SIMSWATCH[1]=$fn"
#   if [ -s $fn ]; then fcmdwatch="SIMSWATCH"; simdirs="1"; else fcmdwatch="JOBWATCH"; fi
#   debug2 "fn=$fn  fcmdwatch=$fcmdwatch"
#   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
#   for ((nnn=1;nnn<=nnodes;nnn++)); do
##      buildfn1 SIMSCREEN $nnn; fn="$fninn"
#      buildfn1 $fcmdwatch $nnn; fn="$fninn"
##      simscreenfiles="$simscreenfiles $fn"
#      verbose2 "   NODE${nnn}: watching $fn"
#   done
#   #echo "$simscreenfiles"

   # generate reference node string
   for ((nnn=1;nnn<=nnodes;nnn++)); do
      [ "$nreflist" = "" ] && nreflist="$nnn" || nreflist="$nreflist $nnn"
   done
#echo "nreflist='$nreflist'"

   #
   SAVEDKILLOK="0"
   monitorjobs="1"
   monitorloopexitcode="0"
   dontwarnme="0"
   debug1 "--- ENTERING JOB MONITORING LOOP"
   #
   #--------------------------------------------START LOOP
   while [ "$monitorjobs" = "1" ]; do

      # check disk space
      #curdf=`df -Pm . | tail -1 | awk '{print $4}'`
      curdf=`df -Pm $PWD | awk 'NR==2{print $4}'`
      [ "$curdf" -lt "$CRITICALDF" ] && cecho "WARNING: ONLY $curdf MB of disk free !!" "$yellow"

      #
      okterm="0"

      # CHECKING IF SFW IS UP AND RUNNING:
      if [ "$monitoringsfwpids" = "1" ]; then
#         if [ "$quiet" = "0" -a "$verbose" -gt "0" ]; then
         if [ "$quiet" = "0" ]; then echo -ne "$waitingstr"; waitingstr="${waitingstr}."; fi
         detectsfwpids
         if [ "$SFWOKPIDS" != "" ]; then
#ps axu |grep cotson
#            if [ "$quiet" = "0" -a "$verbose" -gt "0" ]; then
#            monitoringsfwpids="0"
#            comment " (simpid='$SFWPIDS')"
#            sleep 3 # to wait the sandboxes to come up
            # wait for sandboxes to come up
            debug3 "checking DB pids"
            sdbw="0"
            for ((nnn=1;nnn<=nnodes;nnn++)); do
               buildfn1 SIMSTXTDB $nnn; fn=$fninn
               [ -s $fn ] && sdbw=`expr $sdbw + 1`
#               [ -s $fn ] && echo "fn=$fn"
            done
            if [ "$sdbw" = "$nnodes" ]; then
               monitoringsfwpids="0"
               comment " (SFWPIDS='$SFWPIDS')"
            fi
         fi
         if [ "$timecounter" -ge "$timeoutcotsonpid" ]; then
            echo ""
            cecho "ERROR: Couldn't detect $SIMSMALL pid (timeoutcotsonpid=${timeoutcotsonpid}s exceeded)" "$red"
            single_process_kill $mypid
            monitorloopexitcode="1"; monitorjobs="0"
            return 1
         fi
      fi

      debug3 "------------- (sleep $timeinterval)"
      #################################
      sleep $timeinterval
      #################################

      [ "$timecounter" = "0" ] && timeinterval="1"
      timecounter=`expr $timecounter + $timeinterval`
      [ "$timecounter" -ge "$timegrainthr1" ] && timeinterval="$timeinterval1"
      [ "$timecounter" -ge "$timegrainthr" ] && timeinterval="$timeinterval2"
      diedpids=""; alivepidcount="0"
      myec="0"
      if [ ! -z "$mypid" ]; then
         kill -0 $mypid 2>/dev/null > /dev/null
         myec=$?
      fi
      detpidsprev="$DETECTEDPIDS"
#      DETECTEDPIDS=`ps axuc |grep $procnames|egrep "^$DEFUSER "|awk '{printf("%d ", $2)}'`
#      detectedmypid=`ps axuc |grep $mypid|egrep "^$DEFUSER "|awk '{printf("%d ", $2)}'`
      ################
      debug3 "monitoringsfwpids=$monitoringsfwpids   CHECKINGPIDS=$CHECKINGPIDS SFWOKPIDS=$SFWOKPIDS"
#      [ "$monitoringsfwpids" = "1" ] && detectsandboxpids
SIMINITPIDCHECKTIMOUT="9"
SIMINITPIDCHECKTIMOUT="15"
      if [ "$timecounter" -ge "$SIMINITPIDCHECKTIMOUT" -o "$SFWOKPIDS" != "" ]; then
         if [ "$CHECKINGPIDS" = "1" -a "$SFWOKPIDS" != "" ]; then
            detectsandboxpids; # Sets: DETECTEDPIDS, killpds, SIMSNPID, SIMMDPID, SIMVNPID, SIMSCPID, PIDTONID
         fi
      fi
      debug3 "checksimproc: DETECTEDPIDS=$DETECTEDPIDS  detectedokpids=$detectedokpids"
#      if [ "$detectedokpids"="" ]; then
#         echo ""
#         cecho "ERROR: Couldn't detect sandbox pids." "$red"
#         single_process_kill $mypid
#         monitorloopexitcode="1"; monitorjobs="0"
#         return 1
#      fi
      # Sets DETECTEDPIDS killpids detectedokpids
      if [ "$SAVEDKILLOK" = "0" -a "$detectedokpids"="ok" ]; then
          SAVEDKILLPIDS="$killpids"
          SAVEDKILLOK="1"
      fi

###############################
#echo "`pwd`"
#fnfn="${RUNID}-${SFWPIDS}-node-"
#fnfn1="${fnfn}1"
#if [ -d "$fnfn1" ]; then
#  dndn="${fnfn1}/data/screen.log"
#else
#  dndn=""
#fi
#if [ "$simdris" = "0" ]; then
      buildfn1 SIMSWATCH 1; fn="$fninn"
      debug3 "SIMSWATCH: fn=$fn"
      [ -s $fn ] && debug3 "`ls -l $fn`"
      if [ -s $fn ]; then fcmdwatch="SIMSWATCH"; simdirs="1"; else fcmdwatch="JOBWATCH"; fi
#fi
#echo "fn=$fn"
##################################

      #check if simulation nodes have started
      nlistprev="$nlist"
      debug3 "simstarted=$simstarted"
      if [ "$simstarted" = "0" ]; then
            nlist=""

            nnodes=`echo "$nodes"|sed 's/^0*//g'`
            for ((nnn=1;nnn<=nnodes;nnn++)); do
      #         buildfn1 SIMSCREEN $nnn; fn="$fninn"
               buildfn1 $fcmdwatch $nnn; fn="$fninn"
      #if [ "$dndn" != "" ]; then fn="$dndn"; fi
               [ -s $fn ] && a=`grep "$SIMSTARTED" $fn` || a=""
      #         [ -z "$a" ] || nlist="$nlist $nnn"
               b=`echo "$nlist"|grep $nnn`
               if [ "$b" = "" -a "$a" != "" ]; then [ "$nlist" = "" ] && nlist="$nnn" || nlist="$nlist $nnn"; fi
               nlist=`for i in $nlist; do echo $i; done |sort -n|tr '\n' ' '|sed 's/ *$//g'`
            done
      fi
      debug4 "nlist='$nlist'"
#         if [ ! -z "$a" ]; then simstarted="1"; nlist="$nlist $nnn"; fi
#         if [ "$nlist" = "$nreflist" ]; then simstarted="1"; nlist="$nlist $nnn"; fi
      if [ "$nlist" = "$nreflist" ]; then simstarted="1"; fi
      debug4 "simstarted='$simstarted'"
      vid1=( $SIMVID )
      vidcount=${#vid1[@]}
      if [ "$vidcount" != "$nnn" -a "$simstarted" = "1" ]; then
#            nlist="$nlist $nnn"
         cpid=`pstree -pl $mypid 2>/dev/null|grep $SIMSMALL|sed -r 's/.*cotson\(([0-9]+)\).*/\1/'|tr '\n' ' '| sed -e 's/[[:space:]]*$//'`
         spid=`pstree -pl $cpid 2>/dev/null|grep "$simbinname("|sed -r "s/.*$simbinname\(([0-9]+)\).*/\1/"|tr '\n' ' '| sed -e 's/[[:space:]]*$//'`
#            SIMVID=`ls -ltr /tmp/.X11-unix|grep $DEFUSER|awk '{print $9}'|tr '\n' ' '| sed -e 's/[[:space:]]*$//'`
#            SIMVID=`find /tmp/.X11-unix -user $DEFUSER -cmin -3 -printf "%f\n"|tr '\n' ' '| sed -e 's/[[:space:]]*$//'`
         detectvids
         SIMVID="$detectedvids"
         if [ "$SIMVID" != "" ]; then
            tc1=`echo ""|awk '{printf("%6s",v1)}' v1="$timecounter" -`
            pidstatus2="mypid=$mypid cpid='$cpid' spid='$spid' SIMVID='$SIMVID'"
#               if [ "$pidstatus2" != "$pidstatus1" ] ; then pidstatus1="$pidstatus2"; else pidstatus1=""; fi
#               if [ "$pidstatus1" != "" ]; then pidstatus="$tc1 -- $pidstatus1"; else pidstatus=""; fi
            if [ "$pidstatus2" != "$pidstatus1" ] ; then
               if [ "$cpid" != "" -o "$spid" != "" ]; then
                  pidstatus1="$pidstatus2"; pidstatus="$tc1 -- $pidstatus1";
               else
                  pidstatus="";
               fi
            else
               pidstatus="";
            fi
         fi
      else
         pidstatus=""
      fi

      # check if simulation nodes have completed
      nlist2prev="$nlist2"
#      nlist2=""
      nnodes=`echo "$nodes"|sed 's/^0*//g'`
      for ((nnn=1;nnn<=nnodes;nnn++)); do nodecompleted[$nnn]=""; done
      nodcomplok="0"
      for ((nnn=1;nnn<=nnodes;nnn++)); do
#         buildfn1 SIMSCREEN $nnn
         buildfn1 $fcmdwatch $nnn; fn="$fninn"
#if [ "$dndn" != "" ]; then fn="$dndn"; fi
         [ -s $fn ] && a=`grep "$SIMCOMPLETED" $fn` || a=""
         [ -s $fn ] && nodecompleted[$nnn]=`grep "$NODCOMPLETED" $fn`
#         [ -z "${nodecompleted[$nnn]}" ] && nodcomplok=`expr $nodcomplok + 1`
         [ -z "${nodecompleted[$nnn]}" ] || nodcomplok=`expr $nodcomplok + 1`
#         [ -z "$a" ] || nlist2="$nlist2 $nnn"
         if [ "$a" != "" ]; then
            b=`echo "$nlist2"|grep "$nnn"`
            if [ "$b" = "" ]; then
               [ "$nlist2" = "" ] && nlist2="$nnn" || nlist2="$nlist2 $nnn"
            fi
            nlist2=`for i in $nlist2; do echo $i; done |sort -n|tr '\n' ' '|sed 's/ *$//g'`
         fi
#         if [ "$a" != "" ]; then [ "$nlist2" = "" ] && nlist2="$nnn" || nlist2="$nlist $nnn"; fi
      done

      newdetectedpids=""
      debug4 "DETECTEDPIDS=$DETECTEDPIDS"
      if [ ! -z "$DETECTEDPIDS" ]; then
         pidisstarted="1"
         for pid in $DETECTEDPIDS; do
            kill -0 $pid 2>/dev/null >/dev/null
            retpid=$?
            if [ "$retpid" != "0" -a -z "${nodecompleted[${PIDTONID[$pid]}]}" ]; then
               if [ "$quiet" = "0" ]; then
                  echo ""
                  cecho "WARNING(checksimproc): '$procnames' process (pid=$pid) has just died (`date +%y%m%d%H%M`)" "$yellow"
               fi
               previousalivepids=`expr $previousalivepids - 1`
               [ -z "$diedpids" ] && diedpids="$pid" || diedpids="$diedpids $pid"
            else
               alivepidcount=`expr $alivepidcount + 1`
               [ "$newdetectedpids" = "" ] && newdetectedpids="$pid" || newdetectedpids="$newdetectedpids $pid"
            fi
         done
      fi
      DETECTEDPIDS="$newdetectedpids"
      CURRENTALIVE="$alivepidcount"
      debug3 "checksimproc: CURRENTALIVE=$CURRENTALIVE  DETECTEDPIDS=$DETECTEDPIDS"
 
      #
      nochange="0"
      if [ "$nlist" = "$nlistprev" -a "$nlist2" = "$nlist2prev" -a "$DETECTEDPIDS" = "$detpidsprev" ]; then
         nochange="1"
      fi

      # extract lastline of SIMSCREEN of NODE 1
#      buildfn1 SIMSCREEN 1; fn1="$fninn"
      buildfn1 $fcmdwatch 1; fn1="$fninn"
#if [ "$dndn" != "" ]; then fn1="$dndn"; fi
      lastfatal=""
      if [ -s "$fn1" ]; then #SIMSCREEN-1 START
         lastbuf10=`tail -n10 $fn1`
#         lastline=`tail -1 $fn1|tr -d '\n\r'`
         lastline=`echo "$lastbuf10"|tail -1|tr -d '\n\r'`

         # check TIME in lastline
         lasttime=`echo "$lastline"|awk '/TIME=/{for(f=1;f<=NF;++f){if(match($f,/TIME=/)){split($f,a,/=/);res=a[2]; print res;}}}'`
         if [ ! -z "$lasttime" ]; then
            if [ "$lasttime1" != "" -a "$lasttime1" = "$lasttime" ]; then 
               starvingcount=`expr $starvingcount + $timeinterval`
            fi
            if [ "$lasttime1" != "" -a "$lasttime1" != "$lasttime" ]; then 
               starvingcount="0"
            fi
            # force a new line the first time that TIME= is appearing
            if [ "$lasttime1" = "" ]; then nochange="0"; fi

            lasttime1="$lasttime"
         else
            starvingcount="0"
         fi

         # Check for 'no progress'
         lastprogress=`echo "$lastline"|egrep "Warning: No progress now=.* gt=.*"`
#         lastsynctimo=`echo "$lastline"|egrep -i "(Warning: No progress now=.* gt=.*|Warning: Sync timeout now=.* gt=.*|fixit|exception|error)"`
         # filter out "Session" messages
         lastsynctimo=`echo "$lastline"|egrep -i "(Warning: No progress now=.* gt=.*|Warning: Sync timeout now=.* gt=.*|fixit|exception|error)"|grep -v "no-error"|grep -P '^(?:(?!Session).)*$'`
#         lastfatal=`echo "$lastbuf10"|egrep -i "(fixit|exception|error)"|tr -d '\n\r'`
         lastfatal=`echo "$lastbuf10"|egrep -i "(fixit|exception|error )"|grep -v "no-error"|grep -P '^(?:(?!Session).)*$'|tr -d '\n\r'`
         if [ ! -z "$lastprogress" -o ! -z "$lastsynctimo" -o ! -z "$lastfatal" ]; then

            # Check for an error message
            nnodes=`echo "$nodes"|sed 's/^0*//g'`
            for ((nnn=1;nnn<=nnodes;nnn++)); do
                if [ "${nodeerrprinted[$nnn]}" != "1" ]; then
#                   buildfn1 SIMSCREEN $nnn; fn="$fninn"
                   buildfn1 $fcmdwatch $nnn; fn="$fninn"
                   if [ -s "$fn" ]; then #SIMSCREEN-1 START
#                   buferr=`egrep -hi -A3 -B4 "(fixit|error|exception)" $fn`
#                   buferr=`tail -n100 $fn|egrep -hi -A3 -B6 "(Warning: No progress now=.* gt=.*|Warning: Sync timeout now=.* gt=.*|fixit|error|exception)"`
         # filter out "Session" messages
#ERRMSGBEFOR="6"
#ERRMSGAFTER="3"
ERRMSGBEFOR="10"
ERRMSGAFTER="5"
                      buferr=`tail -n100 $fn|egrep -hi -A$ERRMSGAFTER -B$ERRMSGBEFOR "(Warning: No progress now=.* gt=.*|Warning: Sync timeout now=.* gt=.*|fixit|error|exception)"|grep -P '^(?:(?!Session).)*$'`
                      if [ "$buferr" != "" ]; then
                         buflaberr=`echo "$buferr"|sed "s/^\(.*\)/N$nnn:\1/"`
                         echo ""; cecho "$buflaberr" "$red"
                         nodeerrprinted[$nnn]="1"

                         # Dump /proc/n/maps
                         myfnout="$HOMEBASE/N$nnn-maps-${SIMSNPID[$nnn]}"
                         #cecho "Dumping /proc/${SIMSNPID[$nnn]}/maps into $myfnout" "$yellow"
                         #cat /proc/${SIMSNPID[$nnn]}/maps >$myfnout
                         # Check for a "fixit" location
                         addr=`echo "$buferr"|awk '/FixIt/{print $6}'`
                         if [ ! -z "$addr" ]; then
                         #    mymap=`cat /proc/${SIMSNPID[$nnn]}/maps| awk -F- '{a=$0}{if("0x"$1 > "0x"addr && !done){ print a; done=1} }' addr=$addr`
                            mymap=`cat /proc/${SIMSNPID[$nnn]}/maps| awk -F- --non-decimal-data '{b=a;a=$0;h2=h1;h11="0x"addr;h10="0x"$1;h1=h11-h10;if(h1<0 && !done){ printf("%x %s\n", h2, b); done=1} }' addr=$addr`
                            mymfile=`echo "$mymap"|awk '{print $7}'`
                            mymaddr=`echo "$mymap"|awk '{print $1}'`
                            mydebug=`addr2line -e $mymfile 0x$mymaddr`
                            cecho "$mymap" "$yellow"
                            cecho "$mydebug" "$yellow"
                         fi
                      fi
                   fi
                fi
            done

            if [ "$lastprogress1" != "" -a "$lastprogress1" = "$lastprogress" ]; then
               starvingpcount=`expr $starvingpcount + $timeinterval`
            fi
            if [ "$lastprogress1" != "" -a "$lastprogress1" != "$lastprogress" ]; then
               starvingpcount="0"
            fi
            if [ "$lastsynctimo1" != "" -a "$lastsynctimo1" = "$lastsynctimo" ]; then
               starvingpcount=`expr $starvingpcount + $timeinterval`
            fi
            if [ "$lastsynctimo1" != "" -a "$lastsynctimo1" != "$lastsynctimo" ]; then
               starvingpcount="0"
            fi
            # force a new line the first time that TIME= is appearing
            if [ "$lastprogress1" = "" ]; then nopchange="0"; fi

            lastprogress1="$lastprogress"
            lastsynctimo1="$lastsynctimo"
         fi


         
#         llleng=${#lastline}
#         if [ "$llleng" -le "80" ]; then
#            lpadding=`printf ' %.0s' {1..80}`
#            lleng=`expr 80 - ${#lastline}`
#            lpadding="${lpadding:0:$lleng}"
#            lastline="$lastline$padding"
#         fi
      fi # SIMSCREEN-1 END

      # Check Starving Timeout
      if [ "$starvingcount" -ge "$starvtimeout" ]; then
         starvkill="1"; starvreason="Reached timeout starvtimeout=$starvtimeout"
      fi
      if [ "$starvingpcount" -ge "$tostarv2" ]; then
         starvkill="1"; starvreason="Reached timeout tostarv2=$tostarv2"
      fi
      if [ ! -z "$lastfatal" ]; then
         starvkill="1"; starvreason="FATAL ERROR! (${lastfatal//\\r})"
      fi

      #
      if [ "$quiet" = "0" -a "$monitoringsfwpids" = "0"  ]; then
         tc1=`echo ""|awk '{printf("%6s",v1)}' v1="$timecounter" -`
         tne=`echo ""|awk '{printf("%2s",v1)}' v1="$timenumempty" -| sed -e 's/^[[:space:]]*//'`
         tsp=`echo ""|awk '{printf("%2s",v1)}' v1="$timesomepidhasdied" -`
         if [ "$keypressed" = "0" ]; then
            if [ "$verbose" -gt "0" ]; then
               detailpids=" ($tne $tsp) -- $DETECTEDPIDS"
            else
               detailpids=""
            fi
            if [ "$simstarted" = "1" ]; then
               if [ $alivepidcount -lt $expectedpids ]; then
                  pidsup=" -- SOME_NODE_DOWN(UP=$alivepidcount/$expectedpids) ec=$myec"
               else
                  pidsup=" -- ALL_NODES_UP($alivepidcount/$expectedpids)  "
#if [ "$debug" -gt "0" ]; then
#if [ "$firsttime" = "" ]; then
#ls -ltr
#ls -ltr $RUNID-*
#ls -ltr $RUNID-*/data
#echo "---- LOG_TSU4 -------"
#ls -ltr LOG_TSU4
#echo "---- LOG_TSU4/report -------"
#ls -ltr LOG_TSU4/report
#firsttime="1"
#fi
#fi
               fi
               if [ $alivepidcount = "0" ]; then
                  pidsup=" -- ALL_NODES_DOWN(UP=$alivepidcount/$expectedpids) ec=$myec"
               fi
            else
               if [ "$nlist" = "" ]; then
                  pidsup=" -- $CPSTARTING(UP=$alivepidcount/$expectedpids) "
               else
                  pidsup=" -- $CPSTARTED(UP=$alivepidcount/$expectedpids): $nlist"
               fi
            fi
#echo " nlist='$nlist'"
            if [ "$nlist2" = "" ]; then
               pidsend=""
            else
#               pidsend=" -- $nlist2"
               pidsend=""
               if [ "$nlist2" = "$nreflist"  ]; then
                  pidsup=" FINISHED_ALL_NODES"
                  dontwarnme="1"
                  nnodes=`echo="$nodes"|sed 's/^0*//g'`
                  [ "$nodcomplok" = "$nnodes" ] || okterm="1"
               else
                  pidsup=" -- FINISHED_NODES={ $nlist2 }"
               fi
            fi 
            # starvingcount information
	    if [ "$starvingcount" = "0" ]; then
               starvinfo=""
            else
               starvinfo=" -- $starvingcount"
               if [ "$starvkill" = "1" ]; then
                  starvinfo="$starvinfo -- TIME TO KILL SIMULATION"
               fi
            fi
            # starvingpcount information
	    if [ "$starvingpcount" = "0" ]; then
               starvpinfo=""
            else
               starvpinfo="($starvingpcount)"
#               if [ "$starvkill" = "1" ]; then
                  starvinfo="$starvinfo $starvpinfo"
#               fi
            fi
            #
            if [ "$lastline" = "" ]; then 
               lastinfo=""
            else
               lastinfo=" -- $lastline"
            fi
#            outline1="$tc1 -- $myec ($alivepidcount/$expectedpids) $tne $tsp$detailpids$pidsup$pidsend -- $starvingcount -- $lastline"
            outline1="$tc1$detailpids$pidsup$pidsend$starvinfo$lastinfo"
            tcols=`tput cols` 
            llpadding=""; for((k=0;k<$tcols;++k)); do llpadding="$llpadding "; done
#            llpadding=`printf ' %.0s' {1..$tcols}`
#echo "llp=${#llpadding}"
            llleng="${#outline1}"
            if [ "$llleng" -gt "$tcols" ]; then
               tcut=`expr $tcols - 4`; outline2="${outline1:0:$tcut} ..."; outline3="$outline2"
            else
               outline2="$outline1$llpadding"; outline3="${outline2:0:$tcols}";
            fi
            
            echo -n "$outline3"
#echo "${#outline3}"
#            echo -n "$tc1 -- $myec ($alivepidcount/$expectedpids) $tne $tsp -- $DETECTEDPIDS -- $nlist -- $nlist2 ---- $lastline"
#           if [ "$nlist" = "$nlistprev" -a "$nlist2" = "$nlist2prev" ]; then
#            if [ "$nlist" = "$nlistprev" -a "$nlist2" = "$nlist2prev" -a "$DETECTEDPIDS" = "$detpidsprev" ]; then
            if [ "$nochange" = "1" ]; then
#           printf "\r" # echo -n "\r" # doesn't work
#               echo -ne "$llpadding\r" # doesn' work
               echo -ne "\r"
            else
               verbose1 ""
               [ ! -z "$pidstatus" ] && echo "$pidstatus"
            fi
         fi
      fi

      if [ "$alivepidcount" -lt "$expectedpids" ]; then
         somepidhasdied=`expr $somepidhasdied + 1`
         timesomepidhasdied=`expr $timesomepidhasdied + $timeinterval`
      fi
      if [ "$nochange" = "0" -a "$DETECTEDPIDS" != "" ]; then # some pid is still around
          timesomepidhasdied=0;
      fi
#       if [ "$somepidhasdied" -gt "$maxnumattempts" ]; then
#      if [ "$timesomepidhasdied" -ge "$tosomepiddiedmaxw" ]; then
      debug4 "starvkill=$starvkill"
      if [ "$starvkill" = "1" ] || [ "$timesomepidhasdied" -ge "$tosomepiddiedmaxw" -a "$alivepidcount" -gt "1" ]; then
         comment ""
         comment "WARNING(checksimproc): $starvreason"
         comment "WARNING(checksimproc): timeout: killing process parentpid=$parentpid ..."
         single_process_kill $parentpid
         if [ "$keypressed" = "0" ]; then [ "$quiet" = "0" ] && echo ""; fi
         comment "WARNING(checksimproc): mypid=$parentpid has been killed since it was apparently starving!"
         monitorloopexitcode="1"; monitorjobs="0"
      else
#       [ "$alivepidcount" = "0" ] && numempty=`expr $numempty + 1` || numempty="0"
#      [ "$alivepidcount" = "0" ] && timenumempty=`expr $timenumempty + $timeinterval` || timenumempty="0"
         [ "$alivepidcount" = "0" -o \( "$alivepidcount" = "1" -a "$expectedpids" -gt "1" \) ] && timenumempty=`expr $timenumempty + $timeinterval` || timenumempty="0"
#      if [ "$alivepidcount" = "0" -a "$timecounter" -gt "$tonoalive" ]; then echo ""; return 0; fi
#      if [ "$alivepidcount" = "0" -a "$timenumempty" -gt "$tonoalive" ]; then
         if [ \( "$alivepidcount" = "0" -o "$alivepidcount" = "1" -a "$expectedpids" -gt "1" \) -a "$timenumempty" -ge "$tonoalive" ]; then
            if [ "$keypressed" = "0" ]; then [ "$quiet" = "0" ] && echo ""; fi
            cecho "WARNING(checksimproc): timeout reached (tona=$tonoalive, alive=$alivepidcount/$expectedpids)" "$yellow"
            debug1 "alivepidcount=$alivepidcount  timenumempty=$timenumempty  okterm=$okterm"
               mypidstat=`kill -0 $mypid 2>&1 >/dev/null; echo $?`
            debug1 "mypidstat($mypid)=$mypidstat"
            if [ "$mypidstat" = "0" ]; then
               comment "WARNING(checksimproc): timeout: need to kill Job process $mypid ..."
               single_process_kill $mypid
            fi
            monitorloopexitcode="0"; monitorjobs="0"
         else
            if [ "$okterm" = "1" ]; then
               if [ "$keypressed" = "0" ]; then [ "$quiet" = "0" ] && echo ""; fi
               verbose1 "   Reached condition for concluding (okterm=$okterm)"
               monitorloopexitcode="0"; monitorjobs="0"
            else
               if [ "$myec" = "1" ]; then
                  if [ "$timeexeok" -ge "$minwait" ]; then
                     if [ "$keypressed" = "0" ]; then [ "$quiet" = "0" ] && echo ""; fi
                     monitorloopexitcode="0"; monitorjobs="0"
                  else
                     timeexeok=`expr $timeexeok + $timeinterval`
                  fi
               fi
            fi
         fi
      fi
   done
   #--------------------------------------------END LOOP
   debug1 "--- EXITING JOB MONITORING LOOP"
   return $monitorloopexitcode
}

#####################################################
function checkdpid {
#####################################################
#  GLOBAL listdokpid
#  GLOBAL listdpid
#  GLOBAL nodes
#  OUTPUT listpid
#  OUTPUT listokpid
   local nco
   local nnn
   local outpid="$1"
   local nlast="$2"
   local parsefn="$3"
   local dbkey="$4"
   local process="$5"
   local awktest="$6"
   local awkparam="$7"
   local caller="$8"
   local fn
   local a
   local ego
   local out=""
   local awkassign
   local param
   local espre
   local value
   listdpid=""
   listdokpid=""
   nco="0"
   [ -z "$awkparam" ] || out=`$parsefn|grep $DEFUSER 2>/dev/null`
   for ((nnn=1;nnn<=nlast;nnn++)); do
      if [ -z "$awkparam" -a ! -z "$parsefn" ] ; then
         buildfn1 $parsefn $nnn; fn="$fninn"
         debug3 "checkdpid: parsefn=$parsefn  fn=$fn"
#ls -l $fn
#cat $fn
         if [ ! -s "$fn" ]; then
            cecho "WARNING(${caller}::checkdpid): '$fn' not found." "$yellow"
            out=""
         else
            out=`cat $fn`
         fi
      fi
      if [ ! -z "$awkparam" ]; then
         param=${awkparam%%=*}
         espre=${awkparam##*=}
         eval value=$espre
         awkassign="$param=$value"
         [ -z "$value" ] && out=""
      fi
#      debug4 "out='$out'"

      ego=`echo "$out"|egrep "$dbkey"| awk "$awktest" $awkassign|tr -d \"\'`
      debug4 "ego='$ego'"
#      if ! [[ "$ego" =~ ^[0-9]+$ ]] ; then listdpid=""; return; fi
#      if ! [[ "$ego" =~ ^[0-9]+$ ]] ; then break; fi
      if ! [[ "$ego" =~ ^[0-9]+$ ]] ; then continue; fi
      a=""; if [ "$ego" != "" ]; then a=`echo "$listdpid"|grep "$ego"`; fi
      if [ -z "$a" ]; then
         nco=`expr $nco + 1`
         [ -z "$listdpid" ] && listdpid="$ego" || listdpid="$listdpid $ego"
         eval $outpid[$nnn]="$ego"
         debug4 "SIMSCPID[$nnn]=${SIMSCPID[$nnn]}"
      fi
   done
   if [ "$nco" = "$nlast" ]; then listdokpid="ok"; fi
   debug4 "nlast=$nlast nco=$nco listdokpid=$listdokpid"
   if [ -z "$listdpid" -a "$dontwarnme" = "1" ]; then cecho "WARNING: no detected pids" "$yellow"; fi
}

#####################################################
function detectvids {
#####################################################
   detectedvids=""
   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   checkdpid SIMVIDS "$nnodes" "SIMSTXTDB" "display:" "vnc" '{print $2}' "" "detectevids"
   detectedvids="$listdpid"
   debug3 "detectedvids=$detectedvids"
}

#####################################################
function detectsandboxpids {
#####################################################
   debug3 "--- ENTERING detectsandboxpids"
   killpids=""
   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   checkdpid SIMSCPID "$nnodes" "SIMSTXTDB" "screen_pid" "screen" '{print $2}' "" "detectsandboxpids"
   debug3 "SIMSCPID "; for ((nnn=1;nnn<=nnodes;nnn++)); do debug3 "${SIMSCPID[$nnn]} "; done; debug3 ""
   killpids="$listdpid"
   checkdpid SIMVNPID "$nnodes" "SIMSTXTDB" "vnc_pid" "Xvnc" '{print $2}' "" "detectsandboxpids"
   debug3 "SIMVNPID "; for ((nnn=1;nnn<=nnodes;nnn++)); do debug3 "${SIMVNPID[$nnn]} "; done; debug3 ""
   killpids="$killpids $listdpid"
#$PSSTRING|grep simnow 2>/dev/null
   checkdpid SIMSNPID "$nnodes" "$PSSTRING" "" "simnow" '{if ($5 ~ cego) print $6}' 'cego=${SIMSCPID[$nnn]}' "detectsandboxpids"
   debug3 "SIMSNPID "; for ((nnn=1;nnn<=nnodes;nnn++)); do [ "$debug" -gt "2" ] && echo -n "${SIMSNPID[$nnn]} "; done; debug3 ""
   killpids="$killpids $listdpid"

   # Reverse pid mapping
   for ((nnn=1;nnn<=nnodes;nnn++)); do PIDTONID[${SIMSNPID[$nnn]}]=$nnn; done
   DETECTEDPIDS="$listdpid"; detectedokpids="$listdokpid"

   # MEDIATOR
   if [ "$NOMEDIATOR" = "0" ]; then
      checkdpid SIMMDPID 1 "SIMMTXTDB" "mediator_pid" "mediator" '{print $2}' "" "detectsandboxpids"
      debug3 "SIMVNPID "; debug3 "${SIMMDPID[$nnn]} "
      killpids="$killpids $listdpid"
   fi
   debug2 "killpids=$killpids"
   debug2 "DETECTEDPIDS=$DETECTEDPIDS"
   debug3 "--- EXITING detectsandboxpids"
}

#####################################################
function extractsfwpids {
#####################################################
# Extracting the SFW PID
# METHOD: parsing the JOBWATCH file
# INPUT:  node number (1..N)
# OUTPUT: EGO=the pid of the detected SFW or the empty string
#####################################################
   local nnn
   local fn
   nnn="$1"
   debug3 ""; # to break the '.......'
   buildfn1 JOBWATCH $nnn; fn="$fninn"
   debug3 "extractsfwpids: JOBWATCH=$fn nnn=$nnn nnodes=$nnodes"
   if [ -s "$fn" ]; then
      debug3 "extractsfwpid: `ls -l $fn`"
   
      # Try two methods for extracting the SFW PID:
      EGO=`egrep "$SIMPIDDETECT2" $fn 2>/dev/null|tail -1|awk "{for(w=1;w<=NF;++w) if (\\$w ~ /$SIMPIDDETECT2/) print \\$(w+1)}"`
      debug4 "extractsfwpids: EGO=$EGO (method 1)"
      if [ -z "$EGO" ]; then
#         EGO=`egrep "$SIMPIDDETECT" $fn 2>/dev/null| awk '{print $4}'|cut -d- -f2`
         EGO=`egrep "$SIMPIDDETECT" $fn 2>/dev/null| sed  's/[^0-9\+]*\([0-9]\+\).*/\1/'|tail -1`
         debug4 "extractsfwpids: EGO=$EGO (method 2)"
      fi
   fi
   debug3 "extractsfwpids: EGO='$EGO'"
   if ! [[ "$EGO" =~ ^[0-9]+$ ]] ; then
      # no-hope...
      EGO=""
   fi
}

#####################################################
function detectsfwpids {
#####################################################
# PURPOSE: Detecting the $SIMSMALL pid via host output parsing
# Looking for the SIMPIDDETECT2 string 
# The 4th word is xxx-pid-xxxxx
#----
# OUTPUT
# SFWPIDS - detected simulator pid
# SFWOKPIDS - "ok" string
#---
# GLOBALS
# nnodes
#####################################################
   local nnn
   local nco
   local a
   local pspresent
   local pspids="0"
   declare -a tmpsimpid

   SFWPIDS=""; SFWOKPIDS=""; nco="0"; EGO=""
   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   for ((nnn=1;nnn<=nnodes;nnn++)); do
      extractsfwpids $nnn
      a=""; if [ "$EGO" != "" ]; then nco=`expr $nco + 1`; a=`echo "$SFWPIDS"|grep "$EGO"`; fi
      if [ -z "$a" ]; then
         [ -z "$SFWPIDS" ] && SFWPIDS="$EGO" || SFWPIDS="$SFWPIDS $EGO"
      fi
      tmpsimpid[$nnn]="$EGO"
      debug3 "detectsfwpids: SFWPIDS=$SFWPIDS nco=$nco tmpsimpid[$nnn]=${tmpsimpid[$nnn]}"
   done

   # PID Validation
   if [ "$nco" = "$nnodes" ]; then
   debug3 "detectsfwpids: - verifyng simpids..."
      # verify if the detected pids are actually corresponding to sim pids
      for ((nnn=1;nnn<=nnodes;nnn++)); do
         # note: we must use 'ps axu' NOT 'ps axuc' (e.g., cotson is launched via ruby-mri)
#ps axu 2>/dev/null |grep cotson
#ps axu 2>/dev/null |grep simnow
         pspresent=`ps axu 2>/dev/null| awk '$2 == pid {print $0}' pid=${tmpsimpid[$nnn]} 2>/dev/null|grep $SFWPSAXUEXE `
         [ -z "$pspresent" ] || { pspids=`expr $pspids + 1`; COTSONPID[$nnn]="${tmpsimpid[$nnn]}"; }
         debug4 "detectsfwpids: pspresent=$pspresent   pspids=$pspids"
      done
      [ "$pspids" = "$nnodes" ] && SFWOKPIDS="ok"
   fi
   debug3 "detectsfwpids: SFWOKPIDS=$SFWOKPIDS  nco=$nco  pspids=$pspids  SFWPIDS=$SFWPIDS"
}

#####################################################
# Detecting the $SIMSMALL pid via host output parsing
# Looking for the SIMPIDDETECT string 
# The 4th word is xxx-pid-xxxxx
#####################################################
function detectsimpid {
#####################################################
# $1 = mypid
#----
# OUTPUT
# SFWPIDS - detected simulator pid
#---
# GLOBALS
# retval
# 
   local lmypid
   local mycount
   SIMPDTMOUT="40"
   local mydeltasleep="0.2"
   local mycntstop=`echo "define round(x,n) {auto s,y;s=scale;y=(((10^n)*x)+0.5);scale=0;y=y/1;scale=s;y=y/(10^n); return y} scale=n; round(1.49,0)"|bc`
   lmypid="$1"
   retwait="0"

   if [ "$quiet" = "0" -a "$verbose" -gt "0" ]; then
      echo -n "   Waiting to monitor $SIMSMALL ... "
   fi
   SFWPIDS=""; mycount="0"
   buildfn1 JOBWATCH 1
   myfn="$fninn"
   debug1 ""
   debug1 "myfn=$myfn"
   debug1 "mydeltasleep=$mydeltasleep  mycntstop=$mycntstop"
   while [ "$SFWPIDS" = "" -a "$mycount" -lt $mycntstop ]; do # try for 00 sec
      sleep $mydeltasleep
      mycount=$(($mycount + 1))
#      SFWPIDS=`egrep "$SIMPIDDETECT" $myfn| awk '{print $4}'|cut -d- -f2`
[ -s $fn ] && debug3 "`ls -l $fn`"
      SFWPIDS=`egrep "$SIMPIDDETECT" $fn 2>/dev/null| sed  's/[^0-9\+]*\([0-9]\+\).*/\1/'|tail -1`
   done
   if [ "$quiet" = "0" -a "$verbose" -gt "0" ]; then
      echo "($SFWPIDS)"
   fi
   debug1 "SFWPIDS='$SFWPIDS'  mycount=$mycount"
   if [ "$SFWPIDS" = "" ]; then
      echo "ERROR: Couldn't detect $SIMSMALL pid"
      single_process_kill $lmypid
      retwait="1"
   fi
}

#####################################################
function exesim()
#####################################################
# $1 = exename
# $2 = params
# $3 = outfile
# $4 = exemsg
# $5 = exemsg
# $6 = sim-identifiers description
# $7 = simbinname
# $8 = howmanychildpids
# $9 = goslow
#----
# OUTPUT:
# SIMTIMEMS - time in ms
# SIMTIMEH  - time in hours
#----
# GLOBALS:
# GOSLOW
{
   local edescr="$6"
   local ofile="$3"
   local simbinname
   local tttime
   local tttout
   # increment sim-identifiers ccunter
   EXPCOUNT=`expr $EXPCOUNT + 1`
   simbinname="$7"
   howmanychildpids="$8"

   # Make sure all process have died before starting
#   cleanup

   #####################################################################
   #disable SIGINT propagation to PPID by using different process groups
   set -m
   #####################################################################

   mypid="0"
#   retwait="1"
   retwait="0"
   retwait2="0"
   KILLED="1"
   ctrlccount="0"
   keypressed="1"
   mytaketime=""
   simdirs="0"
   [ -s "$ofile" ] && rm -f $ofile >/dev/null 2>/dev/null
   [ -z "$TAKETIME" ] || tmptime=`mktemp /tmp/mytimeXXXXXX`
   if [ $? != 0 ]; then
      cecho "ERROR: /tmp exhausted (?)" "$red"; exit 14;
   fi
#   while [ $retwait != "0" -a "$ctrlccount" -lt "2" ]; do
#   while [ "$keypressed" = "1" -a "$ctrlccount" -lt "2" ]; do
   while [ "$retwait" = "0" -a "$retwait2" = "0" -a "$keypressed" = "1" -a "$ctrlccount" -lt "2" ]; do
      if [ "$KILLED" != "0" ]; then
         t000=`date`
         wd=`pwd`
         KILLED="0"; tttime="0"; tttout="2"
         # Printing the execution-started message
         comment "$4"
         TEXPSTART=`date +%s%N | cut -b1-13`; debug2 "TEXPSTART=$TEXPSTART"
         [ -z "$TAKETIME" ] || mytaketime="$TAKETIME$tmptime"

         ###################################################################
         $mytaketime $1 $2 2>&1 >>$ofile &
         mypid=$! MYSTATUS=$?
         disown $mypid
         comment "   Working dir: $wd  Cmd: '$1 $2' ($5) pid=$mypid ..."
         debug1 "LAUNCHED: $mytaketime $1 $2"
         if [ ! -z "$TAKETIME" ]; then
            MYSTATUS=""
            # we need to wait until the last line of the file is written (typically it's very fast) !
            while [ -z "$MYSTATUS" -a $tttime -lt $tttout -a ! -s $tmptime ]; do
               [ "$tttime" -gt "0" ] && sleep 1
               MYSTATUS=`awk '/Exit status/{print $3}' $tmptime`
               tttime=`expr $tttime + 1`
            done
            [ -z "$MYSTATUS" ] && MYSTATUS=0  # sanity check
         fi
         ###################################################################

         debug1 "MYSTATUS=$MYSTATUS"
         debug2 "edescr='$edescr'"
#         if [ "$BINMAKE" = "0" ]; then detectsimpid $mypid; fi  # outputs $SFWPIDS
      fi
#echo "KILLED=$KILLED"
#      if [ "$KILLED" = "0" ]; then
      if [ "$MYSTATUS" = "0" -a "$KILLED" = "0" -a "$mypid" != "0" ]; then
         keypressed="0"
         checkpid=`ps axuc|grep -q $mypid;echo $?`
         [ "$checkpid" != "0" ] && debug1 "      WARNING: $mypid already died!"
#         wait $mypid;
         cpid=`pstree -p $mypid|grep $SIMSMALL|sed -r "s/\.\*$SIMSMALL\(([0-9]+)\).*/\1/"`
# spid and SIMVID are not yet available here
#      spid=`pstree -p $cpid|grep "simnow*"|sed -r 's/.*simnow\(([0-9]+)\).*/\1/'`
#      SIMVID=`ls -ltr /tmp/.X11-unix|grep $DEFUSER|awk '{print $9}'`
#echo "simbinname=$simbinname   howmanychildpids=$howmanychildpids   mypid=$mypid cpid=$cpid spid=$spid SIMVID=$SIMVID"
         debug1 "simbinname=$simbinname   howmanychildpids=$howmanychildpids   mypid=$mypid"
#echo "check_p"
         DETECTEDPIDS=""
         checksimproc "$simbinname" "$howmanychildpids" "$mypid"
         retwait=$?;
      fi
#      if [ "$KILLED" = "0" ]; then wait ; retwait=$?; fi

      # Manage the case when the simulation launch fails
      if [ "$MYSTATUS" != "0" ]; then
         cecho "ERROR: while issuing '$1 $2' something failed (errorcode=$MYSTATUS)" "$red"
         retwait2=$MYSTATUS

         # Print some lines to possibly identify the failure reason
         buferr=`tail -n15 $ofile`
         if [ "$buferr" != "" ]; then
            cecho "$buferr" "$red"
         fi

         if [ "$interactive" = "1" ]; then
            echo "If you want to stop the experiment, press CTRL-C now."
            read dummy
         fi
      fi
   done

   #-----------------------------------------------------------------------------
   if [ "$GUESTOUT" = "1" -a "$quiet" = "0" ]; then
      buildfn1 NODEOUT 1
      GSTFILE=$fninn
      GSTFILEOUT1=""
      if [ -s "$GSTFILE" ]; then
         verbose1 "   Found GSTFILE=$fninn"
         GSTFILEOUT0=`echo "----------------------------------- GUESTOUT (NODE_1) START"`
         GSTFILEOUT1=`cat  $GSTFILE 2>/dev/null`
         GSTFILEOUT2=`echo "----------------------------------- GUESTOUT (NODE_1) END"`
      else
         verbose1 "   GSTFILE=$fninn  NOT FOUND."
      fi
      if [ -z "$GSTFILEOUT1" ]; then
         GUESTFILEOUT=""
      else
         GUESTFILEOUT="${GSTFILEOUT0}\n${GSTFILEOUT1}\n${GSTFILEOUT2}"
      fi
   fi
   #-----------------------------------------------------------------------------

   if [ "$retwait" = "0" -a "$retwait2" = "0" -a "$mypid" != "0" ]; then
      [ "$BINMAKE" = "0" ] && verbose1 "   Waiting for process $mypid ..."
      wait $mypid 2>&1 >/dev/null
#echo "retwait=$retwait  MYSTATUS=$MYSTATUS"
   fi

   #######################################
   #set job control to default
   set +m
   #######################################

   TEXPEND=`date +%s%N | cut -b1-13`; debug2 "TEXPEND=$TEXPEND"
   debug1 "retwait=$retwait   retwait2=$retwait2   ctrlccount=$ctrlccount"

   # Examine the exit status of the Job
   if [ "$retwait" != "0" -o "$retwait2" != "0" ]; then
      comment "   Terminated with exitcodes: retwait=$retwait retwait2=$retwait2"
      # Make sure the Job is terminated
      kill -0 $mypid 2>/dev/null >/dev/null
      [ "$?" = "0" ] && single_process_kill $mypid
   else
      comment "   Terminated correctly"
   fi

#IS THIS NEEDED? (outfile might be incomplete at this point)
#   if [ -s $ofile ]; then
#      comment "   Generated outfile: '$ofile'"
#   else
#      comment "   outfile NOT generated!"
#   fi

   #
   TEXESTOP=`date`

   # Wait until the Job has printed its SIMFINISHED MESSAGE
   [ "$BINMAKE" = "0" ] && exewait "$SIMFINISHED"

   #
   if [ ! -z "$mytaketime" ]; then
      maxsimmem=`cat $tmptime|awk '/Maximum resident/{print $6}'`
      rm -f $tmptime 2>&1 >/dev/null
   fi
   # Some time the $tmptime file is not available
   [ -z "$maxsimmem" ] && maxsimmem="NA"

   # simtime monitoring
   SIMTIMEMS=`expr $TEXPEND - $TEXPSTART`
   SIMTIMEH=`echo ""|awk 'END{v=simtimems/3600000;printf("%7.2f",v)}' simtimems=$SIMTIMEMS`
   verbose2 "   simtimems=$SIMTIMEMS   simtimeh=$SIMTIMEH   maxsimmem=$maxsimmem";

   # sleep a bit to see if there is a TRIPLE CTRL-C
   if [ "$ctrlccount" -ge "2" ]; then
      echo "--- (another CTRL-C may interrupt the DSE loop)"
      if [ "$GOSLOW" = "1" ]; then sleep 5; fi
#      cleanup INTERRUPTED
      cleanup
   fi
   if [ "$GOSLOW" = "1" -a "$PROCKILLED" = "1" ]; then sleep 2; fi

   #
   extractq def $MYQRUNNING ${MYMACH}-$MYPID "" "NOWARNING"; # -->$FIRSTELEM
   EXTRACTQ_FAILED=$?
}

# trap keyboard interrupt (control-c)
trap control_c SIGINT
trap control_q SIGQUIT

#####################################################
function exewait() {
#####################################################
   local efm
   local nnn
   local a
   local b
   local nlist
   local nreflist
   local tttime
   local tttout
   local fcmdwatch
   local simdirs
   efm="$1" # exe finished message

   nlist=""; tttime="0"; tttout="30"
   comment "   Waiting for finishing the execution... (timeout=$tttout)"

   # generate reference node string
   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   for ((nnn=1;nnn<=nnodes;nnn++)); do
      [ "$nreflist" = "" ] && nreflist="$nnn" || nreflist="$nreflist $nnn"
   done
#echo "nreflist='$nreflist'"

   buildfn1 JOBWATCH 1; fn="$fninn"
   debug3 "JOBWATCH: fn=$fn"
[ -s $fn ] &&    debug3 "`ls -l $fn`"
   while [ "$nlist" != "$nreflist" -a $tttime -lt $tttout ]; do
      sleep 1; tttime=`expr $tttime + 1`
      if [ -s $fn ]; then fcmdwatch="JOBWATCH"; simdirs="1"; else fcmdwatch="SIMSWATCH"; fi
      for ((nnn=1;nnn<=nnodes;nnn++)); do
#         buildfn1 SIMSCREEN $nnn; fn="$fninn"
         buildfn1 $fcmdwatch $nnn; fn="$fninn"
         debug3 "$fcmdwatch: fn=$fn"
[ -s $fn ] &&          debug3 "`ls -l $fn`"
#if [ "$dndn" != "" ]; then fn="$dndn"; fi
         [ -s $fn ] && a=`grep "$efm" $fn` || a=""
#         [ -z "$a" ] || nlist="$nlist $nnn"
         b=`echo "$nlist"|grep $nnn`
         if [ "$b" = "" -a "$a" != "" ]; then [ "$nlist" = "" ] && nlist="$nnn" || nlist="$nlist $nnn"; fi
         nlist=`for i in $nlist; do echo $i; done |sort -n|tr '\n' ' '|sed 's/ *$//g'`
      done
   done
   [ $tttime = $tttout ] && debug1 "exewait: exiting with timeout tttout=$tttout"
   [ $tttime = $tttout ] || debug1 "exewait: FINISHED (nlist==nlistref)"
#ls -ltr
}


#####################################################
function copyinf2locdir {
#####################################################
# PURPOSE:   Copy the TAG-file into the LOCALCINFO directory
#####################################################
   verbose2 "---------- STARTING copyinf2locdir"
   local myname="$1"
   local mylocd="$LOCALCINFO"
   local mysrvd="$SERVCINFO"
   local dofileupd="$INFOFILEUPD"
   local callingfun="copyinf2locdir"
   debug3 "myname=$myname"
   debug3 "mylocd=$mylocd"
   debug3 "mysrvd=$mysrvd"
   LISTCCFILES="$LISTINFOF"
   NOREPL="$NOINFO"
   debug3 "NOREPL=$NOREPL"
   SETFP=""
   checkandcopy $myname $mylocd $mysrvd $dofileupd $callingfun
   LISTINFOF="$LISTCCFILES"
   NOINFO="$NOREPL"

#   local myinfo=$EXPTAG
#   local infopos="0"
#   local warning="0"
#   local infocheck
#   local myrpinfo=`readlink -e $EXPTAG` # realpath
#   local mybninfo=`basename $EXPTAG` # filter out initial path --> basename
#   local mylocinfo="$LOCALCINFO/$mybninfo" # local  full path name of infofile
#   local mysrvinfo="$SERVCINFO/$mybninfo"  # remote full path name of infofile
#   #
#   NOINFO="0" # flag to indicate if this function sets the INFOFILE or not
#   debug1 "myinfo=$myinfo"
#   debug1 "mybninfo=$mybninfo"
#   debug1 "mylocinfo=$mylocinfo"
#   debug1 "mysrvinfo=$mysrvinfo"
#
#   # Create a list of INFOFILES in LISTINFOF
#   local a=`echo "$LISTINFOF"|grep "$myinfo"`
#   if [ "$a" = "" ]; then
#      [ -z "$LISTINFOF" ] && LISTAINFOF="$myinfo" || LISTINFOF="$LISTINFOF $myinfo"
#   fi
#
#   #
#   verbose1 "* Looking for '$myinfo' possible locations..."
#
#   # if myinfo is different the mylocinfo: update mylocinfo
#   infocheck=`diff $myinfo $mylocinfo 2>&1`
#   if [ -f "$myinfo" -a "$infocheck" != "" ]; then
#      [ "$quiet" = "0" ] && comment "  Copying '$myinfo' into '$LOCALCINFO' ..."
#      cp -rf $myinfo $LOCALCINFO
#      infopos="1"
#   fi
#   # assert: myinfo and mylocinfo are now synchronized
#
#   # in the case of distributed simulation
#   if [ "$NOSERVERSIMROOT" = "0" ]; then
#      verbose1 "  Looking for '$myinfo' in SERVCINFO='$SERVCINFO'..."
#
#      # update the server INFOFILE when the user asks so:
#      if [ "$INFOFILEUPD" = "1" ]; then
#         infocheck=`diff $mylocinfo $mysrvinfo 2>&1`
#         if [ -s "$mylocinfo" -a "$infocheck" != "" ]; then
#            [ "$quiet" = "0" ] && comment "  Copying '$mylocinfo' into SERVCINFO:'$mysrvinfo' ..."
#            cp -rf $mylocinfo $mysrvinfo
#         fi
#      fi
#      # assert: myinfo and mysrvinfo are now synchronized
#
#      # Sanity check (remote can be temporaly unaccessible)
##      if [ ! -s $SERVCINFO/$myinfo ]; then
#      if [ ! -f $mysrvinfo ]; then # NONET==0-->NOSERVERSIMROOT==0  (-a "NONET" = "0" ]; then)
#         if [ "$quiet" = "0" ]; then
#            verbose1 "WARNING(copyinf2locdir): cannot find INFOFILE '$mybninfo' in SERVCINFO='$SERVCINFO'"
#            warning="1"
#         fi
#      else
#         # if mysrvinfo has been updated then update myloccinfo too
#         infocheck=`diff $mysrvinfo $mylocinfo 2>&1`
#         if [ -s "$mysrvinfo" -a "$infocheck" != "" ]; then
#            verbose1 "  Copying '$mybninfo' from $SERVCINFO into $LOCALCINFO"
#            cp -rf $mysrvinfo $LOCALCINFO
#         fi
#         infopos="2"
#      fi   
#   fi
#
#   #
#   if [ ! -s $LOCALCINFO/$mybninfo ]; then
#      verbose1 "  Looking for '$mybninfo' in LOCALCINFO='$LOCALCINFO'..."
##   if [ ! -f $mylocinfo ]; then
#      cecho "ERROR(copyinf2locdir): cannot find INFOFILE '$mybninfo' in LOCALCINFO='$LOCALCINFO'" "$red"
##      comment "WARNING(copyinf2locdir): cannot find INFOFILE '$mybninfo' in LOCALCINFO='$LOCALCINFO'"
#      NOINFO="1"
##   else
##      infopos="1"
#   else
#      comment "* LOCAL version available: $mylocinfo"
#      # if the connection is available and the file is different, then update the remove copy
#      if [ "$NOSERVERSIMROOT" = "0" ]; then # MUST SPLIT HERE TO AVOID LOCKING ON NO NET
#         if [ -d "$SERVCINFO" ]; then
#            infocheck=`diff $mysrvinfo $mylocinfo 2>&1`
#            if [ "$infocheck" != "" ]; then
#               comment "* Copying $mylocinfo into SERVCINFO='$SERVCINFO',,,"
#               cp $mylocinfo $SERVCINFO
#            fi
#         fi
#      fi
#   fi
#
   verbose2 "---------- ENDING copyinf2locdir"
}

#####################################################
function bin2xdir {
#####################################################
# PURPOSE: checks the app for current appi_size_LIBVTARGET
#          (note: this function is run inside an iterator)
# OUTPUTS:
#  LISTAPPBIN: list of checked app-binaries
#####################################################
   verbose2 "---------- STARTING bin2xdir"
   local myname="${appi}_${size}_${LIBVTARGET}"
   local mylocd="$LOCALCBIN"
   local mysrvd="$SERVCBIN"
   local dofileupd="$BINFILEUPD"
   local callingfun="bin2xdir"
   debug3 "myname=$myname"
   debug3 "mylocd=$mylocd"
   debug3 "mysrvd=$mysrvd"
   LISTCCFILES="$LISTAPPBIN"
   NOREPL="$NOAPP2"
   checkandcopy $myname $mylocd $mysrvd $dofileupd $callingfun
   LISTAPPBIN="$LISTCCFILES"
   NOAPP2="$NOREPL"
   verbose2 "---------- EXITING bin2xdir"
}

#####################################################
function checkandcopy {
#####################################################
   local inputfname="$1"
   local localdir="$2"
   local srverdir="$3"
   local dofileupd="$4"
   local callingfun="$5"
   local myfnpos="0"
   local myfdir=""
   local warning="0"
   local diffcheck
   local myrptfname=`readlink -e $inputfname` # realpath
   local mybasfname=`basename $inputfname`    # filter out initial path --> basename
   local currnfpath="$CURPATH/$mybasfname"    # current full path name of inputfname
   local localfpath="$localdir/$mybasfname"   # local   full path name of inputfname
   local srverfpath="$srverdir/$mybasfname"   # remote  full path name of inputfname
   #
#   NOREPL="0" # flag to indicate if this function replaces the inputfname or not
   debug3 "   inputfname=$inputfname"
   debug3 "   mybasfname=$mybasfname"
   debug3 "   currnfpath=$currnfpath"
   debug3 "   localfpath=$localfpath"
   debug3 "   srverfpath=$srverfpath"
 
   #  Update a list of inputfname(s) in LISTCCFILES
   local a=`echo "$LISTCCFILES"|grep "$inputfname"`
   if [ "$a" = "" ]; then
      [ -z "$LISTCCFILES" ] && LISTCCFILES="$inputfname" || LISTCCFILES="$LISTCCFILES $inputfname"
   fi

   #
   verbose1 "* Looking for '$inputfname' possible locations (1)..."

   # Default location for inputfname: current directory '.'
   if [ "$NOREPL" = "1" -a -s "$inputfname" ]; then NOREPL="0"; myfdir="."; fi
   
   # if the currnfpath exists...
   if [ -s "$currnfpath" -a "$NOREPL" = "0" ]; then
      verbose1 "  ...found in dir: $CURPATH"
      # if the currnfpath is different from inputfname: update inputfname
      diffcheck=`diff $currnfpath $inputfname 2>&1`
      debug4 "   diffcheck='$diffcheck'"
      if [ -s "$currnfpath" -a "$diffcheck" != "" ]; then
         [ "$quiet" = "0" ] && verbose1 "  Copying current '$currnfpath' to working '$inputfname'"
         cp -rf $currnfpath $inputfname
         myfnpos="4"
      fi
      # recheck:
      diffcheck=`diff $currnfpath $inputfname 2>&1`
      debug4 "   diffcheck='$diffcheck'"
      if [ -s "$currnfpath" -a "$diffcheck" != "" ]; then
         [ "$quiet" = "0" ] && cecho "ERROR while syncing '$currnfpath' and '$inputfname'"
      else
         [ "$quiet" = "0" ] && verbose1 "  ...synced with working '$inputfname'."
      fi
      # assert: currnfpath and inputfname are now synchronized

   fi

   # if the inputfname is different from localfpath: update localfpath
   debug4 "   diffcheck (3)..."
   diffcheck=`diff $inputfname $localfpath 2>&1`
   debug4 "   diffcheck='$diffcheck'"
   if [ -s "$inputfname" -a "$diffcheck" != "" -a "$NOREPL" = "0" ]; then
      [ "$quiet" = "0" ] && verbose1 "  Copying working '$inputfname' to local '$localdir'"
      cp -rf $inputfname $localdir
      myfnpos="1"
      # recheck:
      debug4 "   diffcheck (4)..."
      diffcheck=`diff $inputfname $localfpath 2>&1`
      debug4 "   diffcheck='$diffcheck'"
      if [ -s "$localfpath" -a "$diffcheck" != "" ]; then
         [ "$quiet" = "0" ] && cecho "ERROR while syncing '$inputfpath' and '$localfpath'"
      else
         [ "$quiet" = "0" ] && verbose2 "  ...synced with local '$localfpath'."
      fi
   fi
   # assert: inputfname and localfpath are now synchronized

   debug3 "dofileupd=$dofileupd"
   # CASE1: THE REMOTE SERVER IS AVAILABLE (distributed simulation)
   if [ "$NOSERVERSIMROOT" = "0" ]; then
      verbose2 "  Looking for '$inputfname' in '$srverdir' (2)..."

      # update the server file when the user asks so:
      if [ "$dofileupd" = "1" -a "$NOREPL" = "0" ]; then
         diffcheck=`diff $localfpath $srverfpath 2>&1`
         debug4 "   diffcheck='$diffcheck'"
         if [ -s "$localfpath" -a "$diffcheck" != "" ]; then
            [ "$quiet" = "0" ] && verbose1 "  Copying local '$localfpath' to server '$srverfpath' ..."
            cp -rf $localfpath $srverfpath
         fi
         # recheck:
         diffcheck=`diff $localfpath $srverfpath 2>&1`
         debug4 "   diffcheck='$diffcheck'"
         if [ -s "$srverfpath" -a "$diffcheck" != "" ]; then
            [ "$quiet" = "0" ] && cecho "ERROR while syncing '$localfpath' and '$srverfname'"
         else
            [ "$quiet" = "0" ] && verbose2 "  ...synced with server '$srverfname'."
         fi
      fi
      # assert: inputfname and srverfpath are now synchronized

      #
      if [ "$NOREPL" = "1" -a -s "$localfpath" ]; then NOREPL="0"; myfdir="$localdir"; fi

      # Sanity check (remote can be temporaly unaccessible)
      if [ ! -f $srverfpath ]; then # NONET-->NOSERVERSIMROOT==0  (-a "NONET" = "0" ]; then)
         if [ "$quiet" = "0" ]; then
            comment "WARNING($callingfun): cannot find file '$mybasfname' in srverdir='$srverdir'"
            warning="1"
         fi
      else
         # if mysrvinfo has been updated then update localdir too
         diffcheck=`diff $srverfpath $localfpath 2>&1`
         debug4 "   diffcheck='$diffcheck'"
         if [ -s "$srverfpath" -a "$diffcheck" != "" ]; then
            verbose2 "  Copying '$mybasfname' from $srverdir to $localdir"
            cp -rf $srverfpath $localdir
         fi
         myfnpos="2"
         if [ "$NOREPL" = "1" -a -s "$srverfpath" ]; then NOREPL="0"; myfdir="$srverdir"; fi
         # recheck:
         diffcheck=`diff $srverfpath $localfpath 2>&1`
         debug4 "   diffcheck='$diffcheck'"
         if [ -s "$localfpath" -a "$diffcheck" != "" ]; then
            [ "$quiet" = "0" ] && cecho "ERROR while syncing '$srverfpath' and '$localfpath'"
         else
            [ "$quiet" = "0" ] && verbose2 "  ...synced with local '$localfpath'."
         fi
      fi   
   fi

   #
   verbose2 "  Looking for '$inputfname' in '$localdir' (3)..."
   if [ ! -s $localdir/$mybasfname ]; then
      [ "$NOREPL" = "0" ] && cecho "ERROR($callingfun): cannot find file '$mybasfname' in localdir='$localdir'" "$red"
      NOREPL="1"
   else
      verbose1 "* LOCAL version available for '$inputfname' in dir '$localdir'"
      NOREPL="0"; myfdir="$localdir"
      # if the connection is available and the file is different, then update the remote copy
      if [ "$NOSERVERSIMROOT" = "0" ]; then # MUST SPLIT HERE TO AVOID LOCKING ON NO NET
         if [ -d "$srverdir" ]; then
            diffcheck=`diff $srverfpath $localfpath 2>&1`
            debug4 "   diffcheck='$diffcheck'"
            if [ "$diffcheck" != "" ]; then
               verbose1 "* Copying local '$localfpath' into srverdir='$srverdir'..."
               cp -f $localfpath $srverdir
            fi
         fi
         # recheck:
         diffcheck=`diff $localfpath $srverfpath 2>&1`
         debug4 "   diffcheck='$diffcheck'"
         if [ -s "$srverfpath" -a "$diffcheck" != "" ]; then
            [ "$quiet" = "0" ] && cecho "ERROR while syncing '$localfpath' and '$srverfpath'"
         else
            [ "$quiet" = "0" ] && verbose2 "  ...synced with local '$srverfpath'."
         fi
      fi
   fi

#   if [ ! -f $localfpath ]; then
##      echo "ERROR: cannot find APP2FILE '$inputfname' in '$localdir'"
#      comment "WARNING($callingfun): cannot find file '$mybasfname' in localdir='$localdir'"
#      NOREPL="1"
##   else
##      myfnpos="1"
#   else
#      verbose1 "* Using $localfpath"
#      NOREPL="0"; myfdir="$localdir"
#      if [ "$NOSERVERSIMROOT" = "0" ]; then # MUST SPLIT HERE TO AVOID LOCKING ON NO NET
#         if [ -d "$srverdir" ]; then
#            verbose1 "* Copying $localfpath into srverdir='$srverdir'..."
#            cp $localfpath $srverdir
#         fi
#      fi
#   fi

   debug2 "SETFP=$SETFP  myfdir='$myfdir'"
   if [ "$SETFP" = "SETFP" ]; then
      XFP[$inputfname]="$myfdir"
      debug2 "XFP[$inputfname]='${XFP[$inputfname]}'"
   fi

   #
#   verbose2 "Looking for '$inputfname' in '${EXEDIR[$model]}'..."
#   if [ ! -s ${EXEDIR[$model]}/$inputfname ]; then
#      cecho "ERROR: cannot find '$inputfname' in '${EXEDIR[$model]}'" "$red"
#      NOREPL="1"
#   fi   
}

#####################################################
function copybin2exedir {
#####################################################
# GLOBALS: SERVCBIN LOCALCBIN EXEDIR[$model] NOAPP2 LIBVTARGET
# OUTPUT: NOAPP2 is set to 1 if some of the binary is not avaialble in the working dir (EXEDIR[$model])
   debug2 "---------- STARTING copybin2exedir"
   SETFP="$1"
   debug2 "SETFP=$SETFP"
   ITNESTING="0"
   #ITPARAMS="$DSEPARAMS"
   ITPARAMS="$DEFAULTDSEBINGEN"
   LISTAPPBIN=""
   iterator bin2xdir
   debug1 "   LISTAPPBIN=$LISTAPPBIN"
   for model in $listmodel; do
      if [ "${EXEDIR[$model]}" != "" ]; then
         for myapp2 in $LISTAPPBIN; do
            debug1 "   myapp2=$myapp2 XFP='${XFP[$myapp2]}' cp -f ${XFP[$myapp2]} ${EXEDIR[$model]}"
            # for now copy binares in the EXEDIR[$model] (in the future in /tmp)
            [ -z "${XFP[$myapp2]}" ] || cp -f ${XFP[$myapp2]}/$myapp2 ${EXEDIR[$model]}
         done
      fi
   done
   SETFP="" # clear SETFP
   debug2 "---------- EXITING copybin2exedir"
}

#####################################################
function cleanq() {
#####################################################
   local myq="$1"
   [ -d $QUEUEDIR ] || mkdir $QUEUEDIR
   if [ -z $EXP ]; then
       echo "Empty EXP string"; return;
   fi
   QFILE=${QUEUEDIR}/${EXP}.$myq
   rm -f $QFILE 2>/dev/null
}

#####################################################
function listqelem() {
#####################################################
# INPUT: myq - the queue name
# INPUT: elemno - number (or "LAST") indicating the element to extract
# GLOBAL: ELEMLIST
# GLOBAL: QUEUEDIR
# GLOBAL: EXP
#####################################################
   local myq="$1"
   local elemno="$2" #element number
   local a=""
   local f
   ELEMLIST=""
   QSIZE="0"

   [ -d $QUEUEDIR ] || mkdir $QUEUEDIR
   if [ -z "$EXP" ]; then echo "Empty EXP string"; return; fi
   QFILE=${QUEUEDIR}/${EXP}.$myq
   if [ -z "$elem" ]; then echo "Empty elem for $QFILE"; return; fi
   QFILELOCK=${QFILE}.lock
   $MYLOCKF1  $QFILELOCK ##LOCK
   if [ -f $QFILELOCK ]; then
      if [ -s "$QFILE" ]; then
         if [ "$elemno" = "LAST" ]; then
            ELEMLIST=`awk '{if (a!="") a=a "\n" $NF; else a=$NF}END{print a}' $QFILE|sort|uniq|tr '\n' ' '|sed -e's/[ \t]*$//'`
         else
            ELEMLIST=`awk '{if (a!="") a=a "\n" $en; else a=$NF}END{print a}' en=$elemno $QFILE|sort|uniq|tr '\n' ' '|sed -e's/[ \t]*$//'`
         fi
      else
         ELEMLIST=""
      fi
      [ -s "$QFILE" ] && QSIZE=`wc $QFILE 2>/dev/null|awk '{print $1}'`
      [ -z "$QSIZE" ] && QSIZE="0"

      rm -f $QFILELOCK ##UNLOCK
   fi
}

#####################################################
function checkqelem() {
#####################################################
   local myq="$1"
   local elem="$2"
   local a=""
#   local retcode
   retcode="1"

   [ -d $QUEUEDIR ] || mkdir $QUEUEDIR
   if [ -z "$EXP" ]; then
       echo "Empty EXP string"; return;
   fi
   QFILE=${QUEUEDIR}/${EXP}.$myq
   if [ -z "$elem" ]; then
       echo "Empty elem for $QFILE"; return;
   fi
   QFILELOCK=${QFILE}.lock
   $MYLOCKF1  $QFILELOCK ##LOCK
   if [ -f $QFILELOCK ]; then
      a=`grep $elem $QFILE 2>/dev/null|cut -d" " -f 1|grep -v "^$" |tail -1`
      if [ "$a" = "$elem" ]; then retcode="0"; fi
      rm -f $QFILELOCK ##UNLOCK
   fi
#   return $retcode
}

#####################################################
function inserttopq() {
#####################################################
   local myq="$1"
   local elem="$2"
   local cost="$3"
   local retry="$4"
   local tstart="$5"
   local tend="$6"
   local machines="${@:7}"
   local a=""
   local b=""
   local retcode
   retcode="1"

   [ -d $QUEUEDIR ] || mkdir $QUEUEDIR
   if [ -z "$EXP" ]; then
       echo "Empty EXP string"; return;
   fi
   QFILE=${QUEUEDIR}/${EXP}.$myq
   if [ -z "$elem" ]; then
       echo "Empty elem for $QFILE"; return;
   fi
   # assert: QFILE is defined
   if [ "$quiet" = "0" ]; then
      verbose2 "------------- inserttopq: QFILE=$QFILE"
   fi

   QFILELOCK=${QFILE}.lock
   $MYLOCKF1  $QFILELOCK ##LOCK
   if [ -f $QFILELOCK ]; then
#      a=`grep -q $1 ${QUEUEDIR}/${EXP}queue 2>/dev/null`
      a=`grep $elem $QFILE 2>/dev/null|cut -d" " -f1|grep -v "^$" |tail -1`
      if [ "$a" = "$elem" ]; then
         if [ "$quiet" = "0" ]; then
            echo "WARNING(insertq): NOT INSERTING element '$elem' - already present in '$QFILE'"
         fi
      fi
      if [ "$a" = "" ]; then
         sed -i "1s/^/$elem $cost $retry $tstart $tend $machines\n/" $QFILE
#         echo "$elem $cost $retry $tstart $tend $machines" >>$QFILE
         # verify if present
         b=`grep $elem $QFILE 2>/dev/null|cut -d" " -f1|grep -v "^$" |tail -1`
         if [ "$b" = "$elem" ]; then retcode="0"; fi
      fi
      rm -f $QFILELOCK ##UNLOCK
   fi
   [ "$debug" -ge "1" ] && cat $QFILE
   verbose2 "------------- inserttopq: END (ret=$retcode)"
   return $retcode
}

#####################################################
function insertq() {
#####################################################
   local myq="$1"
   local elem="$2"
   local cost="$3"
   local retry="$4"
   local tstart="$5"
   local tend="$6"
   local machines="${@:7}"
   local a=""
   local b=""
   local retcode
   retcode="1"

   [ -d $QUEUEDIR ] || mkdir $QUEUEDIR
   if [ -z "$EXP" ]; then
       echo "Empty EXP string"; return;
   fi
   QFILE=${QUEUEDIR}/${EXP}.$myq
   if [ -z "$elem" ]; then
       echo "Empty elem for $QFILE"; return;
   fi
   # assert: QFILE is defined
   if [ "$quiet" = "0" ]; then
      verbose2 "------------- insertq: QFILE=$QFILE"
   fi

   QFILELOCK=${QFILE}.lock
   $MYLOCKF1  $QFILELOCK ##LOCK
   if [ -f $QFILELOCK ]; then
#      a=`grep -q $1 ${QUEUEDIR}/${EXP}queue 2>/dev/null`
      a=`grep $elem $QFILE 2>/dev/null|cut -d" " -f1|grep -v "^$" |tail -1`
      if [ "$a" = "$elem" ]; then
         if [ "$quiet" = "0" ]; then
            echo "WARNING(insertq): NOT INSERTING element '$elem' - already present in '$QFILE'"
         fi
      fi
      if [ "$a" = "" ]; then
         echo "$elem $cost $retry $tstart $tend $machines" >>$QFILE
         # verify if present
         b=`grep $elem $QFILE 2>/dev/null|cut -d" " -f1|grep -v "^$" |tail -1`
         if [ "$b" = "$elem" ]; then retcode="0"; fi
      fi
      rm -f $QFILELOCK ##UNLOCK
   fi
   [ "$debug" -ge "1" ] && cat $QFILE
   verbose2 "------------- insertq: END (ret=$retcode)"
   return $retcode
}

#####################################################
function copyq() {
#####################################################
#OUTPUT:
   local myq1="$1"
   local myq2="$2"

   if [ ! -d $QUEUEDIR ]; then
      return 1
   fi
   QFILE1=${QUEUEDIR}/${EXP}.$myq1
   QFILE2=${QUEUEDIR}/${EXP}.$myq2
   QFILELOCK1=${QFILE1}.lock
   QFILELOCK2=${QFILE2}.lock
   $MYLOCKF1  $QFILELOCK1 ##LOCK
   $MYLOCKF1  $QFILELOCK2 ##LOCK
   rm -f $QFILE2 2>/dev/null
   cp $QFILE1 $QFILE2
   rm -f $QFILELOCK2 ##UNLOCK
   rm -f $QFILELOCK1 ##UNLOCK
   return 0
}

#####################################################
function checkq() {
#####################################################
#OUTPUT: QOUT - 
#OUTPUT: QSIZE - 
   local lmode="$1"
   local myq="$2"
   local arg2="$3"
   local timeout="$4"
   local lqout
   local mylf3timeout
   QOUT=""
   QSIZE="0"

   [ -z "$retry" ] && retry="3"
   if [ "$lmode" = "file" ]; then
      QFILE="$myq"
      if [ ! -z $QFILE ]; then
      if [ ! -s $QFILE ]; then
         return 0
      fi
      fi
   else
      if [ ! -d $QUEUEDIR ]; then
         return 0
      fi
      QFILE=${QUEUEDIR}/${EXP}.$myq
   fi
 
   QFILELOCK=${QFILE}.lock
#   lockfile -r $retry  $QFILELOCK ##LOCK

   if [ -z $timeout ]; then
     $MYLOCKF2 $QFILELOCK ##LOCK
   else
     eval mylf3timeout="$MYLF3TIMEOUT"
     $MYLOCKF3 $mylf3timeout $QFILELOCK ##LOCK
   fi

   [ -s $QFILE ] && QSIZE=`wc $QFILE 2>/dev/null|awk '{print $1}'` || touch $QFILE
   [ -z "$QSIZE" ] && QSIZE="0"
   if [ ! -z "$arg2" ]; then
      if [ -s "$QFILE" ]; then
         lqout=`cat $QFILE|cut -d" " -f$arg2|tr ' ' '\n'|sort|uniq|tr '\n' ' '|sed -e's/[ \t]*$//'`
         [ -z "$QOUT" ] && QOUT="$lqout" || QOUT="$QOUT $lqout"
      fi
   fi
   rm -f $QFILELOCK ##UNLOCK
}

#####################################################
function extractq() {
#####################################################
#OUTPUT:
#   ELEMLINE
#   FIRSTELEM
#   SECONDELEM
#   THIRDELEM
#   FOURTHELEM
#   FIFTHELEM
#   SIXTHANDREST
#   RESTOFELEMS
#   QSIZE
#OUTPUT:
#   returns 0 if suceeded or 1 otherwise
#####################################################
   local lmode="$1"
   local myq="$2"
   local matchingarg="$3"
   local headortail="$4"
   local nowarning="$5"
   local f
   local tmplist
   local iniqsz

   # by default extract from head
   [ -z "$headortail" ] && headortail="head" || "tail"

   FIRSTELEM=""
   QSIZE="0"
   if [ "$lmode" = "file" ]; then
      QFILE="$myq"
      if [ ! -z $QFILE ]; then
      if [ ! -s $QFILE ]; then
         return 1
      fi
      fi
   else
      if [ ! -d $QUEUEDIR ]; then
         if [ "$quiet" = "0" ]; then
            cecho "WARNING: Queue directory '$QUEUEDIR' not found!" "$yellow"
         fi
         return 1
      fi
      QFILE=${QUEUEDIR}/${EXP}.$myq
   fi
   # assert: QFILE is defined
   if [ "$quiet" = "0" ]; then
      debug1 "------------- extractq: QFILE=$QFILE"
   fi

   # Check if QFILE exists
   if [ ! -f $QFILE ]; then
      if [ "$quiet" = "0" -a "$nowarning" = "" ]; then
         cecho "WARNING: Queue file '$QFILE' not found!" "$yellow"
         debug1 "------------- extractq END"
      fi
      return 1
   fi
   # Check if QFILE is not empty
   if [ ! -s $QFILE ]; then
      if [ "$quiet" = "0" ]; then
         verbose1 "* Queue file '$QFILE' empty!"
         debug1 "------------- extractq END"
      fi
      return 1
   fi
 
   # Try to lock
   QFILELOCK=${QFILE}.lock
   debug1 "  Trying to lock $QFILELOCK ..."
   $MYLOCKF1  $QFILELOCK ##LOCK
   if [ ! -f $QFILELOCK ]; then return 1; fi
   # assert: "got lock"

      for f in $QFILE; do
         if [ -s $f ]; then
            iniqsz=`wc $f 2>/dev/null|awk '{print $1}'`
            debug1 "  iniqsz=$iniqsz"
            mnum="1" # by default extract from top
            if [ ! -z "$matchingarg" ]; then
               mnum=`grep -Hn "$matchingarg" $f|awk -F: '{print $2}'|head -1`
               headortail="head" #ignore previous headortail in case of matchingarg
               [ -z "$mnum" ] && mnum="0" # this means that matching failed!
               if [ "$mnum" = "0" ]; then
                  rm -f $QFILELOCK ##UNLOCK
                  debug1 "------------- extractq END"
                  return 1
               fi
            fi

            #
            ELEMLINE=`$headortail -$mnum $f`
            FIRSTELEM=`echo "$ELEMLINE" |awk '{print $1}'`
            SECONDELEM=`echo "$ELEMLINE"|awk '{print $2}'`
            THIRDELEM=`echo "$ELEMLINE" |awk '{print $3}'`
            FOURTHELEM=`echo "$ELEMLINE" |awk '{print $4}'`
            FIFTHELEM=`echo "$ELEMLINE" |awk '{print $5}'`
            SIXTHANDREST=`echo "$ELEMLINE" |awk '{print $6}'`
            RESTOFELEMS=`echo "$ELEMLINE" |awk '{$1="";print $0}'`

            [ "$headortail" = "head" ] && sed -i -e "${mnum}d" $f || sed -i '$ d' $f
            [ -s $f ] && QSIZE=`wc $f 2>/dev/null|awk '{print $1}'`
            [ -z "$QSIZE" ] && QSIZE="0"
            [ "$QSIZE" = 0 ] && rm -f $QFILE 2>/dev/null
            if [ "$quiet" = "0" ]; then
               debug1 "  current QSIZE=$QSIZE"
               debug2 "  ELEMLINE=$ELEMLINE"
               debug1 "  FIRSTELEM=$FIRSTELEM"
               if [ -s $f ]; then
                  debug2 "  f=$f"
               else
                  debug2 "  (now) file not found: '$f'"
               fi
            fi
            rm -f $QFILELOCK ##UNLOCK
            debug1 "------------- extractq END"
            return 0
         else
            if [ "$quiet" = "0" ]; then
               echo "* Cannot extract from (empty) '$QFILE'"
            fi
            rm -f $QFILELOCK ##UNLOCK
            debug1 "------------- extractq END"
            return 1
         fi
         if [ -f $f ]; then
            if [ ! -s $f ]; then
                rm -f $f
            fi
         fi
      done

   rm -f $QFILELOCK ##UNLOCK
   debug1 "------------- extractq END"
   return 0
}

#####################################################
function printsummarystats() {
#####################################################
# INPUTS: 
#   validfile=$1
#   initialcomment=$2
#   outputstatsfile=$3
#   timerstatsfile=$4
# GLOBALS:
#   expkcount
#   nodes
#   masternode
#   stdoutpref
#   stdounewf
#   fnout
#####################################################
   local nnn
   local nnnn
   local fn
   local summ1
   local summ2
   local summ3
   local validfile=$1
   local initialcomment=$2
   local hsimout="$3"
   local hnodout="$4"
   local hsimtim="$5"
   local outputstatsfile
   local timerstatsfile
   local fn
   local mfile
   dftime=""
   gsp=""
   inst=""
   cpi=""
   speed=""
   maxdftime=""
   totgsp=""
   totinst=""
   totcpi=""
   local tifile=""
   local mhz
   local dfcy

   #
   debug2 "------------ ENTERING function printsummarystats()"
   debug2 "validfile=$validfile hsimout=$hsimout hnodeout=$hnodeout hsimtim=$hsimtim"
   buildfn1 $hsimout
   outputstatsfile="$OUTDIR/$fnout"

   # sim stats
   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   if [ "$validfile" = "1" ]; then
      buildfn1 $hnodout $masternode
      mfile="$OUTDIR/$fnout"

      dftime=`grep "$threadsttimeid" $mfile |head -1 |awk '{printf("%13d", $3)}'`
      gsp=`awk '/^[ \t].core/{if(c<cores){sum+=$5;sum1+=$7;c+=1;}}END{den=sum+sum1;if(den!=0){v=sum/den;}else{v=0;}printf("%5.2f",v)}' cores=$CORES1 $mfile`
      inst=`awk '/^[ \t].core/{if(c<cores){sum+=$3;c+=1;}}END{printf("%13d",sum)}' cores=$CORES1 $mfile`
      cpi=`awk '/^[ \t].core/{if(c<cores){sum+=$3;sum1+=$9;c+=1;}}END{den=sum;if(den!=0){v=sum1/den;}else{v=0;}printf("%5.2f",v)}' cores=$CORES1 $mfile`
      speed=`awk '/^[ \t].core/{if(c<cores){sum1+=$9;c+=1;}}END{den=simtimems;if(den!=0){v=sum1/den;}else{v=0;}printf("%5.0f",v)}' cores=$cores simtimems=$SIMTIMEMS $mfile`
      maxdftime="0"
      totgsp="0"
      totinst="0"
      totcpi="0"
      totspeed="0"

      for ((nnn=1;nnn<=nnodes;nnn++)); do
         #
         buildfn1 "$hnodout" "$nnn"
         fn="$OUTDIR/$fnout"

         tmpdftime=`grep "$threadsttimeid" $fn |head -1 |awk '{print $3}'`
         if [ ! -z "$tmpdftime" ]; then
            if [ "$tmpdftime" -gt "$maxdftime" ]; then maxdftime=$tmpdftime; fi
         fi
         tmpgsp=`awk '/^[ \t].core/{if(c<cores){sum+=$5;sum1+=$7;c+=1;}}END{den=sum+sum1;if(den!=0){v=sum/den;}else{v=0;}printf("%5.2f",v)}' cores=$CORES1 $fn`
         if [ ! -z "$tmpgsp" ]; then totgsp=`echo ""|awk '{printf("%5.2f",v1+v2)}' v1="$totgsp" v2="$tmpgsp" -`; fi
         #echo "tmpgsp=$tmpgsp"
         #echo "totgsp=$totgsp"
         tmpinst=`awk '/^[ \t].core/{if(c<cores){sum+=$3;c+=1;}}END{printf("%13d",sum)}' cores=$CORES1 $fn`
         if [ ! -z "$tmpinst" ]; then totinst=`echo ""|awk '{printf("%13d",v1+v2)}' v1="$totinst" v2="$tmpinst" -`; fi
         #echo "tmpinst=$tmpinst"
         #echo "totinst=$totinst"
         tmpcpi=`awk '/^[ \t].core/{if(c<cores){sum+=$3;sum1+=$9;c+=1;}}END{den=sum;if(den!=0){v=sum1/den;}else{v=0;}printf("%5.2f",v)}' cores=$CORES1 $fn`
         if [ ! -z "$tmpcpi" ]; then totcpi=`echo ""|awk '{printf("%5.2f",v1+v2)}' v1="$totcpi" v2="$tmpcpi" -`; fi
         #echo "tmpcpi=$tmpcpi"
         #echo "totcpi=$totcpi"
         tmpspeed=`awk '/^[ \t].core/{if(c<cores){sum1+=$9;c+=1;}}END{den=simtimems;if(den!=0){v=sum1/den;}else{v=0;}printf("%5.0f",v)}' cores=$CORES1 simtimems=$SIMTIMEMS $fn`
         if [ ! -z "$tmpspeed" ]; then totspeed=`echo ""|awk '{printf("%5.0f",v1+v2)}' v1="$totspeed" v2="$tmpspeed" -`; fi
         #echo "tmpspeed=$tmpspeed"
         #echo "totspeed=$totspeed"
      done
   fi

   # Make sure we print something for easier parsing
   if [ "$dftime" = "" ]; then dftime="0"; fi
   if [ "$gsp" = "" ]; then gsp=" 0   "; fi
   if [ "$inst" = "" ]; then inst="0"; fi
   if [ "$cpi" = "" ]; then cpi=" 0   "; fi
   if [ "$speed" = "" ]; then speed=" 0   "; fi
   if [ "$maxdftime" = "" ]; then maxdftime="0"; fi
   if [ "$totgsp" = "" ]; then totgsp=" 0   "; fi
   if [ "$totinst" = "" ]; then totinst="0"; fi
   if [ "$totcpi" = "" ]; then totcpi=" 0   "; fi
   if [ "$totspeed" = "" ]; then totspeed=" 0   "; fi

   # Get Mhz from the timer.log file
   buildfn1 SIMNODTIMER $masternode
   tifile="$OUTDIR/$fnout"
   if [ -s $tifile ]; then
      mhz=`grep cycles_per_usec $tifile|head -1|awk '{print $2}'`
   else
      mhz=""
   fi
   if [ "$mhz" = "" ]; then mhz="0"; fi
   dfcy=`echo ""|awk '{printf("%5.2f",v2*(v1/1000))}' v1="$mhz" v2="$dftime" -`
   if [ "$dfcy" = "" ]; then dfcy="0"; fi

   dftime=`echo ""|awk '{printf("%13d",v1)}' v1="$dftime" -`
   gsp=`echo ""|awk '{printf("%5.2f",v1)}' v1="$gsp" -`
   inst=`echo ""|awk '{printf("%13d",v1)}' v1="$inst" -`
   cpi=`echo ""|awk '{printf("%5.2f",v1)}' v1="$cpi" -`
   speed=`echo ""|awk '{printf("%5.0f",v1)}' v1="$speed" -`
   maxdftime=`echo ""|awk '{printf("%13d",v1)}' v1="$maxdftime" -`
   totgsp=`echo ""|awk '{printf("%5.2f",v1/v2)}' v1="$totgsp" v2="$nnodes" -`
   totinst=`echo ""|awk '{printf("%13d",v1)}' v1="$totinst" -`
   totcpi=`echo ""|awk '{printf("%5.2f",v1/v2)}' v1="$totcpi" v2="$nnodes" -`
   totspeed=`echo ""|awk '{printf("%5.0f",v1/v2)}' v1="$totspeed" v2="$nnodes" -`

   summ0="$OKSTRING simtime= $SIMTIMEH h"
   summ1=" dftime= $dftime ns    gsp= $gsp    inst= $inst    cpi= $cpi    speed= $speed (kc/s)   dfcy=$dfcy"
   summ2="mdftime= $maxdftime ns   tgsp= $totgsp   tinst= $totinst   tcpi= $totcpi   tspeed= $totspeed (kc/s)"
   summ3="$msgSIM__END $TEXESTOP $EXP $VERSION1"

   # print some stats
   ###################
   MYMETRIC=""
   printstats $hsimtim indse
   ###################

#summ4="cycles=$cy+$sdcy  ic=$ii  icmr=$icmr+$sdicmr dcmr=$dcmr+$sddcmr  l2mr=$l2mr+$sdl2mr  l3mr=$l3mr+$sdl3mr  simt=$simt+$sdsimt mn=$masternode   icm=$icm   dcm=$dcm   l2m=$l2m   l3m=$l3m   icrm=$icrm   icwm=$icwm   icr=$icr   icw=$icw   dcrm=$dcrm   dcwm=$dcwm   dcr=$dcr   dcw=$dcw   l2rm=$l2rm   l2wm=$l2wm   l2r=$l2r   l2w=$l2w   l3rm=$l3rm   l3wm=$l3wm   l3r=$l3r   l3w=$l3w   tp=$tp   tr=$tr   tw=$tw   trs=$trs   tws=$tws   ts=$ts   td=$td"


   summ5=""
   summ6=""
   for n in 1 $masternode; do
      buildfn1 $hsimtim $n
      tfile="$OUTDIR/$fnout"
      #echo "$tfile"
      if [ -s "$tfile" ]; then
         cpu0cycles=`awk '/cpu0.timer.cycles/{print $2}' $tfile`
         #   cpu0instcount=`awk '/cpu0.instcount/{print $2}' $tfile`
         cpu0instructions=`awk '/cpu0.timer.instructions/{print $2}' $tfile`
         cpu0icmr=`awk '/cpu0.timer.icache[0]?.read_miss_rate/{print $2}' $tfile`
         #   cpu0dcmr=`awk '/cpu0.timer.dcache.read_miss_rate/{a=$2}/cpu0.timer.dcache.write_miss_rate/{b=$2}END{print a+b}' $tfile`
         cpu0dcmr=`awk '/cpu0.timer.dcache[0]?.read_miss/{a=$2}/cpu0.timer.dcache[0]?.write_miss/{b=$2}/cpu0.timer.dcache[0]?.read/{c=$2}/timer.dcache[0]?.write/{d=$2}END{print (c+d)/(a+b+0.00000000001)}' $tfile`
         #   cpu0l2mr=`awk '/cpu0.timer.dcache.l2cache.read_miss_rate/{a=$2}/cpu0.timer.dcache.l2cache.write_miss_rate/{b=$2}END{print a+b}' $tfile`
         cpu0l2mr=`awk '/\<cpu0.timer.dcache[0]?.l2cache[0]?.read_miss\>/{a=$2}/\<cpu0.timer.dcache[0]?.l2cache[0]?.write_miss\>/{b=$2}/\<cpu0.timer.dcache[0]?.l2cache[0]?.read\>/{c=$2}/\<timer.dcache[0]?.l2cache[0]?.write\>/{d=$2}END{print (c+d)/(a+b+0.00000000001)}' $tfile`
         #   cpu0l3mr=`awk '/cpu0.timer.dcache.l2cache.bus.l3cache.read_miss_rate/{a=$2}/cpu0.timer.dcache.l2cache.bus.l3cache.write_miss_rate/{b=$2}END{print a+b}' $tfile`
         cpu0l3mr=`awk '/\<cpu0.timer.dcache[0]?.l2cache[0]?.bus.l3cache[0]?.read_miss\>/{a=$2}/\<cpu0.timer.dcache[0]?.l2cache[0]?.bus.l3cache[0]?.write_miss\>/{b=$2}/\<cpu0.timer.dcache[0]?.l2cache[0]?.bus.l3cache[0]?.read\>/{c=$2}/\<timer.dcache[0]?.l2cache[0]?.bus.l3cache[0]?.write\>/{d=$2}END{print (c+d)/(a+b+0.00000000001)}' $tfile`
         if [ "$n" = "1" ]; then
            summ5="n$n: cycles=$cpu0cycles  ic=$cpu0instructions  icmr=$cpu0icmr dcmr=$cpu0dcmr  l2mr=$cpu0l2mr  l3mr=$cpu0l3mr"
         else
            summ6="n$n: cycles=$cpu0cycles  ic=$cpu0instructions  icmr=$cpu0icmr dcmr=$cpu0dcmr  l2mr=$cpu0l2mr  l3mr=$cpu0l3mr"
         fi
      fi
   done

   sed -i "1i/$initialcomment/" $outputstatsfile
   if [ "$threadstats" = "1" ]; then
      if [ "$quiet" = "0" ]; then
         cecho "$summ0" "$OKSTRCOLOR"
         echo "$summ1"
         echo "$summ2"
      fi
      #update output file
      echo "$summ0" >> $outputstatsfile
      echo "$summ1" >> $outputstatsfile
      echo "$summ2" >> $outputstatsfile
   else
      if [ "$quiet" = "0" ]; then
         cecho "$summ0" "$OKSTRCOLOR"
      fi
      echo "$summ0" >> $outputstatsfile
   fi

   if [ "$quiet" = "0" ]; then
      echo "$summ4"
      [ -z "$summ5" ] || echo "$summ5"
      [ -z "$summ6" ] || echo "$summ6"
      echo "$summ3"
   fi

   echo "$summ4" >> $outputstatsfile
   [ -z "$summ5" ] || echo "$summ5" >> $outputstatsfile
   [ -z "$summ6" ] || echo "$summ6" >> $outputstatsfile
   echo "$summ3" >> $outputstatsfile
   debug2 "EXITING function printsummarystats()"
}

#####################################################
printf_new() {
#####################################################
   local istab="0"
   local ntab="0"

   debug4 "------------ ENTERING function printf_new()"

   [ "${nindent:0:1}" = "t" ] && istab="1" && ntab="${nindent:1}"

   if [ "$istab" = "0" ]; then
      indent=""
      if [ "$1" = "0" ]; then return; fi
      local str=" "
      local num=`expr $1 - 1`
      local v
      if [ "$num" = "0" ]; then
         indent="\ "
      else 
         v=$(printf "%-${num}s" "$str")
         indent="\ ${v// /$str}"
      fi
   else
      local i
      indent="\ "
      for ((i=0;i<ntab;++i)); do
         indent="$indent\t"
      done
   fi
# echo "'${v}'"
# echo "'${v// /$str}'"
   debug4 "------------ EXITING function printf_new()"
}

#####################################################
function chelem() {
#####################################################
   local check1
   local inout="$1"
   local nindent="$2"
   local prefx="$3"
   local elem="$4"
   local news="$5"
   local matching="$6"
   local outelem="$7"
   local delimstart="$8"
   local delimend="$9"
   local commchar
   local commstr
   local inserting="1"
   local replacing="0"
   local fileext
   #local fileext="${modfile##.*\.}"
   local fileext="${modfile##*\.}"
   local pfx="^.*"
   debug4 "---------- ENTERING chelem"
   debug4 "fileext=$fileext  delimstart='$delimstart'  delimend='$delimend'"
   #
   [ "$fileext" = "lua" ] && commchar="-" || commchar="\#"
   [ "$fileext" = "lua" ] && commstr="--" || commstr="\#"
   [ "inout" = "co" ] && matching="^[ \t]*[-]*[#]*[ \t]*$elem" && inserting="0"
#   [ -z "$matching" ] && matching="^[ \t]*[-]*[ \t]*$elem" && inserting="0"
#   [ -z "$matching" ] && matching="^[ \t]*[-]*[#]*[ \t]*$elem" && inserting="0"
   [ -z "$matching" ] && matching="[ \t]*[-]*[#]*[ \t]*$elem" && inserting="0"
   [ -z "$elem" ] && replacing="1"
   [ -z "$prefx" ] || pfx="$prefx"
   printf_new "$nindent"; # sets 'indent'
   debug4 "elem='$elem' modfile='$modfile' INSPOINTPREV='$INSPOINTPREV' news='$news'"

   # if there is at least one delimiter, test the presence in that text range
   if [ "$delimstart" != "" -o "$delimend" != "" ]; then
      check1=`cat $modfile|sed -n '/$delimstart/,/$delimend/ p'|egrep "$pfx[ ]?$matching"`
   else
      check1=`cat $modfile|egrep "$pfx[ ]?$matching"`
   fi
   debug2 "commchar='$commchar'  inserting=$inserting  replacing=$replacing  matching='$matching'  check1='$check1'"
   debug3 "check1='$check1'  indent='$indent'  elem='$elem'  news='$news'"

if [ "$inout" = "ci" -o "$inout" = "ins" -o "$inout" = "ci2" ]; then
   if [ ! -z "$check1" ]; then
      if [ "$replacing" = "0" ]; then
         if [ "$inserting" = "0" ]; then
            debug1 "   updating '$elem' in $modfile"
#           sed -i "s/^[ \t]*[-]*[ \t]*${elem}.*/$news/" $modfile
#            sed -i "s/^\([ \t]*[-]*[ \t]*\)${elem}.*/\1${elem}$news/g" $modfile
            if [ "$inout" = "ci2" ]; then
               sed -i "s/$pfx ${elem}.*$/$indent${elem}$news/g" $modfile
            else
               sed -i "s/$pfx${elem}.*$/$indent${elem}$news/g" $modfile
            fi
         fi
      else
         debug1 "   replacing '$matching' in $modfile"
         sed -i "s/$pfx$matching.*$/$indent$news/g" $modfile
      fi
   else
      debug1 "   inserting '$elem' in $modfile"
      if [ -z "$INSPOINTPREV" ]; then
         sed -i "1i$indent${elem}$news" $modfile
      else
         sed -i "/$INSPOINTPREV/a$indent${elem}$news" $modfile
      fi
   fi
elif [ "$inout" = "del" ]; then
   if [ -z "$outelem" ]; then
         debug1 "   deleting '$matching' in $modfile"
         sed -i "/$pfx$matching.*$/d" $modfile
   else
         debug1 "   replacing '$matching' in $modfile"
         sed -i "s/$pfx$matching.*$/$indent$outelem/g" $modfile
   fi
else
   if [ -z "$outelem" ]; then
         debug1 "   commenting out '$matching' in $modfile"
         sed -i "s/^\([ \t]*[^$commchar]*$matching.*\)$/$commstr\1/g" $modfile
   else
         debug1 "   replacing '$matching' in $modfile"
         sed -i "s/$pfx$matching.*$/$indent$outelem/g" $modfile
   fi
fi
   INSPOINTPREV="$elem"
   debug4 "---------- EXITING chelem"
}

#####################################################
function archdef() {
#####################################################
   local lev="$1"
   local elem="$2"
   local nxt="$3"
   local ind
   local localbuf
   local mytrfile
   local cpval="${cpuv[0]}"
   debug2 "---------- STARTING archdef"

   [ "$lev" = "$INDENTLEV0" ] && ind="4"
   [ "$lev" = "$INDENTLEV1" ] && ind="8"
   debug4 "elem=$elem lev=$lev nxt=$nxt ind=$ind"

#   [ "$cores" -gt "1" -a "$cpval" = "cputracer" ] && cpval="ncputracer"

   case $elem in
      cpu)  chelem ci $ind "" "cpu=get_cpu" "(i)"
            case $cpval in
              memtracer)  MEMTRACING="1"; # MEMTRACE
                 chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"memtracer\", tracefile=\"$metracename.txt.gz\", shared=\"${cpuv[1]}\", binary=\"${cpuv[2]}\", size=\"${cpuv[3]}\",line_size=\"${cpuv[4]}\",num_sets=\"${cpuv[5]}\" }"
              ;; 
               tt0|\
               cputracer)  CPUTRACING="1"; # CPTRACE
                 chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"$cpval\", tracefile=\"$cptracename.txt.gz\", shared=\"${cpuv[1]}\", binary=\"${cpuv[2]}\", size=\"${cpuv[3]}\",line_size=\"${cpuv[4]}\",num_sets=\"${cpuv[5]}\" }"
              ;;

              exectracer)  EXECTRACING="1"; #EXECUTION TRACE
                 chelem ci $ind "" "cpu\:timer" "{name=\'cpu\'\.\.i, type=\"${cpuv[1]}\", tracing=\"true\", tracefile=\"$exectracename.txt.gz\"} " ;; 

              tracestats) STATRACING="1"; # STTRACE
                 chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"trace_stats\", trace_file=\"$sttracename.txt.gz\"}" ;;
              ntracestats) localbuf=`echo "${cpuv[1]}"|sed 's/\//\\\\\\//g'`; localbuf="\"${localbuf}\"..i..\".txt.gz\""
                 chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"trace_stats\", trace_file=${localbuf}}" ;;
              gzipsink) chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"gzip_sink\" }" ;;
              simpoint) chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"simpoint_profile\" }" ;;
              timerdep) chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"timer_dep\" }" ;;
              bandwidth) 
                  [ ! -z "${cpuv[1]}" ] && dcports="${cpuv[1]}" || dcports="2"
                  [ ! -z "${cpuv[2]}" ] && robsize="${cpuv[2]}" || robsize="128"
                  chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"$cpval\",data_cache_ports=\"$dcports\", reorder_buffer_size=\"$robsize\" }" ;;
              # timer0 timer1 random
              *) chelem ci $ind "" "cpu\:timer" "{ name=\'cpu\'\.\.i, type=\"$cpval\" }" ;;
            esac ;;

      trace)
            [ -z "${memtrfile}" ] || mytrfile="$memtrfile"
            [ -z "${cputrfile}" ] || mytrfile="$cputrfile"
            localbuf=`echo "${mytrfile}"|sed 's/\//\\\\\\//g'`
            chelem ci $ind "" "$elem=Tracer" "{name=\"${ARCELMNAME[$elem]}\", trace_file=\"${localbuf}\", next=$nxt}" ;;
      mem)  chelem ci $ind "" "$elem=Memory" "{name=\"${ARCELMNAME[$elem]}\", latency=${memlat}}" ;;
      l3)   chelem ci 4 "" "$elem=Cache" "{name=\"${ARCELMNAME[$elem]}\", size=\"${l3size}\", line_size=${l3blks}, latency=${l3late}, num_sets=${l3ways}, next=$nxt, write_policy=\"${l3whpo}\", write_allocate=\"${l3wmpo}\"}" ;;
      bus)  chelem ci $ind "" "$elem=Bus" "{name=\"${ARCELMNAME[$elem]}\", protocol=\"${pbus[0]}\", latency=${pbus[1]}, bandwidth=${pbus[2]}, next=$nxt}" ;;
      busT) chelem ci $ind "" "$elem=Bus" "{name=\"${ARCELMNAME[$elem]}\", protocol=\"${ptbus[0]}\", latency=${ptbus[1]}, bandwidth=${ptbus[2]}, next=$nxt}" ;;
#      l2)   chelem ci $ind "" "$elem=Cache" "{name=\"${ARCELMNAME[$elem]}\", size=\"${l2size}\", line_size=${l2blks}, latency=${l2late}, num_sets=${l2ways}, next=$nxt, write_policy=\"${l2whpo}\", write_allocate=\"${l2wmpo}\"}" ;;
      l2)   chelem ci $ind "" "$elem=Cache" "{name=\"${ARCELMNAME[$elem]}\"..i, size=\"${l2size}\", line_size=${l2blks}, latency=${l2late}, num_sets=${l2ways}, next=$nxt, write_policy=\"${l2whpo}\", write_allocate=\"${l2wmpo}\"}" ;;
#      t2)   chelem ci $ind "" "$elem=TLB" "{name=\"${ARCELMNAME[$elem]}\", page_size=\"${pt2s[0]}\", entries=${pt2s[1]}, latency=${pt2s[5]}, num_sets=${pt2s[2]}, next=$nxt, write_policy=\"${pt2s[3]}\", write_allocate=\"${pt2s[4]}\"}" ;;
      t2)   chelem ci $ind "" "$elem=TLB" "{name=\"${ARCELMNAME[$elem]}\"..i, page_size=\"${t2pgsz}\", entries=${t2entr}, latency=${t2late}, num_sets=${t2ways}, next=$nxt, write_policy=\"${t2whpo}\", write_allocate=\"${t2wmpo}\"}" ;;
      ic)   chelem ci $ind "" "$elem=Cache" "{name=\"${ARCELMNAME[$elem]}\"..i, size=\"${l1icsize}\", line_size=${l1icblks}, latency=${l1iclate}, num_sets=${l1icways}, next=$nxt, write_policy=\"${l1icwhpo}\", write_allocate=\"${l1icwmpo}\"}" ; chelem ci $ind "" "cpu:instruction_cache" "(ic)" ;;
      dc)   chelem ci $ind "" "$elem=Cache" "{name=\"${ARCELMNAME[$elem]}\"..i, size=\"${l1dcsize}\", line_size=${l1dcblks}, latency=${l1dclate}, num_sets=${l1dcways}, next=$nxt, write_policy=\"${l1dcwhpo}\", write_allocate=\"${l1dcwmpo}\"}" ; chelem ci $ind "" "cpu:data_cache" "(dc)" ;;
#      it)   chelem ci $ind "" "$elem=TLB" "{name=\"${ARCELMNAME[$elem]}\", page_size=\"${pt1i[0]}\", entries=${pt1i[1]}, latency=${pt1i[5]}, num_sets=${pt1i[2]}, next=$nxt, write_policy=\"${pt1i[3]}\", write_allocate=\"${pt1i[4]}\"}" ; chelem ci $ind "" "cpu:instruction_tlb" "(it)" ;;
#      dt)   chelem ci $ind "" "$elem=TLB" "{name=\"${ARCELMNAME[$elem]}\", page_size=\"${pt1d[0]}\", entries=${pt1d[1]}, latency=${pt1d[5]}, num_sets=${pt1d[2]}, next=$nxt, write_policy=\"${pt1d[3]}\", write_allocate=\"${pt1d[4]}\"}" ; chelem ci $ind "" "cpu:data_tlb" "(dt)" ;;
      it)   chelem ci $ind "" "$elem=TLB" "{name=\"${ARCELMNAME[$elem]}\"..i, page_size=\"${t1ipgsz}\", entries=${t1ientr}, latency=${t1ilate}, num_sets=${t1iways}, next=$nxt, write_policy=\"${t1iwhpo}\", write_allocate=\"${t1iwmpo}\"}" ; chelem ci $ind "" "cpu:instruction_tlb" "(it)" ;;
      dt)   chelem ci $ind "" "$elem=TLB" "{name=\"${ARCELMNAME[$elem]}\"..i, page_size=\"${t1dpgsz}\", entries=${t1dentr}, latency=${t1dlate}, num_sets=${t1dways}, next=$nxt, write_policy=\"${t1dwhpo}\", write_allocate=\"${t1dwmpo}\"}" ; chelem ci $ind "" "cpu:data_tlb" "(dt)" ;;
   esac
   debug2 "---------- EXITING archdef"
}

#####################################################
function buildarch() {
#####################################################
#INPUT: SIMCONFIG BINMAKE ARCELMNAME
#####################################################
   local checkfb
   local checknic
   local checkloopnic
   local ak
   local narchh
   local narchl

   debug1 "   updating the memory hierarchy in $SIMCONFIG"
   #find first insertion point
   checkfb=`grep "function build" $SIMCONFIG`
   if [ -z "$checkfb" ]; then
      echo "ERROR: '$SIMCONFIG' doesn't have 'function build'"
      exit 20
   fi
   checknic=`grep "get_nic" $SIMCONFIG`
   if [ -z "$checknic" ]; then
      echo "ERROR: '$SIMCONFIG' doesn't have 'get_nic'"
      exit 21
   fi
   checkloopnic=`sed '/while[ ]*i[ ]*<[ ]*nics/,/end/!d' $SIMCONFIG`
   if [ -z "$checkloopnic" ]; then
      echo "ERROR: '$SIMCONFIG' doesn't have the nic generating loop"
      exit 22
   fi
   #setup nic throughput and latency
   if [ "$BINMAKE" = "0" ] ; then
      chelem ci 8 "" "nic\:timer" "{ name=\'nic\'\.\.i, type=\"simple_nic\", thruput=\"${nic_trput}\", min_delay=\"${nic_latency}\", setup=\"${nic_setup}\"}"
   fi
   #first insertion point: mark it with a comment
   firstinspoint="    --memory hierarchy"
   checkmemhier=`grep "$firstinspoint" $SIMCONFIG`
   if [ -z "$checkmemhier" ]; then
      sed -i "/while[ ]*i[ ]*<[ ]*nics/,/end/s/end/end\n$firstinspoint/" $SIMCONFIG
   fi
   
   INSPOINTPREV="$firstinspoint"
   modfile="$SIMCONFIG"

   #architecture definition 
   debug1 "--------------------- STARTING ARCHITECTURE DEFINITION (function build)"
   debug2 "modfile=$modfile"
   debug2 "INSPOINTPREV=$INSPOINTPREV"
   IFS='.' read -a archlev <<< "$archstring"
   IFS='+' read -a archh <<< "${archlev[0]}"
   IFS='+' read -a archl <<< "${archlev[1]}"

   # insert all memory hierarchy if not present
   narchh=${#archh[@]} 
   narchl=${#archl[@]} 
   debug1 "narchh=$narchh  archh=${archh[*]}"
   debug1 "narchl=$narchl  archl=${archl[*]}"

   #
   arch_features="cpu\:timer cpu\:instruction_cache cpu\:data_cache cpu\:instruction_tlb cpu\:data_tlb"
   #arch_elements="cpu trace mem l3 bus busT l2 t2 ic dc it dt"
   arch_elements="${!ARCELMNAME[@]}" # list of architecture elements (keys)

   #clean up current architecture definition
   debug1 "Cleaning up the architecture definition..."
   for el1 in $arch_elements $arch_features; do
      debug2 "   removing element '$el1'..."
      el2=`echo "$el1"|egrep ":"`
      [ "$el1" = "$el2" ] && { hascolon="1"; } || { hascolon="0"; el1="$el1="; }
      chelem del 0 "^[ \t]*" "$el1"
   done
   INSPOINTPREV="$firstinspoint" # reset insertion point
   debug1 "...architecture definition is now cleaned up."

   debug1 "Parsing the architecture definition '$archstring'"

   #scan archh to see if 'main' is defined otherwise add it (linked to first element)
   debug1 "   verifying if I need to add the 'main' element..."
   checkmain="0"; firstmain=""
   local arch_elm_check=""
   for ((ak=0;ak<narchh;++ak)); do
      arch2=${archh[$ak]%-*}
      arch1=${archh[$ak]#*-}
      [ "$arch2" = "main" ] && checkmain="1"
      [ "$ak" = "0" ] && firstmain="$arch2"
      debug2 "- archh(scan):: ${archh[$ak]} -- $arch1 -- $arch2 -- checkmain=$checkmain -- firstmain=$firstmain"
   done
   if [ "$checkmain" = "0" ]; then
      debug2 "- archh(insert):: DEFAULT -- $firstmain -- main"
      archdef "$INDENTLEV0" "$firstmain" "main"
      arch_elm_check="$arch_elm_check mem"
   fi

   # insert all elements from archh
   for ((ak=0;ak<narchh;++ak)); do
      arch2=${archh[$ak]%-*}
      arch1=${archh[$ak]#*-}
      [ "$arch2" = "main" ] && checkmain="1"
      [ "$ak" = "0" ] && firstmain="$arch1"
      debug1 "- archh(insert):: ${archh[$ak]} -- $arch1 -- $arch2"
      archdef "$INDENTLEV0" "$arch1" "$arch2"
      arch_elm_check="$arch_elm_check $arch1"
   done

   # insert all elements from archl
#   INSPOINTPREV="cpu:timer"
   INSPOINTPREV="while[ \t]*i[ \t]*<[ \t]*cpus"
   for ((ak=0;ak<narchl;++ak)); do
      arch2=${archl[$ak]%-*}
      arch1=${archl[$ak]#*-}
      debug1 "- archl:: ${archl[$ak]} -- $arch1 -- $arch2"
      archdef "$INDENTLEV1" "$arch1" "$arch2"
      arch_elm_check="$arch_elm_check $arch1"
   done
   debug1 "arch_elm_check=$arch_elm_check"

   # eventually remove a memtracer (if not defined)
   if [ -z "$memtrfile" -a -z "$cputrfile" ]; then
      chelem del 0 "" "trace=Tracer"
   fi
}

#####################################################
function updatelistomissingparams() {
#####################################################
#INPUT: SIMCONFIG listomissingparams listoparams
#OUTPUT: listomissingparams
#####################################################
   local arch_elements="${!ARCELMNAME[@]}" # list of architecture elements (keys)
   local arch_elm_missing=""
   local arch_elm_present=""
   local ae
   # check which elements have been effectively added
   for ae in $arch_elements; do
      el1="$ae"
      el2=`echo "$el1"|egrep ":"`
      [ "$el1" = "$el2" ] && { hascolon="1"; } || { hascolon="0"; el1="$el1="; }
      checkel=`egrep "^[ \t]*$el1" $SIMCONFIG`
      if [ "$checkel" = "" ]; then
         arch_elm_missing="$arch_elm_missing $ae"
      else
         arch_elm_present="$arch_elm_present $ae"
      fi
   done
   debug1 "arch_elm_missing=$arch_elm_missing"
   debug1 "arch_elm_present=$arch_elm_present"

   # identify metrics to be addeed to listomissingparams
   declare -a lop
   local found
   local p
   IFS=' ' read -r -a lop <<< "$listoparams"
   for ae in $arch_elm_missing; do
      for p in ${lop[*]}; do
         found=`echo "$p"|egrep ${ARCELMNAME[$ae]}`
         if [ "$found" != "" ]; then
            listomissingparams="$listomissingparams $found"
         fi 
      done
   done
   # eliminate duplicates:
   listomissingparams=`echo "$listomissingparams"| awk '{for (i=1;i<=NF;i++) if (!a[$i]++) printf("%s%s",$i,FS)}{printf("\n")}'`
   debug1 "listomissingparams(updated)=$listomissingparams"
}

#####################################################
function read_oparam(){
#####################################################
# OUTPUT: $outv -- output value
#####################################################
   local v="$1"
   local f="$2"
   local pr="$3"
#   local pr1="$pr$v[ \t]"
   local pr0="$pr$v"
   local pr1="${pr0}[ \t]"
   local vcnt
   local vals

#debug2 "---------"
#debug2 "pr1=$pr1"

#      eval efnpattern="$FNPATTERN" #evaluated fn pattern
#debug2 "efnpattern='$efnpattern'"
#      fefnpat=`echo "$efnpattern"|sed 's/\+/\\\+/g'`
#debug2 "fefnpat='$fefnpat'"
#      parsepref="${fefnpat}"
#      parsetrailer1="${parsetrailer}:"
#debug2 "parsepref='$parsepref'"
#debug2 "parsetrailer1=$parsetrailer1"
#debug2 "OPARAMCOREUNIQUEID=$OPARAMCOREUNIQUEID"


#   outv=`awk '$0 ~ p {print $2}' p="$pr$v[ \t]" $f`
#   outv=`awk '$0 ~ p {print $2}' p="$pr1" $f`
#   outv=`awk '$0 ~ /p/ {print $2}' p="$pr1" $f`
######################################
#   outv=`awk "/$pr1/{print \\$2}" $f`
#   vcnt=`awk "/$pr1/{++count;}END{print count}" $f`
######################################
######################################
#   vcnt=`egrep "$pr1" $f|uniq|awk "/$pr1/{++count;}END{print count}"`
#   if [ "$vcnt" = "1" ]; then
##      outv=`awk "/$pr1/{print \\$2}" $f`
#      outv=`egrep "$pr1" $f|uniq|awk "/$pr1/{print \\$2}"`
#      debug3 "FOUND outv='$outv' for '$pr0'"
#   else
#      outv=`awk "/$pr1/{if (count==0) val=\\$2; ++count}END{print val}" $f`
#      vals=`awk "/$pr1/{print \\$2 \" \"}" $f|tr -d '\n'`
#      if [ "$vals" = "" ]; then
#         echo "ERROR: (read_oparam): no value while looking for param '$pr0' in '$f'"
#      else
#         echo "ERROR: (read_oparam): multiple values while looking for param '$pr0' in '$f': '$vals'"
#      fi
#      ERRCOUNT=`expr $ERRCOUNT + 1`
#   fi
######################################
   local outs
   if [ "$f" != "$FNCACHED" ]; then
      FNCACHED="$f"
      FCACHED=`cat $f`
   fi
#   outs=`awk "/$pr1/{if (count==0) val=\\$2; ++count}END{printf(\"%d %g\", count, val)}" $f`
#echo "egrep '$pr1' $f"
   outs=`echo "$FCACHED"|awk "/$pr1/{if (count==0) val=\\$2; ++count}END{printf(\"%d %g\", count, val)}"`
   vcnt=`echo "$outs"|awk '{print $1}'`
   outv=`echo "$outs"|awk '{print $2}'`
   if [ "$vcnt" = "1" ]; then
      debug3 "FOUND outv='$outv' for '$pr0'"
   else
      outv=`awk "/$pr1/{if (count==0) val=\\$2; ++count}END{print val}" $f`
      vals=`awk "/$pr1/{print \\$2 \" \"}" $f|tr -d '\n'`
      if [ "$vals" = "" ]; then
         [ "$SUPPRESSERROPARAMNOVALUE" = "0" ] && cecho "ERROR: (read_oparam): no value while looking for param '$pr0' in '$f'" "$red"
      else
         cecho "ERROR: (read_oparam): multiple values while looking for param '$pr0' in '$f': '$vals'" "$red"
      fi
      ERRCOUNT=`expr $ERRCOUNT + 1`
   fi
######################################
   
######################################
#debug2 "outv=$outv"
#debug2 "---------"
}

#####################################################
function std_dev() {
#####################################################
#OUTPUT: stddev
#
#####################################################
   local av="$1" # avg
   local ss="$2" # sum of squares
   local nn="$3" # N
   local nsquare # avg * avg * N
   local stddev0
   local stddev1
   local stddevp
   local n1      # N - 1
   local v1
   local v2
   [ "$nn" = "0" -o "$nn" = "1" ] && n1="1" || n1=`expr $nn - 1`
   [ "$av" = "0" ] && av="1"

   nsquare=`echo ""|awk '{printf("%5.8f",v1*v1*v2)}' v1="$av" v2="$nn" -`
   stddev0=`echo ""|awk '{printf("%5.8f",v1>v2?v1-v2:v2-v1)}' v1="$ss" v2="$nsquare" -`
   stddev1=`echo ""|awk '{printf("%5.2f",sqrt(v1/v2))}' v1="$stddev0" v2="$n1" -`
   # percent
#   [ "$av" = "0" ] && av="1"
#   stddevp=`echo ""|awk '{if (v3<1e-7) v3=1; printf("%4.1f%%",sqrt(v1/v2)/v3*100)}' v1="$stddev0" v2="$n1" v3="$av" -`
   stddevp=`echo ""|awk '{if (v3<1e-7) v3=1; printf("%4.1f%%",v1/v3*100)}' v1="$stddev1" v3="$av" -`
   stddev=`echo "$stddevp"|sed 's/^ *//'`
if [ "$debug" -gt "3" ]; then
   echo "nn=$nn  av=$av  stddev1=$stddev1  stddevp=$stddevp  ss=$ss  nsquare=$nsquare  stddev0=$stddev0"
fi
}


#####################################################
function initoparams(){
#####################################################
# OUTPUTS: listoparams listfoparams listomissingparams listoformulas listosummary
#####################################################
   local indse="$1"

   debug1 "initoparams::LISTOPARAMS=$LISTOPARAMS"
   debug1 "initoparams::outparams=$outparams"
   debug1 "initoparams::LISTOMISSINGPARAMS=$LISTOMISSINGPARAMS"

   listoparams="$LISTOPARAMS"
   listomissingparams="$LISTOMISSINGPARAMS"
   listoformulas="$LISTOFORMULAS"
   listosummary="$LISTOSUMMARY"
   if [ "$indse" = "indse" ]; then
      OPARAMCOREUNIQUEID="cpu.*type"
   else
      listoparams="$listoparams type+c+i+cc"
   fi

   # Update output parameter list (e.g., timer.cycles+m+i+cy)
   [ ! -z "$outparams" ] && listoparams="$listoparams $outparams"
   [ ! -z "$localoutparams" ] && listoparams="$listoparams $localoutparams"
   # eliminate duplicates
   listoparams=`echo "$listoparams"| awk '{for (i=1;i<=NF;i++) if (!a[$i]++) printf("%s%s",$i,FS)}{printf("\n")}'`

   # make a substitution of _ccc in order to be compatible with the eval function below
#   if [ "$indse" = "indse" ]; then
      listfoparams=`echo "$listoparams"|sed  's/_ccc/\\\\(\$ccc\\\\)?/g'`
#   else
#      listfoparams=`echo "$listoparams"|sed  's/_ccc/\\\\\\\\(\$ccc\\\\\\\\)?/g'`
#   fi

   debug1 "initoparams::localoutparams=$localoutparams"
   debug1 "initoparams::input0=$input0"

   # Update missing output parameter list (e.g., timer.kinstructions)
   [ ! -z "$outmissingparams" ] && listomissingparams="$listomissingparams $outmissingparams"
   [ ! -z "$localoutmissingparams" ] && listomissingparams="$listomissingparams $localoutmissingparams"
   # eliminate duplicates
   listomissingparams=`echo "$listomissingparams"| awk '{for (i=1;i<=NF;i++) if (!a[$i]++) printf("%s%s",$i,FS)}{printf("\n")}'`

   # Update output formula list (e.g., (dcrm+dcwm)/(dcr+dcw+0.00000000001),dcmr,a,f))
   [ ! -z "$outformulas" ] && listoformulas="$listoformulas $outformulas"
   [ ! -z "$localoutformulas" ] && listoformulas="$listoformulas $localoutformulas"
   # eliminate duplicates
   listoformulas=`echo "$listoformulas"| awk '{for (i=1;i<=NF;i++) if (!a[$i]++) printf("%s%s",$i,FS)}{printf("\n")}'`

   # Update output summary list (e.g., cycles=$cy+$sdcy))
   [ ! -z "$outsummary" ] && listosummary="$listosummary $outsummary"
   [ ! -z "$localoutsummary" ] && listosummary="$listosummary $localoutsummary"
   # eliminate duplicates
   listosummary=`echo "$listosummary"| awk '{for (i=1;i<=NF;i++) if (!a[$i]++) printf("%s%s",$i,FS)}{printf("\n")}'`

   debug1 "initoparams::listoparams=$listoparams"
   debug1 "initoparams::listfoparams=$listfoparams"
   debug1 "initoparams::listomissingparams=$listomissingparams"
   debug1 "initoparams::listoformulas=$listoformulas"
   debug1 "initoparams::listosummary=$listosummary"

}

#####################################################
function printstats(){
#####################################################
# OUTPUT: summ4 - a string with a printout of selected statistics
#
# INPUTS:  $1 - hsimtim
#          $2 - indse
# GLOBALS: FNPATTERN
#          PARSETRAILER
#          OPARAMCOREUNIQUED
#          MYMETRIC
#####################################################
   local hsimtim="$1"
   local indse="$2"
   local k
   local j
   local len
   local elem

   debug1 "------------ ENTERING function printstats()"

   oparampref="cpu"
   debug1 "printstats::OPARAMCOREUNIQUEID=$OPARAMCOREUNIQUEID"
   debug1 "printstats::listoparams=$listoparams"
   debug1 "printstats::listfoparams=$listfoparams"
   debug1 "printstats::listomissingparams=$listomissingparams"
   debug1 "printstats::listoformulas=$listoformulas"
   debug1 "printstats::listosummary=$listosummary"
   debug3 "printstats::FNPATTERN=$FNPATTERN"
   debug1 "printstats::MYMETRIC=$MYMETRIC"
   local mymetric1=`echo "${MYMETRIC^^}"|tr '.' '_ '`
   PUREPARAMETERS=""

   #
   declare -a gopp # param name
   declare -a gopt # type
   declare -a gopm # maximum
   declare -a gopa # average
   declare -a gops # std dev
   declare -a gopc # cumulative
   declare -a gopn # nickname
   declare -a gopf # format
   declare -a gopd # default value
   declare -a gopk # pref
   declare -a gfxe # equation
   declare -a gfxf # equation flag
   declare -a gfxu # equation has been used
   declare -A gpno # reverse lookup of name--> index
   declare -a gopskip # parameter must be skipped
   #declare -a gfxh # equation

   declare -a opv # param vector
   declare -a opp # param name
   declare -a opt # type
   declare -a opm # maximum
   declare -a opa # average
   declare -a ops # std dev
   declare -a opc # cumulative
   declare -a opn # nickname
   declare -a opf # format
   declare -a opd # default value
   declare -a opk # pref
   declare -a fxe # equation
   declare -a fxf # equation flag
   declare -a fxu # parameter has been used in an equation
   declare -A pno # reverse lookup of name--> index
   declare -a opskip # parameter must be skipped
   #declare -a fxh # equation

   #declare -a osm # equation
   #declare -a oss # equation
   #
   ### Parsing listosummary
   #k="0"
   #for elem in $listosummary; do
   #   len=$((${#elem}-1))
   #   lastchar="${elem:$len:1}"
   #   if [ "$lastchar" = "+" ]; then
   #      osm[$k]="${elem:0:$len}"
   #      oss[$k]="1"
   #   else
   #      osm[$k]="$elem"
   #   
   oss[$k]="0"
   #   fi
   #   k=$((${k}+1))
   #done

   debug1 "input0=$input0"
   
   ## Parsing listoparams: storing all raw parameters in a global list
   ## Parsing listfoparams: storing all filtered parameters in a global list
   k="0"
   debug1 "* Storing a list of PURE parameters..."
   IFS=' ' read -r -a rpl <<< "$listoparams"  # raw param list (-r preserves the backslashes)
   IFS=' ' read -r -a fpl <<< "$listfoparams" # filtered param list
   local nrpl="${#rpl[@]}" # no. of elem in rpl
   local nfpl="${#fpl[@]}" # no. of elem in fpl (should be the same as in rpl)
   local skipflag
   for ((kp=0;kp<$nrpl;kp++)); do
      psr="${rpl[$kp]}"                  # param string raw
      psf="${fpl[$kp]}"                  # param string filtered
      IFS='+' read -r -a opu <<< "$psr"  # out param vector-u (raw)
      IFS='+' read -r -a opv <<< "$psf"  # out param vector-v (filt)
      pnr="${opu[0]}"                    # param name raw
      pnf="${opv[0]}"; pnick="${opu[3]}" # param name,nick filtered
      debug3 "-- pnr=$pnr pnf=$pnf pnick=$pnick"
      skipping=`echo "$pnr"|sed -e 's/\^/\\\^/g' -e 's/\+/\\\+/g'`
      misscheck=`echo "$listomissingparams"|egrep "$skipping"`
      if [ "$misscheck" != "" ]; then gopskip[$k]="1"; else gopskip[$k]="0"; fi
      # assert: the parameter is not in the listomissinglist

      # store the parameter
      local opn1="${opv[3]^^}"
      gopk[$k]="0"
      gopp[$k]=${opv[0]} ; [ "${opv[0]:0:1}" = "^" ] && gopp[$k]=${opv[0]:1} && gopk[$k]="1"
      gopt[$k]=${opv[1]}
      gopf[$k]=${opv[2]}
      gopn[$k]=${opv[3]^^}
      gopd[$k]=${opv[4]}
      gopm[$k]="0"
      gopa[$k]="0"
      gopc[$k]="0"
      gops[$k]="0"
      gfxe[$k]=""
      gfxf[$k]="0"
      gfxu[$k]="0"
      gpno[$opn1]="$k"
#      if [ "${gopd[$k]}" != "" ]; then gopskip[$k]="0"; fi # don't skip if there is a default value
      [ "${gopskip[$k]}" = "1" ] && skipflag=" (SKIP)" || skipflag=""
      debug3 "$k$skipflag -- ${gopp[$k]} -- ${gopt[$k]} -- ${gopf[$k]} -- ${gopn[$k]} --  ${gopd[$k]}"
      k=`expr $k + 1`
   done
   gnoparams="$k"
   debug1 "- STORED PURE PARAMETERS gnoparams=$gnoparams"

   debug1 "* Storing a list of FORMULAS..."
   ## Parsing listoformulas: storing all equations in a global list
   ## note: the order of this formula is important: each formula is using
   ## symbols or formulas that are defined before
   j="0"
   for elem in $listoformulas; do
      k=`expr $gnoparams + $j`
      IFS=',' read -a fv <<< "$elem"
      local opn1="${fv[1]^^}"
      gfxe[$k]="${fv[0]^^}" # equation
      gfxf[$k]="1" # equation flag
      gfxu[$k]="0" # equation is not yet used
      gopn[$k]=${fv[1]^^} # parameter
      gopt[$k]=${fv[2]} # type
      gopf[$k]=${fv[3]} # format
      gopd[$k]=${fv[4]} # default value
      gopm[$k]="0"
      gopa[$k]="0"
      gopc[$k]="0"
      gops[$k]="0"
      gpno[$opn1]="$k"
      # check if the formula output-parameter is a name already in the parsed output-parameters
      p="0"
      for ((p=0;p<=gnoparams;p++)); do
         [ "${gopn[$k]}" = "${gopn[$p]}" ] && gfxf[$p]="1"
      done
      debug3 "$j -- ${gfxe[$k]} -- ${gopn[$k]}"  # -- ${gfxh[$j]}"
      j=`expr $j + 1`
   done
   gnoformulas="$j"
   debug1 "- STORED FORMULAS gnoformulas=$gnoformulas"

   ## Parsing listfoparams for selecting pure parameters
   k="0"
   debug1 "* Selecting pure parameters (mymetric1='$mymetric1')..."
#   for elem1 in $listfoparams; do
   for ((kp=0;kp<$nrpl;kp++)); do
#      IFS='+' read -r -a opv <<< "$elem1" # -r preserves the backslashes
#      local opn1="${opv[3]^^}"
      local opn1="${gopn[$kp]}"
      local opskip1="${gopskip[$kp]}"
      debug4 "opn1=$opn1 opt1=$opt1 opf1=$opf1 opn1=$opn1 opskip1=$opskip1"
      if [ \( "$opn1" = "$mymetric1" -o "$mymetric1" = "" \) -a \( "$opskip1" = "0" -o \( "$opskip1" = "1" -a "${gopd[$kp]}" != "" \) \) ]; then
         opk[$k]=${gopk[$kp]}
         opp[$k]=${gopp[$kp]}
         opt[$k]=${gopt[$kp]}
         opf[$k]=${gopf[$kp]}
         opd[$k]=${gopd[$kp]}
         opn[$k]=${gopn[$kp]}
         opm[$k]=${gopm[$kp]}
         opa[$k]=${gopa[$kp]}
         opc[$k]=${gopc[$kp]}
         ops[$k]=${gops[$kp]}
         opskip[$k]=${gopskip[$kp]}
         fxe[$k]=${gfxe[$kp]}
         fxf[$k]=${gfxf[$kp]}
         fxu[$k]=${gfxu[$kp]} # parameter is used
         pno[$opn1]="$k"
         debug3 "$k -- ${opp[$k]} -- ${opt[$k]} -- ${opf[$k]} -- ${opn[$k]} -- ${opd[$k]}"
         [ -z "$PUREPARAMETERS" ] && PUREPARAMETERS="${opp[$k]}" || PUREPARAMETERS="$PUREPARAMETERS ${opp[$k]}"
         k=`expr $k + 1`
      fi
   done
   noparams="$k"
   debug1 "- SELECTED PURE PARAMETERS noparams=$noparams"

   # add those parameters that are in formulas
   if [ "$mymetric1" != "" ]; then
      debug1 "* Analyzing FORMULAS to find dependent pure parameters..."
      formu3=""; opn2="$mymetric1"; formu4="$opn2"
      while [ "$opn2" != "" ]; do   
         y="${gpno[$opn2]}"
         debug4 "y=gpno[$opn2]='$y'"
         if [ "$y" != "" ]; then opn1="${gopn[$y]}"; else opn1=""; opn2=""; fi
         debug4 "opn1=$opn1 opn2=$opn2"
         if [ "$opn1" = "$opn2" -a "$opn1" != "" ]; then
            formu1="${gfxe[$y]}" # equation
            formu2=`echo "$formu1"|sed 's/[-()\+\/\*\^]\|[-\+]\?[^a-zA-Z][0-9]*\.\?[0-9]\+\([eE][-\+]\?[0-9]\+\)\?/ /g'`
            gfxu[$y]="1"
            debug3 "formu1=$formu1"
            debug3 "formu2=$formu2"
            if [ ! -z "$formu2" ]; then for z1 in $formu2; do
               z="${z1^^}"
               found="0"
               debug4 "noparams=$noparams  z=$z  pno[$z]=${pno[$z]} opn[${pno[$z]}]=${opn[${pno[$z]}]}"
               if [ "$noparams" -gt "0" ]; then # already in selected params?
                  if [ "${opn[${pno[$z]}]}" = "$z" ]; then
                     found="1"
                  fi
               fi
               if [ "$found" = "0" ]; then # dep. param. has to be inserted in selected pure param. list
                  found="0"
                  if [ "$gnoparams" -gt "0" ]; then
                     x="${gpno[$z]}"
                     if [ "${gopn[$x]}" = "$z" ]; then
                        found="1"
                     fi
                     debug4 "inserting x=$x z=$z gopn[$x]=${gopn[$x]} found=$found gfxf[$x]=${gfxf[$x]} pno[$z]=${pno[$z]} opn[${pno[$z]}]=${opn[${pno[$z]}]}"
                     if [ "$found" = "1" -a "${gfxf[$x]}" = "0" ]; then
#                        if [ "$gopskip[$x]}" = "1" ]; then # param to be skipped
#                           #inherit skip property
#                        else
                           debug4 "found $x in stored (${gopn[$x]}), inserting in $k..."
                           opk[$k]="${gopk[$x]}"
                           opp[$k]="${gopp[$x]}"
                           opt[$k]="${gopt[$x]}"
                           opf[$k]="${gopf[$x]}"
                           opd[$k]="${gopd[$x]}"
                           opn[$k]="${gopn[$x]}"
                           opm[$k]="${gopm[$x]}"
                           opa[$k]="${gopa[$x]}"
                           opc[$k]="${gopc[$x]}"
                           ops[$k]="${gops[$x]}"
                           opskip[$k]="${gopskip[$x]}"
                           fxe[$k]="${gfxe[$x]}"
                           fxf[$k]="${gfxf[$x]}"
                           fxu[$k]="1" # parameter is used
                           pno[$z]="$k"
                           debug3 "$k -- ${opp[$k]} -- ${opt[$k]} -- ${opf[$k]} -- ${opn[$k]} -- ${opd[$k]}"
                           [ -z "$PUREPARAMETERS" ] && PUREPARAMETERS="${opp[$k]}" || PUREPARAMETERS="$PUREPARAMETERS ${opp[$k]}"
                           k=`expr $k + 1`
                           noparams="$k"
#                        fi
                     fi
                     if [ "$found" = "1" -a "${gfxf[$x]}" = "1" ]; then
                        [ -z "$formu3" ] && formu3="$z" || formu3="$formu3 $z"
                     fi
                  fi
               fi
            done; fi
            debug3 "formu3=$formu3"
            [ "$formu3" != "" ] && { opn2=`echo "$formu3" | awk '{print $1}'`;  } || { opn2=""; }
            formu3=`echo "$formu3"|awk '{$1=""; print $0}'`
            formu4="$formu4 $opn2"
            debug3 "opn2='$opn2'   formu3='$formu3'"
         fi
      done # while
      noparams="$k"
      debug1 "- FROM FORMULAS: noparams=$noparams"
      debug1 "- PUREPARAMETERS=$PUREPARAMETERS"
   fi
   if [ "$indse" = "getpureparameters" ]; then return; fi 

   ## Parsing listoformulas in the correct order for the evalation
   debug1 "* Selecting formulas (mymetric1='$mymetric1' formu4='$formu4')..."
   j="0"
   if [ "$formu4" != "" -o "$mymetric1" = "" ]; then for ((f1=0;f1<gnoformulas;f1++)); do
      f2=`expr $gnoparams + $f1`
      if [ "${gfxu[$f2]}" = "1" -o "$mymetric1" = "" ]; then
         opn2="${gopn[$f2]}"
         k=`expr $noparams + $j`
         x="${gpno[$opn2]}"
         local opn1="${gopn[$x]}"
         if [ "$opn1" = "$opn2" -o "$mymetric1" = "" ]; then
                        debug4 "found $x in stored (${gopn[$x]}), inserting in $k..."
                        opk[$k]="${gopk[$x]}"
                        opp[$k]="${gopp[$x]}"
                        opt[$k]="${gopt[$x]}"
                        opf[$k]="${gopf[$x]}"
                        opd[$k]="${gopd[$x]}"
                        opn[$k]="${gopn[$x]}"
                        opm[$k]="${gopm[$x]}"
                        opa[$k]="${gopa[$x]}"
                        opc[$k]="${gopc[$x]}"
                        ops[$k]="${gops[$x]}"
                        opskip[$k]="${gopskip[$x]}"
                        fxe[$k]="${gfxe[$x]}"
                        fxf[$k]="${gfxf[$x]}"
            # check if the formula output-parameter is a name already in the parsed output-parameters
            p="0"
            for ((p=0;p<=noparams;p++)); do
               [ "${opn[$k]}" = "${opn[$p]}" ] && fxf[$p]="1"
            done
            debug3 "$j -- ${fxe[$k]} -- ${opn[$k]}"  # -- ${fxh[$j]}"
            j=`expr $j + 1`
         fi
      fi
   done; fi
   noformulas="$j"
   debug1 "noformulas=$noformulas"

   #
   goodfiles="0"
   totcores="0"
   debug2 "${DSE_THISTOOL}.sh:printstats:nodes=$nodes"
   local nnodes=`echo "$nodes"|sed 's/^0*//g'`
   debug2 "${DSE_THISTOOL}.sh:printstats:nnodes=$nnodes"
   funnodes $nodes; nodes="$RETVAL"
   for ((nnn=1;nnn<=nnodes;nnn++)); do
      funnodes $nnn; nnn1="$RETVAL"

      if [ "$indse" = "indse" ]; then
         buildfn1 $hsimtim $nnn
         tfile="$OUTDIR/$fnout"
         parsepref=""
         PARSETRAILER=""
      else
         tfile="$indse"
         eval efnpattern="$FNPATTERN" #evaluated fn pattern
         debug3 "efnpattern=$efnpattern"
         fefnpat=`echo "$efnpattern"|sed 's/\+/\\\+/g'`
         debug3 "fefnpat=$fefnpat"
         parsepref="${fefnpat}"
#         parsetrailer1="$nnn\.${PARSETRAILER}:"
         parsetrailer1="\.$nnn\.${PARSETRAILER}:"
      fi
      debug2 "file check1 tfile=$tfile"
      if [ -s "$tfile" ]; then
         goodfiles=`expr $goodfiles + 1`
         # ncores=`awk '/cpu.*type/{++m}END{print m}' $tfile`
         debug3 "parsepref='$parsepref'"
         debug3 "parsetrailer1=$parsetrailer1"
         debug3 "OPARAMCOREUNIQUEID=$OPARAMCOREUNIQUEID"
#         ncores=`awk "/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}" $tfile`
         debug3 "egrep \"$parsepref$parsetrailer1$OPARAMCOREUNIQUEID\" $tfile|uniq|awk \"/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}\""
         ncores=`egrep "$parsepref$parsetrailer1$OPARAMCOREUNIQUEID" $tfile|uniq|awk "/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}"`
         debug2 "ncores=$ncores"
#         ncores=`awk "/$parsepref${parsetrailer1}cpu.*type/{++m}END{print m}" $tfile`
#debug2 "ncores=$ncores"
         if ! [[ "$ncores" =~ ^[0-9]+$ ]] ; then ncores="0"; fi
#echo "ncores=$ncores"
         totcores=`expr $totcores + $ncores`
      fi
   done

   # sanity check
   [ "$totcores" = "0" ] && totcores="1"
   debug1 "Goodfiles=$goodfiles totcores=$totcores noparams=$noparams nodes=$nodes cores=$cores nnodes=$nnodes ncores=$ncores"
   debug2 "================"

   #
   debug2 "${DSE_THISTOOL}.sh:printstats:nodes=$nodes"
   debug2 "${DSE_THISTOOL}.sh:printstats:nnodes=$nnodes"
   funnodes $nodes; nodes="$RETVAL"
   for ((nnn=1;nnn<=nnodes;nnn++)); do
      funnodes $nnn; nnn1="$RETVAL"
      if [ "$indse" = "indse" ]; then
         buildfn1 $hsimtim $nnn
         tfile="$OUTDIR/$fnout"
         parsepref=""
         PARSETRAILER=""
      else
         tfile="$indse"
         eval efnpattern="$FNPATTERN" #evaluated fn pattern
#debug2 "efnpattern='$efnpattern'"
         fefnpat=`echo "$efnpattern"|sed 's/\+/\\\+/g'`
#debug2 "fefnpat='$fefnpat'"
         parsepref="${fefnpat}"
#         parsetrailer1="$nnn\.${PARSETRAILER}:"
         parsetrailer1="\.$nnn\.${PARSETRAILER}:"
      fi
      debug2 "file check2 tfile=$tfile"
      if [ -s "$tfile" ]; then
#debug2 "parsepref='$parsepref'"
#debug2 "parsetrailer1=$parsetrailer1"
#debug2 "OPARAMCOREUNIQUEID=$OPARAMCOREUNIQUEID"
#         ncores=`awk "/$OPARAMCOREUNIQUEID/{++m}END{print m}" $tfile`
         debug2 "parsepref $parsepref parsetrailer1 $parsetrailer1 OPARAMCOREUNIQUEID $OPARAMCOREUNIQUEID"
#         ncores=`awk "/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}" $tfile`
         debug2 "awk \"/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}\" $tfile"
#         ncores=`awk "/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}" $tfile 2>/dev/null`
         ncores=`egrep "$parsepref$parsetrailer1$OPARAMCOREUNIQUEID" $tfile|uniq|awk "/$parsepref$parsetrailer1$OPARAMCOREUNIQUEID/{++m}END{print m}"`
#         ncores=`awk "/$parsepref${parsetrailer1}cpu.*type/{++m}END{print m}" $tfile`
         if ! [[ "$ncores" =~ ^[0-9]+$ ]] ; then ncores="0"; fi
###   cores=`echo "$cores"|sed 's/^0*//g'`
         ncores=`echo "$ncores"|sed 's/^0*//g'`
         debug2 "nnn=$nnn : ncores=$ncores"
         ###funcores $ncores; ncores1="$RETVAL"
         ###debug3 "ncores1=$ncores1"
         funcores $ncores; cores="$RETVAL"; debug3 "---"; debug3 "ncores=$ncores cores=$cores"

         for ((ccc=0;ccc<ncores;ccc++)); do
            ###funcores $ccc; ccc1="$RETVAL"
            ###funcores $ccc; cores="$RETVAL"; debug3 "---"; debug3 "ccc=$ccc  cores=$cores"
            i=`expr \( $nnn - 1 \) \* $ccc`
            awks=""
            debug3 "********** VALUES ********* (nnn=$nnn,ccc=$ccc,nodes=$nodes,cores=$cores,noparams=$noparams)"
            for ((k=0;k<noparams;k++)); do
               ppp="${opn[$k]}"
#               [ "${opk[$k]}" = "0" ] && ppprrr="$oparampref$ccc" || ppprrr=""
               [ "${opk[$k]}" = "0" ] && ppprrr="$oparampref${ccc}." || ppprrr=""
               debug3 "----------------------- ppprrr=$ppprrr opd=${opd[$k]} opskip=${opskip[$k]}"

               # check if the parameter needs to be skipped and we have a default value
               if [ "${opd[$k]}" != "" -a "${opskip[$k]}" = "1" ]; then
                  ttt="${opd[$k]}" # in such case, assign default value
               else
                  if [ "$indse" = "indse" ]; then
                     ppprrr1="${ppprrr}"
                  else
                     eval efnpattern="$FNPATTERN" #evaluated fn pattern
   #debug2 "efnpattern='$efnpattern'"
                     fefnpat=`echo "$efnpattern"|sed 's/\+/\\\+/g'`
   #debug2 "fefnpat='$fefnpat'"
                     parsepref="${fefnpat}"
                     parsetrailer1="\.$nnn\.${PARSETRAILER}:"
                     ppprrr1="$parsepref$parsetrailer1${ppprrr}"
   #debug2 "parsepref='$parsepref'"
   #debug2 "parsetrailer1=$parsetrailer1"
   #debug2 "OPARAMCOREUNIQUEID=$OPARAMCOREUNIQUEID"
                  fi
   #echo "ppprrr1=$ppprrr1"
                  debug3 "ppprrr1=$ppprrr1"
                  # Function read_oparam outputs a value in $outv
                  outv=""
                  debug3 "opp[$k]=${opp[$k]}"
                  if [ "${opp[$k]}" = "nan" -o "${opp[$k]}" = "-nan" ]; then
                     oppval="${opp[$k]}"
                  else
                     eval oppval="${opp[$k]}"  # TODO: check if the value 'nan' creates problems
                  fi
   #               eval oppval="${opp[$k]}"
   #echo "oppval=$oppval"
                  debug3 "oppval=$oppval"
                  ############################ read_oparam ###############################
   #               [ "${fxf[$k]}" = "0" ] && read_oparam "${opp[$k]}" "$tfile" "$ppprrr1"
                  [ "${fxf[$k]}" = "0" ] && read_oparam "$oppval" "$tfile" "$ppprrr1"
                  ########################################################################
                  debug3 "outv(n${nnn}c$ccc)=$outv  pr=$ppprrr1  opp=${opp[$k]}"
   #debug2 "outv(n${nnn}c$ccc)=$outv  pr=$ppprrr1  opp=${opp[$k]}"
   #               case ${opt[$k]} in
   #                 m)
   #      if [ "$outv" != "" ]; then [ "$outv" -gt "${opm[$k]}" ] && opm[$k]="$outv"; fi
   #      ops[$k]=`echo ""|awk '{printf("%5.8f",v1+v2*v2)}' v1="${ops[$k]}" v2="$outv" -`
   #      opc[$k]=`echo ""|awk '{printf("%5.8f",v1+v2)}' v1="${opc[$k]}" v2="$outv" -`
   #                    ;;
   #                 a)
   #      ops[$k]=`echo ""|awk '{printf("%5.8f",v1+v2*v2)}' v1="${ops[$k]}" v2="$outv" -`
   #      opc[$k]=`echo ""|awk '{printf("%5.8f",v1+v2)}' v1="${opc[$k]}" v2="$outv" -`
   #                    ;;
   #                 c)
   #      opc[$k]=`echo ""|awk '{printf("%5.8f",v1+v2)}' v1="${opc[$k]}" v2="$outv" -`
   #                    ;;
   #                 *) echo "ERROR in computing output metrics" ;;
   #               esac
   #               if [ "$outv" != "" ]; then [ "$outv" -gt "${opm[$k]}" ] && opm[$k]="$outv"; fi
                  [ "$outv" != "" ] && opm[$k]=`echo ""|awk '{printf("%5.8f",v1>v2?v1:v2)}' v1="$outv" v2="${opm[$k]}" -`
                  ops[$k]=`echo ""|awk '{printf("%5.8f",v1+v2*v2)}' v1="${ops[$k]}" v2="$outv" -`
                  opc[$k]=`echo ""|awk '{printf("%5.8f",v1+v2)}' v1="${opc[$k]}" v2="$outv" -`
                  debug4 "VALUE: k=$k  outv='$outv'   opm[$k]=${opm[$k]}   opc[$k]=${opc[$k]}   opn[$k]=${opn[$k]}"
                  if [ "$outv" = "nan" -o "$outv" = "-nan" ]; then
                     eval $ppp="$outv"
                  else
                     eval $ppp="$outv"  # TODO: check if the value 'nan' creates problems
   # to verify the content of $ppp
                     eval ttt=\$$ppp
                  fi
   #               eval $ppp="$outv"
   ## to verify the content of $ppp
   #               eval ttt=\$$ppp
               fi
               debug3 "VALUE: $ppp=$ttt"
               # the following line is what works for progressively evaluating
               # variables based on previous variables (the eval statement works but
               # it's not useful because we use awk)
               [ "$awks" = "" ] && awks="$ppp=$outv" || awks="$awks $ppp=$outv"
            done
            debug3 "********** FORMULAS ********* (nnn=$nnn,ccc=$ccc,noformulas=$noformulas,noparams=$noparams)"
            debug4 "awks='$awks'"
            # undef previous vals
            for ((k=noparams;k<noformulas+noparams;k++)); do
               ppp="${opn[$k]}"
               eval unset $ppp
            done
            for ((k=noparams;k<noformulas+noparams;k++)); do
               vvv="${fxe[$k]}"
               debug4 "vvv='$vvv'"
               outv=`echo ""|awk "{printf(\"%5.8f\",$vvv)}" $awks -`
               ppp="${opn[$k]}"
               debug4 "FORMULA: k=$k  vvv=$vvv   outv=$outv   ppp=$ppp"
               if [ "$outv" = "nan" -o "$outv" = "-nan" ]; then
                  eval $ppp="$outv"
               else
                  eval $ppp="$outv"  # TODO: check if the value 'nan' creates problems
# to verify the content of $ppp
                  eval ttt=\$$ppp
               fi
               debug3 "FORMULA: $ppp=$ttt"
               # the following line is what works for progressively evaluating
               # variables based on previous variables (the eval statement works but
               # it's not useful because we use awk
               [ "$awks" = "" ] && awks="$ppp=$outv" || awks="$awks $ppp=$outv"
#      if [ "$outv" != "" ]; then [ "$outv" -gt "${opm[$k]}" ] && opm[$k]="$outv"; fi
               if [ "$outv" != "" ]; then opm[$k]=`echo ""|awk '{printf("%5.8f",v1>v2?v1:v2)}' v1="$outv" v2="${opm[$k]}" -`; fi
               ops[$k]=`echo ""|awk '{printf("%5.8f",v1+v2*v2)}' v1="${ops[$k]}" v2="$outv" -`
               opc[$k]=`echo ""|awk '{printf("%5.8f",v1+v2)}' v1="${opc[$k]}" v2="$outv" -`
            done
         done
      fi
   done

   #
   debug3 "********** AVERAGES,CUMULATIVES,MAXES *********"
   debug3 "================"
   for ((k=0;k<noparams+noformulas;k++)); do
      ppp="${opn[$k]}"
      eval unset $ppp
      debug4 "VARIABLE: ppp=$ppp"
      qqq="sd${opn[$k]}"
#echo "ppp=$ppp qqq=$qqq"
      thev="0"; stddev="0.0%"
 
      # Calculate the average: opa=opc/n
      opa[$k]=`echo ""|awk '{printf("%5.8f",v1/v2)}' v1="${opc[$k]}" v2="$totcores" -`
      # Calculate the relative-standard-deviation(percentage) or variation-factor: 
      # stddev=sqrt((ops-n*opa*opa)/(n-1))/opa   (n=totcores)
      std_dev "${opa[$k]}" "${ops[$k]}" "$totcores"

      case ${opt[$k]} in
         m) thev="${opm[$k]}"
#      std_dev "$thev" "${ops[$k]}" "$totcores"
#echo "${opc[$k]} -- ${opa[$k]} -- $stddev -- $totcores"
         ;;
         a) thev="${opa[$k]}"
#      std_dev "$thev" "${ops[$k]}" "$totcores"
#echo "${opc[$k]} -- ${opa[$k]} -- $stddev -- $totcores"
         ;;
         c) thev="${opc[$k]}"
         ;;
         *) echo "ERROR: unknown type for parameter '$ppp'"
         ;;
      esac
      case ${opf[$k]} in
         i) vtmp=`echo ""|awk '{printf("%-d",v1 + 0)}' v1="$thev" -`
           ;;
         f) vtmp=`echo ""|awk '{printf("%-5.8f",v1)}' v1="$thev" -`
            vtmp=`echo ""|awk '{printf("%-s",v1)}' v1="$vtmp" -`
           ;;
         *) vtmp="0"; echo "ERROR: unknown format for parameter '$ppp'" ;;
      esac
      eval $ppp="$vtmp"
#      eval $qqq=\$$stddev ### DOES NOT WORK
      export $qqq="$stddev"
      debug3 "$thev -- $ppp -- $vtmp -- $stddev -- ${opt[$k]},${opf[$k]} -- ${opp[$k]}"
      debug3 "================"
   done
   #

   ## Printing listosummary
   k="0"; summ4=""
   for elem in $listosummary; do
      len=$((${#elem}-1))
      lastchar="${elem:$len:1}"
      if [ "$lastchar" = "+" ]; then
         varn="${elem:0:$len}"
         varo="1"
      else
         varn="$elem"
         varo="0"
      fi
      varn=`echo "$varn"|sed 's/_ccc/_c/g'`
      varn1=${varn^^}

      eval varm=\$$varn1
      debug3 "elem=$elem varn=$varn varm=$varm"
      if [ "$k" != "0" ]; then summ4="$summ4 "; fi
      summ4="$summ4$varn=$varm"
      if [ "$varo" != "0" ]; then
         eval vars=\$sd$varn1
         summ4="${summ4}+${vars}"
      fi

      k=$((${k}+1))
   done

   debug1 "------------ EXITING function printstats()"
   debug3 "summ4=$summ4"
}

#####################################################
function setsimfilenamesdef(){
#####################################################
   copyfromsandbox="1"
   GUESTSCRIPTTYPE="builtin"
   INPUTFILTERTYPE="builtin"
#set some DEFAULT names for SIMULATOR FILES
   simsrcpref="$SIMBASE"
   simsrcbranch="/$DEFAULTSIMBRANCH"
   simexesuff="$simsrcbranch/$DEFAULTSIMPATH"
   CBACKPREF="$simsrcpref$simsrcbranch"
   CBACKLIB="/$DEFAULTSIMLIB"
   luafile="$luafile1"
   runmode="$runmode1"
   stdoutpref="node."
   timersuff=".${RUNID}.log"
   screenpref="\$SOUTF0"
   screensuff="" #n/a
   nodescriptnum=""
   nodescriptoutpref="" #n/a
   nodescriptoutnum=""
   nodescriptounewf="" #n/a
   nodescriptoutosuff="" #n/a

   stdounewf=".stdout.log"
   stdoutosuff=".stdout.log"
   stdoutnum="x"
   timerosuff=".timer.log"
   timernum="x"
   nodescripnewf="" #n/a
   nodescriptosuff=".nodescript.sh"
   threadstatpref="$RUNID"
   threadstatnum="z"
   threadstat1pref="$RUNID"
   threadstat1num=""
   tsumonpref=""
   tsumonnum=""
   tsumonsuff=""
   tsumonosuff=""
   screennum="x"
   screenosuff=".screen.log"
   ctrlpref="$RUNID"
   ctrlsuff="-ctrl"
#   jobwpref="`mktemp /tmp/${DSE_THISTOOL}outXXXXXX`" # cannot work as it is in the simloop
#   jobwpref="OUT1"
   simcfgpref="\$SIMCONFIG0"
   simcfgsuff=""
   simcfgnum=""
   simcfgosuff="lua"
   simmkfpref="\$SIMMAKEF0"
   simmkfsuff=""
   simmkfnum=""
   simmkfosuff="make"
   soutpref="\$SOUTF0"
   soutsuff=""
   soutnum=""
   soutosuff="out"
   jobwpref="\$OUTFILE0"
   jobwsuff=""
   jobwnum=""
   simswpref="${RUNID}-\${COTSONPID[\$nnnnn]}-node-\$nnnnn"
   simswsuff="/data/screen.log"
   simswnum=""
   simlogpref="${RUNID}-\${COTSONPID[\$nnnnn]}-node-\$nnnnn"
   simlogsuff="/data/stdout.log"
   simlognum=""
   simlogosuff="log"
   simstxtdbpref="$SANDBOX/data/"
   simstxtdbsuff="database"
   simstxtdbnum=""
   simstxtdbosuff=""
   simmtxtdbpref="$SANDBOXMED/data/"
   simmtxtdbsuff="database"
   simmtxtdbnum=""
   simmtxtdbosuff=""
   simdbpref="\$DBOUT0"
   simdbsuff=""
   simdbosuff="db"

if [ "$nodes" -gt "1" ]; then
   luafile="$luafile2"
   runmode="$runmode2"
   stdoutpref="${RUNID}.node."
   timersuff=".timer.log"
   screenpref=$stdoutpref
   screensuff=".screen.log"
   nodescriptnum="c"
   nodescriptoutpref=$stdoutpref
   nodescriptoutnum="x"
   nodescriptounewf=".node_config.log"
   nodescriptoutosuff=".node_config.log"
fi
   timerpref=$stdoutpref

   exectracename="exectrace"
   exectracepref="$SANDBOX/share/$exectracename"
   exectracesuff=".txt.gz"
   exectracenum=""
   exectraceosuff=".cpt.txt.gz"
   #CPTRACE
   cptracename="cptrace"
   cptracepref="$SANDBOX/share/$cptracename"
   cptracesuff=".txt.gz"
   cptracenum=""
   cptraceosuff=".cpt.txt.gz"

   #METRACE
   metracename="metrace"
   metracepref="$SANDBOX/share/$metracename"
   metracesuff=".txt.gz"
   metracenum=""
   metraceosuff=".met.txt.gz"

   #STTRACE
   sttracename="sttrace"
   sttracepref="$SANDBOX/share/$sttracename"
   sttracesuff=".txt.gz"
   sttracenum=""
   sttraceosuff=".stt.txt.gz"
}

#####################################################
function loadstdmodel(){
#####################################################
# PURPOSE: load the 'standard' (std) execution model
#####################################################
   MODELNAME="std"
   luafile1="std.lua"
   luafile2="std.lua"
   runmode1="run"
   runmode2="run"
   RUNID="std"
   LUARUNID="XRUNID"
   LUANNODES="NNODES"
   masternodesig="Starting master node"

   prependstdoutlog="1"
   roiexternalandmakefile="0"
   roiexternalandrscript="0"
   rscript=""
   updatemakefile="1"
   updaterscript="0"
   logdir=""
   nodebase="1"
   SANDBOX="${RUNID}-\${COTSONPID[\$nnnnn]}-node-\$nnnnn"
   SANDBOXMED="${RUNID}-\${COTSONPID[\$nnnnn]}-med"
   setsimfilenamesdef
   setclusternodes="1"
   classicroi="0"
   threadstats="0"
   stdounewf="stdout.log"
   stdoutnum="c"
   timernum="c"

   dontchecksucc="1"
   setcontrolscript="1"
   setclusterunconditionally="1"
   copyfromsandbox="0"
   nodeconfigfn="/bin/true"
   nodeconfigcomm=" --all done in the 'SCRIPT'"
   stdoutpref="$SANDBOX/data/"
   #    stdounewf="stdout.log"
   #		stdoutnum="x"
   simlognum="1"
   nodescriptpref="${RUNID}-\${COTSONPID[\$nnnnn]}-node-\$nnnnn/data/user_script"
   timerpref="$SANDBOX/data/"
   timersuff="timer.log"
   timernum=""
   medtimerpref="$SANDBOXMED/data/"
   medtimersuff="timer.log"
   medtimerosuff="mediator.timer.log"
   medoutpref="$SANDBOXMED/data/"
   medounewf="mediator.log"
   medoutosuff="mediator.stdout.log"
   nodescriptoutpref="${RUNID}.node."
   nodescriptoutnum="x"
   nodescriptounewf=".node_config.log"
   nodescriptoutosuff=".node_config.log"
   screenpref="$SANDBOX/data/" 
   screensuff="screen.log"
   screennum=""
}

#####################################################
function selectmodeldriver(){
#####################################################
   local verbmode="$1"
   local md mdselected lmlist
   local lmu=`echo $model|tr '[:lower:]' '[:upper:]'`
   local lms=`echo $model|tr '[:upper:]' '[:lower:]'`
   local modirexists
   local wodirexists="0"

   # Sanity checks
   [ -z "$nodes" ] && nodes="1"
   [ -z "$model" ] && cecho "ERROR: selectmodeldriver: model not set!" "$red"
#   local modeldriver="${DSEDRIVERPREF}-${model}.sh"
   MODELDRIVER="${DSEDRIVERPREF}-${model}.sh"

   # Load model-driver
   MODELNAME=""
   local modeldriverfound="0"

   # Sanity check
   if [ ! -d "$MYBASEDIR" ]; then return; fi

   # List of search directories where to look up for the DSEDRIVER
   local lmlist="$SCRIPTPATH $BASEDIR $MYBASEDIR/$lmu $MYBASEDIR/$lms $MYBASEDIR/${model}-${MYSIMVER}"


   # Looking for the MODELDIR directory
   # Hypothesis: the MODELDIR contains the MODELDRIVER file
   modirexists="0" 
   for md in $lmlist; do
      debug1 "Checking out DSEDRIVER ($MODELDRIVER)) in $md ..."
      if [ -d "$md" ]; then 
         modirexists="1"
         if [ -s "$md/$MODELDRIVER" ]; then
            MODELDIR[$model]="$md"
            mdselected="$md"
            modeldriverfound="1"
            debug1 "   --> $MODELDRIVER found!"
         fi
      fi
   done
   # ERROR CHECKING
   debug1 "modirexists=$modirexists"
   if [ "$modirexists" = "0" -a "$NORUN" = "0" ]; then
      echo "ERROR: cannot find MODELDIR[$model]='${MODELDIR[$model]}'"
      [ -z "${MODELDIR[$model]}" -o -d "${MODELDIR[$model]}" ] \
         || echo "  ERROR: MODELDIR[$model]='${MODELDIR[$model]}' not found."
      [ "$model" != "$DEFAULTMODEL" ] \
         && echo "You may miss the DSEDRIVER for '$model' model or '$model' model is not installed."
      exit 2
   fi
   # Assert: MODELDIR found, MODELDRIVER found
   verbose1 "* Selected DSE driver dir: MODELDIR[$model]=${MODELDIR[$model]}"


   # Print info
   [ "$verbmode" != "QUIET" ] && verbose2 "------------------------------------"
   if [ "$modeldriverfound" = "0" ]; then
      # apply defaults
      echo -n ""
   else
#      MODELDIR[$model]="$mdselected"
      debug1 "--------------- sourcing $mdselected/$MODELDRIVER, refreshing default and sourcing again"
      source "$mdselected/$MODELDRIVER"
      setsimfilenamesdef; # refresh defaults
      source "$mdselected/$MODELDRIVER"
      debug1 "--------------- sourcing END"
      [ "$verbmode" != "QUIET" ] && verbose1 "* Model driver '$MODELDRIVER' found in $mdselected"
      debug2 "simstxtdbpref=$simstxtdbpref"
   fi
   [ "$verbmode" != "QUIET" ] && verbose2 "* MODELDIR[$model]=${MODELDIR[$model]}"
   [ "$verbmode" != "QUIET" ] && verbose2 "------------------------------------"

   ###################### LOAD DSEDRIVER FOR THE SELECTED MODEL   ################
   # DEFAULT MODEL: std  (overrides a previously loaded model if std is specified)
   case "$model" in
      std) loadstdmodel ;;
   esac
   ###############################################################################

   # Sanity checks:
   if [ "$MODELNAME" = "" ]; then
      echo "DSE driver file not found!"
      if [ "$model" != "" ]; then
         cecho "You must have a '${DSEDRIVERPREF}-$model.sh' to perform this experiment." "$red"
      else
         cecho "You must have a '${DSEDRIVERPREF}-xxxx.sh' file to explore the model xxxx." "$red"
      fi
      echo "Such file should be located in one of the directories: $mdlist"
      exit 40
   fi

   # Overriding the working subdir, if requested
   # MYWSUBDIR is defined in the MODELDRIVER
   debug1 "   workingsubdir[$model]=${workingsubdir[$model]}"
   [ -z "$MYWSUBDIR" ] || workingsubdir[$model]="$MYWSUBDIR"
   [ -z "${workingsubdir[$model]}" ] && workingsubdir[$model]="$DEFAULTWSUBDIR"
   debug1 "   workingsubdir[$model]=${workingsubdir[$model]}"

   # Looking for the EXEDIR directory
   # Hypothesis: the EXEDIR contains the luafile1 file
   wodirexists="0"
   for md in $lmlist; do
      debug1 "Checking out EXEDIR in $md ..."
      debug1 "   reference config file: ${workingsubdir[$model]}/$luafile1"
      if [ -s "$md/${workingsubdir[$model]}/$luafile1" ]; then
         EXEDIR[$model]="$md/${workingsubdir[$model]}"; 
         wodirexists="1"
      fi
      debug1 "   EXEDIR[$model]=${EXEDIR[$model]}"
   done
   debug1  "   wodirexists=$wodirexists"
   if [ "$wodirexists" = "0" -a "$NORUN" = "0" ]; then
      cecho "ERROR: cannot find workingdir[$model]='${workingdir[$model]}' - is '$model' model installed?" "$red"
      [ -z "${workingdir[$model]}" -o -d "${workingdir[$model]}" ] \
         || cecho "  ERROR: workingdir[$model]='${workingdir[$model]}' not found." "$red"
      exit 40;
   fi
   # Sanity checks
   [ -z "${EXEDIR[$model]}" ] && { cecho "ERROR: EXEDIR[$model] not defined!" "$red"; exit 40; }
   [ -d "${EXEDIR[$model]}" ] || { cecho "ERROR: EXEDIR[$model]=${EXEDIR[$model]} not found!" "$red"; exit 40; }
   # ASSERT: EXEDIR found
   verbose1 "* Selected working dir:    EXEDIR[$model]=${EXEDIR[$model]}"
#echo "RUNID=$RUNID"
#echo "SANDBOX=$SANDBOX"

} # END function selectmodeldriver()

########################################################
function gstsetuphandler() {
########################################################
# PARMS: "üpdatë" or "remove"
# PURPOSE: Manage the guestsetup in Makefile
########################################################
   debug1 "---------- STARTING gstsetuphandler '$1' '$2'"
   local parms="$1"
   local app2="$2"

   # sanity checks
   if [ -z "$parms" ]; then 
      echo "INTERNAL ERROR: gstsetuphandler: variable 'parms' shouldn't be empty!"
      exit 70
   fi
   if [ -z "$app2" ]; then
      echo "INTERNAL ERROR: gstsetuphandler: variable 'app2' shouldn't be empty!"
      exit 70
   fi
   if [ ! -s "$SIMMAKEF" ]; then
      echo "INTERNAL ERROR: gstsetuphandler: Cannot find the reference '$SIMMAKEF'."
      exit 71
   fi

   #
   local location_script_pattern="#@@@guestsetup_location_call@@@"
   local filteredgsfp=`echo ${GSFP[$app2]}|sed 's;/;\\\\/;g'`
   local presence_guestsetup_pattern="@@@guestsetup_mod@@@"
   debug1 "Received parms: '$parms'"
   debug1 "Received app2: $app2"
   local presence=`grep "@@@guestsetup_mod@@@" $SIMMAKEF 2>/dev/null`
   debug1 "SETUP SCRIPT: ${GSFP[$app2]}"
   local command_setup_script='cat '"$filteredgsfp"' >> \$(XGUESTCONFIG); echo \"#@@@guestsetup_mod@@@\" >> \$(XGUESTCONFIG); \\'
 
   case "$parms" in
      remove)
         debug1 "REMOVING $SIMMAKEF guestscript section"
         if [ ! -z "$presence" ]; then
            debug1 "guestsetup is present in $SIMMAKEF - trying to remove it..."
            sed -i "/$presence_guestsetup_pattern/c\\\t$location_script_pattern" $SIMMAKEF
         else
            debug1 "guestsetup is not present."
         fi
      ;;
      update)
         debug1 "UPDATING $SIMMAKEF guestscript section"
         if [ ! -z "$presence" ]; then
            debug1 "guestsetup is present in $SIMMAKEF - trying to update it..."
            sed -i "/$presence_guestsetup_pattern/c\\\t$location_script_pattern" $SIMMAKEF
         else
            debug1 "guestsetup is not present."
         fi
         sed -i "s/$location_script_pattern/$command_setup_script/" $SIMMAKEF
      ;;
      default)
         echo "INTERNAL ERROR: gstsetuphandler: value for variable 'parms' is not valid."
         exit 72
      ;;
   esac
   debug1 "---------- ENDING   gstsetuphandler '$1' '$2'"
}

#####################################################
function cleanvarnames() {
#####################################################
# CLean=up names of arrays (remove the index part)
#####################################################
   local varstoclean="$1"
   local prefix1="$2"
   local listvec
   local p

   listvec=""
   for p in $varstoclean; do
      splitp $p $prefix1
      if [ ! -z "$outindex" ]; then
         [ -z "$listvec" ] && listvec="$outlistp" || listvec="$listvec $outlistp"
      fi
   done

   RESULT="$listvec"
}

#
#####################################################
function setsimver {
#####################################################
   local rr1 rr2

   #detect simulator svn revision
   if [ "$NORUN" = "0" ]; then
      if [ "$verbose" -gt "1" ]; then echo -n "   Detecting SVN revision of SIMBASE='$SIMBASE'..."; fi
      rr1=`(cd $SIMBASE; svn info 2>/dev/null)|awk '/^Revision/{print $2}'`
   fi
   if ! [[ "$rr1" =~ ^[0-9]+$ ]] ; then SVER="$MYSIMVER"; rr2=""; else SVER="$rr1"; rr2="$rr1"; fi
   if [ "$verbose" != "0" ]; then 
      verbose2 ""
      if [ -z "$rr2" ]; then
         if [ "$quiet" = "0" ]; then 
            echo "WARNING: couldn't detect SVN rev: assuming user specified value ($SVER)!"
         fi
      fi
   fi
   verbose2 "   SVER=$SVER"

   #update SIMSTARTED STRING for old revisions
   if [ "$SVER" -lt "$SIMSTARTEDOLDREV" ]; then
      SIMSTARTED="$SIMSTARTEDOLD"
   fi
}

#
#####################################################
function setupparams1 {
#####################################################
   debug1 "---------- STARTING setupparams1"

   #setup default values
   [ -z "$roi" ] && roi="yes"

   #setup default values
   IFS='+' read -a l1icdef <<< "$DEFAULTL1IC"; [ -z "$listl1i" ] && listl1i="${l1icdef[0]}"
   IFS='+' read -a l1dcdef <<< "$DEFAULTL1DC"; [ -z "$listl1c" ] && listl1c="${l1dcdef[0]}"
   IFS='+' read -a l2ucdef <<< "$DEFAULTL2UC"; [ -z "$listl2c" ] && listl2c="${l2ucdef[0]}"
   IFS='+' read -a l3ucdef <<< "$DEFAULTL3UC"; [ -z "$listl3c" ] && listl3c="${l3ucdef[0]}"
   #[ -z "$listt1i" ] && listt1i="4kB+64+64+WT+false+1+t2"	
   #[ -z "$listt1d" ] && listt1d="4kB+64+64+WT+false+1+t2"	
   #[ -z "$listt2s" ] && listt2s="4kB+512+512+WB+true+1+busT"	
   [ -z "$listbus" ] && listbus="$DEFAULTMBUS"
   [ -z "$listtbus" ] && listtbus="$DEFAULTTBUS"
   [ -z "$listmem" ] && listmem="$DEFAULTLMEM"
   [ -z "$listfreq" ] && listfreq="$DEFAULTFREQ"


   # manage mem architecture
   if [ ! -z "$memtrfile" ]; then
      ismempresent=`echo "$listarch"|grep "mem+mem"`
      if [ "$ismempresent" = "$listarch" ]; then
         # add the memory tracer
         listarch=`echo "$listarch"|sed 's/mem+mem/mem+mem-trace+trace/'`
      else
         echo "WARNING: 'memory' is not present in the architecture! Can't insert the memory tracer."
      fi
   fi

   if [ ! -z "$cputrfile" ]; then
      iscpupresent=`echo "$listarch"|grep "ic-cpu"`
      if [ "$iscpupresent" = "$listarch" ]; then
         # add the memory tracer
         listarch=`echo "$listarch"|sed 's/ic-cpu/ic-trace+trace-cpu/'`
      else
         echo "WARNING: 'cpu' is not present in the architecture! Can't insert the cpu tracer."
      fi
   fi

   debug2 "firstparam=$firstparam"
   debug2 "firstparamvals=$firstparamvals"

   ## UNNECESSARY: (done already in startup2)
   ## # OTHER CHECKS on the availability of other input files
   ## IFS=' ' read -a dseparamsv <<< "$DSEPARAMS"
   ## firstparam=${dseparamsv[0]}
   ## eval firstparamvals=\$list$firstparam  # indirect reference !
   ## 
   ## # Expecting the model list in firstparamvals; if empty set to listmodel
   ## if [ "$firstparamvals" = "" ]; then firstparamvals="$listmodel"; fi 
   ## debug1 "model=$fistparamvals"

   #---------------------------------------------------------------------------------------
   #ASSERT: at this point we have defined at least 1 model
   # For each model check if we have defined any $appi
   debug2 "-------- loop through models"
   for model in $firstparamvals; do

      debug2 "listappi=$listappi"
      setappi $model
      # ASSERT: listappi[$model] is now anyway set (listappi was already preset)
      debug2 "listappi=$listappi"

      if [ "$verbose" -gt "1" ]; then
         #echo "DSEPARAMS=$DSEPARAMS"
         echo "   model=$model"
   #      [ -z "${listappi[$model]}" ] && echo "   listappi=${listappi}" || echo "   listappi[$model]=${listappi[$model]}"
         echo "   listappi[$model]=${listappi[$model]}"
      fi

      # Assign EXEDIR[$model],MODELDIR[$model]
      funmodel $model

      # Checking infilter and stringok
      for p in $DSEPARAMS; do [[ $p == appi* ]] && pappi="$p"; done
      splitp $pappi list
      p1list="$outlist"
      debug2 "pappi=$pappi"
      debug2 "p1list='$p1list'"
      debug2 "outp='$outp'"
      debug2 "outlistp='$outlistp'"
#echo "p1lparam='$p1lparam'"

      if [ "$p1list" != "_" ]; then # p1list not empty
         debug2 "-------- loop through apps"
         local p1i
         for p1i in $p1list; do
            verbose2 "   p1i='$p1i'"
            debug2 "p1i='$p1i'"
            debug2 "\${infilter[$p1i]}='${infilter[$p1i]}'"
            debug2 "\${stringok[$p1i]}='${stringok[$p1i]}'"

            setsize $p1i; # sets listsize[$p1i] and GSFP[$p1i] and IFFP[$appi]  # NEEDS $model
            # ASSERT: listsize[$p1i] is now anyway set (listsize was already preset)
            verbose2 "   listsize[$p1i]=${listsize[$p1i]}"

            # setup of guestscript
            debug2 "GUESTSCRIPTTYPE=$GUESTSCRIPTTYPE"
            debug2 "GSFP[$p1i]=${GSFP[$p1i]}"
            if [ "$GUESTSCRIPTTYPE" != "builtin" -a "${GSFP[$p1i]}" != "builtin" ]; then
#               verbose2 "   guestsetup[$p1i]=${guestsetup[$p1i]}"
               if [ -z "${GSFP[$p1i]}" ]; then
                  cecho "ERROR: guest-script NOT DEFINED. Set variable 'guestsetup[$p1i]' to a proper script." "$red"
                  cecho "       OR define GUESTSCRIPTTYPE=\"builtin\" in the model-driver '$MODELDRIVER'" "$red"
                  exit 19
               fi
               if [ ! -s "${GSFP[$p1i]}" ]; then
                  cecho "ERROR: file '${GSFP[$p1i]}' not found or empty." "$red"
                  exit 19
               fi
#            else
#               verbose2 "   guestsetup[$p1i]=<builtin>"
            fi
            verbose2 "   guestsetup[$p1i]=${guestsetup[$p1i]}"

            # setup of input filter
            debug2 "INPUTFILTERTYPE=$INPUTFILTERTYPE"
            debug2 "IFFP[$p1i]=${IFFP[$p1i]}"
            if [ "$INPUTFILTERTYPE" != "builtin" -a "${IFFP[$p1i]}" != "builtin" -a "$BINMAKE" = "1" ]; then
#               verbose2 "   infilter[$p1i]=${infilter[$p1i]}"
               if [ -z "${IFFP[$p1i]}" ]; then
                  cecho "ERROR: input-filter' NOT DEFINED. Set variable 'infilter[$p1i]' to a proper script." "$red"
                  cecho "       OR define INPUTFILTERTYPE=\"builtin\" in the model-driver '$MODELDRIVER'" "$red"
                  exit 19
               fi
               if [ ! -s "${IFFP[$p1i]}" ]; then
                  cecho "ERROR: file '${IFFP[$p1i]}' not found or empty." "$red"
                  exit 19
               fi
#            else
#               verbose2 "   infilter[$p1i]=<builtin>"
            fi
            verbose2 "   infilter[$p1i]=${infilter[$p1i]}"

#            # check the presence of the input filter (if specified)
#            if [ "${infilter[$p1i]}" = "" -a "$BINMAKE" = "1" ]; then # infilter was specified
#               infilter[$p1i]="${p1i}.$INPUTFILTERSUFF"
#            fi
#            if [ "${infilter[$p1i]}" != "" -a "$BINMAKE" = "1" ]; then # infilter was specified
#               if [ ! -s ${infilter[$p1i]} ]; then # specified but not present
#                   cecho "ERROR: missing input filter '${infilter[$p1i]}' (current dir: `pwd`)." "$red"
#                   exit 6
#               fi
#            fi

            # check the presence of the success string
            if [[ -z ${stringok[$p1i]} ]]; then
               stringok[$p1i]="$DEFVERIFICATIONSTRING"
               if [ "$quiet" = "0" ]; then
                  echo -n "* Verification string has not been set: "
                  echo    "using the default value '$DEFVERIFICATIONSTRING'"
               fi
   #            exit 19
            fi


         done
         debug2 "-------- end-loop through apps"
      else
         # The app list is empty
         echo "ERROR: you need to specify at least an app in the variable 'list$outp[$model]'"
         exit 19
      fi
      #Assert: at this point I should know the app list

      # Set SVER, SIMSTARTED (uses MYSIMVER, SIMBASE):
      setsimver

      # Check if common binaries are available
      if [ "$nobin" = "0" -a "$BINMAKE" = "0" -a "$NORUN" = "0" ]; then
         copybin2exedir "SETFP"  #sets NOAPP2
         if [ "$NOAPP2" = "1" ]; then
            cecho "--> You may want to re-run this SAME command on the MASTER machine (e.g. tfx1) with the '--bingen' option." "$yellow"
            exit 7
         fi
      fi
   done
   debug2 "-------- end-loop through models"
   # ASSERT: at this point I should have the condition to start all simulations
   debug1 "---------- ENDING setupparams1"
} # END of setupparams1

#
#####################################################
function setupparams2 {
#####################################################
   debug1 "---------- STARTING setupparams2"
   DSTRT=`date +%y%m%d%H%M`
   EPREF="us${MYUSER}_ma${MYMACH}_rr${SVER}_da${DSTRT}"
   verbose2 "   EPREF=$EPREF"
   verbose2 "   EXPBASE=$EXPBASE"

   #
   exp_id=1


   if [ "$quiet" = "0" ]; then
      echo "---------------------------------------------------------------"
   fi

   checkq def "$MYQRUNNING"; sizer=$QSIZE
   listqelem "$MYQRUNNING" "LAST"; sizer="$QSIZE" # result in ELEMLIST, QSIZE
   # Reinsert in waiting queue the died/interrupted processes that are wrongly remained in the running queue
   verbose2 "* Moving interrupted elements from $MYQRUNNING to $MYQWAITING queue..."
   local jb
   local jbcount="0"
   for jb in $ELEMLIST; do
      local m=${jb%%-*}
      local p=${jb##*-}
      if [ "$m" = "$MYMACH" -a "$p" != "$MYPID" ]; then
         local a=`ps -ewwo pid,args | egrep "^[ \t]*$p[ \t]*.*$THISSCRIPT|grep -v grep"`
#         kill -0 $p 2>&1 >/dev/null
#         if [ "$?" != "0" ]; then # move it from MYQRUNNING to MYQWAITING
         if [ "$a" != "" ]; then # move it from MYQRUNNING to MYQWAITING
            extractq def "$MYQRUNNING" "$jb" "" "NOWARNING"; # -->$FIRSTELEM, $RESTOFELEMS
            EXTRACTQ_FAILED=$?; # it should never fail
            if [ "$EXTRACTQ_FAILED" = "0" ]; then
               cleaninsertq $MYQWAITING "$FIRSTELEM" $RESTOFELEMS 
               debug1 "- INSERTED in $MYQWAITING: $FIRSTELEM $RESTOFELEMS"
               jbcount=`expr $jbcount + 1`
            fi
         fi
      fi
   done
   # Eventually updated ELEMLIST and sizer
   [ "$jbcount" -gt "0" ] && { listqelem "$MYQRUNNING" "LAST"; sizer="$QSIZE"; } # result in ELEMLIST, QSIZE
   verbose2 "  ...moved $jbcount elements."

   if [ "$CLOVERRIDE" = "0" ]; then
      # check if there is an existing queue with some tag
      local qfile1 qfile2 tmplist1 tmplist2
      qfile1="${QUEUEDIR}/${EXP}.$MYQFAILED"
      qfile2="${QUEUEDIR}/${EXP}.$MYQWAITING"
      if [ "$noretry" = "0" ]; then
         tmplist1=`ls -tr $qfile1 2>/dev/null|tail -1 2>/dev/null`
         tmplist2=`ls -tr $qfile2 2>/dev/null|tail -1 2>/dev/null`
      fi
      debug1 "tmplist1=$tmplist1"
      debug1 "tmplist2=$tmplist2"

         # Conditions that define a NEW EXPERIMENT
      if [ \( -z "$tmplist1" -a -z "$tmplist2" \) -o "$resumequeue" = "0" ]; then
         if [ "$quiet" = "0" ]; then 
   #            echo "WARNING: resume requested but no file '$qfile1' found"
   #            echo "WARNING: new experiment run or no previous failed experiment in '$qfile1'"
            echo "* NEW EXPERIMENT"
         fi
      fi

      #
      debug1 "resumequeue=$resumequeue"
      if [ "$resumequeue" = "1" ]; then
         if [ ! -z "$tmplist1" ]; then
            checkq file "$tmplist1"; sizef=$QSIZE
            if [ "$quiet" = "0" ]; then
               echo "* Found FAILED queue (size=$sizef): $tmplist1"
            fi
            if [ "$noretry" = "0" ]; then
               if [ "$sizef" -gt "0" ]; then
                  if [ "$quiet" = "0" ]; then
                     echo "* Found $sizef failed sim-identifiers..."
                  fi
                  movedcount="0"
                  while [ "$sizef" -gt "0" ]; do
                     extractq file $tmplist1 "" "" "NOWARNING"
                     # -->$FIRSTELEM
                     EXTRACTQ_FAILED=$?
                     if [ "$EXTRACTQ_FAILED" != "0" ]; then break; fi
   #echo "extract sizef=$sizef"
   #              sizef=$?
                     sizef=`expr $sizef - 1`
                     if [ ! -z "$FIRSTELEM" ]; then
                        movedcount=`expr $movedcount + 1`
                        calculate_simcost $FIRSTELEM
                        insertq $MYQWAITING "$FIRSTELEM" "$mycost 0 0 0"
                     fi
                     if [ "$quiet" = "0" ]; then
                        [ "$movedcount" ] && echo "* Moved 1 sim-identifier in current sim-queue (moved=$movedcount)"
                     fi
                  done
               fi
            fi
         fi
      fi   
   fi   

   #############################################
   # Preliminary setup and check on model-driver
   selectmodeldriver QUIET
   #############################################

   checkq def $MYQWAITING "" 30; wsize=$QSIZE; wqout="$QOUT"; ## PREREQUISIT: $EXP must be defined before
   # get names of machines where simulations are running
   checkq def $MYQRUNNING "6-"; rsize=$QSIZE; rqout="$QOUT"
   checkq def "$MYQDONE" "6-"; dsize=$QSIZE; dqout="$QOUT"
   if [ "$rsize" -gt "0" ]; then
      if [ "$quiet" = "0" ]; then
         echo "* Found RUNNING queue (size=$rsize){$rqout}  ${QUEUEDIR}/${EXP}.$MYQRUNNING"
      fi
   fi
   if [ "$dsize" -gt "0" ]; then
      if [ "$quiet" = "0" ]; then
         echo "* Found DONE queue (size=$dsize){$dqout}  ${QUEUEDIR}/${EXP}.$MYQDONE"
      fi
   fi

   #
   if [ "$CLOVERRIDE" = "0" -a "$wsize" -gt "0" ]; then
      if [ "$quiet" = "0" ]; then
         if [ ! -z "$wqout" ]; then wqout=" $rsize launched on $wqout"; fi
         echo "* Found WAITING queue (size=$wsize)  ${QUEUEDIR}/${EXP}.$MYQWAITING"
      fi
      SIM_COUNT="0";
      ITNESTING="0"; ITPARAMS="$DSEPARAMS"; GENCOUNT="0"; COUNTONLY="1"
      iterator buildexpstring
      [ "$GENCOUNT" -gt 1 ] && endings="s are" || endings=" is"
      [ "$GENCOUNT" -gt "0" ] && comment "* $GENCOUNT sim-identifier$endings connected to this sim-tag."
   else

      IAMSIMMASTER="1"
      #
      comment "* Start generating simulation identifiers..."

      #
      debug1 "DSEPARAMS=$DSEPARAMS"
      SIM_COUNT="0";
      ITNESTING="0"; ITPARAMS="$DSEPARAMS"; GENCOUNT="0"; COUNTONLY="0"
      iterator buildexpstring

      [ "$SIM_COUNT" -gt "0" ] && verbose1 "* Finished generating $SIM_COUNT simulation identifiers..."
   fi
   comment "---------------------------------------------------------------"
   if [ "$GENCOUNT" = "0" ]; then
      cecho "WARNING: no sim-identifier has been generated. Use '--new' to start a new experiment." "$yellow"
      exit 1
   fi

#
   checkq def "$MYQWAITING"; sizeq=$QSIZE
   checkq def "$MYQRUNNING"; sizer=$QSIZE
   checkq def "$MYQFAILED"; sizef=$QSIZE
   checkq def "$MYQDONE"; sized=$QSIZE
   TOTEXPECTEDSIMS=`expr $sizeq + $sizer + $sizef + $sized`   
   verbose1 "* INITIAL QUEUES: w(waiting)=$sizeq r(running)=$sizer f(failed)=$sizef d(done)=$sized"

#
if [ "$NOSIM" = "1" ]; then
   echo "Stopping here after popolating queue"
   myexpend
fi

#
#if [ "$POPULATE" = "1" ]; then
   globalt00000=`date +%s%N | cut -b1-13`
#fi

# print some info (the follwong operation takes some seconds) if verbose=0
if [ "$verbose" = "0" -a "$BINMAKE" = "0" ]; then
   comment "Wait a bit to cleanup before we start..."
   comment ""
fi

# Cleaning up...
PROCKILLED="0"
[ "$GOSLOW" = "1" ] && cleanup
[ "$GOSLOW" = "1" -a "$nokill" = "0" -a "$PROCKILLED" = "1" ] && sleep 3 # some time to let process die...
   debug1 "---------- ENDING setupparams2"
}

#
#####################################################
function splitsimstring() {
#####################################################
   local myextracteds="$1"
   local i
   local v1
   local v
   local p
   local rex='^[0-9]+$'
#       local inputv
#       local dsev
   #split
#echo "PIPPO2 cpu=$cpu size=$size"
   IFS='_' read -a inputv <<< "$myextracteds"
   IFS=' ' read -a dsev <<< "$DSEPARAMS"

#echo "${dsev[@]}"
#echo "${inputv[@]}"
#echo "dim dsev=${#dsev[@]}"
#echo "dim inputv=${#inputv[@]}"
   EXPDESC1=""
   DIMDSEV=`expr ${#dsev[@]} - 1`
   for i in $(seq 0 $DIMDSEV); do
      v1=${inputv[$i]}
      v=${v1:2}
      p=${dsev[$i]}
#echo "p=$p  v=$v"
      splitp $p
      p="$outp"
      debug2 "  $i $p=$v"
#echo "p=$p  v=$v"
      if ! [[ $v =~ $rex ]] ; then
         export $p="$v"
      else
         export $p=`expr $v + 0`
      fi
      EXPDESC1="$EXPDESC1 $p=$v"
#echo "EXPDESC1=$EXPDESC1  p=$p  v=$v"
   done
#echo "PIPPO3 cpu=$cpu  size=$size"
}

#
#####################################################
function checksimfeasable() {
#####################################################
   local myextracteds="$1"
   local myrestofparm="$2"
   local ok9="0"
   # assert: simulation tag EXTRACTEDSIM is different from zero
   # CHECK IF SIMULATION IS FEASABLE
   splitsimstring "$myextracteds" #out: DIMDSEV EXPDESC1 (all DSEPARAMS)
   debug2 "nodes=$nodes  CPUS=$CPUS  $myextracteds"
   if [ "$nodes" = "" ]; then nodes="1"; fi # set to default if empty
   if [ "$LIMITTONODELECPUS" = "0" -o "$BINMAKE" = "1" ]; then
      ok9="1"
   else
      if [ "$nodes" -le "$CPUS" ]; then
         ok9="1"
      else
         # assert: the simulation has to be reinserted in the waiting queue
         cecho "   Discarding simulation due to nodes=$nodes > CPUS=$CPUS - $myextracteds ..." "$yellow"
         insertq $MYQWAITING "$myextracteds" $myrestofparm
      fi
   fi
   return $ok9;
}

#
#####################################################
function extractqandset() {
#####################################################
   extractq def $MYQWAITING "" "" "NOWARNING"
   # -->$FIRSTELEM
   EXTRACTQ_FAILED=$?
   SIMREMAINING="$QSIZE"
   EXTRACTEDSIM="$FIRSTELEM"
   EXTRACTEDCOST="$SECONDELEM"
   # Sanity check
   [ -z "$EXTRACTEDSIM" -a -z "$EXTRACTEDCOST" ] && EXTRACTEDCOST="100"
   # Sanity check
   GLOBFAILCNT="$THIRDELEM"
   # Sanity check
   [ -z "$EXTRACTEDSIM" -a -z "$GLOBFAILCNT" ] && GLOBFAILCNT="0"
   TIMESTART="$FOURTHELEM"
   # Sanity check
   [ -z "$EXTRACTEDSIM" -a -z "$TIMESTART" ] && TIMESTART="0"
   TIMEEND="$FIFTHELEM"
   # Sanity check
   [ -z "$EXTRACTEDSIM" -a -z "$TIMEEND" ] && TIMEEND="0"
   #
   FAILEDMACHLIST="$SIXTHANDREST"
   debug1 "EXTRACTQ_FAILED=$EXTRACTQ_FAILED"
   debug1 "1  $FIRSTELEM"
   debug1 "2  $SECONDELEM"
   debug1 "3  $THIRDELEM"
   debug1 "4  $FOURTHELEM"
   debug1 "5  $FIFTHELEM"
   debug1 "6+ $SIXTHANDREST"
}

#
#####################################################
function cleaninserttopq() {
#####################################################
   local queue="$1"
   local p1="$2"  # FIRSTELEM
   local p2="$3"  # SECONDELEM
   local p3="$4"  # THIRDELEM
   local p4="$5"  # FOURTHELEM
   local p5="$6"  # FIFTHELEM
   local p6="${@:7}"  # SIXTHANDREST

   # sanity check to avoid duplicates in MYQFAILED queue
   FIRSTELEM="$p1"
   while [ "$FIRSTELEM" != "" ]; do
      extractq def "$queue" "$FIRSTELEM" "" "NOWARNING"
      [ "$?" = "1" ] && FIRSTELEM="" || FIRSTELEM="$p1"
   done
   
   inserttopq $queue "$p1" "$p2" "$p3" "$p4" "$p5" $p6
}

#
#####################################################
function cleaninsertq() {
#####################################################
   local queue="$1"
   local p1="$2"  # FIRSTELEM
   local p2="$3"  # SECONDELEM
   local p3="$4"  # THIRDELEM
   local p4="$5"  # FOURTHELEM
   local p5="$6"  # FIFTHELEM
   local p6="${@:7}"  # SIXTHANDREST

   # sanity check to avoid duplicates in MYQFAILED queue
   FIRSTELEM="$p1"
   while [ "$FIRSTELEM" != "" ]; do
      extractq def "$queue" "$FIRSTELEM" "" "NOWARNING"
      [ "$?" = "1" ] && FIRSTELEM="" || FIRSTELEM="$p1"
   done
   
   insertq $queue "$p1" "$p2" "$p3" "$p4" "$p5" $p6
}

#
#####################################################
function mysimloop() {
#####################################################
   [ "$GOSLOW" = "1" ] && cleanup
   [ "$GOSLOW" = "1" -a "$nokill" = "0"  -a "$PROCKILLED" = "1" ] && sleep 5 # some time to let process die...

   debug1 "Extracting simulation string from queue '$MYQWAITING' ..."

   # PRELIMINARLY:
   # check if this sim is feasable on this machine (criterium: nodes<=cpus)
   checkq def "$MYQWAITING"; sizew1=$QSIZE; EXTRACTEDSIM=""
   debug1 "Found $sizew1 elements in the '$MYQWAITING' queue"

   for ((i=0;i<sizew1;++i)); do
      extractqandset def $MYQWAITING
      # -->$EXTRACTEDSIM ...

      if [ -z "$EXTRACTEDSIM" ]; then
         break;
      else
         checksimfeasable "$EXTRACTEDSIM" "$RESTOFELEMS" # out: $?
         if [ "$?" = "1" ]; then break; else EXTRACTEDSIM=""; fi
      fi
   done
   debug1 "First scan of '$MYQWAITING': extracted '$EXTRACTEDSIM'"

   if [ "$CLOVERRIDE" = "0" ]; then
      # if no jobs in the MYQWAITING, check out the MYQFAILED
      printline="0"
      if [ -z "$EXTRACTEDSIM" ]; then
         if [ "$noretry" = "0" ]; then
            checkq def "$MYQFAILED"; sizef=$QSIZE
            if [ "$sizef" -gt "0" ]; then
               if [ "$quiet" = "0" ]; then
                  echo "---------------------------------------------------------------"
                  printline="1"
                  echo "* Found $sizef failed sim-identifiers..."
               fi
               retryexp="0"
               while [ "$sizef" -gt "0" ]; do
#         copyq $MYQFAILED $MYQWAITING
                  extractq def $MYQFAILED "" "" "NOWARNING"
                  # -->$FIRSTELEM
                  EXTRACTQ_FAILED=$?
                  if [ "$EXTRACTQ_FAILED" != "0" ]; then break; fi
#            sizef=$?
                  sizef=`expr $sizef - 1`
                  GLOBFAILCNT1="$THIRDELEM"
                  [ -z "$GLOBFAILCNT1" ] && GLOBFAILCNT1="0" # sanity check
                  if [ "$GLOBFAILCNT1" -le "$MAXRETRYEXP" ]; then
                     insertq $MYQWAITING "$FIRSTELEM" $RESTOFELEMS
#echo "$FIRSTELEM $RESTOFELEMS"
                     SIMRETRY=`expr $SIMRETRY + 1`
                     retryexp=`expr $retryexp + 1`
                     EXTRACTEDSIM="something"
                  else
                     # if retry-limit is exceeded then reinsert in failed queue
                     cleaninsertq $MYQFAILED "$FIRSTELEM" $RESTOFELEMS
                  fi
               done
               if [ "$retryexp" != "0" ]; then
                  if [ "$quiet" = "0" ]; then
                     echo "* trying to re-execute $retryexp previously failed sim-identifiers"
                  fi
               else
                  if [ "$quiet" = "0" ]; then
                     echo "* retry-limit reached for all sim-identifiers in the failed queue"
                  fi
                  EXTRACTEDSIM=""
               fi
            fi
#else
#   EXTRACTEDSIM="something"
         fi
#      continue # restart from the above while
#         return # restart from the above simloop (external control on simloop)
      fi
   fi

   # monitor some experiment stats
   checkq def "$MYQWAITING"; sizeq=$QSIZE
   checkq def "$MYQRUNNING"; sizer=$QSIZE
   checkq def "$MYQFAILED"; sizef=$QSIZE
   checkq def "$MYQDONE"; sized=$QSIZE

   if [ "$sizeq" -lt "0" -o "$sizef" -lt "0" ]; then
      if [ -z "$EXTRACTEDSIM" -a "$BINMAKE" = "0" ]; then # STOP EXERIMENT if no simulation is found
         SIMLOOPEMPTY=`expr $SIMLOOPEMPTY + 1`
         if [ "$SIMLOOPEMPTY" -ge "$SIMLOOPTHRESHOLD" ]; then
            cecho "* ENDING EXPERIMENT DUE TO NO MORE FEASABLE SIMULATIONS ON THIS MACHINE" "$yellow"
         else
            echo "*****************************"   
            echo "   SIMLOOP RETRY #$SIMLOOPEMPTY..."
            EXTRACTEDSIM="something"
         fi
      fi
   fi

   if [ "$EXTRACTEDSIM" = "something" -o "$EXTRACTEDSIM" = "" ]; then return; fi # restart from the above simloop (external control on simloop)

   # INSERT THE SIMULATION IN MYQRUNNING
   TIMESTART=`date +%y%m%d%H%M%S`
   [ -z "$TIMEEND" ] && TIMEEND="0" # sanity check
   [ -z "$EXTRACTEDCOST" ] && EXTRACTEDCOST="0" # sanity check
   [ -z "$GLOBFAILCNT" ] && GLOBFAILCNT="0" # sanity check
   insertq $MYQRUNNING "$EXTRACTEDSIM" "$EXTRACTEDCOST" "$GLOBFAILCNT" "$TIMESTART" "$TIMEEND" "${MYMACH}-$MYPID"
   FPREF="${EPREF}_$EXTRACTEDSIM."
   FPRF1="${EPREF}_$EXTRACTEDSIM"
#echo "$FPREF"

   listqelem "$MYQRUNNING" "LAST"; sizer="$QSIZE" # result in ELEMLIST, QSIZE

   #
   if [ ! -z "$globalt00000" ]; then
      globalt0000x=`date +%s%N | cut -b1-13`
      globaldtmsxx=`expr $globalt0000x - $globalt00000`
#      globaldthhxx=`echo ""|awk 'END{v=globaldtms/3600000;printf("%7.2f",v)}' globaldtms=$globaldtmsxx`
      globaldthhxx=`echo ""|awk 'END{v=globaldtms/3600000;printf("%3.2f",v)}' globaldtms=$globaldtmsxx`
      globalstatsx="(${globaldthhxx}h)"
   fi
   
#echo "printline=$printline"
   if [ "$quiet" = "0" ]; then
      if [ "$printline" = "1" ]; then echo "---------------------------------------------------------------"; fi
      echo -n "* EXTRACTED: "
   fi
   cecho -f -n "$EXTRACTEDSIM" "$blue"
   if [ "$quiet" = "0" ]; then
      [ -z "$GLOBFAILCNT" ] && GLOBFAILCNT="0" # sanity check
      if [ "$GLOBFAILCNT" -gt "0" ]; then
         echo " (retry #$GLOBFAILCNT - failed on $SIXTHANDREST)"
      else
         echo ""
      fi
      echo "* QUEUES${globalstatsx} - LOCAL($MYMACH): w=$SIMREMAINING f=$SIMFAILCOUNT d=$SIMOKCOUNT - GLOBL($ELEMLIST)=$sizer: w=$sizeq f=$sizef d=$sized";
   else
      echo ""
   fi

   debug2 "EXTRACTEDSIM=$EXTRACTEDSIM"
   debug2 "DSEPARAMS=$DSEPARAMS"
   
   # ASSIGN ALL DSEPARAMETERS BASED ON THE EXTRACTEDSIM string #########
   splitsimstring "$EXTRACTEDSIM" #out: DIMDSEV EXPDESC1 (all DSEPARAMS)
   #####################################################################

   OUTFILE0=`mktemp /tmp/${DSE_THISTOOL}outXXXXXX.log`
   SOUTF0=`mktemp /tmp/mysimoutXXXXXX.log`
   SIMMAKEF0=`mktemp /tmp/mymakefXXXXXX.make`
   SIMCONFIG0=`mktemp /tmp/myluafXXXXXX.lua`
   [ "$DBMODE" -gt "0" ] && DBOUT0=`mktemp /tmp/mydbXXXXXX.db`
   exp_desc="us=$MYUSER ma=$MYMACH rr=$SVER da=$DSTRT$EXPDESC1 (rem=$SIMREMAINING eok=$SIMOKCOUNT efail=$SIMFAILCOUNT)"

   # SET the EXEDIR[$model] based on the $model
   funmodel $model
   
#-------------------------------------------------------------------------------
#
#-----------------------------------------------------------------------------
   # Sanity checks
   if [ -z "$appi" ]; then
      echo "ERROR: 'appi' is empty."
      echo "You have to specify at least one app in the 'listappi' variable or on the command line."
      exit 30
   fi
   if [ -z "$size" ]; then
      echo "ERROR: 'size' is empty."
      echo "You have to specify at least one input size in the 'listsize' variable "
      echo "of the experiment file or on the command line."
      exit 30
   fi
   # ASSERT: the variables $appi and $size should be set at this point

   # SET the myapp name
   myapp="${appi}_${size}_${LIBVTARGET}"

   # Print info
   if [ "$verbose" != "0" ]; then
      if [ -z "${listsize[$appi]}" ]; then
         echo "   listsize=${listsize}    myapp=$myapp    model=$model"
      else
         echo "   listsize[$appi]=${listsize[$appi]}    myapp=$myapp    model=$model"
      fi
   fi


#manage architecture
#echo "listcpu=$listcpu"
#echo "cpu=$cpu"
#setup default values
[ -z "$tsulatency" ] && tsulatency="$listtsulatency"
[ -z "$mem" ] && mem="$listmem"	
[ -z "$freq" ] && freq="$listfreq"
[ -z "$nic" ] && nic="$listnic"
[ -z "$cpu" ] && cpu="$listcpu"	
#[ -z "$t1i" ] && t1i="$listt1i"	
#[ -z "$t1d" ] && t1d="$listt1d"	
#[ -z "$t2s" ] && t2s="$listt2s"	
[ -z "$bus" ] && bus="$listbus"	
[ -z "$tbus" ] && tbus="$listtbus"	
[ -z "$archstring" ] && archstring="$listarch"	
IFS='+' read -a cpuv <<< "$cpu"
#echo "cpu=$cpu"
#echo "cpuv[0]=${cpuv[0]}"
#echo "cpuv[1]=${cpuv[1]}"
#echo "cpuv[2]=${cpuv[2]}"
#IFS='+' read -a pt1i <<< "$t1i"
#IFS='+' read -a pt1d <<< "$t1d"
#IFS='+' read -a pt2s <<< "$t2s"
IFS='+' read -a pbus <<< "$bus"
IFS='+' read -a ptbus <<< "$tbus"

#manage NICs
IFS='+' read -a nicv <<< "$nic"
[ -z "${nicv[0]}" ] && nic_trput=1000M || nic_trput=${nicv[0]}
[ -z "${nicv[1]}" ] && nic_latency=0 || nic_latency=${nicv[1]}
[ -z "${nicv[2]}" ] && nic_setup=0 || nic_setup=${nicv[2]}
debug2 "NIC throughput=$nic_trput latency=$nic_latency setup=$nic_setup"

#manage memory
IFS='+' read -a lmemv <<< "$mem"
IFS='+' read -a lmemvdef <<< "$DEFAULTLMEM"
[ -z "${lmemv[0]}" ] && memlat="${lmemvdef[0]}" || memlat=${lmemv[0]}
debug2 "MEM memlat=$memlat"

#manage frequency
IFS='+' read -a lfreqv <<< "$freq"
IFS='+' read -a lfreqvdef <<< "$DEFAULTFREQ"
[ -z "${lfreqv[0]}" ] && frequency="${lfreqvdef[0]}" || frequency=${lfreqv[0]}
debug2 "FREQ frequency=$frequency"


#manage tsu latencies
IFS='+' read -a tsulv <<< "$tsulatency"
defl=${tsulv[0]}
rdl=${tsulv[1]}
wrl=${tsulv[2]}
subl=${tsulv[3]}
schl=${tsulv[4]}
snd=${tsulv[5]}
rcv=${tsulv[6]}

#echo "defl=$defl rdl=$rdl wrl=$wrl subl=$subl schl=$schl"


#default l1ic values
[ -z "$l1i" ] && l1i="$listl1i"	
IFS='+' read -a l1ic <<< "$l1i"
IFS='+' read -a l1icdef <<< "$DEFAULTL1IC"
[ -z "${l1ic[0]}" ] && l1icsize="${l1icdef[0]}" || l1icsize="${l1ic[0]}"
[ -z "${l1ic[1]}" ] && l1icblks="${l1icdef[1]}" || l1icblks="${l1ic[1]}"
[ -z "${l1ic[2]}" ] && l1icways="${l1icdef[2]}" || l1icways="${l1ic[2]}"
[ -z "${l1ic[3]}" ] && l1icwhpo="${l1icdef[3]}" || l1icwhpo="${l1ic[3]}"
[ -z "${l1ic[4]}" ] && l1icwmpo="${l1icdef[4]}" || l1icwmpo="${l1ic[4]}"
[ -z "${l1ic[5]}" ] && l1iclate="${l1icdef[5]}" || l1iclate="${l1ic[5]}"

#default l1dc values
[ -z "$l1c" ] && l1c="$listl1c"	
IFS='+' read -a l1dc <<< "$l1c"
IFS='+' read -a l1dcdef <<< "$DEFAULTL1DC"
[ -z "${l1dc[0]}" ] && l1dcsize="${l1dcdef[0]}" || l1dcsize="${l1dc[0]}"
[ -z "${l1dc[1]}" ] && l1dcblks="${l1dcdef[1]}" || l1dcblks="${l1dc[1]}"
[ -z "${l1dc[2]}" ] && l1dcways="${l1dcdef[2]}" || l1dcways="${l1dc[2]}"
[ -z "${l1dc[3]}" ] && l1dcwhpo="${l1dcdef[3]}" || l1dcwhpo="${l1dc[3]}"
[ -z "${l1dc[4]}" ] && l1dcwmpo="${l1dcdef[4]}" || l1dcwmpo="${l1dc[4]}"
[ -z "${l1dc[5]}" ] && l1dclate="${l1dcdef[5]}" || l1dclate="${l1dc[5]}"

#default l2uc values
[ -z "$l2c" ] && l2c="$listl2c"	
IFS='+' read -a l2 <<< "$l2c"
IFS='+' read -a l2def <<< "$DEFAULTL2UC"
[ -z "${l2[0]}" ] && l2size="${l2def[0]}" || l2size="${l2[0]}"
[ -z "${l2[1]}" ] && l2blks="${l2def[1]}" || l2blks="${l2[1]}"
[ -z "${l2[2]}" ] && l2ways="${l2def[2]}" || l2ways="${l2[2]}"
[ -z "${l2[3]}" ] && l2whpo="${l2def[3]}" || l2whpo="${l2[3]}"
[ -z "${l2[4]}" ] && l2wmpo="${l2def[4]}" || l2wmpo="${l2[4]}"
[ -z "${l2[5]}" ] && l2late="${l2def[5]}" || l2late="${l2[5]}"

#default l3uc values
[ -z "$l3c" ] && l3c="$listl3c"	
IFS='+' read -a l3 <<< "$l3c"
IFS='+' read -a l3def <<< "$DEFAULTL3UC"
[ -z "${l3[0]}" ] && l3size="${l3def[0]}" || l3size="${l3[0]}"
[ -z "${l3[1]}" ] && l3blks="${l3def[1]}" || l3blks="${l3[1]}"
[ -z "${l3[2]}" ] && l3ways="${l3def[2]}" || l3ways="${l3[2]}"
[ -z "${l3[3]}" ] && l3whpo="${l3def[3]}" || l3whpo="${l3[3]}"
[ -z "${l3[4]}" ] && l3wmpo="${l3def[4]}" || l3wmpo="${l3[4]}"
[ -z "${l3[5]}" ] && l3late="${l3def[5]}" || l3late="${l3[5]}"

#default t1i values
[ -z "$t1i" ] && t1i="$listt1i"	
IFS='+' read -a t1i <<< "$t1i"
IFS='+' read -a t1idef <<< "$DEFAULTT1I"
[ -z "${t1i[0]}" ] && t1ipgsz="${t1idef[0]}" || t1ipgsz="${t1i[0]}"
[ -z "${t1i[1]}" ] && t1ientr="${t1idef[1]}" || t1ientr="${t1i[1]}"
[ -z "${t1i[2]}" ] && t1iways="${t1idef[2]}" || t1iways="${t1i[2]}"
[ -z "${t1i[3]}" ] && t1iwhpo="${t1idef[3]}" || t1iwhpo="${t1i[3]}"
[ -z "${t1i[4]}" ] && t1iwmpo="${t1idef[4]}" || t1iwmpo="${t1i[4]}"
[ -z "${t1i[5]}" ] && t1ilate="${t1idef[5]}" || t1ilate="${t1i[5]}"

#default t1d values
[ -z "$t1d" ] && t1d="$listt1d"	
IFS='+' read -a t1d <<< "$t1d"
IFS='+' read -a t1ddef <<< "$DEFAULTT1D"
[ -z "${t1d[0]}" ] && t1dpgsz="${t1ddef[0]}" || t1dpgsz="${t1d[0]}"
[ -z "${t1d[1]}" ] && t1dentr="${t1ddef[1]}" || t1dentr="${t1d[1]}"
[ -z "${t1d[2]}" ] && t1dways="${t1ddef[2]}" || t1dways="${t1d[2]}"
[ -z "${t1d[3]}" ] && t1dwhpo="${t1ddef[3]}" || t1dwhpo="${t1d[3]}"
[ -z "${t1d[4]}" ] && t1dwmpo="${t1ddef[4]}" || t1dwmpo="${t1d[4]}"
[ -z "${t1d[5]}" ] && t1dlate="${t1ddef[5]}" || t1dlate="${t1d[5]}"

#default t2s values
[ -z "$t2s" ] && t2c="$listt2s"	
IFS='+' read -a t2 <<< "$t2s"
IFS='+' read -a t2def <<< "$DEFAULTT2S"
[ -z "${t2[0]}" ] && t2pgsz="${t2def[0]}" || t2pgsz="${t2[0]}"
[ -z "${t2[1]}" ] && t2entr="${t2def[1]}" || t2entr="${t2[1]}"
[ -z "${t2[2]}" ] && t2ways="${t2def[2]}" || t2ways="${t2[2]}"
[ -z "${t2[3]}" ] && t2whpo="${t2def[3]}" || t2whpo="${t2[3]}"
[ -z "${t2[4]}" ] && t2wmpo="${t2def[4]}" || t2wmpo="${t2[4]}"
[ -z "${t2[5]}" ] && t2late="${t2def[5]}" || t2late="${t2[5]}"

#default hd values
[ -z "$hd" ] && hd="$listhd"




#manage timing
if [ "$timing" = "" ]; then #assign a default timing
   IFS=' ' read -a ptiming <<< "$listtiming"
   timing=${ptiming[0]}
fi
#if [ "$verbose" != "0" ]; then echo "   timing=$timing"; fi

# Check the presence of the app binary
if [ "$BINMAKE" = "0" ]; then
   if [ ! -s ${EXEDIR[$model]}/$myapp ]; then
      echo "ERROR: cannot find the benchmark binary '${EXEDIR[$model]}/$myapp'"
      exit 12
   fi
fi

# NODES1 (numeric value of nodes), nodes (string with a leading 0)
[ -z "$nodes" ] && nodes="1"
funnodes $nodes; nodes="$RETVAL"
NODES1=`echo "$nodes"|sed 's/^0*//g'`

# CORES1 (numeric value of cores), cores (string with a leading 0)
[ -z "$cores" ] && cores="1"
funcores $cores; cores="$RETVAL"
CORES1=`echo "$cores"|sed 's/^0*//g'`

# WORKERS1
#  if workers unspecified set equal to cores
#if [ "$workers" = "-" ]; then workers=$cores; fi
funworkers $workers; workers="$RETVAL"
WORKERS1=`echo "$workers"|sed 's/^0*//g'`

if [ "$debug" -gt "1" ]; then echo "   size=$size"; fi
if [ "$debug" -gt "1" ]; then echo "   cores=$cores     CORES1=$CORES1"; fi
if [ "$debug" -gt "1" ]; then echo "   nodes=$nodes     NODES1=$NODES1"; fi
if [ "$debug" -gt "1" ]; then echo "   workers=$workers   WORKERS1=$WORKERS1"; fi


#filter the multiple values
IFS='+' read -a SIZE1V <<< "$size"
size11="${SIZE1V[@]}"
debug2 "size11='$size11'"

# PRELIMINARY ROI definitions
if [ "$roi" = "yes" ]; then
   INOUT="ci"
   INSDEL="ins"
else
   INOUT="co"
   INSDEL="del"
fi


   #########################
   # SELECT THE MODEL-DRIVER
   selectmodeldriver
   #########################


   # Sanity check
   dsevcount="0"
   DIMDSEV=`expr $DIMDSEV + 1`
   for p in $DSEPARAMS; do
      eval v1=\$$p
      [ -z "$v1" ] || dsevcount=`expr $dsevcount + 1`
   done
   if [ "$DIMDSEV" != "$dsevcount" -o "$dsevcount" = "0" ]; then
      cecho "ERROR: Some DSE variable is not set (try option --new)." "$red"
      exit 75
   fi

   # Set the SIMULATOR ROOT

   # SET the Makefile-template full-path
   MKFFP=""
   [ -s ${MODELDIR[$model]}/$DEFAULTMKFILE ] && MKFFP="${MODELDIR[$model]}/$DEFAULTMKFILE"
   [ -s ${EXEDIR[$model]}/$DEFAULTMKFILE ] && MKFFP="${EXEDIR[$model]}/$DEFAULTMKFILE"
   # Check if the Makefile exists
   if [ -z "$MKFFP" ]; then
      cecho "ERROR: cannot find '$DEFAULTMKFILE' in MODELDIR[$model]='${MODELDIR[$model]}' nor in EXEDIR[$model]='${EXEDIR[$model]}'." "$red"
      exit 8
   fi
   verbose2 "* Template-Makefile: $MKFFP"
   # assert: MKFFP contains the full-path to the Makefile-template

   CFGFP=""
   [ -s ${MODELDIR[$model]}/$luafile ] && CFGFP="${MODELDIR[$model]}/$luafile"
   [ -s ${EXEDIR[$model]}/$luafile ] && CFGFP="${EXEDIR[$model]}/$luafile"

   # check if luafile file exists
   if [ ! -s "$CFGFP" ]; then
      cecho "ERROR: cannot find '$luafile' in MODELDIR[$model]='${MODELDIR[$model]}' nor in EXEDIR[$model]='${EXEDIR[$model]}'." "$red"
      exit 8
   fi
   verbose2 "* Template-config-file: $CFGFP"

if [ "$BINMAKE" = "0" ]; then
   # Check if the BSD image is accessible
   BSDFN="${CORES1}p-${hd}.bsd" # NEW RULE (160705)
   BSDFP="$SIMBASE/$DEFAULTSIMBRANCH/$DEFAULTBSDPATH/$BSDFN"
   if [ ! -s "$BSDFP" ]; then
      cecho "ERROR: cannot find '$BSDFN' in $SIMBASE/$DEFAULTSIMBRANCH/$DEFAULTBSDPATH" "$red"
      exit 8
   fi
   verbose2 "* BSD-file: $BSDFP"

   # Check if the HD image is accessible
   HDFN="${hd}.img"
   HDFP="$SIMBASE/$DEFAULTSIMBRANCH/$DEFAULTBSDPATH/$HDFN"
   if [ ! -s "$HDFP" ]; then
      cecho "ERROR: cannot find '$HDFN' in $SIMBASE/$DEFAULTSIMBRANCH/$DEFAULTBSDPATH" "$red"
      exit 8
   fi
   verbose2 "* HD-file: $HDFP"
fi

   # manage application command line parameters
   local inputfilter="${infilter[$appi]}"
   if [ -s ./$inputfilter -a "$BINMAKE" = "1" ]; then
      verbose2 "      pwd=pwd"
      verbose2 "      sh ./$inputfilter ${SIZE1V[@]}"
      sh ./$inputfilter ${SIZE1V[@]}
   fi

#-----------------------------------------------------------------------------
# Check if the selected HD image is accessible
#

   # Build the SIMOUT filename
   buildfn1 SIMOUT 1; SOUTFILE="$fninn"

   # Build the JOBWATCH filename
   buildfn1 JOBWATCH 1; OUTFILE="$fninn"

   # Build the SIMMAKEF filename
   buildfn1 SIMMAKEF 1; SIMMAKEF="$fninn"
   [ -z "$SIMMAKEF" ] && { cecho "ERROR: SIMMAKEF not defined" "$red"; exit 9; }
   cp $MKFFP $SIMMAKEF
   verbose2 "* Actual Makefile: $SIMMAKEF"

   # Build the SIMCONFIG filename
   buildfn1 SIMCONFIG 1; SIMCONFIG="$fninn"
   [ -z "$SIMCONFIG" ] && { cecho "ERROR: SIMCONFIG not defined" "$red"; exit 9; }
   cp $CFGFP $SIMCONFIG
   verbose2 "* Actual ConfigFile: $SIMCONFIG"

   # Build the SIMCBLIB filename
   buildfn1 SIMCBLIB 1; SIMCBLIB="$fninn"
   [ -z "$SIMCBLIB" ] && { cecho "ERROR: SIMCBLIB not defined" "$red"; exit 9; }
   verbose2 "* Callback-Lib: $SIMCBLIB"

   # Build the SIMSRCDIR filename
   buildfn1 SIMSRCDIR 1; SIMSRCDIR="$fninn"
   [ -z "$SIMSRCDIR" ] && { cecho "ERROR: SIMSRCDIR not defined" "$red"; exit 9; }
   verbose2 "* Sim-Source Dir: $SIMSRCDIR"

   # Build the SIMEXE filename
   buildfn1 SIMEXE 1; SIMEXE="$fninn"
   [ -z "$SIMEXE" ] && { cecho "ERROR: SIMEXE not defined" "$red"; exit 9; }
   verbose2 "* Sim-Executable: $SIMEXE"

   # Build the XBINDIR filename
   buildfn1 XBINDIR 1; XBINDIR="$fninn"
   [ -z "$XBINDIR" ] && { cecho "ERROR: XBINDIR not defined" "$red"; exit 9; }
   verbose2 "* Local-Bin Dir: $XBINDIR"

   # Build the SIMDBFN filename
   [ "$DBMODE" -gt "0" ] && {
      buildfn1 SIMDBFN ""; SIMDBFN="$fninn"
      [ -z "$SIMDBFN" ] && { cecho "ERROR: SIMDBFN not defined" "$red"; exit 9; }
      verbose2 "* Simulation DB Filename: $SIMDBFN"
   }


if [ "$rscript" != "" ]; then
   # check if rscript file exists
   if [ ! -s "$rscript" ]; then
      echo "ERROR: cannot find run_script file: '$rscript'"
      exit 8
   fi

   # UPDATE $rscript (e.g., run_scpript.sh)
   if [ "$verbose" -gt "1" ]; then echo "   updating '$rscript'"; fi
   sed -i "s/^NUM_NODE[ ]*=.*/NUM_NODE=\"$NODES1\"/" $rscript
   sed -i "s/^CORES[ ]*=.*/CORES=\"$CORES1\"/" $rscript
ttcores=`expr $NODES1 \* $CORES1`
#   sed -i "s/^TESTS[ ]*=.*/TESTS=\"${myapp}+${SIZE1V[@]}+$ttcores\"/" $rscript
#   sed -i "s/^TESTS[ ]*=.*/TESTS=\"${myapp}+${size}+$ttcores\"/" $rscript
   sed -i "s/^TESTS[ ]*=.*/TESTS=\"${myapp},$size,$ttcores\"/" $rscript
   local simconfig1=`echo "$SIMCONFIG"|sed 's;/;\\\\/;g'`
   sed -i "s/^TSUCONF[ ]*=.*/TSUCONF=\"${simconfig1}\"/" $rscript
   if [ "$display" = "yes" ]; then
      sed -i "s/^DISP[ ]*=.*/DISP=\"1\"/" $rscript
   else
      sed -i "s/^DISP[ ]*=.*/DISP=\"0\"/" $rscript
   fi
   if [ "$displayhold" = "yes" ]; then
      sed -i "s/^HOLDDISP[ ]*=.*/HOLDDISP=\"1\"/" $rscript
   else
      sed -i "s/^HOLDDISP[ ]*=.*/HOLDDISP=\"0\"/" $rscript
   fi
fi


#FIXME: TEST IF THE FOLLOWING WORKS FINE FOR A GENERIC model
#   #some cleanup
#   if [ "${stdoutpref}" != "" ]; then
#      eval ttt=${stdoutpref}
#      debug2 "\$stdoutpref=$ttt"
#      debug1 "   cleaning up '${stdoutpref}*' files"
#      rm -f ${EXEDIR[$model]}/${stdoutpref}* 2>/dev/null
#   fi
#   if [ "${screenpref}" != "" ]; then
#      eval ttt=${screenpref}
#      debug2 "\$screenpref=$ttt"
#      debug1 "   cleaning up '${screenpref}*' files"
#      rm -f ${EXEDIR[$model]}/${screenpref}* 2>/dev/null
#   fi
#   if [ "${nodescriptpref}" != "" ]; then
#      eval ttt=${nodescriptpref}
#      debug2 "\$nodescriptpref=$ttt"
#      debug1 "   cleaning up '${nodescriptpref}*' files"
#      rm -f ${EXEDIR[$model]}/${nodescriptpref}* 2>/dev/null
#   fi
#
#   debug1 "   cleaning up 'SIMDONE' file"
#   rm -f ${EXEDIR[$model]}/SIMDONE 2>/dev/null
#   debug1 "   cleaning up 'SIMFAILED' file"
#   rm -f ${EXEDIR[$model]}/SIMFAILED 2>/dev/null
#####################

   # filter the Makefile to prepare the execution of the guestsetup script (the user script)
   # prerequisite: SIMMAKEF has to be already defined
   if [ -z "${guestsetup[$appi]}" ]; then
      debug1 "'guestsetup' variable is empty or not found."
      [ "$NORUN" = "0" ] && gstsetuphandler "remove" "$appi"
   else
      debug1 "'guestsetup' variable found: ${guestsetup[$appi]} - index: $appi"
      [ "$NORUN" = "0" ] && gstsetuphandler "update" "$appi"
   fi


# Update $SIMMAKEF variables: TESTS TESTS1 SZ NT HOLDDISP
# add loop for TESTS1
if [ "$updatemakefile" = "1" ]; then
   #update the LUAFILE name in the SIMMAKEF
   # NOTE: $SIMCONFIG may contain a pathname with '/'
#   sed -i "s+$luafile+$SIMCONFIG+g" $SIMMAKEF
   INSPOINTPREV="" # insert at the beginning of the file
   modfile="$SIMMAKEF"
   # pre-filtering the '/'in paths:
   local simconfig1=`echo "$SIMCONFIG"|sed 's;/;\\\\/;g'`
   local simsrcdir1=`echo "$SIMSRCDIR"|sed 's;/;\\\\/;g'`
   local xmodeldir1=`echo "${MODELDIR[$model]}"|sed 's;/;\\\\/;g'`
   local simcblib1=`echo "$SIMCBLIB"|sed 's;/;\\\\/;g'`
   local simexe1=`echo "$SIMEXE"|sed 's;/;\\\\/;g'`
   local xbindir1=`echo "$XBINDIR"|sed 's;/;\\\\/;g'`
   # inserting elements:
   chelem ins 0 "^[ \t]*" "" "XLUAFILE=$simconfig1" "XLUAFILE[ \t]*="
   chelem ins 0 "^[ \t]*" "" "COTSONVER=$SVER" "COTSONVER[ \t]*="
   chelem ins 0 "^[ \t]*" "" "COTSONDIR=$simsrcdir1" "COTSONDIR[ \t]*="
   chelem ins 0 "^[ \t]*" "" "XMODELDIR=$xmodeldir1" "XMODELDIR[ \t]*="
   chelem ins 0 "^[ \t]*" "" "XSMSIM=$simcblib1" "XSMSIM[ \t]*="
   chelem ins 0 "^[ \t]*" "" "COTSON=$simexe1" "COTSON[ \t]*="
   chelem ins 0 "^[ \t]*" "" "XBINDIR=$xbindir1" "XBINDIR[ \t]*="

   chelem ins 0 "^[ \t]*" "" "TESTS=${appi}" "TESTS[ \t]*="
   chelem ins 0 "^[ \t]*" "" "TESTS1=${myapp}" "TESTS1[ \t]*="

   #update for loop on TESTS1
   sed -i "s/for n in \$(TESTS)/for n in \$(TESTS1)/g" $SIMMAKEF
   #sed -i "s/^SZ[ ]*=.*/SZ=\"$size11\"/" $SIMMAKEF
   sed -i "s/^[ \t]*[^#M]*SZ[ ]*=.*/SZ=\"$size11\"/" $SIMMAKEF
   #sed -i "s/^NT[ ]*=.*/NT=$WORKERS1/" $SIMMAKEF
   sed -i "s/^[ \t]*[^#]*NT[ ]*=.*/NT=$WORKERS1/" $SIMMAKEF
   #sed -i "s/^\($runmode:.*\)\$(TESTS)/\1\$(TESTS1)/g" $SIMMAKEF
   sed -i "s/^[ \t]*[^#]*\($runmode:.*\)\$(TESTS)/\1\$(TESTS1)/g" $SIMMAKEF

   # removing and eventual Make.params
   sed -i 's/^[ \t]*[^#]*\([-]\?include[ \t]*Make.params.*\)/#\1/' $SIMMAKEF

   #
   if [ "$displayhold" = "yes" ]; then
#      sed -i "s/^HOLDDISP[ ]*=.*/HOLDDISP=1/" $SIMMAKEF
      sed -i "s/^[ \t]*[^#]*HOLDDISP[ ]*=.*/HOLDDISP=1/" $SIMMAKEF
   else
#      sed -i "s/^HOLDDISP[ ]*=.*/HOLDDISP=0/" $SIMMAKEF
      sed -i "s/^[ \t]*[^#]*HOLDDISP[ ]*=.*/HOLDDISP=0/" $SIMMAKEF
   fi
fi

# ROI RELATED for rscript based ROI management
   # DONE IN DFLIB.C SINCE OCT-2014
if [ "$roiexternalandrscript" = "1" ]; then
   echo -n "" #dummy line
#   INSPOINTPREV="export GOMP_CPU_AFFINITY"; modfile="$rscript"
#   chelem $INOUT 16 "" "echo \"xget \$ROOTL\/cotson_tracer cotson_tracer; chmod +x .\/cotson_tracer\">> \$new_SCRIPT;" "" "ROOTL\/cotson_tracer"
#   INSPOINTPREV="ROOTL\/cotson_tracer"; modfile="$rscript"
#   chelem $INOUT 16 "" "" "echo \"echo \'RUNNING \$n\'; sleep \$SLEEP ; \[ \$i -gt 0 \] \&\& .\/cotson_tracer 10 1 0; .\/\$n \$i \$CORES  \$ISZSTR 2>\&1 | tee -a \$new_LOG \" >> \$new_SCRIPT;" "[^#]RUNNING" "echo \"echo \'RUNNING \$n\'; sleep \$SLEEP ; .\/\$n \$i \$CORES  \$ISZSTR 2>\&1 | tee -a \$new_LOG \" >> \$new_SCRIPT;"
#
#   INSPOINTPREV="[^#]RUNNING"; modfile="$rscript"
#   chelem $INOUT 16 "" "echo \"./cotson_tracer 10 1 1\" >> \$new_SCRIPT;" "" "cotson_tracer 10 1 1"
fi

# ROI RELATED for $SIMMAKEF based ROI management
if [ "$roiexternalandmakefile" = "1" ]; then
   INSPOINTPREV="sudo sysctl -w k"; modfile="$SIMMAKEF"
   local loccotsontr=`echo ""|sed 's;/;\\\\/;g'`
   chelem $INSDEL t1 "" "cp $LOCALCBIN/cotson_tracer \$(HOSTTMP)" "" ""
   chelem $INSDEL t1 "" "echo \"xget \$(HOSTTMP)/cotson_tracer cotson_tracer; chmod +x ./cotson_tracer\" >> \$(TSCRIPT); \\\\" "" "cotson_tracer cotson_tracer"
   INSPOINTPREV="[^#]RUNNING"; modfile="$SIMMAKEF"
   chelem $INSDEL t2 "" "echo \"if \[ \\\\\"\\\\\$\$1\\\\\" != \\\\\"\\\\\" \]; then if \[ \\\\\"\\\\\$\$1\\\\\" -gt \\\\\"1\\\\\" \]; then echo -n \\\\\"CT \\\\\"; .\/cotson_tracer 10 1 0; echo \\\\\"DONE\\\\\"; fi; fi; \" >> \$(TSCRIPT); \\\\" "" "cotson_tracer 10 1 0"
   INSPOINTPREV="echo \\\".\/"; modfile="$SIMMAKEF"
   chelem $INSDEL t2 "" "echo \"./cotson_tracer 10 1 1\" >> \$(TSCRIPT); \\\\" "" "cotson_tracer 10 1 1"
fi
#fi

#some name fixes
sed -i "s/runid[ ]*=.*/runid=$LUARUNID/" $SIMCONFIG

#
sed -i "s/xtsu2n/$RUNID/g" $SIMMAKEF
sed -i "s/^\(XRUNID=\).*/\1$RUNID/g" $SIMMAKEF
sed -i "s/^\(NNODES=\).*/\1$NODES1/g" $SIMMAKEF
sed -i "s/^OWMSZ[ ]*=.*/OWMSZ=$owmsize/" $SIMMAKEF


###############################################################################
# LUA FILTERING
# Overried copyfromsandbox
[ "$docopy" = "1" ] && copyfromsandbox="1"

INSPOINTPREV="tmpdir"; modfile="$SIMCONFIG"
debug2 "copyfromsandbox=$copyfromsandbox"
if [ "$copyfromsandbox" = "0" ]; then
   chelem co 0 "" "copy_files_prefix" 
   chelem ci 0 "" "copy_files=\"\"" 
else
   chelem ci 0 "" "copy_files_prefix" "=runid..\"-\"..cotson_pid..\".\""
   chelem co 0 "" "copy_files="
fi
# SET clean_sandbox to false
#INSPOINTPREV="node_config="; modfile="$SIMCONFIG"
   INSPOINTPREV="runid=";
   chelem ci 0 "" "clean_sandbox=" "false"
   chelem ci 0 "" "debug=" "true"


# SET control_script so that it includes the cotson_pid
if [ "$setcontrolscript" = "1" ]; then
   chelem ci 0 "" "control_script_file" "=runid\.\.\"-\"\.\.cotson_pid\.\.\"-ctrl\""
fi


# SET THE CLUSTER NODES
if [ "$setclusterunconditionally" = "1" -o "$NODES1" -gt 1 -a "$setclusternodes" = "1" ]; then
   clusternodes_tsu=`egrep "^[ \t]*[-]*[ \t]*cluster_nodes" $SIMCONFIG`
   if [ ! -z "$clusternodes_tsu" ]; then
      debug1 "   updating cluster_nodes in $SIMCONFIG"
#      sed -i "s/^[ \t]*[-]*[ \t]*cluster_nodes[ ]*=.*/    cluster_nodes=$nodes/" $SIMCONFIG
      sed -i "s/^[ \t]*[-]*[ \t]*cluster_nodes[ ]*=.*/cluster_nodes=$LUANNODES/" $SIMCONFIG
   else
      debug1 "   inserting cluster_nodes in $SIMCONFIG"
#      sed -i "/copy_files_prefix/a\    cluster_nodes=$nodes" $SIMCONFIG
      sed -i "/copy_files_prefix/a\cluster_nodes=$LUANNODES" $SIMCONFIG
   fi
#----
#   controlscript_tsu=`egrep "^[ \t]*[-]*[ \t]*control_script_file" $SIMCONFIG`
#   if [ ! -z "$controlscript_tsu" ]; then
#      if [ "$verbose" -gt "0" ]; then echo "   updating control_script_file in $SIMCONFIG"; fi
##      sed -i "s/^[ \t]*[-]*[ \t]*control_script_file[ ]*=.*/    control_script_file=runid\.\.\"-ctrl\"/" $SIMCONFIG
##      sed -i "s/^[ \t]*[-]*[ \t]*control_script_file[ ]*=.*/control_script_file=runid\.\.\"-ctrl\"/" $SIMCONFIG
#      sed -i "s/^[ \t]*[-]*[ \t]*control_script_file[ ]*=.*/control_script_file=runid\.\.\"-\"\.\.cotson_pid\.\.\"-ctrl\"/" $SIMCONFIG
#   else
#      if [ "$verbose" -gt "1" ]; then echo "   inserting control_script_file in $SIMCONFIG"; fi
##      sed -i "/copy_files_prefix/a\    control_script_file=runid\.\.\"-ctrl\"" $SIMCONFIG
##      sed -i "/copy_files_prefix/a\control_script_file=runid\.\.\"-ctrl\"" $SIMCONFIG
#      sed -i "/copy_files_prefix/a\control_script_file=runid\.\.\"-\"\.\.cotson_pid\.\.\"-ctrl\"" $SIMCONFIG
#   fi
#----
   nodeconfig_tsu=`egrep "^[ \t]*[-]*[ \t]*node_config" $SIMCONFIG`
   ncfn=`echo "$nodeconfigfn"|sed "s@/@\\\\\\\\/@g"`
   ncco=`echo "$nodeconfigcomm"|sed "s@/@\\\\\\\\@g"`
   if [ ! -z "$nodeconfig_tsu" ]; then
      if [ "$debug" -gt "1" ]; then echo "[DEBUG]: ===> HERE commented node_config <====="; fi
      debug1 "   updating node_config in $SIMCONFIG"
      sed -i "s/^[ \t]*[-]*[ \t]*node_config[ ]*=.*/--node_config=\"$ncfn\"$ncco/" $SIMCONFIG
   else
      debug1 "   inserting node_config in $SIMCONFIG"
      sed -i "/copy_files_prefix/a\node_config=\"$ncfn\"$ncco" $SIMCONFIG
   fi
else
   # comment out the following
   sed -i "s/^[ \t]*[^-]*[ \t]*cluster_nodes[ ]*=\(.*\)/    -- cluster_nodes=\1/" $SIMCONFIG
#   sed -i "s/^[ \t]*[^-]*[ \t]*control_script_file[ ]*=\(.*\)/    -- control_script_file=\1/" $SIMCONFIG
   sed -i "s/^[ \t]*[^-]*[ \t]*node_config[ ]*=\(.*\)/-- node_config=\1/" $SIMCONFIG
fi


# SET THE TSU STATFILE
#add if missing
if [ -z "$threadstatfile" ]; then
#   sed -i "s/^[ \t]*[-]*[ \t]*tsu_statfile=.*/    -- $threadstatopt=\"..\/..\/\"\.\.runid\.\.\".$threadstatfile\",/" $SIMCONFIG
   sed -i "s/^[ \t]*[-]*[ \t]*tsu_statfile=.*/    -- $threadstatopt=runid\.\.\".$threadstatfile\",/" $SIMCONFIG
else
   INSPOINTPREV="custom_asm"; modfile="$SIMCONFIG"
#   chelem ci 4 "" "$threadstatopt=" "\"..\/..\/\"\.\.runid\.\.\".$threadstatfile\","
   chelem ci 4 "" "$threadstatopt=" "runid\.\.\".$threadstatfile\","
fi


# SET THE DISPLAY
if [ "$display" = "yes" ]; then
   sed -i "s/^[ \t]*[-]*[ \t]*display=\(.*\)/    display=\1/" $SIMCONFIG
else
   sed -i "s/^[ \t]*display[ ]*=\(.*\)/    -- display=\1/" $SIMCONFIG
fi

# SET THE BSD
sed -i "s/^[ \t]*[^-]*[ \t]*use_bsd\(.*\)/    use_bsd\(\'$BSDFN\'\)/" $SIMCONFIG


samplerpref="^[ \t]*[^-]*[ \t]*sampler[ ]*=[ ]*"


# SET ROI definitions
if [ "$roi" = "yes" ]; then # ROIDEF BEGIN
   # COTSON TRACER
   [ -s  $LOCALCBIN/cotson_tracer ] || isoinfo -x '/COTSON_G.TGZ;1' -i $SIMBASE/trunk/data/cotson-guest-tools.iso |tar xzf - usr/bin/cotson_tracer -O > $LOCALCBIN/cotson_tracer
   chmod +x  $LOCALCBIN/cotson_tracer
   [ -s $LOCALCBIN/cotson_tracer.h ] || isoinfo -x '/COTSON_G.TGZ;1' -i $SIMBASE/trunk/data/cotson-guest-tools.iso |tar xzf - usr/include/cotson_tracer.h -O >$LOCALCBIN/cotson_tracer.h
#   [ -s cotson_tracer.h ] || cp $SIMBASE/trunk/src/examples/tracer/cotson_tracer.h .
   [ -s  $LOCALCBIN/cotson_tracer -a -s $LOCALCBIN/cotson_tracer.h ] || ( echo "ERROR: cannot perform ROI estimation without cotson_tracer and cotson_tracer.h";  exit 34 )

   # ROI definition for SIMCONFIG file
   debug1 "   updating the ROI in $SIMCONFIG"
   checksimnowcom=`sed '/simnow.commands/,/end/!d' $SIMCONFIG`
   if [ -z "$checksimnowcom" ]; then
      echo "ERROR: '$SIMCONFIG' doesn't have the simnow.commands section"
      exit 23
   fi

   modfile="$SIMCONFIG"
   firstinspoint="\-\- ROI helper function 1"
   checkroidef=`grep "$firstinspoint" $SIMCONFIG`
   if [ -z "$checkroidef" ]; then
#NOTE: the following is NOT bulletproof but can't find a better alternate for now
      sed -i "/simnow.commands/,/^end/s/^end/end\n\n$firstinspoint/" $SIMCONFIG
#      sed -i "/simnow.commands/{N;s/\(simnow.commands[^e]*end\)/\1\n\n$firstinspoint/}" $SIMCONFIG
   fi
   
   INSPOINTPREV="$firstinspoint"
   checksamplercons=`sed '/function sampler_constructor/,/end/!d' $SIMCONFIG`
   if [ -z "$checksamplercons" ]; then
      chelem ci 0 "" "function sampler_constructor" "(i)"
      chelem ci 4 "" "if i == 0 then return " "{type=\"no_timing\", quantum=\"1M\"} end"
      chelem ci 4 "" "if i == 1 then return " "{type=\"simple\", quantum=\"4M\"} end"
      chelem ci 0 "" "end -- sampler_constructor" ""
   fi

   firstinspoint="\-\- ROI helper function 2"
   checkzonechdef=`grep "$firstinspoint" $SIMCONFIG`
   if [ -z "$checkzonechdef" ]; then
#NOTE: the following is NOT bulletproof but can't find a better alternate for now
      sed -i "/function sampler_constructor/,/end -- sampler_constructor/s/end -- sampler_constructor/end -- sampler_constructor\n\n$firstinspoint/" $SIMCONFIG
   fi
   
   INSPOINTPREV="$firstinspoint"
   checkzonechan=`sed '/function zone_changer/,/end/!d' $SIMCONFIG`
   if [ -z "$checkzonechan" ]; then
      chelem ci 0 "" "function zone_changer" "(start,i)"
      chelem ci 4 "" "if start then -- entering zone i" ""
      chelem ci 8 "" "print(\"\### entering zone \" .. i)" ""
      chelem ci 8 "" "return 1 -- zone start" ""
      chelem ci 4 "" "end -- zone start" ""
      chelem ci 4 "" "if not start then -- leaving zone i" ""
      chelem ci 8 "" "print(\"\### leaving zone \" .. i)" ""
      chelem ci 8 "" "return 0 -- zone end" ""
      chelem ci 4 "" "end -- zone end" ""
      chelem ci 0 "" "end -- zone_changer" ""
   fi
   sed -i "s/^[ \t]*[^-]*[ \t]*sampler[ ]*={.*}[,]*/    sampler={ type=\"selective_tracing\",quantity=\"2\",constructor=\"sampler_constructor\",changer=\"zone_changer\" },/" $SIMCONFIG
   samplerpref="^[ \t]*if i == 1 then return[ ]*"
#   sed -i "s/\(^[ \t]*[^-]*[ \t]*sampler[ ]*={.*}[,]*\)/--\1/" $SIMCONFIG

if [ "$classicroi" = "1" ]; then
   # INSERT ROI IN THE C FILE (hypothesis: START after 'int main', END after 'void report'
   modfile="${appi}.c"
   if [ ! -s "$modfile" ]; then modfile="${appi}.cpp"; fi
   if [ ! -s "$modfile" ]; then echo "WARNING: cannot find ${appi}.c nor ${appi}.cpp"; fi

   debug1 "   updating the ROI in $modfile"
   INSPOINTPREV=""  # insert at the beginning of the file
   local cotsontracerh=`echo "$LOCALCBIN/cotson_tracer.h"|sed 's;/;\\\\/;g'`
   chelem ci 0 "" "#include \"$cotsontracerh\"" ""
#   check1=`grep -zo "$ROISTART" $modfile` # gives '-bash: warning: command substitution: ignored null byte in input'
   check1=`grep -o "$ROISTART" $modfile`
#echo "check1(1)='$check1'  '$ROISTART'"
   if [ -z "$check1" ]; then
      if [ "$verbose" -gt "1" ]; then echo "   updating $modfile"; fi
#      sed -i "/int main/,/{/s/{/{\n    $ROISTART\n/" $modfile  #takes too large match
      sed -i "/int main/{N;s/\(int main[^{]*{\)/\1\n    $ROISTART\n/}" $modfile
   fi
#   check1=`grep -zo "$ROIEND" $modfile` # gives '-bash: warning: command substitution: ignored null byte in input'
   check1=`grep -o "$ROIEND" $modfile`
#echo "check1(2)='$check1'  '$ROIEND'"
   if [ -z "$check1" ]; then
      if [ "$verbose" -gt "1" ]; then echo "   updating $modfile"; fi
#      sed -i "/void report/,/{/s/{/{\n    $ROIEND\n/" $modfile  #takes too large match
      sed -i "/void report/{N;s/\(void report[^{]*{\)/\1\n    $ROIEND\n/}" $modfile
   fi
fi
fi #ROIDEF END

# INSERT OPTION log_sim_time
debug1 "   updating the log_sim_time in $SIMCONFIG"
INSPOINTPREV="options[ \t]*="; modfile="$SIMCONFIG"  # insert at the beginning of the fil
chelem ci 4 "" "log_sim_time=" "\"true\","

# SET THE TIMING SAMPLER
debug1 "   updating the sampler in $SIMCONFIG"
IFS='+' read -a t1 <<< "$timing"
if [ "${t1[0]}" = "notiming" ]; then t1[0]="no_timing"; fi # avoid conflicts within $pref
if [ "${t1[0]}" = "dynamic" ]; then
   sed -i "s/\(${samplerpref}\){.*}\([,]*\)/\1{type=\"dynamic\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\", maxfunctional=${t1[4]}, sensitivity=\"${t1[5]}\", variable={\"cpu\.\*\.other_exceptions\"},}\2/" $SIMCONFIG
elif [ "${t1[0]}" = "smarts" ]; then
   sed -i "s/\(${samplerpref}\){.*}\([,]*\)/\1{type=\"smarts\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\"}\2/" $SIMCONFIG
#   sed -i "s/\(^[ \t]*[^-]*[ \t]*sampler[ ]*=\){.*}[,]*/\1{type=\"smarts\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\"},/" $SIMCONFIG
elif [ "${t1[0]}" = "interval" ]; then
   sed -i "s/\(${samplerpref}\){.*}\([,]*\)/\1{type=\"interval\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\"}\2/" $SIMCONFIG
else
   sed -i "s/\(${samplerpref}\){.*}\([,]*\)/\1{type=\"${t1[0]}\", quantum=\"${t1[1]}\"}\2/" $SIMCONFIG
fi
#if [ "${t1[0]}" = "dynamic" ]; then
#   sed -i "s/^[ \t]*[^-]*[ \t]*sampler[ ]*={.*}[,]*/    sampler={type=\"dynamic\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\", maxfunctional=${t1[4]}, sensitivity=\"${t1[5]}\", variable={\"cpu\.\*\.other_exceptions\"},},/" $SIMCONFIG
#elif [ "${t1[0]}" = "smarts" ]; then
#   sed -i "s/^[ \t]*[^-]*[ \t]*sampler[ ]*={.*}[,]*/    sampler={type=\"smarts\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\"},/" $SIMCONFIG
#elif [ "${t1[0]}" = "interval" ]; then
#   sed -i "s/^[ \t]*[^-]*[ \t]*sampler[ ]*={.*}[,]*/    sampler={type=\"interval\", functional=\"${t1[1]}\", warming=\"${t1[2]}\", simulation=\"${t1[3]}\"},/" $SIMCONFIG
#else
#   sed -i "s/^[ \t]*[^-]*[ \t]*sampler[ ]*={.*}/    sampler={type=\"${t1[0]}\", quantum=\"${t1[1]}\"},/" $SIMCONFIG
#fi


    if [ ! -z "$narch" ]; then
      INSPOINTPREV="mediator[ \t]*=[ \t]*{"; modfile="$SIMCONFIG"
      IFS='+' read -a t2 <<< "$narch"
      chelem ci 4 "" "timer=" "\{ type=\"topology\", num_nodes=NNODES, verbose=netverbose \},"
      case $t2[0] in
         simple) chelem ci 4 "" "topology_timing=" \
            "{ type=\"simple\",  bandwidth=$t2[1], node_latency=$t2[2], verbose=netverbose \}," ;;
         *)      chelem ci 4 "" "topology_timing=" \
            "{ type=\"simple\",  bandwidth=1000, node_latency=20, verbose=netverbose \}," ;;
      esac
    fi

    if [ ! -z "$ntopology" ]; then
      INSPOINTPREV="mediator[ \t]*=[ \t]*{"; modfile="$SIMCONFIG"
      IFS='+' read -a t2 <<< "$ntopology"
      chelem ci 4 "" "timer=" "\{ type=\"topology\", num_nodes=NNODES, verbose=netverbose \},"
      case $t2[0] in
         ring)    chelem ci 4 "" "topology=" \
            "{ type=\"ring\", num_nodes=NNODES, duplex=$t2[1], verbose=netverbose \}," ;;
         star)    chelem ci 4 "" "topology=" \
            "{ type=\"star\", num_nodes=NNODES, verbose=netverbose \}," ;;
         torus2D) chelem ci 4 "" "topology=" \
            "{ type=\"torus2D\", num_nodes=NNODES, rows=-1, columns=-1, duplex=$t2[1], verbose=netverbose \}," ;;
         *)        chelem ci 4 "" "topology=" \
            "{ type=\"ring\", num_nodes=NNODES, duplex=true, verbose=netverbose \},"
      esac
    fi

    if [ ! -z "$medtrfile" ]; then
      INSPOINTPREV="mediator[ \t]*=[ \t]*{"; modfile="$SIMCONFIG"
      localbuf=`echo "$medtrfile"|sed 's/\//\\\\\\//g'`
      chelem ci 4 "" "tracefile=" "$localbuf"
   fi


initoparams indse # initialize parameters for printstats

# SET THE CACHE HIERARCHY
stdcachehier="1"
if [ "$stdcachehier" = "1" ]; then
   buildarch
   updatelistomissingparams
fi


#manage tsu latencies
defl=${tsulv[0]}
rdl=${tsulv[1]}
wrl=${tsulv[2]}
subl=${tsulv[3]}
schl=${tsulv[4]}
sed -i "s/^[ \t]*[^-]*[ \t]*${strtsudeflat}=.*${strtsucomlat}/   ${strtsudeflat}=${defl}*${strtsucomlat}/" $SIMCONFIG
sed -i "s/^[ \t]*[^-]*[ \t]*${strtsurdlat}=.*${strtsucomlat}/    ${strtsurdlat}=${rdl}*${strtsucomlat}/" $SIMCONFIG
sed -i "s/^[ \t]*[^-]*[ \t]*${strtsuwrlat}=.*${strtsucomlat}/     ${strtsuwrlat}=${wrl}*${strtsucomlat}/" $SIMCONFIG
sed -i "s/^[ \t]*[^-]*[ \t]*${strtsusublat}=.*${strtsucomlat}/    ${strtsusublat}=${subl}*${strtsucomlat}/" $SIMCONFIG
sed -i "s/^[ \t]*[^-]*[ \t]*${strtsuschlat}=.*${strtsucomlat}/    ${strtsuschlat}=${schl}*${strtsucomlat}/" $SIMCONFIG
#to review
#sed -i "s/^[ \t]*[^-]*[ \t]*${strtsusndlat}=.*${strtsucomlat}/    ${strtsusndlat}=${snd}*${strtsucomlat}/" $SIMCONFIG
#sed -i "s/^[ \t]*[^-]*[ \t]*${strtsurcvlat}=.*${strtsucomlat}/    ${strtsurcvlat}=${rcv}*${strtsucomlat}/" $SIMCONFIG

#manage hdd
sed -i "s/^[ \t]*[^-]*[ \t]*use_hdd.*/    use_hdd('${hd}\.img')/" $SIMCONFIG

# manage database
chelem del 4 "" "heartbeat1="
chelem del 4 "" "heartbeat="
INSPOINTPREV="options[ \t]*=.*{"; modfile="$SIMCONFIG"
#chelem ci2 4 "" "heartbeat1=" "{type=\"file_last\", logfile=runid..\".log\" },"
if [ "$DBMODE" -ge "1" ]; then # DB is generated only for general stats (option section)
   local dbfile="${SIMDBFN//\//\\/}"
   chelem ins 4 "" "heartbeat={type=\"sqlite\",dbfile=\"$dbfile\",experiment_id=${exp_id},experiment_description=\"${FPRF1}\"},"
   chelem ci2 4 "" "heartbeat1=" "{type=\"file_last\", logfile=runid..\".log\" }," "" "" "options[ \t]*=.*{" "}"
else
   chelem ins 4 "" "heartbeat=" "{type=\"file_last\", logfile=runid..\".log\" }," "" "" "options[ \t]*=.*{" "}"
#   chelem ins 4 "" "heartbeat=" "{type=\"file_last\" }," "" "" "options[ \t]*=.*{" "}"
fi

# power section
if [ "$POWER" -gt "0" ]; then
   #assert: DBMODE>0 therefore heartbeat is  already inserted
   #assert: SIMCONFIG is already set
   debug1 "--------------------- POWER OPTIONS STARt"
   local pwrdir="$SIMSRCDIR/$POWERSUBDIR"
   if [ ! -d "$pwrdir" ]; then
      cecho "ERROR: power extraction hasb been requested, but power tool dir '$pwrdir' is not found!" "$red"
      exit 1
   fi
   psubdir=`echo "$pwrdir"|sed 's/\//\\\\\//g'` # manage '/' inside the path
   chelem ins 4 "" "power_per_hb={"
   chelem ins 8 "" "mcpat_script_path=\"$psubdir\","
   chelem ins 8 "" "mcpat_script=\".\/$POWERSCRIPT\","
   chelem ins 8 "" "script_additional_options=\"$POWERSCROPT\","
   chelem ins 8 "" "n_heartbeats=$POWERPERHBS,"
   chelem ins 4 "" "}, -- end power options" # do not chelem a '{' alone !
   debug1 "--------------------- POWER OPTIONS END"
fi

# manage database in the mediator
INSPOINTPREV="mediator[ \t]*=.*{"; modfile="$SIMCONFIG"
if [ "$DBMODE" = "2" ]; then # DB is generated both for general stats and mediator (option and mediator sections)
   local dbfile="${SIMDBFN//\//\\/}"
   chelem ins 4 "" "heartbeat=" "{type=\"file_last\", logfile=runid..\"mediator.log\" }," "" "" "mediator[ \t]*=.*{" "}"
   chelem ins 4 "" "heartbeat1={type=\"sqlite\",dbfile=\"$dbfile\",experiment_id=${exp_id},experiment_description=\"${FPRF1}\"}," "" "" "mediator[ \t]*=.*{" "}"
else
   chelem ins 4 "" "heartbeat=" "{type=\"file_last\", logfile=runid..\"mediator.log\" }," "" "" "mediator[ \t]*=.*{" "}"
fi
################################ LAUNCHING OPERATION
if [ "$BINMAKE" != "0" ]; then
   CPSTARTING="MAKING"
   CPSTARTED="MAKING_DONE"

#   # manage application command line parameters
#   local inputfilter="${infilter[$appi]}"
#   if [ -s ./$inputfilter ]; then
#      verbose2 "      pwd=pwd"
#      verbose2 "      sh ./$inputfilter ${SIZE1V[@]}"
#      sh ./$inputfilter ${SIZE1V[@]}
#   fi
   exesim make "-f $SIMMAKEF $appi" "make-$myapp.log" "---- STARTED MAKE `date` $EXP $MYDSETG" "compiling" "" "make" "1"


   if [ -s "$appi" ]; then mv $appi $myapp; fi
#
#   cp -af ${myapp} $SERVCBIN
   ok1="0"
   if [ -s "$myapp" -a "$MYSTATUS" = "0" ]; then ok1="1"; fi

else
   CPSTARTING="BOOTING_NODES"
   CPSTARTED="BOOTED"
   MSGEXPSTART="$msgSIMSTART `date` $EXP $VERSION1 $DSTRT ${MYMACH}-$MYPID"
   exesim "$exename" "-f $SIMMAKEF $runmode" "$OUTFILE" "$MSGEXPSTART" "$MSGSIMRUN" "$exp_desc" "$VIRTUALIZER" "$NODES1"

   # create 'SIMOUT' file
#   rm -f SOUT >/dev/null 2>/dev/null; cp $OUTFILE SOUT
   rm -f $SOUTFILE >/dev/null 2>/dev/null; cp $OUTFILE $SOUTFILE
   rm -f $OUTFILE 2>/dev/null

   # create 'NODEOUT' file (if missing)
   for ((nnn=1;nnn<=NODES1;nnn++)); do
      buildfn1 SIMLOG $nnn; fns="$fninn"
      buildfn1 NODEOUT $nnn; fno="$fninn"
      if [ "$nnn" = "1" ]; then
         verbose2 "* SIMLOG[1]=$fns"
         verbose2 "* NODEOUT[1]=$fno"
      fi
      if [ "$fns" != "$fno" -a "$fns" != "" ]; then
         debug2 "fns='$fns' --> '$fno'"
         if [ -s $fns ]; then
            verbose2 "   $fns --> $fno"
            rm -f $fno >/dev/null 2>/dev/null
#          cp $fns $fno
            mv $fns $fno
         else
            if [ "$verbose" != "0" ]; then
               cecho "   WARNING: SIMLOG file '$fns' not found." "$yellow"
            fi
         fi
      fi
   done


   # check if all workers are done on all nodes
   ok1="1"

   #search report node (master node), i.e. THE node which says SUCCESS or FAIL
   found=""
   for ((nnn=1;nnn<=NODES1;nnn++)); do
      buildfn1 NODEOUT $nnn; fno="$fninn"
      if [ -s $fno ]; then
         if [ "$NODES1" -gt "1" ]; then
            isrep=`grep "$masternodesig" $fno 2>/dev/null` || isrep=""
            debug2 "$nnn: isrep=$isrep"
            if [ "$isrep" != "" ]; then found=$nnn; fi
         else
            found="1"
         fi
      fi
   done
   masternode="$found"
   if [ -z "$masternode" ]; then
      if [ "$quiet" = "0" ]; then 
         cecho "WARNING: cannot locate the verification-masternode -- assuming node1." "$yellow"; ok1="0";
      fi
      masternode="1"
   fi
   verbose2 "   masternode=$masternode"
   
   goodbye=""
   checksuccess=""
   foundsucc="0"
   nodesfailed=""
#   if [ "$verbose" != "0" ]; then echo "   model=$model"; fi
   for ((nnn=1;nnn<=NODES1;nnn++)); do
      nnnn=`expr $nnn - 1 + $nodebase`
      buildfn1 NODEOUT $nnn; fn="$fninn";
      if [ -s $fn ]; then
         goodbye=`grep "$goodstring" $fn`
         checksuccess=`grep "${stringok[$appi]}" $fn`
         if [ "$checksuccess" != "" ]; then 
            if [[ $checksuccess == *"${stringok[$appi]}"* ]]; then
               foundsucc="$nnn"
            fi
         fi

#         # temporary patch for generic models:
         if [ "$dontchecksucc" = "1" -a "$foundsucc" -gt "0" ]; then goodbye="ok"; fi

         if [ "$goodbye" = "" ]; then
            ok1="0"; 
            if [ "$nodesfailed" = "" ]; then nodesfailed="$nnn"; else nodesfailed="$nodesfailed $nnn"; fi
         fi
      else
         ok1="0"
         if [ "$nodesfailed" = "" ]; then nodesfailed="$nnn"; else nodesfailed="$nodesfailed $nnn"; fi
      fi
   done

   # Stricter check: only masternode should print SUCCESS
   if [ "$masternode" -gt "0" ]; then
      # foundsucc="0" # enables stricer check: SUCCESS should be on masternode
      nnnn=`expr $masternode - 1 + $nodebase`
      buildfn1 NODEOUT $nnn; fn="$fninn";
      if [ -s $fn ]; then
         goodbye=`grep "$goodstring" $fn`
         checksuccess=`grep "${stringok[$appi]}" $fn`
         if [ "$checksuccess" != "" ]; then 
            if [[ $checksuccess == *"${stringok[$appi]}"* ]]; then
               foundsucc="$masternode"
            fi
         fi
      fi
   fi

# 
#   if [ "$goodbye" != "" -a "$foundsucc" != "0" ]; then
   if [ "$nodesfailed" = "" -a "$foundsucc" != "0" ]; then
#      OKSTRING="[OK_____]"; OKSTRCOLOR="$green"
      OKSTRING="[OK_____]"; OKSTRCOLOR="$green"; ok1="1"
   else
      OKSTRING="[ERROR__]"; ok1="0"; OKSTRCOLOR="$magenta";
   fi
   if [ "$quiet" = "0" ]; then 
      [ -z "$nodesfailed" ] || echo "   nodesfailed={ $nodesfailed }(no '$goodstring' in their output)"
      [ "$verbose" -gt "0" ] && echo -n "   ok1='$ok1'"
      [ "$foundsucc" != "0" ] && verbose1 "  (checkernode=$foundsucc)" || verbose1 "  (no checker node)"
   fi

   # prepend the content of *node_config.log (NODESCRIPTOUT)
   # to the content of stdout.log (NODEOUT)
   if [ "$prependstdoutlog" = "1" ]; then
      for ((nnn=1;nnn<=NODES1;nnn++)); do
         buildfn1 NODEOUT $nnn; fna="$fninn";
         buildfn1 NODESCRIPTOUT $nnn; fnb="$fninn";
         debug1 "fna='$fna' fnb='$fnb'"
#         if [ -s "$fna" -a -s "$fnb" ]; then
         if [ -f "$fna" -a -f "$fnb" ]; then
            debug1 "fnb+fna-->fna"
            echo "------- NODESCRIPTOUT (former $fnb) above -------------">>$fnb
            cat $fna >> $fnb
            rm -f $fna 2>/dev/null
            mv $fnb $fna
         else
            debug1 "fnb or fna is missing"
         fi
      done
   fi

   #
   if [ "$docopy" = "1" ]; then
      buildfn1 SIMCONFIG 1
      rm -f ${RUNID}-${COTSONPID[1]}.luafile; cp $fninn ${RUNID}-${COTSONPID[1]}.luafile
      buildfn1 SIMMAKEF 1
      rm -f ${RUNID}-${COTSONPID[1]}.makefile; cp $fninn ${RUNID}-${COTSONPID[1]}.makefile
      buildfn1 NODESCRIPT 1
      rm -f ${RUNID}-${COTSONPID[1]}.guestconfig; cp $fninn ${RUNID}-${COTSONPID[1]}.guestconfig
   fi

   #-----------------------------------------------------------------------------
   if [ "$GUESTOUT" = "1" ]; then
      [ "$verbose" -gt "0" ] && echo -e "$GUESTFILEOUT"
   fi
   #-----------------------------------------------------------------------------

   # Check the presence of SIMNODTIMER files
   oksntimer="0"
   for ((nnn=1;nnn<=NODES1;nnn++)); do
      buildfn1 SIMNODTIMER $nnn; fn="$fninn"
      [ -s "$fn" ] && oksntimer=`expr $oksntimer + 1`
   done
   debug1 "oksntimer=$oksntimer  NODES1=$NODES1"

   #-----------------------------------------------------------------------------
   SAVFILEERR="0"
   comment "   Saving files into $OUTDIR/$FPREF ..."
   savefile1 "SIMOUT"
   savefile1 "SIMMAKEF"
   savefile1 "SIMCONFIG"
   #-----------------------------------------------------------------------------
   savefile1 "SIMMONITOR"
   savefile1 "SIMMEDTIMER" 1
   savefile1 "SIMMEDOUT" 1
   for ((nnn=1;nnn<=NODES1;nnn++)); do
      savefile1 "NODESCRIPT" "$nnn"
      savefile1 "NODEOUT" "$nnn"
      savefile1 "SIMNODTIMER" "$nnn"
      [ "$threadstats" = "1" ] && savefile1 "SIMTSUSTATS" "$nnn"
      savefile1 "SIMSCREEN" "$nnn"
      [ "$EXECTRACING" = "1" ] && savefile1 "EXECTRACE"   "$nnn"
      [ "$CPUTRACING" = "1"  ] && savefile1 "CPTRACE"     "$nnn"
      [ "$MEMTRACING" = "1"  ] && savefile1 "METRACE"     "$nnn"
      [ "$STATRACING" = "1"  ] && savefile1 "STTRACE"     "$nnn"
   done
   [ "$threadstats" = "1" ] && { nnn=1; savefile1 "SIMTSUSTATS1"; }

   # OUTPUT MANAGER FOR DB
   [ "$DBMODE" != "0" ] && savefile1 "SIMDBFN"
   [ "$SAVFILEERR" = "0" ] && comment "   ...all SAVED SUCCESSFULLY."
   #-----------------------------------------------------------------------------

   # PRINTOUT OF SOME STATISTICS FOR A QUICK CHECK
   if [ "$oksntimer" = "$NODES1" ]; then
      # Print a summary of the stats of the experiment
      printsummarystats "$ok1" "$MSGEXPSTART" SIMOUT NODEOUT SIMNODTIMER
   else
      echo ""
      cecho "******************************************************************" "$yellow"
      cecho "WARNING: no SIMNODTIMER files found --> no printout of statistics." "$yellow"
      cecho "******************************************************************" "$yellow"
   fi
   if [ "$GOSLOW" = "1" ]; then sleep 3; fi # some time to read on screen

   ###
   if [ "$ok1" = "1" ]; then
      SIMOKCOUNT=`expr $SIMOKCOUNT + 1`
      TIMEEND=`date +%y%m%d%H%M%S`
      [ -z "$EXTRACTEDSIM" ] || insertq $MYQDONE "$EXTRACTEDSIM" $EXTRACTEDCOST $GLOBFAILCNT $TIMESTART $TIMEEND $FAILEDMACHLIST ${MYMACH}-$MYPID
   else
      SIMFAILCOUNT=`expr $SIMFAILCOUNT + 1`
      GLOBFAILCNT=`expr $GLOBFAILCNT + 1`
      TIMEEND=`date +%y%m%d%H%M%S`
      [ -z "$EXTRACTEDSIM" ] || cleaninsertq $MYQFAILED "$EXTRACTEDSIM" $EXTRACTEDCOST $GLOBFAILCNT $TIMESTART $TIMEEND $FAILEDMACHLIST ${MYMACH}-$MYPID
   fi
   ##

fi #EXESIM IF

   # Cleanup the sandbox and related files
   if [ "$RUNID" != "" -a "$simdirs" = "1" ]; then
      for ((nnn=1;nnn<=NODES1;++nnn)); do
         if [ "${COTSONPID[$nnn]}" != "" ]; then
            [ "$docopy" = "0" ] && rm -rf $RUNID-${COTSONPID[$nnn]}* 2>/dev/null
         fi
      done
   fi


   if [ "$BINMAKE" = "1" ] ; then
      if [ "$ok1" = "1" ]; then
         SIMOKCOUNT=`expr $SIMOKCOUNT + 1`
         TIMEEND=`date +%y%m%d%H%M%S`
         [ -z "$EXTRACTEDSIM" ] || insertq $MYQDONE "$EXTRACTEDSIM" $EXTRACTEDCOST $GLOBFAILCNT $TIMESTART $TIMEEND $FAILEDMACHLIST ${MYMACH}-$MYPID
      else
         SIMFAILCOUNT=`expr $SIMFAILCOUNT + 1`
         GLOBFAILCNT=`expr $GLOBFAILCNT + 1`
         TIMEEND=`date +%y%m%d%H%M%S`
         [ -z "$EXTRACTEDSIM" ] || cleaninsertq $MYQFAILED "$EXTRACTEDSIM" $EXTRACTEDCOST $GLOBFAILCNT $TIMESTART $TIMEEND $FAILEDMACHLIST ${MYMACH}-$MYPID
      fi

      if [ "$MYSTATUS" = "0" -a "$NOSIM" = "0" ]; then 
         if [ -d "$EXPBASE" ]; then
            verbose1 "* Trying to backup '$myapp' into '$EXPBASE'..."
            comment "   cp -af ${myapp} $EXPBASE"
            cp -af ${myapp} $EXPBASE
            ec=$?
            verbose1 "  ...done with exitcode=$ec"
         fi
      fi

      if [ -s "$EXPBASE/${myapp}" ]; then
         cecho "  `ls -l $EXPBASE/${myapp}`" "$green"
      else
         cecho "ERROR: cannot find $EXPBASE/${myapp}" "$red"
      fi
   fi
   

#    # set condition to finish quickly in the last iteration
#   [ "$SIMREMAINING" = "0" ] && EXTRACTEDSIM=""

   if [ "$quiet" = "0" ]; then 
      echo "---"
   fi
   exp_id=$(($exp_id + 1))
}

##############################################################################
#-----------------------------------------------------------------------------
# SCRIPT STARTS HERE
#-----------------------------------------------------------------------------

NEWCOMMANDLINE="$*"
simloopcontinue="1"
SIMLOOPEMPTY="0"
while [ "$simloopcontinue" = "1" ]; do
   SAVEDCOMMANDLINE="$NEWCOMMANDLINE"
   md_commandline $SAVEDCOMMANDLINE
   debug1 "SAVEDCOMMANDLINE=$SAVEDCOMMANDLINE"
   startup1
   #-----------------------------------------------------------------------------
   debug1 "---------- START sourcing external file '$INFOFILE'"

   # NOTE: I need to to this both here and later since here I may have already 
   #       defined DSEPARAMS (later it might be sourced)
   [ -z "$DSEPARAMS" ] || [ "$BINMAKE" = "0" ] && DSEPARAMS="$DEFAULTDSEPARAMS" || DSEPARAMS="$DEFAULTDSEBINGEN"
   debug1 "DSEPARAMS=$DSEPARAMS"
   cleanvarnames "$DSEPARAMS" list
   debug1 "   RESULT=$RESULT"
   for elem in $RESULT; do eval unset $elem; eval declare -A $elem; done

   ##################################################
   # sourcing INFO FILE PARAMETERS
   verbose1 "* Sourcing INFOFILE=$INFOFILE"
   [ -s "$INFOFILE" ] && source $INFOFILE
   ##################################################

   # for now preset DSEPARAMS to this in case the user hasn't defined it
   [ -z "$DSEPARAMS" ] || [ "$BINMAKE" = "0" ] && DSEPARAMS="$DEFAULTDSEPARAMS" || DSEPARAMS="$DEFAULTDSEBINGEN"

   # for now preset DEPPARAMS to this in case the user hasn't defined it
   [ -z "$DEPPARAMS" ] && DEPPARAMS="$DEFAULTDEPPARAMS"

   debug1 "DSEPARAMS=$DSEPARAMS"
   cleanvarnames "$DSEPARAMS" list
   debug1 "RESULT=$RESULT"
   for elem in $RESULT; do eval unset $elem; eval declare -A $elem; done
   cleanvarnames "$DSEPARAMS"
   debug1 "RESULT=$RESULT"
   for elem in $RESULT; do eval unset $elem; eval declare -A $elem; done
   debug1 "DEPPARAMS"=$DEPPARAMS""
   cleanvarnames "$DEPPARAMS" ""
   debug1 "RESULT=$RESULT"
   for elem in $RESULT; do eval unset $elem; eval declare -A $elem; done

   ##################################################
   # sourcing INFO FILE PARAMETERS
   verbose1 "* Sourcing (again) INFOFILE=$INFOFILE"
   [ -s "$INFOFILE" ] && source $INFOFILE
   ##################################################

   debug1 "---------- END sourcing external file '$INFOFILE'"
   #-----------------------------------------------------------------------------
   libverdetect
   startup2
   #-----------------------------------------------------------------------------
   setupparams1

   #-----------------------------------------------------------------------------
   #[ "$NORUN" = "1" ] && exit 0  # doesn_t work: "return" is needed for sourcing this script
   [ "$NORUN" = "1" ] && return
   #-----------------------------------------------------------------------------
   #
   myexpstart 
   #-----------------------------------------------------------------------------
   setupparams2

   #-----------------------------------------------------------------------------
   #############################################################################
   # SIMULATION LOOP 
   #############################################################################
   #if [ "$BINMAKE" = "0" ]; then
   #   copybin2exedir
   #fi
   #
   #QFILE=${QUEUEDIR}/${EXP}.$MYQWAITING
   EXTRACTEDSIM="something"
   while [ ! -z "$EXTRACTEDSIM" ]; do  ## START SIMULATION LOOP
      #########
      mysimloop
      #########
   done  ## END SIMULATION LOOP
   #-----------------------------------------------------------------------------

   debug1 "SIMOKCOUNT=$SIMOKCOUNT  TOTEXPECTEDSIMS=$TOTEXPECTEDSIMS"
   if [ "$bingenrun" = "1" -a "$SIMOKCOUNT" = "$TOTEXPECTEDSIMS" ]; then
#      bingenrun="0"; BINMAKE="0"; GOSLOW="1"; POPULATE="0"; noretry="0"; nokill="0";
      bingenrun="0"; BINMAKE="0";
      cecho "=========================================================================" "$blue"
   else
      simloopcontinue="0"
   fi
done ## simloopcontinue
myexpend

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
##########################################################################
# MIT LICENSE:
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERi
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
##########################################################################
