/********************************************************************
 * matrixmult.c
 *
 * Demonstrates the use of openmp to parallelize computing the product
 * of two matrices, A and B of rank NxM and MxN, respectively, so it's
 * product, C, is NxN.
 * 
 * C_{ij} = \Sum_k A_{ik} B_{kj}
 *
 * Copyright Research Computing Center, University of Chicago
 * Author: Douglas Rudd - 4/19/2013
 *******************************************************************/ 
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <papi.h>

#define N 300
#define M 4000
#define NUM_EVENTS 3
int main( int argc, char *argv[] ) {
	int i, j, k;
	double *A, *B, *C;
	int retval;
	long long counters[3];
	int Events[] = {
		PAPI_TOT_CYC,
		PAPI_L2_DCM,
		PAPI_L2_DCA };
	int EventSet = PAPI_NULL;
	retval = PAPI_library_init(PAPI_VER_CURRENT);
    	retval = PAPI_create_eventset(&EventSet);
    	retval = PAPI_add_events(EventSet, Events, NUM_EVENTS);
//    	retval = PAPI_start(EventSet);
/*	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT) {
		printf(__FILE__,__LINE__,"PAPI_library_init",retval);
	}

	retval=PAPI_create_eventset(&EventSet);
	if (retval!=PAPI_OK) {
		printf( __FILE__, __LINE__, "PAPI_create_eventset", retval );
	}

	retval=PAPI_add_named_event(EventSet,"PAPI_L1_DCM");
	if (retval!=PAPI_OK) {
		printf( __FILE__, __LINE__, "adding PAPI_L1_DCM", retval );
	}*/
		PAPI_reset(EventSet);
		PAPI_start(EventSet);

	A = (double *)malloc(N*M*sizeof(double));
	B = (double *)malloc(N*M*sizeof(double));
	C = (double *)malloc(N*N*sizeof(double));

	if ( A == NULL || B == NULL || C == NULL ) {
		fprintf(stderr,"Error allocating memory!\n");
		exit(1);
	}

	/* initialize A & B */
	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) { 
			A[M*i+j] = 3.0;
			B[N*j+i] = 2.0;
		}
	}

	for ( i = 0; i < N*N; i++ ) {
		C[i] = 0.0;
	}

	retval = PAPI_start(eventset);

	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < N; j++ ) {
			for ( k = 0; k < M; k++ ) {
				C[N*i+j] += A[M*i+k]*B[N*k+j];
			}
		}
	}

	PAPI_stop( PAPI_events, counters );

	printf("%lld L2 cache misses (%.3lf%% misses) in %lld cycles\n", 
		counters[1],
		(double)counters[1] / (double)counters[2],
		counters[0] );

	free(A);
	free(B);
	free(C);
	
	return 0;
}
