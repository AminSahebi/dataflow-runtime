#include "tsu.h"
#include <stdio.h>
#include <time.h>

#ifdef PAPI
#include <papi.h>
#include "testcode.h"
#endif

int quiet, retval;
#ifdef PAPI
#define NUM_EVENTS 2 
int Events[] = {PAPI_TOT_CYC, PAPI_TOT_INS};
int EventSet = PAPI_NULL;
long long int values[NUM_EVENTS];
#endif
int nn; // input, for checking purposes
int th;
uint64_t freq;
uint64_t t1,t2,t3,t4,t5,t6;
struct timespec tv;
extern uint64_t df_ts0[100],df_ts1[100]; // timestamp buffers
extern uint64_t df_tt;
#ifdef clk
clock_t start_cycles, end_cycles;
uint64_t sum_add = 0;
uint64_t sum_fib = 0;
uint64_t sum_rep = 0;
clock_t adder_start,adder_stop,rep_start,rep_stop;
clock_t fib_start,fib_stop;
clock_t app_start,app_stop;
#endif
#ifdef hardware_counter
uint64_t start_cycles, end_cycles;
uint64_t sum_add = 0;
uint64_t sum_fib = 0;
uint64_t sum_rep = 0;
uint64_t adder_start,adder_stop,rep_start,rep_stop;
uint64_t temp4,temp5,temp6;
uint64_t fib_start,fib_stop;
uint64_t app_start,app_stop;
#endif
#ifdef PAPI
long long start_cycles, end_cycles;
uint64_t sum_add = 0;
uint64_t sum_fib = 0;
uint64_t temp6;
uint64_t temp4,temp5;
uint64_t sum_rep = 0;
long long adder_start,adder_stop,rep_start,rep_stop;
long long fib_start,fib_stop;
long long app_start,app_stop;
#endif
/*
   uint64_t get_cpu_freq(void) {
   FILE *fd;
   uint64_t freq = 0;
   float freqf = 0;
   char *line = NULL;
   size_t len = 0;

   fd = fopen("/proc/cpuinfo", "r");
   if (!fd) {
   fprintf(stderr, "failed to get cpu frequency\n");
   perror(NULL);
   return freq;
   }

   while (getline(&line, &len, fd) != EOF) {
   if (sscanf(line, "cpu MHz\t: %f", &freqf) == 1) {
   freqf = freqf * 1000000UL;
   freq = (uint64_t) freqf;
   break;
   }
   }

   fclose(fd);
   return freq;
   }
 */
void fib(void);
void report(void);
int main(int argc, char **argv)
{
#ifdef PAPI

	//PAPI initialization Library
	//Compile with
	//  gcc fib-tsu4.c -I/${PAPI_DIR}/include -L/${PAPI_DIR}/lib -O3 -o  fibtest -lpapi
	retval = PAPI_library_init(PAPI_VER_CURRENT);
    	retval = PAPI_create_eventset(&EventSet);
    	retval = PAPI_add_events(EventSet, Events, NUM_EVENTS);
	if (retval != PAPI_VER_CURRENT) {
		printf(__FILE__,__LINE__,"PAPI_library_init",retval);
	}
	PAPI_reset(EventSet);
	PAPI_start(EventSet);
#endif
	//nn = 10;
	if (argc > 1)
		nn = atoi(argv[1]);
	if (argc > 2)
		th = atoi(argv[2]);
	printf("\ncomputing fibonacci(%d,%d)\n",nn,th);
	df_tt = df_tstamp(df_ts0);
	//tt = df_tstamp(ts0);
	//	xzonestart(1);
	//	rdtscll(app_start);
#ifdef PAPI
	start_cycles = PAPI_get_real_cyc();
#endif	
#ifdef clk
	start_cycles = clock();
#endif
#ifdef Ctime
	clock_gettime(CLOCK_REALTIME, &tv);
	t1= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
#endif
#ifdef hardware_counter
	rdtscll(start_cycles);
#endif
	uint64_t trep = df_tschedule(&report,1); // spawn reporter
	uint64_t tfib = df_tschedule(&fib, 2);  // spawn fib
	df_write(tfib,0, _TLOC(trep,0)); // edge: fib.out -> report[0]
	df_write(tfib,1, nn); // send fib, nn
	//	printf("++main\n");
	df_destroy();
	return 0;
}

void adder(void)
{
#ifdef PAPI
	adder_start = PAPI_get_real_cyc();
#endif
#ifdef clk
	adder_start = clock();//PAPI_get_real_cyc();
#endif	
#ifdef hardware_counter
	rdtscll(adder_start);
#endif	
#ifdef Ctime
	clock_gettime(CLOCK_REALTIME, &tv);
	t3= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
#endif	
	df_ldframe();
	uint64_t n1 = df_frame(1); // receive n1
	uint64_t n2 = df_frame(2); // receive n2
	df_writeN(df_frame(0),n1+n2); // send n1+n2
	df_destroy();
#ifdef PAPI	
	adder_stop = PAPI_get_real_cyc();
	sum_add+=(adder_stop-adder_start);
#endif
#ifdef clk	
	adder_stop = clock();
	sum_add+=(adder_stop-adder_start);
#endif
#ifdef hardware_counter	
	rdtscll(adder_stop);
	sum_add+=(adder_stop-adder_start);
#endif

#ifdef Ctime
	clock_gettime(CLOCK_REALTIME, &tv);
	t4= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	sum_add+=(t4-t3);
#endif
}

// scalar version
uint64_t xfib(int n) {
	if( n<=1) return n;
	else return xfib(n-1) + xfib(n-2);}

void fib(void)
{
#ifdef PAPI	
	fib_start = PAPI_get_real_cyc();
#endif
#ifdef clk
	fib_start = clock();
#endif
#ifdef hardware_counter
	rdtscll(fib_start);
#endif
#ifdef Ctime
	clock_gettime(CLOCK_REALTIME, &tv);
	t5= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
#endif
	df_ldframe();
	int n = df_frame(1); // receive n
	//    printf("n = df_frame(1) = %d\n",n);
	//    fflush(stdout);
#if 0 
	if (n < XFIB_THRESHOLD) {
		df_writeN(df_frame(0),xfib(n)); // send n
	}
#endif
	if (n <= th+1 || n<2){
		rdtscll(temp4);
		df_writeN(df_frame(0),xfib(n)); // send n
		rdtscll(temp5);
		temp6+=(temp5-temp4);
	}
	else {
		uint64_t tfib1 = df_tschedule(&fib,2);  // spawn fib1
		uint64_t tfib2 = df_tschedule(&fib,2);  // spawn fib2

		df_write(tfib1,1, n-1); // send fib1, n-1
		df_write(tfib2,1, n-2); // send fib2, n-2
		uint64_t tadd = df_tschedule(&adder,3); // spawn adder
		df_write(tadd,0, df_frame(0)); // edge: adder.out -> self.out
		df_write(tfib1,0, _TLOC(tadd,1)); // edge: fib1.out -> adder[1]
		df_write(tfib2,0, _TLOC(tadd,2)); // edge: fib2.out -> adder[2]
	}
	df_destroy();
#ifdef PAPI	
	fib_stop = PAPI_get_real_cyc();
	sum_fib+=(fib_stop-fib_start);
#endif
#ifdef clk	
	fib_stop = clock();
	sum_fib+=(fib_stop-fib_start);
#endif	
#ifdef hardware_counter	
	rdtscll(fib_stop);
	sum_fib+=(fib_stop-fib_start);
#endif	
#ifdef Ctime	
	clock_gettime(CLOCK_REALTIME, &tv);
	t6= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	sum_fib+=(t6-t5);
#endif
}

void report(void)
{
	//	freq = get_cpu_freq();
	df_ldframe();
	int n=df_frame(0);
	df_destroy();
#ifdef Ctime
	clock_gettime(CLOCK_REALTIME, &tv);
	t2= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
#endif
#ifdef PAPI	

	end_cycles = PAPI_get_real_cyc();
	retval = PAPI_stop(EventSet, &values);

#endif	
#ifdef clk	
	end_cycles = clock();
#endif	
#ifdef hardware_counter	
	rdtscll(end_cycles);
#endif	
//	printf("++report\n");
//	fflush(stdout);
	//tt = df_tstamp(ts1) - tt;
	printf("df fib= %d\n",n);
//	printf("*** SUCCESS ***\n");
//	fflush(stdout);

//	printf("\n");
	printf("-----------------------MODEL STATISTICS---------------------\n");
	printf("tot_df_schedule					%lld\n",sch_count);
	printf("tot_df_write					%lld\n",write_count);
	printf("tot_df_read					%lld\n",read_count);
	printf("tot_df_load					%lld\n",load_count);
	printf("tot_df_destroy					%lld\n",dest_count);
	printf("-----------------------------------------------------------\n");
	printf("modeled_lat_df_schedule				%ld\n", 12);
	printf("modeled_lat_df_write 				%ld\n", 14);
	printf("modeled_lat_df_read				%ld\n", 12);
	printf("modeled_lat_df_load 				%ld\n", 12);
	printf("modeled_lat_df_destroy	 			%ld\n", 16);
	printf("----------------------------------------------------------\n");
	printf("tot_modeled_lat_df_schedule			%lld\n",sch_count*12);
	printf("tot_modeled_lat_df_write			%lld\n",write_count*14);
	printf("tot_modeled_lat_df_read				%lld\n",read_count*12);
	printf("tot_modeled_lat_df_load				%lld\n",load_count*12);
	printf("tot_modeled_lat_df_destroy			%lld\n",dest_count*16);
	printf("---------------------PAPI HW Counter----------------------\n");
	printf("hw_cyc_1_df_schedule				%ld\n", _schedule);
	printf("hw_cyc_1_df_write				%ld\n", _write);
	printf("hw_cyc_1_df_read				%ld\n", _read);
	printf("hw_cyc_1_df_load				%ld\n", _load);
	printf("hw_cyc_1_df_destroy				%ld\n", _dest);
	printf("-----------------------------------------------------------\n");
	printf("tot_hw_cyc_df_schedule				%ld\n", _schedule*sch_count);
	printf("tot_hw_cyc_df_write				%ld\n", _write*write_count);
	printf("tot_hw_cyc_df_read				%ld\n", _read*read_count);
	printf("tot_hw_cyc_df_load				%ld\n", _load*load_count);
	printf("tot_hw_cyc_df_destroy				%ld\n", _dest*dest_count);
	printf("-----------------------------------------------------------\n");
	printf("tot_hw_cycles_benchmark_df_schedule		%ld\n", sum_sch);
	printf("tot_hw_cycles_benchmark_df_write		%ld\n", sum_write);
	printf("tot_hw_cycles_benchmark_df_read			%ld\n", sum_df);
	printf("tot_hw_cycles_benchmark_df_load			%ld\n", sum_ld);
	printf("tot_hw_cycles_benchmark_df_destroy		%ld\n", sum_dest+temp3);
	printf("tot_hw_cycles_benchmark_report			%ld\n", sum_ld+sum_df);
	printf("tot_hw_cycles_benchmakr_adder			%lld\n", sum_add);
	printf("tot_hw_cycles_benchmakr_fib			%lld\n", sum_fib);
	printf("tot_DF_hw_cycles				%ld\n",sum_sch+temp3+sum_write+sum_dest+sum_ld+sum_df);
	//printf("total calculated user cycles is       %lld\n",sum_add+sum_fib+temp6+temp3);
	//printf("total hw benchmark cycles is          %lld\n", (end_cycles - start_cycles));
	printf("---------------------PAPI Events---------------------------\n");
  	printf( "PAPI_TOT_CYC total cycles			%ld\n", values[0] );
        printf( "PAPI_TOT_INS total instructions 		%ld\n", values[1] );
//        printf( "PAPI_L2_DCA  Level 2 data cache accesses =%ld\n", values[2] );
//        printf( "PAPI_L2_DCM  Level 2 data cache misses   =%ld (%.3lf%%)\n",values[3], (double)values[3]/(double)values[2] );

	df_exit();

}
