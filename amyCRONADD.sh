#!/bin/bash
# 161127 Roberto Giorgi - University of Siena, Italy
##########################################################################
# MIT LICENSE:
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERi
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
##########################################################################
# $id$
VERSION="161127"

pushd `dirname $0` > /dev/null; AMY_SCRIPTPATH=`pwd -P`; popd > /dev/null
AMY_PREFIX="amy"
AMY_CRONDEL="CRONDEL.sh"
STOPPER="${AMY_SCRIPTPATH}/${AMY_PREFIX}${AMY_CRONDEL}"

md5sig=`md5sum $0|awk '{print $1}'`
MYSIG="${md5sig:0:2}${md5sig:(-2)}" # reduced-hash: first 2 char and last two char
VERSION1="v${VERSION}-${MYSIG}"

#####################################################
usage() {
   echo "Usage: $0 [<options>] <SCRIPT>"
   echo "   where:"
   echo "   <SCRIPT>  is a **full-path** SCRIPT"
   echo ""
   echo "   and <options> can be:"
   echo "   -v          verbose mode (more '-v': more details)"
   echo "   -d          debug mode   (more '-d': more details)"
   echo "   -q          quiet mode"
   echo "   --cmd <COMMAND> give a specific command-word with the following syntax (see amy.cfg)"
   echo "         <COMMAND>::=<command_acronym_on_two_letters><machine|machine list|machine_set>['Q']"
   echo "   --tag <tag> operate with this tag"
   echo "   --next <tag> next operate with this tag"
   echo "   --timeout <minutes> | -t <minutes>"
   echo "   --repeat <minutes> | -r <minutes>"
   echo "   --mesg <mesg> | -m <mesg>"
   echo "   --version   print version"
   echo "   --help | -h for help"
   echo ""
}

#####################################################
printversion() {
   echo "$0: version $VERSION1";
}

#####################################################
function cecho ()            # Color-echo.
                             # Argument $1 = message
                             # Argument $2 = color
{
local default_msg="No message passed."
                             # Doesn't really need to be a local variable.

message=${1:-$default_msg}   # Defaults to default message.
color=${2:-$white}           # Defaults to white, if not specified.

  echo -en "$color"
  echo -n "$message"
  tput sgr0             # reset to normal
}

#####################################################
function comment() {
   if [ "$QUIET" = "0" ]; then echo "C: $1"; fi
}

#####################################################
function debug2() {
   if [ "$DEBUG" -gt "1" ]; then echo "D2: $1"; fi
}

#####################################################
function debug1() {
   if [ "$DEBUG" -gt "0" ]; then echo "D1: $1"; fi
}

#####################################################
function verbose2() {
   if [ "$VERBOSE" -gt "1" ]; then echo "$1"; fi
}

#####################################################
function verbose1() {
   if [ "$VERBOSE" -gt "0" ]; then echo "$1"; fi
}

#####################################################
shopt -s extglob
declare -A longoptspec
cac_commandline() {
   # set defaults
   VERBOSE="0"
   DEBUG="0"
   QUIET="0"
   AMYCMD=""
   TIMEREP="5"
   TIMEMAX=""
   MESG=""
   MYTAG=""
   MYNEXTTAG=""
   local printusage="0"

   #Note: the firs colon ':'means taht I manage the errors myself
   optspec=":t:r:m:vhdqV-:"

   # Use associative array to declare how many arguments a long option
   # expects. In this case we declare that loglevel expects/has one
   # argument and range has two. Long options that aren't listed in this
   # way will have zero arguments by default.
   longoptspec=( [cmd]=1 [timeout]=1 [repeat]=1 [mesg]=1 [tag]=1 [next]=1 )

   i=$(($# + 1)) # index of the first non-existing argument
   while [ $# -gt 0 ]; do
      # Resetting OPTIND is necessary if getopts was used previously in the script.
      # It is a good idea to make OPTIND local if you process options in a function.
      OPTIND=1
      #echo "STARTING: $@"
      #echo "OPTIND: $OPTIND"
      #echo "OPTARG: $OPTARG"
      #echo "\$#: $#"
      while getopts "$optspec" opt; do
      while true; do
      #echo "opt=$opt"
      case "${opt}" in # all argument that start with a '-' .........
         -)#OPTARG is name-of-long-option or name-of-long-option=value
           if [[ ${OPTARG} =~ .*=.* ]]; then # with this --key=value format only one argument is possible
              opt=${OPTARG/=*/}
              ((${#opt} <= 1)) && {
                 echo "Syntax error: Invalid long option '$opt'" >&2; exit 2;
              }
              if (($((longoptspec[$opt])) != 1)); then
                 echo "Syntax error: Option '$opt' does not support this syntax." >&2; exit 2;
              fi
              OPTARG=${OPTARG#*=}
           else if [ "${longoptspec[$OPTARG]}" = "1"  ]; then
              opt="$OPTARG"
              OPTARG=${@:OPTIND:1}
              ((OPTIND+=1))
           else #with this --key value1 value2 format multiple arguments are possible
              opt=$OPTARG
              ((${#opt} <= 1)) && {
                 echo "Syntax error: Invalid long option '$opt'" >&2; exit 2;
              }
              OPTARG=(${@:OPTIND:$((longoptspec[$opt]))})
              ((OPTIND+=longoptspec[$opt]))
              #echo $OPTIND
              ((OPTIND > i)) && {
                 echo "Syntax error: Not all required arguments for option '$opt' are given." >&2; exit 3;
              }
           fi fi
           continue #now that opt/OPTARG are set we can process them as
           # if getopts would've given us long options
           ;;
         cmd)       AMYCMD=$OPTARG ;;
         t|timeout) TIMEMAX=$OPTARG ;;
         r|repeat)  TIMEREP=$OPTARG ;;
         m|mesg)    MESG=$OPTARG ;;
         tag)       MYTAG=$OPTARG ;;
         next)      MYNEXTTAG=$OPTARG ;;
         h|help)    usage; exit 0 ;;
         v|verbose) VERBOSE=$((VERBOSE+1)) ;;
         d|debug)   DEBUG=$((DEBUG+1)) ;;
         q|quiet)   QUIET="1" ;;
         V|version) printversion; exit 0 ;;
         ?) echo "Syntax error: Unknown short option '$OPTARG'" >&2; exit 2; ;;
         *) echo "Syntax error: Unknown long option '$opt'" >&2; exit 2; ;;
      esac
      break; done
      done
      shift "$((OPTIND-1))" # Shift off the options and optional --.
      #echo "NON-OPTIONS: $@"
      case "$1" in
      !(-)*)
            if [ "$SCRIPT" = "" ]; then
            SCRIPT="$1";
            else
               if [ $# -gt 0 ]; then echo "Syntax error: unknow parameter '$1'."; printusage="1"; fi
            fi
         shift;
            ;;
      esac
      #echo "REMAINING: $@"
   done

   # Everything that's left in "$@" is a non-option.  In our case, a FILE to process.
   cmd1="$SCRIPT"

   # Check
   debug1 "VERBOSE=$VERBOSE"
   debug1 "DEBUG=$DEBUG"
   debug1 "QUIET=$QUIET"
   debug1 "AMYCMD=$AMYCMD"
   debug1 "timeout=$TIMEMAX"
   debug1 "repeat=$TIMEREP"
   debug1 "MESG='$MESG'"
   debug1 "MYTAG='$MYTAG'"
   debug1 "MYNEXTTAG='$MYNEXTTAG'"
   debug1 "cmd1='$cmd1'"
   [ -z "$cmd1" ] && { echo "Syntax error: No <script> was specified."; printusage="1"; }
   [ "$printusage" = "1" ] && { usage; exit 1; }
}

#-----------------------------------------------------------------------------------
cac_commandline "$@"

# min hour day_of_month month day_of_week(1==Mon)
t1="*/$TIMEREP * * * *"
#t2="*/$TIMEMAX * * * *"

[ -z "$TIMEREP" ] && optrep="" || optrep=" -r $TIMEREP"
[ -z "$TIMEMAX" ] && optmax="" || optmax=" -t $TIMEMAX"
[ -z "$MYTAG" ] && optmytag="" || optmytag=" --tag $MYTAG"
[ -z "$MYNEXTTAG" ] && optmynexttag="" || optmynexttag=" --next $MYNEXTTAG"

# Adding an at-job
echo "* Adding a stopper 'at now + $TIMEMAX' job '$cmd1':"
########################################################
echo "$cmd1$stopopts"|at now + $TIMEMAX min 2>/dev/null
########################################################
# Get at-job id
atjid=`atq|sort -r|head -1|awk '{print $1}'`
optatj=" --atj $atjid"
msgatj=" job $atjid"
echo "* STOPPER job-id is $atjid"

[ -z "$MESG" ] && optmsg="" || optmsg=" -m \"$MESG\""
[ -z "$MESG" ] && optstopmsg="" || optstopmsg=" -m \"[TIMEOUT]$msgatj $MESG\""
# NOTE: optmytag MUST BE THE FIRST OPTION ! See crondelcmd.sh
stopopts="$optmytag$optstopmsg --stop --cmd $AMYCMD"

#
extraopts="$optmytag$optmsg$optrep$optmax$optmynexttag$optatj --cmd $AMYCMD"

#
echo "* CMD: $cmd1"
#cmd1=`echo "$cmd"|awk '{print $1}'`
a1=`crontab -l 2>/dev/null`
#b1=`echo "$a1" |grep "$cmd1" 2>/dev/null`
b1=`echo "$a1" |grep "$cmd1$extraopts" 2>/dev/null`
if [ "$b1" = "" ]; then
   echo "* Inserting CMD"
   if [ "$a1" != "" ]; then
      echo "* Updating existing crontab..."
      a2=`echo "$a1"; echo "$t1 $cmd1$extraopts >/dev/null 2>&1"`
      if [ "$DEBUG" -gt 0 ]; then
         a1chrlen=${#a1}
         echo "  - current lenght of crontab: $a1chrlen"
         a2chrlen=${#a2}
         echo "  - new     lenght of crontab: $a1chrlen"
         #echo "------------------------------"
         #echo "(echo \"$a1\"; echo \"$t1 $cmd1$extraopts >/dev/null 2>&1\")|crontab -"
         #echo "------------------------------"
      fi
      echo "$a2"|crontab -
   else
      echo "* Creating crontab"
      echo "$t1 $cmd1$extraopts >/dev/null 2>&1"|crontab -
   fi
else
   echo "* CMD already present in crontab"
fi

if [ "$TIMEMAX" = "" ]; then exit 0; fi

##add a timerto stop the first command and myself
#cmd2="$STOPPER -self $cmd1"
#a2=`crontab -l 2>/dev/null`
#b2=`echo "$a2" |grep "$cmd2" 2>/dev/null`
#if [ "$b2" = "" ]; then
#   echo "* Inserting STOPPER CMD"
#   if [ "$a2" != "" ]; then
#      echo "* Updating existing crontab"
#      (echo "$a2"; echo "$t2 $cmd2 >/dev/nulli 2>&1")|crontab   
#   else
#      echo "* Creating crontab"
#      echo "$t2 $cmd2 >/dev/null 2>&1"|crontab   
#   fi
#else
#   echo "* STOPPER CMD already present in crontab"
#fi

# Better done with 'at'
#at -f $STOPPER now + $TIMEMAX min
