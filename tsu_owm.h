/* DRT - A Dataflow Run Time - v0.1
 * Copyright (c) Roberto Giorgi 2013/09/19 - FREE SOFTWARE: SEE NOTICE BELOW */

#ifndef DRT_H
#define DRT_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stddef.h> /*offsetof*/
#include <string.h>


uint64_t df_ts0[100],df_ts1[100];
uint64_t df_tt;

uint64_t totlaunch,totscans,totbusy,maxfmem,tottsched,tottdestr,tottw,maxdscheddestr,totdfx;
uint64_t totwaiting,totready,totdec,tottcache,tottsub,tottpub;
typedef uint64_t *afp;
typedef void (*fpt)(void);
//OWM STRUCT
typedef struct { afp _ooff; uint64_t osz_mask;}owm_vop_t;


//Memory allocation guards
uint64_t fsize;
uint64_t fmem_sz;
uint64_t owm_mem_sz=0;
uint64_t owm_mask=0;

//Memory pointers
afp fifo;
afp fmem_ptr;
afp owm_start_addr=NULL;
afp owm_end_addr=NULL;


uint64_t start,end,end1;
afp corefp;
#define MAXLOOPS	3
uint64_t launcherbusy, launcherlooping;
uint64_t mydtsudebug;  /* contains DRT_DEBUG environment variable valuie */

#define FGUARD		4 // guard space to overcome a limitation of scale generated code
#define FIRSTFREEOFFSET 6
#define TWAITING	(0x1L)
#define TREADY		(0x2L)
#define TBITS(A)	((A)& 0x7L)
#define TADDR(A)	((A)& 0xFFFFFFFFFFFFFFF8L)
#define TSETEMPTY(A)	(A = 0L)
#define TSETWAIT(A,V)	(A = ((V) & 0xFFFFFFFFFFFFFFF8L)+TWAITING)
#define TSETREADY(A)	((A) = (((A) & 0xFFFFFFFFFFFFFFF8L)+TREADY))

//OWM DEFINES

//MACRO FOR SUBSCRIBE

//general macros for extracting FP and OFFSET
#define _TID_AL_BITS		(3)
#define _TOF_BITS		(16)
#define _TID_BITS		(64 - _TOF_BITS)
#define _TID_STARTBIT		(0)
#define _TOF_BITS1		(_TOF_BITS+_TID_AL_BITS)

#define _LO32(_v)		((uint64_t)(_v)&0xFFFFFFFFULL)

#define _XTID_LO(_v)		((uint64_t)(_v)&((1ULL<<_TOF_BITS1)-1))
#define _XTID_HI(_v)		(((uint64_t)(_v)>>_TOF_BITS1)<<_TID_AL_BITS)

#define _X_OFF(_x)              _XTID_LO(_x)
#define _X_TID(_x)		_XTID_HI(_x)


//xsubscribe macro for frame offset 
#define XDST(df_tid_t,_fun,_off) (((df_tid_t)<<(_TOF_BITS))  |(uint64_t)(offsetof(_fun ## _s,_off)))

#define _OWM_FLAG_BITS          8
#define _OWM_RMASK		(0x01) //enabling read mode
#define _OWM_WMASK		(0x02) //enabling write mode
#define _OWM_AMASK		(0x04) //enabling allocate mode
#define _OWM_SMASK		(0x08) //enabling set mask 
#define _OWM_RBIT		(0)    //bit 0 read
#define _OWM_WBIT		(1)    //bit 1 write
#define _OWM_ABIT		(2)    //bit 3 allocate
#define _OWM_SBIT		(3)    //bit 4 set mask

//packing owm region size and mask
//#define _OWM_SETDESC(rsz,rw)	(((rsz)<<8)|((rw)&0xff))
#define _OWM_RSZ_MASK(rsz,mask)	(((uint64_t) (rsz)<< _OWM_FLAG_BITS)+(mask))

//packing mask flags
#define _OWM_MODE_ARW		(_OWM_SMASK|_OWM_AMASK| _OWM_RMASK|_OWM_WMASK) //setmask+allocate+read+write
#define _OWM_MODE_RW		(_OWM_SMASK|_OWM_RMASK|_OWM_WMASK) //read+write
#define _OWM_MODE_R		(_OWM_SMASK|_OWM_RMASK) //read
#define _OWM_MODE_W		(_OWM_SMASK|_OWM_WMASK) //write
#define _OWM_MODE_A		(_OWM_SMASK|_OWM_AMASK) //allocate
#define _OWM_MODE_AR		(_OWM_SMASK|_OWM_RMASK| _OWM_AMASK) //allocate +read
#define _OWM_MODE_AW		(_OWM_SMASK|_OWM_WMASK| _OWM_AMASK) //allocate +write




//unpacking owm regsize 
#define _OWM_GETRSZ(_b)		((_b)>>_OWM_FLAG_BITS)


//unpacking owm masks
#define _OWM_GETR(_b)		((uint8_t)(((_b)&_OWM_RMASK)>>_OWM_RBIT)) // getting  read bit
#define _OWM_GETW(_b)		((uint8_t)(((_b)&_OWM_WMASK)>>_OWM_WBIT)) // getting  write bit
#define _OWM_GETA(_b)		((uint8_t)(((_b)&_OWM_AMASK)>>_OWM_ABIT)) // getting  allocate
#define _OWM_GETIS(_b)		((uint8_t)(((_b)&_OWM_SMASK)>>_OWM_SBIT)) // getting  mask setted bit

//OWM MASK MACRO
#define _OWM_SET_MASK(_b,_pos)	   ((uint64_t)((_b) |= 1UL << _pos))
#define _OWM_CHECK_MASK(_b,_pos)   ((uint8_t)((_b >>_pos) & 1U))


#define df_subscribe(_tloc,_roff,_sz,_rw) \
				mytsubscribe(_tloc,_roff,_sz,_rw)
#define df_publish(_regptr)	mytpublish(_regptr)


__always_inline static uint64_t mytschedule(uint8_t _cond,fpt _ip, uint64_t _sc, uint32_t sz)
{ afp fp; 
  if (!_cond) return((uint64_t)NULL);

  int64_t fmem_off = (FIRSTFREEOFFSET+_sc+FGUARD); 
	
  fp = (afp) fmem_ptr;
  //fp=(afp)malloc((FIRSTFREEOFFSET+_sc+FGUARD)*sizeof(uint64_t));
  if(fp == NULL) { printf("- DRT: cannot allocate frame. \n"); fflush(stdout); exit(8); }
  
  uint64_t mguard = (uint64_t) fmem_ptr;
  if((tottsched*fmem_off) > fmem_sz) {
    printf(" - DRT: frame ptr %p - num elements %ld \n",fmem_ptr, fmem_sz);
	printf(" - DRT: cannot allocate frame - memory full. \n"); fflush(stdout); exit(8); }

  fmem_ptr += fmem_off;
  
  fp[0]=(uint64_t)(_ip);fp[1]=_sc;
  fp[2]=(uint64_t)(&fifo[end]);
  fp[4]=(uint64_t)(sz);
  /* mytschedule implementation finishes here. Below there is an optimization to recover empty slots in FIFO.*/
  fp[3]=_sc;
  fp[5]=(uint64_t) owm_mask;
  maxfmem+=(FIRSTFREEOFFSET+_sc+FGUARD)*sizeof(uint64_t);
  ++tottsched;
  if (maxdscheddestr<tottsched-tottdestr) maxdscheddestr=tottsched-tottdestr;
  TSETWAIT(fifo[end],(uint64_t)fp); ++totwaiting;
  if (mydtsudebug > 3) {
    printf("    TS: fi=%ld  ip=%ld  fp=%ld  sc=%ld/%ld  st=%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],TADDR(fifo[end]),fp[1],fp[3],TBITS(fifo[end])); fflush(stdout);
  } else  {
      if (mydtsudebug == 2 || mydtsudebug == 3) {
        printf("    TS: fi=%ld  ip=%ld fp=%ld  sc=%ld/%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],TADDR(fifo[end]),fp[1],fp[3]); fflush(stdout);
      }
  }
  end = (end+1) % fsize;
  if (TBITS(fifo[end])) {
    ++totscans;
    /* scan to see if there are empty slots ahead */
    end1=end;
    totbusy=0;
    do {
      end1 = (end1+1) % fsize;
      ++totbusy;
      if (end1==end) { printf("- FIFO: queue is totally full. (totlaunch=%ld,totscans=%ld,totbusy=%ld)\n",totlaunch,totscans,totbusy); fflush(stdout); exit(3); }
    } while (TBITS(fifo[end1]));
    end = end1;
    if (mydtsudebug) {
      printf("- FIFO busy slot (totlaunch=%ld,totscans=%ld,totbusy=%ld e=%ld)\n",totlaunch,totscans,totbusy,end); fflush(stdout);
    }
  }
//  if (end==start) { printf("- FIFO ended (totlaunch=%ld) end=%ld fifo[end]=0x%x, totbusy=%ld\n",totlaunch,end,fifo[end],totbusy); fflush(stdout); exit(1); }
  return((uint64_t)fp);
}

__always_inline static uint64_t mytwrite(afp fp1, uint64_t off1, uint64_t val)
{ 
  /* for df_writeN: */
  afp fp; uint64_t off;
//  printf("     xx: fp1=%p off1=%p val=%p\n",fp1,off1,val); fflush(stdout);
  if (mydtsudebug > 4) {
    printf("     Tw: fp1=%ld off1=%ld val=0x%lx\n",(uint64_t)fp1,off1,val); fflush(stdout);
  }
  if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
    off = ((uint64_t)fp1 & 0xFFUL); 
    fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
  }
  else { 
    fp = fp1; off=off1;
  }
//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);
  fp[off+FIRSTFREEOFFSET]=val; (fp[1])--; if (fp[1] == 0) { TSETREADY(*((afp)(fp[2]))); ++totready; --totwaiting;} ++tottw; 
//  if (mydtsudebug > 4) {
  if (mydtsudebug) {
    printf("     TW: fi=%ld  ip=%ld  fp=%p/%ld val=0x%lx  sc=%ld/%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,off,val,fp[1],fp[3]); fflush(stdout);
  }
}

__always_inline void mydropframe()
{
  ++tottdestr;
  if(!launcherbusy) { launcherbusy=1;
    /* Launcher */
    if(start==end) { printf("- FIFO: attempt to extract from empty queue. \n"); fflush(stdout); exit(2); }
    else {
      uint64_t found,start1,start0,start2;
      uint64_t bits, bits0;
      uint64_t val;
      afp fpnew;
      uint64_t ipnew;
      if (mydtsudebug) {
        printf("- DRT: Starting Dataflow launcher.\n"); fflush(stdout);
      }
      totlaunch = 0;
      launcherlooping = 0;
      do {
        start1 = start; found=0;
        do {
          val = fifo[start1]; fpnew = (afp)(TADDR(val)); bits = TBITS(val);
          ipnew = 0;
          start0 = start1; bits0 = bits;
          start1 = (start1+1) % fsize;
          bits = TBITS(fifo[start1]);
          if (bits0 == TREADY) { 
            TSETEMPTY(fifo[start0]); /* clean fifo slot */
            --totready; ++found;
	    corefp = fpnew; ipnew = fpnew[0];
            ++totlaunch;
            if (mydtsudebug > 3) {
              printf("  TE: fi=%ld st=%ld  ipnew=%ld  fpnew=%ld (s=%ld,s1=%ld,e=%ld,ll=%ld  launched=%ld  R=%ld,W=%ld)\n",start0,bits0,ipnew,(uint64_t)fpnew,start,start1,end,launcherlooping,totlaunch,totready,totwaiting); fflush(stdout);
            } else {
               if (mydtsudebug == 2 || mydtsudebug == 3) {
                  printf("  TE: fi=%ld ipnew=%ld  fpnew=%ld\n",start0,ipnew,(uint64_t)fpnew); fflush(stdout);
               }
            }
            ((fpt)(ipnew))();
          }
//        } while ((bits != 0 || start1!=end) && launcherlooping < MAXLOOPS);
//          if (bits==TWAITING) printf("  pp: fi=%ld st=%ld  ipnew=%p  fpnew=%p (s=%ld,s1=%ld,e=%ld,ll=%ld  launched=%ld  R=%ld,W=%ld)\n",start0,bits,ipnew,fpnew,start,start1,end,launcherlooping,totlaunch,totready,totwaiting); fflush(stdout);
        } while ((bits == TREADY || start1!=end) && launcherlooping < MAXLOOPS);
        if (! found) ++launcherlooping; 
        if (launcherlooping == MAXLOOPS) { printf("- DRT runtime: cannot find READY threads! looping reached %d\n", MAXLOOPS); exit(7); }
//        while (bits == 0 && start!=end) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
//        while (bits == 0 && start!=start2) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
//        while (bits != TREADY && start!=start2) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
//        start2=(start+1) % fsize;
//        bits=TBITS(fifo[start]);
//        while (bits != TREADY && start!=start2) { start = (start-1) % fsize; bits=TBITS(fifo[start]); }
        start2=(start-1) % fsize;
        bits=TBITS(fifo[start]);
//        if (bits != TREADY)
          while (bits != TREADY && start!=start2) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
 //        printf("  qq: fi=%ld st=%ld  ipnew=%p  fpnew=%p (s=%ld,s1=%ld,e=%ld,ll=%ld  launched=%ld  R=%ld,W=%ld)\n",start0,bits,ipnew,fpnew,start,start1,end,launcherlooping,totlaunch,totready,totwaiting); fflush(stdout);
//        start2=(start+1) % fsize;
//        bits=TBITS(fifo[start]);
//        while (bits != 0 && start!=start2) { start = (start-1) % fsize; bits=TBITS(fifo[start]); }
//      } while (bits != 0);
//      } while (bits == TREADY || start==start2);
      } while (bits == TREADY);
    }
    if (mydtsudebug) {
      printf("- DRT: Ending DF launcher (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld R=%ld,W=%ld).\n",maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,totready,totwaiting); fflush(stdout);
      fflush(stdout);
    }
    launcherbusy=0;
    free(fifo);
  } else {
    maxfmem -= (FIRSTFREEOFFSET+corefp[3]+FGUARD)*sizeof(uint64_t);
    if (mydtsudebug > 3) {
      printf("  TD: fi=%ld  ip=%ld  fp=%p  sc=%ld (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld R=%ld,W=%ld)\n",(corefp[2]-(uint64_t)fifo)/sizeof(uint64_t),corefp[0],corefp,corefp[3],maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,totready,totwaiting); fflush(stdout);

//      printf("- DRT: mydfdestroy (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld,fp=%p,sc=%ld).\n",maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,corefp,corefp[3]); fflush(stdout);
    } else { 
        if (mydtsudebug == 2 || mydtsudebug == 3) {
          printf("  TD: fi=%ld  ip=%ld  fp=%p  sc=%ld \n",(corefp[2]-(uint64_t)fifo)/sizeof(uint64_t),corefp[0],corefp,corefp[3]); fflush(stdout);

    //      printf("- DRT: mydfdestroy (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld,fp=%p,sc=%ld).\n",maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,corefp,corefp[3]); fflush(stdout);
        }
    }
    //free(corefp);
  }
}
__always_inline static uint64_t mydfstart(uint64_t df_ts0[])
{
  uint64_t phypz, psize, totcpu, totmem, fszmem, i, t, frame_szmem;
  uint64_t owm_szmem =0;
  char *s;
  phypz = sysconf(_SC_PHYS_PAGES);
  psize = sysconf(_SC_PAGE_SIZE);
  totcpu = sysconf(_SC_NPROCESSORS_CONF);
  totmem = phypz * psize /1024 /1024 /1024;
  fsize = ((phypz * psize) / sizeof(uint64_t)) / 10; /* rule of thumb: allocate a 10% of total phy memory */ 
  owm_mem_sz =0; //no owm allocation if not needed

  s = getenv("DRT_DEBUG"); mydtsudebug = (s) ? atoi(s) : 0;
  s = getenv("DRT_FSIZE"); t = (s) ? atoi(s) : fsize; if (t) fsize = t;
  s = getenv("DRT_MSIZE"); t = (s) ? atoi(s) : fsize; if (t) fmem_sz = t;
  s = getenv("OWM_SIZE"); t = (s) ? atoi(s) : owm_mem_sz; if (t) owm_mem_sz = t;
  
  fszmem = fsize /1024 /1024 *sizeof(uint64_t);
  frame_szmem = fmem_sz /1024 /1024 *sizeof(uint64_t);
  if(owm_mem_sz)
  {
      owm_szmem = owm_mem_sz /1024/1024 *sizeof(uint64_t);
      owm_start_addr = (afp) calloc(owm_mem_sz,sizeof(uint64_t));
      owm_end_addr =  owm_start_addr + owm_mem_sz;
     
  }


  if (mydtsudebug) {
    printf("- DRT: you have %ld GB of physical memory and %ld processors on this host.\n", totmem, totcpu);
    printf("- DRT: allocating a FRAME-MEM queue of %ld MB (%ld entries).\n", fszmem, fsize);
    printf("- DRT: allocating a FRAME-MEM frame of %ld MB.\n", frame_szmem);
    printf("- DRT: size of OWM-MEM allocated  %ld MB.\n", frame_szmem);
    printf("- DRT: OWM-MEM start addr %p \n", owm_start_addr);
    printf("- DRT: OWM-MEM end addr %p \n", owm_end_addr);
    fflush(stdout);
  }
  fmem_ptr = (afp) calloc(fmem_sz, sizeof(uint64_t));
  fifo = (afp)malloc(fsize*sizeof(uint64_t));




  if(fmem_ptr == NULL) { printf("- FRAME-MEM: cannot allocate frame memory. \n"); fflush(stdout); exit(4); }
  if(fifo == NULL) { printf("- FRAME-MEM: cannot allocate queue. \n"); fflush(stdout); exit(4); }

  for (i =0; i<fsize; ++i) TSETEMPTY(fifo[i]);
  if (mydtsudebug) {
    printf("- DRT: FRAME-MEM allocation+initialization done.\n");
    fflush(stdout);
  }
}

__always_inline static uint64_t mydfexit(void) {
  ++totdfx;
  if (totdfx>1) {
    printf("- DRT: mydfexit called more the ONCE !\n");
  }
  if (start!=end && !launcherbusy) {
    printf("- DRT: mydfexit there are pending threads (%ld) start=%ld end=%ld!\n", end-start,start,end);
  }
}

__always_inline static uint64_t mydfread(afp fp, uint64_t off) {
  //check the owm_mask 
  uint64_t omask = fp[5]; 
  uint64_t val=0;
  val = fp[(off)+FIRSTFREEOFFSET];
  if (mydtsudebug > 5) 
  {
      printf("     TR: %lx/%ld val=0x%lx\n",(uint64_t)fp,off,val);fflush(stdout);
  }

  return val;
}

__always_inline static void mymwrite(afp p, uint64_t val) {
  if (mydtsudebug > 5) {
    printf("     MW: %p  val=0x%lx\n",p,val);fflush(stdout);
  }
  *p=val;
}

__always_inline static uint64_t mymread(afp p) {
  if (mydtsudebug > 5) {
    printf("     xx: %p  \n",p);fflush(stdout);
  }
  uint64_t val = ((uint64_t)(*p));
  if (mydtsudebug > 5) {
    printf("     MR: %p  val=0x%lx\n",p,val);fflush(stdout);
  }
  return val;
}

__always_inline static void mytdecreaseN(afp fp1, uint64_t n)
{ 
  afp fp; uint64_t off;
  if (mydtsudebug > 4) {
    printf("     T.: fp1=%p  n=%ld\n",fp1,n); fflush(stdout);
  }
  if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
    off = ((uint64_t)fp1 & 0xFFUL);
    fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
  }
  else {
    fp = fp1; off=0;
  }
//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);

  fp[1] -= n; if (fp[1] == 0) { TSETREADY(*((afp)(fp[2]))); ++totready; --totwaiting; } ++totdec; 
  if (mydtsudebug) {
    printf("     T0: fi=%ld  ip=%ld  fp=%p  sc=%ld/%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,fp[1],fp[3]); fflush(stdout);
  }
}

__always_inline static void *mytcache(afp fp1)
{ 
  afp rfp, fp; uint64_t off;
  if (mydtsudebug > 4) {
    printf("     Tc: fp1=%p\n",fp1); fflush(stdout);
  }
  if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
    off = ((uint64_t)fp1 & 0xFFUL);
    fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
  }
  else {
    fp = fp1; off=0;
  }
//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);

  rfp = fp + FIRSTFREEOFFSET; ++tottcache; 
  if (mydtsudebug > 4) {
    printf("     TC: fi=%ld  ip=%ld  fp=%p  sc=%ld/%ld rfp=%p\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,fp[1],fp[3], rfp); fflush(stdout);
  }
  return rfp;
}
/*
VOP struct
OOFF => OWM offset (64bit)
OSZMASK => (OWM SIZE + OWM MASK) (64 bits)
*/





__always_inline static void* mytsubscribe(uint64_t fp1, uint32_t rstart, uint32_t rsize, uint8_t mask)
{ 
  //V1.0
  uint64_t *rptr;
  //packing rsize and mask for metadata
  
  if(!owm_mem_sz)
  {
      printf("ERROR XSUB: NO OWM MEMORY ALLOCATED: %ld \n", owm_mem_sz); 
      exit(4);		
  } 
  uint64_t oszmk = _OWM_RSZ_MASK(rsize, mask); //56 bits|8bit (rsize|mask)
  
  uint64_t new_regsize = _OWM_GETRSZ(oszmk); 
  uint8_t  iswrite = _OWM_GETW(oszmk);

   //subscribe size check
  uint64_t owm_off_start = (uint64_t)owm_start_addr + rstart; 
  afp ooff = (afp) owm_off_start; 
  
   //subscribe offset check
   if (ooff > owm_end_addr)
   {
      printf("ERROR XSUB: REG START OUT OF OWM MEMORY!\n"); 
      printf("OWM START ADDR: %p \n OWM END ADDR %p \n OWM_SUB_START_ADDR %p\n", 
		owm_start_addr, owm_end_addr, ooff);
      fflush(stdout);
      exit(4);		
   }

   if (!((ooff + rsize) < owm_end_addr))
   {
      printf("ERROR XSUB: SUBSCRIBE OUT OF OWM MEMORY!\n"); 
      printf("OWM START ADDR: %p \n OWM END ADDR %p \n OWM_SUB_END_ADDR %p \n  OWM_SUB_RSIZE %d \n", 
		owm_start_addr, owm_end_addr, (ooff+rsize), rsize);
   
      fflush(stdout);
      exit(4);		
   }
  
  
   if(mydtsudebug)
   { 
      uint8_t isread = _OWM_GETR(oszmk);
      uint8_t isalloc = _OWM_GETA(oszmk);
      printf("- DRT: FP OWM SUSCRIBE %lx\n", fp1);
      printf("- DRT: OWM SUSCRIBE REG_OFF %d, REG_SZ %d\n", rstart,rsize);
      printf("- DRT: OWM OOFF %p\n",ooff);
      printf("- DRT: OWM MASK READ %d, WRITE %d, ALLOC %d\n", isread,iswrite,isalloc);
      fflush(stdout);
   }
 

   //metadata struct
   owm_vop_t owm_s;
   owm_s._ooff = ooff;
   owm_s.osz_mask = oszmk;   
   
   //updating owm mask in the frame metadata 
   uint64_t fp_loc = _X_TID(fp1);
   uint32_t fp_off = _X_OFF(fp1);
   afp fp_sub = (afp) fp_loc; 
   uint32_t fp_sz = fp_sub[4];
   afp fp_data_loc = (afp) fp_loc +FIRSTFREEOFFSET+fp_off;
   if(mydtsudebug)
   { 
      afp fp_data = (afp) fp_loc+FIRSTFREEOFFSET; 
      printf("- DRT: OWM XSUB FP LOC:%lx \n",fp_loc);
      printf("- DRT: OWM XSUB FP ADDR:%p \n",fp_sub);
      printf("- DRT: OWM XSUB FP SIZE:%ld \n",fp_sub[4]);
      printf("- DRT: OWM XSUB FP SC:%ld \n",fp_sub[1]);
      printf("- DRT: OWM XSUB FP OFF:%u \n",fp_off);
      printf("- DRT: OWM XSUB FP SZ:%u \n",fp_sz);
      printf("- DRT: OWM XSUB FP DATA LOCF:%p \n",fp_data);
      printf("- DRT: OWM XSUB FP DATA LOC OFF:%p \n",fp_data_loc);
      printf("- DRT: OWM OOFF %p\n", owm_s._ooff);
      printf("- DRT: OWM OSZMASK %lx\n", owm_s.osz_mask);
      printf("- DRT: OWM_MASK IS: %ld\n",fp_sub[5]);
      fflush(stdout);
   }
 
   //check offset in frame sz
   if(fp_off>fp_sz)
   {
      printf("ERROR: OWM XSUB WRITING VOP OUT OF FRAME SIZE!\n");
      fflush(stdout);
      exit(4);
   }
   (void) memcpy(fp_data_loc, &owm_s, sizeof(owm_vop_t));

   _OWM_SET_MASK(fp_sub[5],(fp_off/sizeof(owm_vop_t)));
   

   
   return (void*)rptr;
}
  
__always_inline static void mytpublish(void* regptr)
{ //TODO
    //printf("OWM OOFF %p PUBLISHED\n",(uint64_t *)regptr);
}


struct timespec tv;
long long start_time, time_in_nano;

__always_inline static void xzonestart(int zone)
{
	printf("\tXZONESTART %d DETECTED\n",zone);
	fflush(stdout);
	/*
	   gettimeofday(&tv,NULL);
	   time_in_mill= (tv.tv_sec) * 1000000 + (tv.tv_usec);
	   start_time = time_in_mill;
	   */
	clock_gettime(CLOCK_REALTIME, &tv);
	time_in_nano= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	start_time = time_in_nano;

}

__always_inline static void xzonestop(int zone)
{

	/*
	   gettimeofday(&tv,NULL);
	   time_in_mill = (tv.tv_sec) * 1000000 + (tv.tv_usec);
	   */
	clock_gettime(CLOCK_REALTIME, &tv);
	time_in_nano= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	printf("\tXZONESTOP %d DETECTED \n",zone);
	printf("\t[INFO]: EXECUTION TIME = %lld ns\n",(time_in_nano - start_time));

	fflush(stdout);

}






#define df_tstamp(df_ts0)	mydfstart(df_ts0);
#define df_ldframe(n)		afp fp = corefp;
#define df_destroy()		mydropframe();
//#define df_frame(_off)	(fp[(_off)+FIRSTFREEOFFSET])
#define df_frame(_off)		mydfread((afp)fp,_off)
#define df_read(_off)		(df_frame(_off))
#define df_write(_tid,_off,_val) \
				mytwrite((afp)(_tid), _off, _val)
#define df_tschedule_cond(_ip,_sc,_cond) \
				(mytschedule(_cond,(fpt)(_ip),_sc,(_sc)*sizeof(uint64_t)))
#define df_tscheduleN(_ip,_sc)	(mytschedule(1,(fpt)(_ip),(_sc),(_sc)*sizeof(uint64_t)))
#define df_tschedule(_ip,_sc)	(mytschedule(1,(fpt)(_ip),(_sc),(_sc)*sizeof(uint64_t)))
#define df_talloc(_type, _sz)	(uint64_t)malloc(_sz)
///* df_writeN assumes we do not care about the 3 most #define df_tread_al(_p)		((uint64_t)(*((afp)(_p))))
#define df_tread_al(_p)		mymread((afp)(_p))
//#define df_twrite_al(_p,_val)	*((afp)(_p))=(_val)
#define df_twrite_al(_p,_val)	mymwrite((afp)(_p),_val)
#define df_tfree(_p)		free((void *)(_p))
#define df_exit()		mydfexit()
//#define _TLOC(_tp,_l)		((_tp)+((uint64_t)(_l)<<3))
//#define _TLOC(_tp,_l) ((_tp)+((unsigned long long)(_l)<<3))
#define _TLOC(_tp,_l)		(((((uint64_t)_tp)<<8)+(_l)) | 0x0080000000000000UL)
#define myL8b(_VL)		(((uint64_t)_VL) & 0xFFUL)
#define myH56b(_VH)		(((uint64_t)_VH) >> 8)
//#define df_writeN(_tid,_val)	mytwrite((afp)(myH56b(_tid)), myL8b(_tid), _val)
#define df_writeN(_tid,_val)	mytwrite((afp)(_tid), 0, _val)
/* df_writeN assumes we do not care about the 3 most sigificant bits of _tid */
#define df_tdecrease(_tid)	mytdecreaseN((afp)(_tid), 1)
#define df_tdecreaseN(_tid,_n)	mytdecreaseN((afp)(_tid), (uint64_t)(_n))
#define df_tschedule64(_ip,_sc) \
				(mytschedule(1,(fpt)(_ip),(_sc),(_sc)*sizeof(uint64_t)))
#define df_tschedulez(_ip,_sc,_sz) \
				(mytschedule(1,(_ip),(_sc),(_sz)))
#define df_tload()		(corefp+FIRSTFREEOFFSET)
#define df_tcache(_tid)		(mytcache((afp)(_tid)))
#define df_tid_t		uint64_t
#define df_constrain(_tid,_xc)	
#define df_printstats(_ts0,_ts1,_t1)	
#define _TLOC_OFF(_tloc)	(myL8b(_tloc))

typedef void (*df_thread_t)(void);



/*
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#endif // DRT_H

