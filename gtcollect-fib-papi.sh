#!/bin/bash
export DRT_DEBUG=0
threshold=1
user=$USER
host=`hostname -s`
release=`lsb_release -r | awk 'NR==1{printf $2}' | cut -c -2`
distro=`lsb_release -d  | awk 'NR==1{print $2.$3}' | cut -c -1`
OS=`lsb_release -d | awk 'NR==1{print $2.$3}'`
date=`date +%Y%m%d%H%M | cut -c 3-`
DATE=`date +%Y/%m/%d/%H:%M`
#core=`lscpu | awk 'NR==4{print "C"$2}'`
core=`getconf _NPROCESSORS_ONLN`
#echo "$distro"
#echo "$date"
#wdir="$HOME/XOCR/ocr" # working dir
wdir=`pwd`
toolname=`pwd | awk -F/ '{print $NF}'`
cpath="fib-papi.sh" #command path
irange=(5+1 10+1 15+1 20+1 25+1) # input range
DFT=(sch wrt rd ld dest) # DFT Functions
inum=9
threshold=1
L2C=`getconf LEVEL2_CACHE_SIZE`
L3C=`getconf LEVEL3_CACHE_SIZE`
FILENAME=$toolname-$host$distro$release"C"$core-$date
echo "# GTable Format V1.1" >> $FILENAME.txt
echo "#C INFOFILE=$wdir" >> $FILENAME.txt
echo "#C VALIDATIONCODE='SUCCESS'" >> $FILENAME.txt
echo "#C explabel=''" >> $FILENAME.txt
echo "#C MYLIBC=''" >> $FILENAME.txt
echo "#C SIMDIR=''" >> $FILENAME.txt
echo "#C MYHD='focal'" >> $FILENAME.txt
echo "#C owmsize=" >> $FILENAME.txt
echo "#C MYSIMVER=" >> $FILENAME.txt
echo "#C listmodel=" >> $FILENAME.txt
echo "#C listappi[std]='fib-papi'" >> $FILENAME.txt
echo "#C listsize[fib-papi]=$irange" >> $FILENAME.txt
echo "#C stringok[fib-papi]=SUCCESS" >> $FILENAME.txt
echo "#C guestsetup[]=" >> $FILENAME.txt
echo "#C infilter[fib-papi]=fib-papi.sh" >> $FILENAME.txt
echo "#C listcores=1" >> $FILENAME.txt
echo "#C listnodes=1" >> $FILENAME.txt
echo "#C listl2c=$(($L2C / 1024)) KiB" >> $FILENAME.txt
echo "#C listl3c=$(($L3C / 1048576)) MiB" >> $FILENAME.txt
echo "#C listtiming=" >> $FILENAME.txt

#if [ "$1" = ""  ]; then  echo "You must specify a Fibonacci index (n)"; exit 1; fi

#if [ "$1" = "5" ]; then export DRT_FSIZE=4000; fi
export DRT_FSIZE=400
./fib-papi 5 $threshold > tmp.txt
echo "-----------------------------------------------------------";
#if [ "$1" = "10" ]; then export DRT_FSIZE=50000; fi
export DRT_FSIZE=4000
./fib-papi 10 $threshold >> tmp.txt
echo "-----------------------------------------------------------"
#if [ "$1" = "15" ]; then export DRT_FSIZE=500000; fi
export DRT_FSIZE=50000
./fib-papi 15 $threshold >> tmp.txt
echo "-----------------------------------------------------------";
#if [ "$1" = "20" ]; then export DRT_FSIZE=5000000; fi
export DRT_FSIZE=500000
./fib-papi 20 $threshold >> tmp.txt
echo "-----------------------------------------------------------";
#if [ "$1" = "25" ]; then export DRT_FSIZE=50000000; fi
export DRT_FSIZE=5000000
./fib-papi 25 $threshold >> tmp.txt
echo "-----------------------------------------------------------";

myar1=(`awk '/modeled_lat_df_schedule/{print $NF}' tmp.txt`)
myar2=(`awk '/modeled_lat_df_write/{print $NF}' tmp.txt`)
myar3=(`awk '/modeled_lat_df_read/{print $NF}' tmp.txt`)
myar4=(`awk '/modeled_lat_df_load/{print $NF}' tmp.txt`)
myar5=(`awk '/modeled_lat_df_destroy/{print $NF}' tmp.txt`)
echo "#-Graph1,xyplot,MLAT,,min auto max auto 10,,min auto max auto 2,INPUT DF-Thread" >> $FILENAME.txt
echo "${DFT[0]} ${myar1[0]}" >> $FILENAME.txt
echo "${DFT[1]} ${myar2[0]}" >> $FILENAME.txt
echo "${DFT[2]} ${myar3[0]}" >> $FILENAME.txt
echo "${DFT[3]} ${myar4[0]}" >> $FILENAME.txt
echo "${DFT[4]} ${myar5[0]}" >> $FILENAME.txt
echo "" >> $FILENAME.txt
echo "" >> $FILENAME.txt
myarr1=(`awk '/tot_modeled_lat_df_schedule/{print $NF}' tmp.txt`)
myarr2=(`awk '/tot_modeled_lat_df_write/{print $NF}' tmp.txt`)
myarr3=(`awk '/tot_modeled_lat_df_read/{print $NF}' tmp.txt`)
myarr4=(`awk '/tot_modeled_lat_df_load/{print $NF}' tmp.txt`)
myarr5=(`awk '/tot_modeled_lat_df_destroy/{print $NF}' tmp.txt`)
echo "#-Graph2,xyplot,TMLAT,,min auto max auto 10,,min auto max auto 2,INPUT sch wrt rd ld dest" >> $FILENAME.txt
echo "${irange[0]} ${myarr1[0]} ${myarr2[0]} ${myarr3[0]} ${myarr4[0]} ${myarr5[0]}" >> $FILENAME.txt
echo "${irange[1]} ${myarr1[1]} ${myarr2[1]} ${myarr3[1]} ${myarr4[1]} ${myarr5[1]}" >> $FILENAME.txt
echo "${irange[2]} ${myarr1[2]} ${myarr2[2]} ${myarr3[2]} ${myarr4[2]} ${myarr5[2]}" >> $FILENAME.txt
echo "${irange[3]} ${myarr1[3]} ${myarr2[3]} ${myarr3[3]} ${myarr4[3]} ${myarr5[3]}" >> $FILENAME.txt
echo "${irange[4]} ${myarr1[4]} ${myarr2[4]} ${myarr3[4]} ${myarr4[4]} ${myarr5[4]}" >> $FILENAME.txt
echo "" >> $FILENAME.txt
echo "" >> $FILENAME.txt
myarray1=(`awk '/tot_hw_cyc_df_schedule/{print $NF}' tmp.txt`)
myarray2=(`awk '/tot_hw_cyc_df_write/{print $NF}' tmp.txt`)
myarray3=(`awk '/tot_hw_cyc_df_read/{print $NF}' tmp.txt`)
myarray4=(`awk '/tot_hw_cyc_df_load/{print $NF}' tmp.txt`)
myarray5=(`awk '/tot_hw_cyc_df_destroy/{print $NF}' tmp.txt`)
echo "#-Graph3,xyplot,CY,,min auto max auto 10,,min auto max auto 2,INPUT sch wrt rd ld dest" >> $FILENAME.txt
echo "${irange[0]} ${myarray1[0]} ${myarray2[0]} ${myarray3[0]} ${myarray4[0]} ${myarray5[0]}" >> $FILENAME.txt
echo "${irange[1]} ${myarray1[1]} ${myarray2[1]} ${myarray3[1]} ${myarray4[1]} ${myarray5[1]}" >> $FILENAME.txt
echo "${irange[2]} ${myarray1[2]} ${myarray2[2]} ${myarray3[2]} ${myarray4[2]} ${myarray5[2]}" >> $FILENAME.txt
echo "${irange[3]} ${myarray1[3]} ${myarray2[3]} ${myarray3[3]} ${myarray4[3]} ${myarray5[3]}" >> $FILENAME.txt
echo "${irange[4]} ${myarray1[4]} ${myarray2[4]} ${myarray3[4]} ${myarray4[4]} ${myarray5[4]}" >> $FILENAME.txt
echo "" >> $FILENAME.txt
echo "" >> $FILENAME.txt
myval1=(`awk '/tot_hw_cycles_benchmark_df_schedule/{print $NF}' tmp.txt`)
myval2=(`awk '/tot_hw_cycles_benchmark_df_write/{print $NF}' tmp.txt`)
myval3=(`awk '/tot_hw_cycles_benchmark_df_read/{print $NF}' tmp.txt`)
myval4=(`awk '/tot_hw_cycles_benchmark_df_load/{print $NF}' tmp.txt`)
myval5=(`awk '/tot_hw_cycles_benchmark_df_destroy/{print $NF}' tmp.txt`)
echo "#-Graph4,xyplot,CY,,min auto max auto 10,,min auto max auto 2,INPUT sch wrt rd ld dest" >> $FILENAME.txt
echo "${irange[0]} ${myval1[0]} ${myval2[0]} ${myval3[0]} ${myval4[0]} ${myval5[0]}" >> $FILENAME.txt
echo "${irange[1]} ${myval1[1]} ${myval2[1]} ${myval3[1]} ${myval4[1]} ${myval5[1]}" >> $FILENAME.txt
echo "${irange[2]} ${myval1[2]} ${myval2[2]} ${myval3[2]} ${myval4[2]} ${myval5[2]}" >> $FILENAME.txt
echo "${irange[3]} ${myval1[3]} ${myval2[3]} ${myval3[3]} ${myval4[3]} ${myval5[3]}" >> $FILENAME.txt
echo "${irange[4]} ${myval1[4]} ${myval2[4]} ${myval3[4]} ${myval4[4]} ${myval5[4]}" >> $FILENAME.txt
echo "" >> $FILENAME.txt
echo "" >> $FILENAME.txt
myval1=(`awk '/tot_DF_hw_cycles/{print $NF}' tmp.txt`)
#myval2=(`awk '/tot_hw_/{print $NF}' tmp.txt`)
#myval3=(`awk '/tot_hw_/{print $NF}' tmp.txt`)
#myval4=(`awk '/tot_hw_/{print $NF}' tmp.txt`)
#myval5=(`awk '/tot_hw_/{print $NF}' tmp.txt`)
echo "#-Graph5,xyplot,CY,,min auto max auto 10,,min auto max auto 2,INPUT Total_DFT_cycles" >> $FILENAME.txt
echo "${irange[0]} ${myval1[0]}" >> $FILENAME.txt
echo "${irange[1]} ${myval1[1]}" >> $FILENAME.txt
echo "${irange[2]} ${myval1[2]}" >> $FILENAME.txt
echo "${irange[3]} ${myval1[3]}" >> $FILENAME.txt
echo "${irange[4]} ${myval1[4]}" >> $FILENAME.txt
echo "" >> $FILENAME.txt
echo "" >> $FILENAME.txt


echo "The end"
echo "plot using: ./gtgraph.sh --histogram $FILENAME.txt"




