#include <stdio.h>
#include "tsu.h"
#include <stdlib.h>
#include <stdint.h>

uint64_t fib_start, fib_stop, sum_fib;
uint64_t RFIB(int n) {
	if (n<=1) return n;
	else return RFIB(n-1) + RFIB(n-2);

}

int main (int argc, char* argv[]){
	int N=10;
	if (argc > 1) 
		N = atoi (argv[1]);
	rdtscll(fib_start);
	uint64_t y = RFIB(N);
	rdtscll(fib_stop);
	sum_fib+=fib_stop-fib_start;
	printf("RFIB result is %d\n",y);
	printf("total cycles are: %ld\n",sum_fib);
	return 0;
}
