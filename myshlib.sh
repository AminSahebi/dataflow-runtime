#!/bin/bash
MSHLVERSION="200319"
# A BASH library
# 170602 Roberto Giorgi - University of Siena, Italy
##########################################################################
# MIT LICENSE:
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHERi
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
##########################################################################
# $id$i
PCUSER="$(id -u -n)"
MSHLCMDL="$*"
##
mshlmd5sig=`md5sum $0|awk '{print $1}'`
MSHLMYSIG="${mshlmd5sig:0:2}${mshlmd5sig:(-2)}" # reduced-hash: first 2 char and last two char
MSHLVERSION1="v${MSHLVERSION}-${MSHLMYSIG}"
# Set $SCRIPTPATH to the same directory where this script is
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
#

#-----------------------------------------------------------------------------
# Colors
black='\E[30;40m'
red='\E[31;40m'
green='\E[32;40m'
yellow='\E[33;40m'
blue='\E[1;34;40m'
magenta='\E[35;40m'
cyan='\E[36;40m'
white='\E[37;40m'

#####################################################
function cecho ()            # Color-echo.
                             # Argument $1 = message
                             # Argument $2 = color
{
   local default_msg="No message passed."
                             # Doesn't really need to be a local variable.
   local nopt=""
   local message=""
   local color=""
   local force="0"

   while [ $# -gt 0 ]; do
      case "$1" in
         -n) nopt="-n "; shift;;
         -f) force="1"; shift;;
         *) if [ -z "$message" ]; then message=${1:-$default_msg}; shift;   # Defaults to default message.
            elif [ -z "$color" ]; then  color=${1:-$white}; shift;          # Defaults to white, if not specified.
            fi ;;
      esac
   done

   [ "$quiet" = "0" -o "$force" = "1" ] && echo -en "$color"
   [ "$quiet" = "0" -o "$force" = "1" ] && echo -n "$message"
   [ "$nopt" = "" -a \( "$quiet" = "0" -o "$force" = "1" \) ] && echo ""
   tput sgr0             # reset to normal
}

#####################################################
function comment() {
   local nopt=""
   local str=""
   if [ "$quiet" = "0" ]; then
      while [ $# -gt 0 ]; do
         case "$1" in
            -n) nopt="-n "; shift;;
            *)  str="$1"; shift;;
         esac
      done
      echo $nopt "$str"
   fi
}

#####################################################
function verbose4() {
   if [ "$verbose" -gt "3" ]; then echo "$1"; fi
}

#####################################################
function verbose3() {
   if [ "$verbose" -gt "2" ]; then echo "$1"; fi
}

#####################################################
function verbose2() {
   if [ "$verbose" -gt "1" ]; then echo "$1"; fi
}

#####################################################
function verbose1() {
   if [ "$verbose" -gt "0" ]; then echo "$1"; fi
}

#####################################################
function debug5() {
   if [ "$debug" -gt "4" ]; then echo "D5: $1"; fi
}

#####################################################
function debug4() {
   if [ "$debug" -gt "3" ]; then echo "D4: $1"; fi
}

#####################################################
function debug3() {
   if [ "$debug" -gt "2" ]; then echo "D3: $1"; fi
}

#####################################################
function debug2() {
   if [ "$debug" -gt "1" ]; then echo "D2: $1"; fi
}

#####################################################
function debug1() {
   if [ "$debug" -gt "0" ]; then echo "D1: $1"; fi
}

#####################################################
# function checkconnection
function checkconnection
{
   local myservice="$1"
   local myhost="$2"
   local rc="1"
   local d
   local c
   local r
   local rpm
#   ch=""
   if [ "$myhost" != "" -a "$myservice" != "" ]; then
#      ch=`$sss ping -c 1 -W 1 $1| egrep "icmp_req|icmp_seq"`
      if [ "$verbose" -gt "1" ]; then
         echo "       Checking '$myservice' service..."
      fi
      case $myservice in
         web)
            wget --spider --timeout=5 --tries=3 ${myhost}/ >/dev/null 2>/dev/null
            rc=$?
            if [ "$verbose" -gt "1" ]; then
               if [ "$rc" = "0" ]; then  echo "       Machine $myhost has web service active."; fi
            fi
            ;;
         nfs)
            if [ "$nodistrib" = "0" ]; then
#               d=`echo "send escape"|telnet $myhost $nfsport 2>&1|$grep Connected |$awk '{print $1}'`
#               c=`echo "send escape"|telnet $myhost $portmapperport 2>&1|$grep Connected |$awk '{print $1}'`
               d=`echo "send escape"|nc -vw 5 $myhost $nfsport 2>&1|egrep "(succeeded|Connected to)"`
               c=`echo "send escape"|nc -vw 5 $myhost $portmapperport 2>&1|egrep "(succeeded|Connected to)"`
               if [ "$verbose" -gt "1" ]; then
                  if [ "$c" != "" ]; then  echo "       Machine $myhost has PORTMAPPER port $portmapperport open.";
                  else echo "Machine $myhost has PORTMAPPER port ($portmapperport) CLOSED."; fi
                  if [ "$d" != "" ]; then  echo "       Machine $myhost has NFS port $nfsport open.";
                  else echo "Machine $myhost has NFS port $nfsport CLOSED."; fi
               fi
               rpm=""
#               if [ "$d" = "Connected" -a "$c" = "Connected" ]; then
               if [ "$d" != "" -a "$c" != "" ]; then
                  r=`${rpcinfo} -p $myhost 2>/dev/null`
                  rpm=`echo "$r"|$grep $nfsport|$awk '{print $4}'|$tail -1`
                  if [ "$verbose" -gt "1" ]; then
                     if [ "$rpm" != "" ]; then  echo "       Machine $myhost has NFS server OK.";
                     else echo "Machine $myhost has no NFS service running."; fi
                  fi
               fi
#               if [ "$d" = "Connected" -a "$c" = "Connected" -a "$rpm" != "" ]; then # portcheck
               if [ "$d" != "" -a "$c" != "" -a "$rpm" != "" ]; then
                  rc="0"
               fi
            else
               echo "Local Simulation (non NFS check)"
            fi
            ;;
         *)
            ping -c 2 -W 3 $myhost >/dev/null 2>/dev/null ### sometime ping is filtered out and doesn't work: better:
            rc=$?
            if [ "$verbose" -gt "1" ]; then
               if [ "$rc" = "0" ]; then  echo "       Machine $myhost responded to ping."; fi
            fi
            ;;
      esac
   fi
#   if [ "$ch" != "" ]; then noconnection="0"; else noconnection="1"; fi
   if [ "$rc" = "0" ]; then noconnection="0"; else noconnection="1"; fi

   if [ "$noconnection" != "0" ]; then
      echo ""
      echo "(TEMPORARY) ERROR: No connection with host '$1'"
      cecho "IF you want to run the simulations only locally" "$yellow"
      cecho "THEN specify the command line option --nonet" "$yellow"
      exit 1
   fi
}

#####################################################
# function getdepfile
# $1 = filename of dependent file
#
# Checks some locations for finding the package in this order:
#  1) the SCRIPPATH
#  2) the current dir (or SAVEDPATH if set)
#  3) the MYWEBSERVER (if specified/known)
# OUTPUT:
#   DEPFNAME - filepath of the dependent file
#
function getdepfile() {
   local TRYFNAME="$1"
   DEPFNAME="${SCRIPTPATH}/$TRYFNAME"
   local testpkg1="$DEPFNAME"
   local testpkg2="$TRYFNAME"
   local locpwd

   debug1 "TRYFNAME=$TRYFNAME"

   # 1) DEPFNAME default: ${SCRIPTPATH}/$TRYFNAME

   # change dir to $SAVEDPATH if it is set
   [ -z "$SAVEDPATH" ] || cd $SAVEDPATH

   # 2) DEPFNAME: If it exists in the current dir use that one (testpkg2)
   # 3) If none of the above exists and there is a webserver: try webserver
   if [ ! -s "$testpkg1" -a ! -s "$testpkg2" -a "$MYWEBSERVER" != "" ]; then
      # try to get it from a server
      echo " * Checking connectivity of '$MYWEBSERVER' site..."
      noconnection="0"
      checkconnection web $MYWEBSERVER
      if [ "$noconnection" = "0" ]; then
         rm -f $TRYFNAME 2>/dev/null
         wget -p -nH --no-cache $MYWEBSERVER/$TRYFNAME # Downloads into SAVEDPATH
         chmod +x $TRYFNAME
      else
         echo "(TEMPORARY) ERROR: No connection with host '$MYWEBSERVER'" 1>&2
      fi
   fi
   locpwd=`pwd`
   [ -s "$TRYFNAME" ] && DEPFNAME="$locpwd/$TRYFNAME"

   # Finale sanity check
   debug1 "DEPFNAME=$DEPFNAME"
#if [ ! -s "$SCRIPTPREP" ]; then
   if [ ! -s "$testpkg1" -a ! -s "$testpkg2" ]; then
#if [ -s "$tespkg1" ]; then ok="1"; fi
#if [ -s "$tespkg2" ]; then ok="1"; fi
#if [ "$ok" = "0" ]; then
      echo "This script needs the file '$TRYFNAME'."
      echo "Please make sure that the file is in the directory '$SCRIPTPATH' or '$SAVEDPATH' "
      echo -n "or that you specified the (remote) source repository (--repo option)"
      if [ -z "$MYWEBSERVER" ]; then
         echo "."
      else
         echo ""
         echo "or that the server '$MYWEBSERVER' is available."
      fi
      exit 1
   fi
}
