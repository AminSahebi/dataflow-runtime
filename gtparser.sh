#!/bin/bash
# This script created by Roberto Giorgi (15-Sep-1995) is version:
VERSION="200319"
###############################################################################
#
# This script parses the input fiel using the following syntax
#
# EBNF Syntax for the 'Graphic Table' (GT) format:
#   <data_file>  ::=<table>{"\n"<table>}.
#   <table>      ::=<title>"\n"<tags_row>"\n"<values_row>{"\n"<values_row>}.
#   <title>      ::=<string>.
#   <tags_row>   ::=<x_tag><sep><y_tag>{<sep><y_tag>}.
#   <values_row> ::=<x_value><sep><y_value>{<sep><y_value>}.
#   <x_tag>      ::=<string>.
#   <y_tag>      ::=<string>.
#   <x_value>    ::=<number>|<invalid_val>.
#   <y_value>    ::=<number>|<invalid_val>.
#   <sep>        ::=<simple_sep>{<simple_sep>}.
#   <simple_sep> ::="\t"|" ".
#   <invalid_val>::="NaN".

###############################################################################

# Exit error codes:
# -1  invalid command line
# -2  some required program is missing
# -3  data file not found
# -4  incorrect data file syntax
#

md5sig=`md5sum $0|awk '{print $1}'`
MYSIG="${md5sig:0:2}${md5sig:(-2)}" # reduced-hash: first 2 char and last two char
VERSION1="v${VERSION}-${MYSIG}"
GTP_THISSCRIPT=`basename ${BASH_SOURCE[0]}`
GTP_THISTOOL="${GTP_THISSCRIPT%%.*}"; GTP_THISTOOLCAP="${GTP_THISTOOL^^}"
GTP_T00000=`date +%s%N | cut -b1-13`
#-----------------------------------------------------------------------------
PLOTNGRAPHS="0"
DEFTABLIST="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40"
lang="english"

#################################
MYSHLIB="myshlib.sh"
MYSTSH="s2t.sh"
MYSTAB="tf2symb.tab"

#-----------------------------------------------------------------------------
#####################################################
function gthp_usage () {
   echo "Usage: $0 [<options>] <data_file.txt|layout_file> [<list_of_table_ids>]"
   echo "   where:"
   echo "   <data_file.txt> is a TEXT GTable-format data file"
   echo "   <layout_file>  "\
         "is a list of GTable-format headers to be used as template layout"
   echo "   <list_of_table_ids>"\
         "is a comma-separated subset of table ids to be processed (e.g. 1,3)"
   echo ""
   echo "   and <options> can be:"
   echo "   --headers   process graphic-headers only"
   echo "   --data      process a data file"
   echo "   --norun     don't actually run"
   echo "   -v          verbose mode"
   echo "   -d          debug mode (more times: more details)"
   echo "   -q          quiet mode"
   echo "   --version   print versioon"
   echo ""
   exit 1
}

#####################################################
function gthp_commandline () {
   debug="0"
   verbose="0"
   strdebu=""
   strverb=""
   quiet="0"
   printversion="0"
   norun="0"
   HEADERSONLY="0"
   MYDATAFILE=""
   MYTABLIST=""
   while [ $# -gt 0 ]; do case $1 in
      -v) verbose=`expr $verbose + 1`; strverb="$strverb -v"; shift;;
      -d) debug=`expr $debug + 1`; strdebu="$strdebu -d"; shift;;
      -q) quiet="1"; shift;;
      --norun) norun="1"; shift;;
      --headers) HEADERSONLY="1"; shift;;
      --data) HEADERSONLY="2"; shift;;
      --version) printversion="1"; shift;;
#      -e) noexplo="1";shift;;
      -*) gthp_usage;;
      *)  if [ "$MYDATAFILE" = "" ]; then MYDATAFILE="$1"; shift; continue; fi
          if [ "$MYTABLIST" = "" ]; then MYTABLIST="$1"; shift; continue; fi
          gthp_usage;;
   esac; done

   # PRINT VERSION
   if [ "$printversion" != "0" ]; then echo "$0: version $VERSION1"; exit 0; fi

   #
   debug1 "MYDATAFILE=$MYDATAFILE"
   debug1 "MYTABLIST=$MYTABLIST"

   # Check syntax
   if [ "$MYDATAFILE" = "" ]; then  gthp_usage; fi
   local t
   local ok="1"
   if [ "$MYTABLIST" != "" ]; then
      MYTABLIST=`echo "$MYTABLIST"|tr ',' ' '`
      for t in $MYTABLIST; do
         if ! [[ "$t" =~ ^[0-9]+$ ]] ; then
            ok=0; break
         fi
      done
   fi
   if [ "$ok" = "0" ]; then
      echo "ERROR: <list_of_table_ids> parameter should be list of numbers (found '$t')"
      exit 1
   fi
}

#####################################################
declare -A PLOTHEADER
declare -A PLOTPARAM0
declare -A PLOTPARAM1
declare -A PLOTPARAM2
declare -A PLOTPARAM3
declare -A PLOTPARAM4
declare -A PLOTPARAM5
declare -A PLOTPARAM6
declare -A PLOTPARAM7
declare -A PLOTPARAM8
declare -A PLOTPARAM9
declare -A PLOTPARAM10
declare -A PLOTPARAM11
declare -A PLOTPARAM12
declare -A PLOTPARAM13
declare -A PLOTPARAM14
declare -A PLOTPARAM15
declare -A PLOTPARAM16
declare -A PLOTPARAM17
declare -A PLOTPARAM18
declare -A PLOTPARAM19
declare -A PLOTPARAM20
declare -A PLOTPARAM21
declare -A PLOTPARAM22
declare -A PLOTPARAM23
declare -A PLOTPARAM24
declare -A PLOTPARAM25
declare -A PLOTPARAM26
declare -A PLOTPARAM27
declare -A PLOTPARAM28
declare -A PLOTPARAM29
declare -A PLOTPARAM30
declare -A PLOTPARAM31
declare -A PLOTPARAM32
declare -A PLOTPARAM33
declare -A PLOTPARAM34
declare -A PLOTPARAM35
declare -A PLOTPARAM36
declare -A PLOTPARAM37
declare -A PLOTPARAM38
declare -A PLOTPARAM39
declare -A PLOTPARAM40
declare -A PLOTPARAM41
declare -A PLOTPARAM42
declare -A PLOTPARAM43
declare -A PLOTPARAM44
declare -A PLOTPARAM45
declare -A PLOTPARAM46
declare -A PLOTPARAM47
declare -A PLOTPARAM48
declare -A PLOTPARAM49

#####################################################
function gthp_initgfxparams {
   local t="$1"

   graphtype1="${PLOTPARAM1[$t]}"
   graphname1="${PLOTPARAM2[$t]}"
   graphlimits1="${PLOTPARAM3[$t]}"
   graphkeys1="${PLOTPARAM7[$t]}"
   minmaxinfo="${PLOTPARAM10[$t]}"

   isnx="${PLOTPARAM26[$t]}"
   kx="${PLOTPARAM27[$t]}"
   ky="${PLOTPARAM28[$t]}"
   xrec="${PLOTPARAM29[$t]}"

   graphytics1="${PLOTPARAM4[$t]}"
   keypos1="${PLOTPARAM5[$t]}"
   graphxtics1="${PLOTPARAM6[$t]}"
   graphzlabel1="${PLOTPARAM8[$t]}"
   genmarkers="${PLOTPARAM9[$t]}"
   fig="${PLOTPARAM11[$t]}"
   dat="${PLOTPARAM20[$t]}"
   tit="${PLOTPARAM21[$t]}"
   keys="${PLOTPARAM22[$t]}"
   graphxlabel1="${PLOTPARAM23[$t]}"
   graphzvalues1="${PLOTPARAM24[$t]}"
   graphztitle1="${PLOTPARAM25[$t]}"
   minx="${PLOTPARAM30[$t]}"
   xstep="${PLOTPARAM31[$t]}"
   maxx="${PLOTPARAM32[$t]}"
   xscal="${PLOTPARAM33[$t]}"
   xlogn="${PLOTPARAM34[$t]}"
   minv="${PLOTPARAM35[$t]}"
   ystep="${PLOTPARAM36[$t]}"
   maxv="${PLOTPARAM37[$t]}"
   yscal="${PLOTPARAM38[$t]}"
   ylogn="${PLOTPARAM39[$t]}"
   y0lim="${PLOTPARAM42[$t]}"
   y1lim="${PLOTPARAM43[$t]}"
   ylimits="${PLOTPARAM44[$t]}"
   x0lim="${PLOTPARAM45[$t]}"
   x1lim="${PLOTPARAM46[$t]}"
   xlimits="${PLOTPARAM47[$t]}"
}

#-----------------------------------------------------------------------------
#####################################################
function gthp_filtersymbol {
# INPUT: $1   mysymbol
# OUTPUT: mys
   local mysymbol
   local mys1
   local aaa
   mysymbol="$1"
   mys=""

   debug1 "    mysymbol=$mysymbol"
   IFS=' ' read -a aaa <<< "$mysymbol"
   if [ "${#aaa[@]}" = "1" ]; then
      if [ ! -s "$MYS2TSH" ]; then
         cecho "WARNING: gthp_filtersymbol: file '$MYS2TSH' not found." "$yellow"
      fi
      if [ ! -s "$MYS2TAB" ]; then
         cecho "WARNING: gthp_filtersymbol: file '$MYS2TAB' not found." "$yellow"
      fi
      debug2 "    mysymbol=$mysymbol lang=$lang MYS2TAB=$MYS2TAB"
      mys1=`$MYS2TSH $mysymbol $lang $MYS2TAB`
      debug2 "    mys1=$mys1"
      if [ "$mys1" = "" ]; then mys1="$mysymbol"; fi
      mys=`echo "$mys1"|tr '_' ' '`
   else
      debug2 "    mysymbol(2)=$mysymbol"
      mys1=`echo "$mysymbol"|tr '_' ' '`
      mys="$mys1"
   fi
   debug1 "    mys='$mys'"
}

#####################################################
function gthp_parse () {
   local magic
   local dfn
   local correct
   local honly="$1" 
   debug1 "honly=$honly"

   # Get the name of the file (last word in full path name)
   dfn=`echo $MYDATAFILE |awk '{n=split($1,a,"/");print a[n]}'`
   debug1 "MYDATAFILE=$MYDATAFILE"
   debug1 "dfn=$dfn"

   # Check if it is a GTable file (if specified, 'magic' gets the Format Version)
   magic=`awk '\
      BEGIN{ok="0"}\
      /^# GTable/{ if (NR=1) ok=1; \
      if ($3 ~ /Format/) { split($4,a,/V/); ok=a[2]; }} \
      END{printf("%s\n", ok);}\
   ' $MYDATAFILE`
   if [ "$magic" = "0" ]; then
      echo "ERROR: This is not a GTable file."
      exit 2
   fi
   debug1 "magic=$magic"

   # Check for duplicate Graph lines
   
   # error 101: duplicate Graph lines
   # Check the correct syntax of the data file
   local gttcheck
   gttcheck=`awk 'BEGIN{err=0}\
      /^[ \t]*#/{}\
      /^$/{if(data==1||tito==1) {tito=0;keys=0;data=0}}\
      /^[ \t]*#-Graph/{anf=split($0,a,/,/);a1=a[1];bnf=split(a1,b,/#-Graph/);\
         b2=b[2]; if(x[b2]==1) { erline=NR; err=101; } else x[b2]=1;\
         if(anf>=3){a3=a[3];gsub(/[[:blank:]]/, "", a3);\
            if (length(a3) != 0) tito=1;\
         }\
         if(anf>=8){a8=a[8]; gsub(/[[:blank:]]/, "", a8);\
            if (length(a8) != 0){keys=1; cur_tags=split(a[8],b,/ +/);}\
         }\
      }\
      /^[^#]/{\
         if (tito == 0) {\
            tito = 1;\
            cur_tags = 0;\
         } else {\
            if (keys == 0) {\
               keys = 1;\
               cur_tags = NF;\
            } else {\
               data=1;\
               if (cur_tags != NF) {\
                  err = 1;
                  erline = NR;\
                 exit (NR);\
               }\
            }\
         }\
      }\
      END{\
         res=tito+keys+data;if(res>0 && res<3){ err=2; erline=NR;}\
         printf("%d %d %d\n", err, erline, cur_tags);\
      }\
   ' $MYDATAFILE`
   debug1 "gttcheck=$gttcheck"
   gtterr=`echo "$gttcheck"|awk '{print $1}'`
   gtterline=`echo "$gttcheck"|awk '{print $2}'`
   gttctags=`echo "$gttcheck"|awk '{print $3}'`
   if [ "$gtterr" -gt "0" ]; then
      cecho "ERROR in file: $dfn --> $MYDATAFILE" "$red"
      case "$gtterr" in
         101) cecho "   Duplicate Graph line at line $gtterline." "$red" ;;
         1)   cecho "   Expected $gttctags fields at line $gtterline." "$red" ;;
         *)   cecho "   Incorrect syntax at line $gtterline"\
              "(error $gtterr)." "$red" ;;
      esac
      exit $gtterr
   fi

   # Extract First Line of Comment header
   heade1=`awk '/^#C /\
      {printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) } exit }\
      ' $MYDATAFILE`
   debug1 "heade1='$heade1'"

   # Extract Body of Comment header
   headeN=`awk '/^#C /\
      { if (f > 0) {\
           printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) }\
        printf("\n");}\
        f = 1 }\
      ' $MYDATAFILE`
   debug1 "headeN='$headeN'"

   # Extract Keys Title
   genzlabel=`awk '/^#-CurveParameter /\
      {for (k = 2; k <= NF; ++k) { printf("%s", $k); if (k < NF) printf(" "); } }\
      ' $MYDATAFILE`
   debug1 "genzlabel='$genzlabel'"

   # Extract CurveTitle ids
   genztitle=`awk '/^#-CurveTitle /\
      {printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) }}' $MYDATAFILE`
   debug1 "genztitle='$genztitle'"

   # Extract CurveValue ids
   genzvalues=`awk '/^#-CurveValue /\
      {printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) }}' $MYDATAFILE`
   debug1 "genzvalues='$genzvalues'"

   # Extract XLabel
   genxlabel=`awk '/^#-XLabel /\
      {printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) }}' $MYDATAFILE`
   debug1 "genxlabel='$genxlabel'"

   # Extract Markers ids
   genmarkers=`awk '/^#-CurveHMark /\
      {printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) }}' $MYDATAFILE`
   if [ "$genmarkers" = "" ]; then
      genmarkers="4 12 5 13 9 15 8 6 7 10 11 14 1 2 3 16 17 18 19 20 21 22 23 24 25 26";
      fi
   debug1 "genmarkers='$genmarkers'"

   # Extract X Tics and Scale
   xticauto=`awk '/^#-XTicAuto /\
      {for (k = 2; k <= NF; ++k) { printf("%s", $k); if (k < NF) printf(" "); } }\
      ' $MYDATAFILE`
   debug1 "xticauto='$xticauto'"
   xlmin=`echo $xticauto |cut -d" " -f 1`; if [ "$xlmin" = "" ]; then xlmin="min"; fi
   xlmax=`echo $xticauto |cut -d" " -f 3`; if [ "$xlmax" = "" ]; then xlmax="max"; fi
   xstep=`echo $xticauto |cut -d" " -f 2`; if [ "$xstep" = "" ]; then xstep="auto"; fi
   xscal=`echo $xticauto |cut -d" " -f 4`; if [ "$xscal" = "" ]; then xscal="auto"; fi
   xlogn=`echo $xticauto |cut -d" " -f 5`; if [ "$xlogn" = "" ]; then xlogn="auto"; fi

   # Extract Y Tics and Scale
   yticauto=`awk '/^#-YTicAuto /\
      {for (k = 2; k <= NF; ++k) { printf("%s", $k); if (k < NF) printf(" "); } }\
      ' $MYDATAFILE`
   debug1 "yticauto='$yticauto'"
   ylmin=`echo $yticauto |cut -d" " -f 1`; if [ "$ylmin" = "" ]; then ylmin="min"; fi
   ylmax=`echo $yticauto |cut -d" " -f 3`; if [ "$ylmax" = "" ]; then ylmax="max"; fi
   ystep=`echo $yticauto |cut -d" " -f 2`; if [ "$ystep" = "" ]; then ystep="auto"; fi
   yscal=`echo $yticauto |cut -d" " -f 4`; if [ "$yscal" = "" ]; then yscal="auto"; fi
   ylogn=`echo $yticauto |cut -d" " -f 5`; if [ "$ylogn" = "" ]; then ylogn="auto"; fi

   # sh has no vectors... so I use files...
   rm -f /tmp/gp*data*.txt /tmp/gp*tito*.txt /tmp/gp*keys*.txt /tmp/gp*maxv*.txt
   tnow=`date +%y%m%d%H%M%S`
   gpdatafn=`mktemp -u /tmp/gp${tnow}-XXXXXX-`
   debug2 "gpdatafn=$gpdatafn"

   #-------------------------------------------------------------------------------------
   # Create temporary file to hold, for each table: title, keys, data.
   # honly=1 --> no temporary file is produced: results are printed on putput
   verbose1 "* Parsing the DATAFILE..."
   dataparse=`awk -v nan="$nan" -v honly=$honly -v gpf=$gpdatafn '\
      function isanumber(x){\
         if (x ~ /^[\+-]*[0-9]*[\.]*[0-9]+([eE][\+-]*[0-9]+)?$/) return 1; else return 0\
      }\
      BEGIN{table=1;minv="-";maxv="-";minx="-";maxx="-"; maxstr="";nox=0;noy=0;xrec=0;\
         if (honly==2 || honly==0) {\
            tabfile = sprintf("%sdata1.txt", gpf);\
         }\
         if (honly==0) {\
            titfile = sprintf("%stito1.txt", gpf);\
            keyfile = sprintf("%skeys1.txt", gpf);\
            maxfile = sprintf("%smaxv1.txt", gpf);\
         };\
      }\
      /^[ \t]*#/{}\
      /^$/{\
         if(data==1){\
            maxima=sprintf("%s %s %s %s %s %s %s", minv,maxv,minx,maxx,nox,xrec,noy);\
            maxstr=maxstr " " maxima;\
            tito=0;keys=0;data=0;xrec=0;ox=0;noy=0;firstok=0;\
            minv="-";maxv="-";minx="-";maxx="-";\
            ++table;\
            if (honly==2 || honly ==0) {\
               if(tabfile) close (tabfile);\
               tabfile = sprintf("%sdata%d.txt", gpf, table);\
            }\
            if (honly==0) {\
               printf(maxima) > maxfile ; close(maxfile);\
               titfile = sprintf("%stito%d.txt", gpf, table);\
               keyfile = sprintf("%skeys%d.txt", gpf, table);\
               maxfile = sprintf("%smaxv%d.txt", gpf, table);\
            }\
         }\
         if(tito==1){tito=0;keys=0;data=0}\
      }\
      /^[ \t]*#-Graph/{anf=split($0,a,/,/);header++;\
         if (honly==0) {\
            titfile = sprintf("%stito%d.txt", gpf, table);\
            keyfile = sprintf("%skeys%d.txt", gpf, table);\
         }\
         if (anf>=3){a3=a[3];gsub(/[[:blank:]]/, "", a3);\
            if (length(a3) != 0) {\
               tito=1;\
               if (honly==0) {print a[3] > titfile; close(titfile);}\
            }\
         }\
         if (anf>=8){a8=a[8]; gsub(/[[:blank:]]/, "", a8);\
            if (length(a8) != 0) {\
               keys=1;\
               cur_tags=split(a[8],b,/ +/);\
               if (honly==0) {print a[8] > keyfile; close(keyfile);}\
            }\
         }\
      }\
      /^[^#]/{ \
         if (tito==0) {\
            tito = 1;\
            cur_tags = 0;\
            maxima=sprintf("%s %s %s %s %s %s %s", minv,maxv,minx,maxx,nox,xrec);\
            maxstr=maxstr " " maxima;\
            if (honly == 0) { \
               printf(maxima) > maxfile ; close(maxfile);\
               if (length($0) > 0) {print $0 > titfile; close(titfile);}\
            }\
            nox=0;noy=0;firstok=0;minv="-";maxv="-";minx="-";maxx="-";xrec=0;\
         } else {\
            if (keys==0) {\
               keys = 1;\
               cur_tags = NF;\
               if (honly == 0) { \
                  if (length($0) > 0) {print $0 > keyfile; close(keyfile);}\
               }\
            } else {\
               data = 1; lastx=$1; xrec++;\
               if (firstok==0) {firstx=$1;firstok=1;}\
               if (nox == 0 && isanumber($1)) {  \
                  minx = maxx = $1; nox = 1;\
               }\
               else {\
                  if (isanumber($1)) {\
                     if ($1 < minx) minx = $1;\
                     if ($1 > maxx) maxx = $1;\
                  }\
               }\
               if (minx=="-") minx=firstx;\
               if (maxx=="-") maxx=lastx;\
               if (nox==0) maxx=lastx;\
               if (NF>1){\
                  for (i=2; i<=NF; ++i){\
                     if(isanumber($i)){\
                        if (noy==0) {minv=maxv=$i; noy=1}\
                        else{\
                           if ($i > maxv) maxv = $i;\
                           if ($i < minv) minv = $i;\
                        }\
                     }\
                  }\
               }\
               if (honly==0 || honly==2) { \
                  if (length($0) > 0) {print $0 >> tabfile; close(tabfile);}\
               }\
            }\
         }\
      }\
      END{\
            maxima=sprintf("%s %s %s %s %s %s %s",minv,maxv,minx,maxx,nox,xrec,noy);\
            maxstr=maxstr " " maxima;\
         if (honly==0 || honly==2) {\
            res=tito+keys+data;if(res==3) ++table;\
            if(tabfile) close (tabfile);\
            if (honly==0) {\
               printf(maxima) > maxfile ; close(maxfile);\
               if(keyfile) close (keyfile);\
               if(titfile) close (titfile);\
            }\
            printf("%d %s", table-1, maxstr);\
         } else {\
            printf("%d %s", header, maxstr);\
         }\
      }' $MYDATAFILE`
   debug2 "dataparse='$dataparse'"

   # ntables
   local ntables="0"
   [ "$dataparse" != "" ] && ntables=`echo "$dataparse"|awk '{print $1}'`

   # Assign TABLELIST
   debug1 "MYTABLIST=$MYTABLIST"
   if [ "$MYTABLIST" = "" ]; then
      TABLELIST=`echo "$DEFTABLIST"|tr ',' ' '`
      local mxlt=`echo "$TABLELIST" | awk '{printf("%d", $NF)}'`
      if [ "$ntables" -lt "$mxlt" ]; then
         local i=1
         TABLELIST=""
         while [ $i -le $ntables ]; do
            [ -z "$TABLELIST" ] && TABLELIST="$i" || TABLELIST="$TABLELIST $i"
            i=`expr $i + 1`
         done
      fi
   else
      TABLELIST=`echo "$MYTABLIST"|tr ',' ' '`
   fi
   debug1 "TABLELIST=$TABLELIST"

   # TABLEV
   read -a TABLEV <<< "$TABLELIST"

   if [ "$dataparse" != "" ]; then
      local k j t o
      local s=7
      for((k=0;k<ntables;++k)); do
#         t=${TABLEV[$k]}
         t=`expr $k + 1`
         o=`echo "$dataparse"|\
            awk -v k=$k -v s=$s \
               '{for(j=0;j<s;++j){if(j>0) printf(" "); printf("%s", $(2+j+s*k))}}'`
         PLOTPARAM10[$k]="$o"
         debug2 "----------------------"
         debug2 "Table[$t]:PLOTPARAM10[$k]='$o'"
         debug2 "----------------------"
      done
   fi
   if [ "$ntables" = "1" ]; then tt="table"; else tt="tables"; fi

   # Sanity Check 1
   if [ "$ntables" = "0" ]; then
      echo "ERROR: Couldn't find any valid table in file $MYDATAFILE"
      exit 4
   else
      comment "* Found $ntables $tt in data file. Analyzing $tt ..."
   fi

   # Sanity Check 2
   local ok="1"
   local k
   for ((k=0;k<ntables;++k)); do
      if [ "${PLOTPARAM10[$k]}" = "" ]; then ok="0"; break; fi
   done
   if [ "$ok" = "0" ]; then 
      echo "ERROR: couldn't extract parameters of table $k from $MYDATAFILE"
      exit 4
   fi

   # Get the test name
   ttn=`basename $dfn .txt`
   # Define output document filename prefix
   PLOTDOCUMENT="$ttn"

   # Define general information about the graphs
   PLOTDOCSUMM="$heade1"
   PLOTDOCCOMM="$headeN"

   #############################################################################
   # For each table in the data file produce a data file suitable for gnuplot
   # (the gnuplot input file)

   # Loop on tables
   local tabcnt=0

   # Setup a Reverse Lookup table
   for((t=0; t<${#TABLEV[@]};++t)); do
      TABLER[${TABLEV[$t]}]="$t";
   done

#   for ((t=0;t<ntables;++t)); do
   if [ "$TABLELIST" = "" ]; then return; fi
   for i in $TABLELIST; do
#      t="${TABLER[$i]}"
      t=`expr $i - 1`
      verbose2 "-----------------------------------------------------------------------"
      verbose1 "* Processing table #$t (TABLE[$i]) ..."

      # Define output figure filename prefix
      PLOTPARAM11[$t]="${ttn}f$i"
 
      #
#      PLOTHEADER[$t]=`awk -F, "/^\#-Graph$i,/{print \\$0}" $MYDATAFILE|sed -e"s/^ \\+//"`
      PLOTHEADER[$t]=`awk -F, "/^#-Graph$i,/{print \\$0}" $MYDATAFILE|sed -e"s/^ \\+//"`
      debug1   "HEADER='${PLOTHEADER[$t]}'"
      # Sanity check
      if [ "${PLOTHEADER[$t]}" = "" ]; then
         cecho "WARNING: TABLE[$i] header not found n $MYDATAFILE !" "$yellow"
         continue
      fi

      # Extract Graph Type
#      graphtype1=`awk -F, "/^\#-Graph$i,/{print \\$2}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphtype1=`awk -F, "/^#-Graph$i,/{print \\$2}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphtype$i='$graphtype1'"
      PLOTPARAM1[$t]="$graphtype1"

      # Extract Graph Name
#      graphname1=`awk -F, "/^\#-Graph$i,/{print \\$3}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphname1=`awk -F, "/^#-Graph$i,/{print \\$3}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphname$i='$graphname1'"
      PLOTPARAM2[$t]="$graphname1"

      # Extract Graph limits
#      graphlimits1=`awk -F, "/^\#-Graph$i,/{print \\$4}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphlimits1=`awk -F, "/^#-Graph$i,/{print \\$4}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphlimits$i='$graphlimits1'"
      PLOTPARAM3[$t]="$graphlimits1"

      # Extract Graph ytics
#      graphytics1=`awk -F, "/^\#-Graph$i,/{print \\$5}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphytics1=`awk -F, "/^#-Graph$i,/{print \\$5}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphytics$i='$graphytics1'"
      PLOTPARAM4[$t]="$graphytics1"

      # Extract Graph Key Position
#      keypos1=`awk -F, "/^\#-Graph$i,/{print \\$6}" $MYDATAFILE| sed -e"s/^ \\+//"`
      keypos1=`awk -F, "/^#-Graph$i,/{print \\$6}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    keypos$i='$keypos1'"
      PLOTPARAM5[$t]="$keypos1"

      # Extract Graph xtics
#      graphxtics1=`awk -F, "/^\#-Graph$i,/{print \\$7}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphxtics1=`awk -F, "/^#-Graph$i,/{print \\$7}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphxtics$i='$graphxtics1'"
      PLOTPARAM6[$t]="$graphxtics1"

      # Extract Graph Keys
#      graphkeys1=`awk -F, "/^\#-Graph$i,/{print \\$8}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphkeys1=`awk -F, "/^#-Graph$i,/{print \\$8}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphkeys$i='$graphkeys1'"
      PLOTPARAM7[$t]="$graphkeys1"

      # Extract Graph ZLabel
#      graphzlabel1=`awk -F, "/^\#-Graph$i,/{print \\$9}" $MYDATAFILE| sed -e"s/^ \\+//"`
      graphzlabel1=`awk -F, "/^#-Graph$i,/{print \\$9}" $MYDATAFILE| sed -e"s/^ \\+//"`
      verbose2 "    graphzlabel$i='$graphzlabel1'"
      PLOTPARAM8[$t]="$graphzlabel1"

      #
      PLOTPARAM9[$t]="$genmarkers"

      # Define data filename
      dat="${gpdatafn}data$i.txt"
      PLOTPARAM20[$t]="$dat"

      # Extract Graph Name from Heading of table
      #tit=`cat tito$i.txt`
      tit3=""
      if [ -s "${gpdatafn}tito$i.txt" ]; then
         tit3=`cat ${gpdatafn}tito$i.txt`
      fi
      if [ -z "$tit3" ]; then
         tit3="$graphname1"
      fi
      debug1 "    tit3=$tit3"
#   tit=`csim_s2t $tit1 $lang`
      gthp_filtersymbol "$tit3"; tit="$mys"
      verbose2 "    tit$i='$tit'"
      PLOTPARAM21[$t]="$tit"

      # Extract Graph Keys from Heading of table
      keys=""
      if [ -s "${gpdatafn}keys$i.txt" ]; then
         keys=`cat ${gpdatafn}keys$i.txt`
      fi
      if [ -z "$keys" ]; then
         keys="$graphkeys1"
      fi
      verbose2 "    keys$i='$keys'"
      PLOTPARAM22[$t]="$keys"

      # Define XLABEL
      graphxlabel0=`echo "$keys" | awk '{print $1}'`
      if [ "$graphxlabel0" = "" ]; then graphxlabel0="$genxlabel"; fi
      local k buf="$graphxlabel0"
      local gxllist=`echo "$graphxlabel0"|sed 's/:/ /g'`
      debug1 "    gxllist=$gxllist"
      graphxlabel1=""
      for k in $gxllist; do
         gthp_filtersymbol "$k" # output in $mys
#         kk=`echo "$mys"|tr ' ' '_'`
         kk="$mys"
         buf=`echo "$buf"|sed "s/$k/$kk/g"`
#         [ -z "$graphxlabel1" ] && graphxlabel1="$mys" || graphxlabel1="${graphxlabel1}, $mys"
      done
      graphxlabel1="$buf"
      verbose2 "    graphxlabel1='$graphxlabel1'"
      PLOTPARAM23[$t]="$graphxlabel1"

      # Define ZVALUES
      graphzvalues1=`echo "$keys" \
         | awk '{printf("%s", $2); for (k = 3; k <= NF; ++k) { printf(" %s", $k) }}'`
      if [ "$graphzvalues1" = "" ]; then graphzvalues1="$genzvalues"; fi
#      sss=""
      local k buf="$graphzvalues1"
      local gzllist=`echo "$graphzvalues1"|sed 's/:/ /g'`
      for k in $gzllist; do
         gthp_filtersymbol "$k"
         kk=`echo "$mys"|tr ' ' '_'`
         buf=`echo "$buf"|sed "s/$k/$kk/g"`
      done
      graphzvalues1="$buf"
      verbose2 "    graphzvalues1='$graphzvalues1'"
      PLOTPARAM24[$t]="$graphzvalues1"
   
      # Define ZTITLE
      # SIMPLIFICATION: here genzlabel and genztitle" are assumed to be the same thing!
      if [ "$graphztitle1" = "" ]; then graphztitle1="$genzlabel"; fi
      if [ "$graphztitle1" = "" ]; then graphztitle1="$genztitle"; fi
      if [ "$graphztitle1" = "" ]; then graphztitle1="$graphzlabel1"; fi
      local k buf="$graphztitle1"
      local gz2list=`echo "$graphztitle1"|sed 's/:/ /g'`
      for k in $gz2list; do
         gthp_filtersymbol "$k"
         kk=`echo "$mys"|tr ' ' '_'`
         buf=`echo "$buf"|sed "s/$k/$kk/g"`
      done
      graphztitle1="$buf"
      verbose2 "    graphztitle1='$graphztitle1'"
      PLOTPARAM25[$t]="$graphztitle1"

      local minmaxinfo=""
      if [ -s "${gpdatafn}maxv$i.txt" ]; then
         minmaxinfo=`cat ${gpdatafn}maxv$i.txt`
      fi
      debug1 "minmaxinfo=$minmaxinfo"
      if [ -z "$minmaxinfo" ]; then
         minmaxinfo="${PLOTPARAM10[$t]}"
      fi
      debug1 "minmaxinfo=$minmaxinfo"
      minv=`echo $minmaxinfo | awk '{print $1}'`
      maxv=`echo $minmaxinfo | awk '{print $2}'`
      minx=`echo $minmaxinfo | awk '{print $3}'`
      maxx=`echo $minmaxinfo | awk '{print $4}'`
      isnx=`echo $minmaxinfo | awk '{print $5}'`
      xrec=`echo $minmaxinfo | awk '{print $6}'`
      verbose2 "    minv=$minv  maxv=$maxv"
      verbose2 "    minx=$minx  maxx=$maxx  isnx=$isnx xrec=$xrec"
      PLOTPARAM10[$t]="$minmaxinfo"

      # TO BE DEVELOPED: what is 0.08?
      if [ "$isnx" = "1" ]; then
         kx=`echo "$minx $maxx" | awk '{printf("%g", $1 + (($2 - $1) * 0.08))}'`
      else
         kx="1"
      fi
      ky=`echo "$minv $maxv" | awk '{printf("%g", $2 - (($2 - $1) * 0.08))}'`
      verbose2 "    kx=$kx  ky=$ky"
      PLOTPARAM26[$t]="$isnx"
      PLOTPARAM27[$t]="$kx"
      PLOTPARAM28[$t]="$ky"
      PLOTPARAM29[$t]="$xrec"

      # Set xtics
      local tminx=""
      local tmaxx=""
      local tminv=""
      local tmaxv=""

#      local xstep=""
#      local xscal=""
#      local xlogn=""
#      local yscal=""
#      local ystep=""
#      local ylogn=""
      if [ "$graphxtics1" != "" ]; then
         tminx=`echo $graphxtics1|cut -d" " -f 1`; [ "$tminx" = "min" ] && tminx="$minx"
         xstep=`echo $graphxtics1|cut -d" " -f 2`
         tmaxx=`echo $graphxtics1|cut -d" " -f 3`; [ "$tmaxx" = "max" ] && tmaxx="$maxx"
         xscal=`echo $graphxtics1|cut -d" " -f 4`
         xlogn=`echo $graphxtics1|cut -d" " -f 5`
         minx="$tminx"; maxx="$tmaxx"
      fi
      if [ "$isnx" = "0" ]; then
         xstep="1"
      fi
      verbose2 \
         "    minx='$minx' xstep='$xstep' maxx='$maxx' xscal='$xscal' xlogn='$xlogn'"
      PLOTPARAM30[$t]="$minx"
      PLOTPARAM31[$t]="$xstep"
      PLOTPARAM32[$t]="$maxx"
      PLOTPARAM33[$t]="$xscal"
      PLOTPARAM34[$t]="$xlogn"

      # Set ytics
      tminv="$minv"; tmaxv="$maxv"; tminx="$minx"; tmaxx="$maxx"
      if [ "$graphytics1" != "" ]; then
         tminv=`echo $graphytics1|cut -d" " -f 1`; [ "$tminv" = "min" ] && tminv="$minv"
         ystep=`echo $graphytics1|cut -d" " -f 2`
         tmaxv=`echo $graphytics1|cut -d" " -f 3`; [ "$tmaxv" = "max" ] && tmaxv="$maxv"
         yscal=`echo $graphytics1|cut -d" " -f 4`
         ylogn=`echo $graphytics1|cut -d" " -f 5`
         minv="$tminv"; maxv="$tmaxv"
      fi
      verbose2 \
         "    minv='$minv' ystep='$ystep' maxv='$maxv' yscal='$yscal' ylogn='$ylogn'"
      PLOTPARAM35[$t]="$minv"
      PLOTPARAM36[$t]="$ystep"
      PLOTPARAM37[$t]="$maxv"
      PLOTPARAM38[$t]="$yscal"
      PLOTPARAM39[$t]="$ylogn"

      verbose2 "    tminv=$tminv  tmaxv=$tmaxv  tminx=$tminx  tmaxx=$tmaxx"
      verbose2 "    new minv=$minv  ystep=$ystep  maxv=$maxv"
      verbose2 "    new minx=$minx  xstep=$xstep  maxx=$maxx"

      ################
      x0lim="";x1lim="";y0lim="";y1lim=""
      debug1 "graphlimits1=$graphlimits1"
      if [ "$graphlimits1" != "" ]; then
         x0lim=`echo $graphlimits1| cut -d" " -f 1`
         x1lim=`echo $graphlimits1| cut -d" " -f 2`
         y0lim=`echo $graphlimits1| cut -d" " -f 3`
         y1lim=`echo $graphlimits1| cut -d" " -f 4`
      fi
      [ "$x0lim" = "min" -o "$x0lim" = "" ] && x0lim="$minx"
      [ "$x1lim" = "max" -o "$x1lim" = "" ] && x1lim="$maxx"
      [ "$y0lim" = "min" -o "$y0lim" = "" ] && y0lim="$minv"
      [ "$y1lim" = "max" -o "$y1lim" = "" ] && y1lim="$maxv"
      verbose2 "    x0lim=$x0lim  x1lim=$x1lim  y0lim=$y0lim  y1lim=$y1lim"
#      if [ "$y0lim" = "" -o "$y0lim" = "-" ]; then
#         ylimits=0; y0lim=$minv; y1lim=$maxv;
#      else
#         ylimits=1;
#      fi
#      debug1 "mod1 y0lim=$y0lim  y1lim=$y1lim"
#      if [ "$y0lim" != "-" -a "$y1lim" != "-" ]; then
#         y0lim=`echo $y0lim|awk '{ printf("%g", $1)}'`
#         y1lim=`echo $y1lim|awk '{ printf("%g", $1)}'`
#      else
#         y0lim="0"; y1lim="0"
#      fi
#      debug1 "mod2 y0lim=$y0lim  y1lim=$y1lim"
#      # if limits are identical then unbalance them
##      if [ "$y0lim" = "$y1lim" ]; then
#      if [ "$y0lim" = "$y1lim" -a "$y0lim" != "-" ]; then
##         y0lim=`expr $y0lim - 1`; y1lim=`expr $y1lim + 1`
#         y0lim=`expr $y0lim - 0.5`; y1lim=`expr $y1lim + 0.5`
#      fi
      if [ "$y0lim" = "" -o "$y0lim" = "-" ]; then
         ylimits=0; y0lim=$minv; y1lim=$maxv;
      else
         ylimits=1;
         debug1 "mod1 y0lim=$y0lim  y1lim=$y1lim"
         if [ "$y0lim" != "-" -a "$y1lim" != "-" ]; then
            y0lim=`echo $y0lim|awk '{ printf("%g", $1)}'`
            y1lim=`echo $y1lim|awk '{ printf("%g", $1)}'`
         else
            y0lim="0"; y1lim="0"
         fi
         debug1 "mod2 y0lim=$y0lim  y1lim=$y1lim"
# DONE IN gtgraph.s -- here may cause error in case of floating point numbers: it requires carefil processing
#         # if limits are identical then unbalance them
#         if [ "$y0lim" = "$y1lim" ]; then
#            y0lim=`expr $y0lim - 1`; y1lim=`expr $y1lim + 1`
#         fi
      fi
      verbose2 "    ylimits=$ylimits  new y0lim=$y0lim  y1lim=$y1lim"
      PLOTPARAM42[$t]="$y0lim"
      PLOTPARAM43[$t]="$y1lim"
      PLOTPARAM44[$t]="$ylimits"

      ################
      if [ "$x0lim" = "" -o "$x0lim" = "-" -o "$isnx" = "0" ]; then
         xlimits=0; x0lim=$minx; x1lim=$maxx;
      else
         xlimits=1;
         debug1 "mod1 x0lim=$x0lim  x1lim=$x1lim"
         if [ "$x0lim" != "-" -a "$x1lim" != "-" ]; then
            x0lim=`echo $x0lim|awk '{ printf("%g", $1)}'`
            x1lim=`echo $x1lim|awk '{ printf("%g", $1)}'`
         fi
# DONE IN gtgraph.s -- here may cause error in case of floating point numbers: it requires carefil processing
#         # if limits are identical then unbalance them
#         if [ "$x0lim" = "$x1lim" ]; then
#            x0lim=`expr $x0lim - 1`; x1lim=`expr $x1lim + 1`
#         fi
         debug1 "mod2 x0lim=$x0lim  x1lim=$x1lim"
      fi
      verbose2 "    xlimits=$xlimits  new x0lim=$x0lim  x1lim=$x1lim"
      PLOTPARAM45[$t]="$x0lim"
      PLOTPARAM46[$t]="$x1lim"
      PLOTPARAM47[$t]="$xlimits"

      debug1 \
         "xlimits='$xlimits'  Xlimits: '$x0lim' '$x1lim'  tics: '$minx' '$xstep' '$maxx'"
      debug1 \
         "ylimits='$ylimits'  Ylimits: '$y0lim' '$y1lim'  tics: '$minv' '$ystep' '$maxv'"

      tabcnt=`expr $tabcnt + 1`
   done
   verbose2 "-----------------------------------------------------------------------"
   PLOTNGRAPHS="$tabcnt"
}

#-----------------------------------------------------------------------------
#SCRIPT STARTS HERE
#-----------------------------------------------------------------------------
[ -s "$HOME/bin/$MYSHLIB" ] && MYSHLIB="$HOME/bin/$MYSHLIB"
if [ ! -s "$MYSHLIB" ]; then
   echo "ERROR: cannot find '$MYSHLIB' in current directory or ~/bin"
   exit 1
fi
source "$MYSHLIB"
#-----------------------------------------------------------------------------
gthp_commandline $*
getdepfile "$MYSTSH"  # Sets DEPFNAME (and cd to dir where script is)
MYS2TSH="$DEPFNAME"
verbose1 "* MYS2TSH=$MYS2TSH"
getdepfile "$MYSTAB"  # Sets DEPFNAME (and cd to dir where script is)
MYS2TAB="$DEPFNAME"
verbose1 "* MYS2TAB=$MYS2TAB"
#-----------------------------------------------------------------------------
if [ ! -s "$MYDATAFILE" ]; then 
   cecho "ERROR: data-file/layout-file '$MYDATAFILE' not found." "$red"
   exit 1
fi
if [ "${MYDATAFILE:0:1}" != "/" ]; then  MYDATAFILE="./$MYDATAFILE"; fi
comment "* MYDATAFILE: $MYDATAFILE"

##################
gthp_parse $HEADERSONLY
##################
#
GTP_T00001=`date +%s%N | cut -b1-13`
gtp_exptimems=`expr $GTP_T00001 - $GTP_T00000`
gtp_exptimehh=`echo ""|awk 'END{v=exptimems/3600000;printf("%7.2f",v)}' exptimems=$gtp_exptimems`

gtp_tots=`expr $gtp_exptimems / 1000`
gtp_closing_str="Goodbye. "`cecho -f -n "$GTP_THISTOOLCAP" "$green"`" $gtp_tots secs."

#[ "$norun" = "1" ] && { echo "GTPARSER Goodbye."; [[ $_ != $0 ]] && return || exit 0; }

# Detect if this run is sourced or not
# if "${BASH_SOURCE[0]}" != "${0}" the script is sourced or called inside a function
mystar=""; sourced="0"; [[ "${BASH_SOURCE[0]}" != "${0}" ]] && { sourced="1";  mystar="* "; }
if [ "$norun" = "1" ]; then
   [ "$sourced" = "0" -a "$quiet" = "0" ] && echo -e "${mystar}$gtp_closing_str";
   [ "$sourced" = "1" ] && return || exit 0
fi
#[ "$norun" = "1" ] && { echo "GTPARSER Goodbye." && return; }
#return

echo "* PLOTNGRAPHS=$PLOTNGRAPHS"
for ((k=0;k<PLOTNGRAPHS;k++)); do
   debug1 "* PLOTHEADER[$k]=${PLOTHEADER[$k]}"
   for ((n=0;n<50;n++)); do
      eval aaa="\${PLOTPARAM$n[$k]}"
      debug1 "* PLOTPARAM$n[$k]=$aaa"
   done
done
