#!/bin/sh
awk -F: '
  BEGIN{ printf "%s\t|%s\t|%s","Roll Number","Mobile Number","Grade"}
  { gsub(/ /,"",$2); printf "%s", ($1 ~ "^Roll" ? RS : "\t|") $2 }
  END { print "" }
' file
