#include "tsu.h"
#include <stdio.h>
#include <time.h>

#include <papi.h>

int quiet, retval;
//int EventSet1 = PAPI_NULL;
//long long count;

#define NUM_EVENTS 6 
int Events[] = {PAPI_TOT_CYC, PAPI_TOT_INS};
int EventSet = PAPI_NULL;
long long int values[NUM_EVENTS];
int count=0;

int nn; // input, for checking purposes
int th;

extern uint64_t df_ts0[100],df_ts1[100]; // timestamp buffers
extern uint64_t df_tt;
long long start_cycles, end_cycles;
uint64_t sum_add = 0;
uint64_t sum_fib = 0;
uint64_t temp6;
uint64_t temp4,temp5;
uint64_t sum_rep = 0;
long long adder_start,adder_stop,rep_start,rep_stop;
long long fib_start,fib_stop;
long long app_start,app_stop;

void fib(void);
void report(void);
int main(int argc, char **argv)
{
	retval = PAPI_library_init(PAPI_VER_CURRENT);
    	retval = PAPI_create_eventset(&EventSet);
    	retval = PAPI_add_events(EventSet, Events, NUM_EVENTS);
    	retval = PAPI_start(EventSet);

	//nn = 10;
	if (argc > 1)
		nn = atoi(argv[1]);
	if (argc > 2)
		th = atoi(argv[2]);
	printf("computing fibonacci(%d,%d)\n",nn,th);
	df_tt = df_tstamp(df_ts0);
	PAPI_reset(EventSet);
	PAPI_start(EventSet);
	//tt = df_tstamp(ts0);
	uint64_t trep = df_tschedule(&report,1); // spawn reporter
	uint64_t tfib = df_tschedule(&fib, 2);  // spawn fib
	df_write(tfib,0, _TLOC(trep,0)); // edge: fib.out -> report[0]
	df_write(tfib,1, nn); // send fib, nn
	//	printf("++main\n");
	df_destroy();
	return 0;
}

void adder(void)
{
	df_ldframe();
	uint64_t n1 = df_frame(1); // receive n1
	uint64_t n2 = df_frame(2); // receive n2
	df_writeN(df_frame(0),n1+n2); // send n1+n2
	df_destroy();
}

// scalar version
uint64_t xfib(int n) {
	if( n<=1) return n;
	else return xfib(n-1) + xfib(n-2);}

void fib(void)
{
	df_ldframe();
	int n = df_frame(1); // receive n
	//    printf("n = df_frame(1) = %d\n",n);
	//    fflush(stdout);
#if 0 
	if (n < XFIB_THRESHOLD) {
		df_writeN(df_frame(0),xfib(n)); // send n
	}
#endif
	if (n <= th+1 || n<2){
		df_writeN(df_frame(0),xfib(n)); // send n
	}
	else {
		uint64_t tfib1 = df_tschedule(&fib,2);  // spawn fib1
		uint64_t tfib2 = df_tschedule(&fib,2);  // spawn fib2

		df_write(tfib1,1, n-1); // send fib1, n-1
		df_write(tfib2,1, n-2); // send fib2, n-2
		uint64_t tadd = df_tschedule(&adder,3); // spawn adder
		df_write(tadd,0, df_frame(0)); // edge: adder.out -> self.out
		df_write(tfib1,0, _TLOC(tadd,1)); // edge: fib1.out -> adder[1]
		df_write(tfib2,0, _TLOC(tadd,2)); // edge: fib2.out -> adder[2]
	}
	df_destroy();
}

void report(void)
{
	df_ldframe();
	int n=df_frame(0);
	df_destroy();
	retval = PAPI_stop(EventSet, values);

	printf("++report\n");
	fflush(stdout);
	//tt = df_tstamp(ts1) - tt;
	printf("df fib= %d\n",n);
	printf("*** SUCCESS ***\n");
	fflush(stdout);

	printf("\n");

	printf("-----------------------PAPI STATISTICS---------------------\n");
  	printf( "  =%ld\n", values[0] );
        printf( "  =%ld\n", values[1] );
        printf( "  =%ld\n", values[2] );
  	printf( "  =%ld\n", values[3] );
  	printf( "  =%ld\n", values[4] );
        printf( "  =%ld\n", values[5] );
        printf( "  =%ld\n", values[6] );
        printf( "  =%ld\n", values[7] );
        printf( "  =%ld\n", values[8] );
	df_exit();

}
