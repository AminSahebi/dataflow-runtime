#define TSU_PRELOAD_FRAME
#define __DEBUG

int N = 128;
int BLOCKSZ = 4;

#define XDATA_INT64
#define XOWM_DATA

#ifdef XOWM_DATA  
   #define OWM_DATA owm_vop_t
#endif

#ifdef XDATA_INT64
 #define DATA uint64_t 
#endif

#include "tsu_owm.h"
#include "drt-debug.h"

#define SIZE(M,N) (N*M*sizeof(DATA))
#define RB_SZ SIZE(N,N)
#define RA_SZ SIZE(N,BLOCKSZ)
#define RC_SZ SIZE(N,BLOCKSZ)

#define RC_OFF(j) (	       SIZE(N,j))	
#define RB_OFF(j) (  SIZE(N,N)+SIZE(N,j))
#define RA_OFF(j) (2*SIZE(N,N)+SIZE(N,j))



typedef struct  { OWM_DATA A; OWM_DATA B; OWM_DATA C; uint64_t xreport; } bmmul_s;
void bmmul()
{
    df_ldframe();

    const DATA *A = (DATA *) df_frame(offsetof(bmmul_s,A));
    const DATA *B = (DATA *)df_frame(offsetof(bmmul_s,B));
    DATA *C = (DATA *) df_frame(offsetof(bmmul_s,C));
    afp rep = (afp) df_frame(offsetof(bmmul_s,xreport));

    int i, j, k;

    df_publish(C);
 
    for (j = 0; j < BLOCKSZ; j++) {
        for (i = 0; i < N; i++) {
            DATA t = 0;
            for (k = 0; k < N; k++) {
                t += A[j * N + k] * B[k * N + i];
            }
            C[i + j * N] = t;
        }
    }
   
    df_publish(C);
    df_tdecrease(rep); // decrement the barrier count
    df_destroy();
}

typedef struct { OWM_DATA scsp; OWM_DATA C; } report_s;
void report()
{ 
    
    df_ldframe();
    const DATA *C = (DATA *)df_frame(offsetof(report_s,C));
    
    const DATA *scsp = (DATA *) df_frame(offsetof(report_s,scsp));
    int i, j;
    int ok = 1;
    const uint64_t superchecksum = *scsp;
    xzonestop(1);
    df_destroy(); // XSM computations are finished at this point

    // Verify the SuperCheckSum
    uint64_t xchecksum = 0L;
    for (i = 0; i < N; i++) {   // i = row pointer
        for (j = 0; j < N; j++) { // j = column pointer
            xchecksum = (xchecksum + (int)(C[i*N + j])) &0xFF;
        }
    }
    if (superchecksum==xchecksum) ok = 1; else ok = 0;
   printf("CHECKSUM=%lu vs %lu   OK=%d\n", xchecksum, superchecksum, ok); fflush(stdout);
   printf("\n*** %s ***\n", ok ? "SUCCESS" : "FAILURE");fflush(stdout);
}

typedef struct { uint64_t xr; } owm_matrix_mul_s;
void owm_matrix_mul() /* OWM version. Compute C=A*B.  */
{
    df_ldframe();
     
    uint64_t xr = df_frame(offsetof(owm_matrix_mul_s,xr));

    xzonestart(1);
   // xtimestamp(); //resets timestamp statistics so we count time from here
    int j,nb;
    for (j=nb=0; j<N; j+=BLOCKSZ,++nb) {

        uint64_t bm = df_tschedulez(&bmmul, 4, sizeof(bmmul_s));
        /* Region B (size N*N)       stores the entire matrix B */
        /* Region A (size N*BLOCKSZ) stores the block of matrix A  */
        /* Region C (size N*BLOCKSZ) stores the computed result.  */
        df_subscribe(XDST(bm, bmmul, A), RA_OFF(j), RA_SZ, _OWM_MODE_R);
        df_subscribe(XDST(bm, bmmul, B), RB_OFF(0), RB_SZ, _OWM_MODE_R);
        df_subscribe(XDST(bm, bmmul, C), RC_OFF(j), RC_SZ, _OWM_MODE_W);

        // finish barrier
        df_write(bm,offsetof(bmmul_s,xreport),xr);
        df_tdecreaseN(bm,3);
    }
    df_destroy();
}




typedef struct { OWM_DATA A; OWM_DATA B; OWM_DATA C; OWM_DATA scs; uint64_t xm;} fill_matrix_s;
void fill_matrix()
{

    df_ldframe();
    
    DATA *A = (DATA *)df_frame(offsetof(fill_matrix_s,A));
    DATA *B = (DATA *)df_frame(offsetof(fill_matrix_s,B));
    DATA *C = (DATA *)df_frame(offsetof(fill_matrix_s,C));
    DATA *scs = (DATA *)df_frame(offsetof(fill_matrix_s,scs));
    uint64_t xm = df_frame(offsetof(fill_matrix_s,xm));

    int i, j, cr, cc;
    uint64_t superchecksum;
    srand(12345); // start always with same numbers in the matrices (optional)
    //C=0
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            C[i * N + j] = 0;
        }
    }
    //B
    for (i = 0; i < N; i++) {
        cr = 0;
        for (j = 0; j < N-1; j++) {
            int val1 = rand()&0xFF;
            cr = (cr + val1) &0xFF;
            B[i*N+j] = (DATA)val1;
        }
        B[i*N+N-1] = (DATA)cr;
    }
    //A
    for (j = 0; j < N; j++) {
        cc = 0;
        for (i = 0; i < N-1; i++) {
            int val2 = rand()&0xFF;
            cc = (cc + val2) &0xFF;
            A[i*N+j] = (DATA)val2;
         }
         A[(N-1)*N+j] = (DATA)cc;
    }
    // Calculate the SuperCheckSum
    superchecksum = 0;
    for (i = 0; i < N; i++) {
        superchecksum = 
           (superchecksum + (uint64_t)(A[(N-1)*N+i]) * (uint64_t)(B[i*N+N-1]))&0xFF;
    }  
    *scs = (superchecksum<<2)&0xFF; //use df_write here? 

    // Publish the Filled Matrices and the expected SuperCheckSum
    df_publish(A); df_publish(B); df_publish(C); df_publish(scs);
    df_destroy();
    
    df_tdecrease(xm);

}


extern uint64_t df_ts0[100],df_ts1[100]; // timestamp buffers
extern uint64_t df_tt;

int main(int argc, char ** argv) 
{ 
    if (argc > 1)
        BLOCKSZ = atoi(argv[1]);
    if (argc > 2)
	    N= atoi(argv[2]);
 printf("Calculating the MMX with blockSize = %d and MatrixSize = %d\n",BLOCKSZ, N); 
    df_tt = df_tstamp(df_ts0);
    
    uint64_t fp_fill = df_tschedulez(&fill_matrix,5,sizeof(fill_matrix_s));
    uint64_t fp_bmul = df_tschedulez(&owm_matrix_mul,2,sizeof(owm_matrix_mul_s)); 
    uint64_t fp_rep = df_tschedulez(&report,N / BLOCKSZ, sizeof(report_s));


    df_subscribe(XDST(fp_fill,fill_matrix,A), RA_OFF(0), SIZE(N,N),_OWM_MODE_W);
    df_subscribe(XDST(fp_fill,fill_matrix,B), RB_OFF(0), SIZE(N,N),_OWM_MODE_W);
    df_subscribe(XDST(fp_fill,fill_matrix,C), RC_OFF(0), SIZE(N,N),_OWM_MODE_W);
    df_subscribe(XDST(fp_fill, fill_matrix, scs), RA_OFF(N), sizeof(OWM_DATA), _OWM_MODE_W);
   
    // Schedule final check
    df_subscribe(XDST(fp_rep, report, scsp), RA_OFF(N), 36, _OWM_MODE_R);
    df_subscribe(XDST(fp_rep, report, C), RC_OFF(0), SIZE(N, N), _OWM_MODE_R);

    df_write(fp_fill,offsetof(fill_matrix_s,xm),fp_bmul);
    df_write(fp_bmul,offsetof(owm_matrix_mul_s,xr),fp_rep);
    df_tdecreaseN(fp_fill,4);
    df_destroy();
    
    return 0; 
} 
