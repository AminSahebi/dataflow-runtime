/*$Id: xdebug.h 19 2015-06-20 14:00:32Z giorgi $*/
#ifdef XDATA_SINGLE
    #define ZERO 0.0
    #define FMT "%4.0f,"
#endif
#ifdef XDATA_DOUBLE
    #define ZERO 0.0
    #define FMT "%4.0f,"
#endif
#ifdef XDATA_INT64
    #define ZERO 0
    #define FMT "%8ld,"
    #define AMT "%8p,"
#endif

#ifdef __DEBUG
#define print_matrix(out,X,Y)                   \
do{                                             \
  int i,j;                                      \
  for (i = 0; i < X; i++){                      \
    for (j = 0; j < Y; j++){                    \
      printf (FMT,out[i*N+j]);             \
    }                                           \
    printf ("\n");                              \
  }                                             \
  printf ("\n");                                \
 }while(0);

#define print_matrix_addr(out,X,Y)                   \
do{                                             \
  int i,j;                                      \
  for (i = 0; i < X; i++){                      \
    for (j = 0; j < Y; j++){                    \
      printf (AMT,&out[i*N+j]);             \
    }                                           \
    printf ("\n");                              \
  }                                             \
  printf ("\n");                                \
 }while(0);

#else


#define print_matrix(out,X,Y) /**/
#define print_matrix_addr(out,X,Y) /**/
#endif

