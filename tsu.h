/* DRT - A Dataflow Run Time - v0.1
 * Copyright (c) Roberto Giorgi 2013/09/19 - FREE SOFTWARE: SEE NOTICE BELOW */

#ifndef DRT_H
#define DRT_H
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <papi.h>

#define rdtscll(val) {                                           \
	unsigned int __a,__d;                                        \
	asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
	(val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}

// More efficient than __rdtsc() in some case, but maybe worse in others
/*uint64_t rdtscll(){
// long and uintptr_t are 32-bit on the x32 ABI (32-bit pointers in 64-bit mode), so #ifdef would be better if we care about this trick there.

unsigned long lo,hi;  // let the compiler know that zero-extension to 64 bits isn't required
__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
return ((uint64_t)hi << 32) + lo;
// + allows LEA or ADD instead of OR
}*/
uint64_t sch_count,write_count,dest_count,read_count,load_count;
uint64_t _schedule,_write,_dest,_read,_load;
uint64_t df_ts0[100],df_ts1[100];
uint64_t df_tt;
long long t7,t8,t9,t10,t11,t12;
/*long long*/clock_t begin_cycles,finish_cycles;
uint64_t totlaunch,totscans,totbusy,maxfmem,tottsched,tottdestr,tottw,maxdscheddestr,totdfx;
uint64_t totwaiting,totready,totdec,tottcache,tottsub,tottpub;
typedef uint64_t *afp;
typedef void (*fpt)(void);
uint64_t temp1,temp2,temp3,tmp4,tmp5,tmp6;
//Memory allocation guards
uint64_t fsize;
uint64_t fmem_sz;
//Memory pointers
afp fifo;
afp fmem_ptr;

struct timespec tv;

uint64_t start,end,end1;
long long sch_start,sch_stop,dest_start,dest_stop,write_start,write_stop;
uint64_t sum_sch,sum_write,sum_dest;
afp corefp;
#define MAXLOOPS	3
uint64_t launcherbusy, launcherlooping;
uint64_t mydtsudebug;  /* contains DRT_DEBUG environment variable valuie */

#define FGUARD		4 // guard space to overcome a limitation of scale generated code
#define FIRSTFREEOFFSET 5
#define TWAITING	(0x1L)
#define TREADY		(0x2L)
#define TBITS(A)	((A)& 0x7L)
#define TADDR(A)	((A)& 0xFFFFFFFFFFFFFFF8L)
#define TSETEMPTY(A)	(A = 0L)
#define TSETWAIT(A,V)	(A = ((V) & 0xFFFFFFFFFFFFFFF8L)+TWAITING)
#define TSETREADY(A)	((A) = (((A) & 0xFFFFFFFFFFFFFFF8L)+TREADY))

__always_inline static uint64_t mytschedule(uint8_t _cond,fpt _ip, uint64_t _sc, uint32_t sz)
{ 
	sch_count++;
//	rdtscll(sch_start);
	//clock_gettime(CLOCK_REALTIME, &tv);
	//t7= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);

	sch_start = PAPI_get_real_cyc();
	afp fp; 
	if (!_cond) return((uint64_t)NULL);

	int64_t fmem_off = (FIRSTFREEOFFSET+_sc+FGUARD); 

	fp = (afp) fmem_ptr;
	//fp=(afp)malloc((FIRSTFREEOFFSET+_sc+FGUARD)*sizeof(uint64_t));
	if(fp == NULL) { printf("- DRT: cannot allocate frame. \n"); fflush(stdout); exit(8); }

	uint64_t mguard = (uint64_t) fmem_ptr;
	if((tottsched*fmem_off) > fmem_sz) {
		printf(" - DRT: frame ptr %p - num elements %ld \n",fmem_ptr, fmem_sz);
		printf(" - DRT: cannot allocate frame - memory full. \n"); fflush(stdout); exit(8); }

	fmem_ptr += fmem_off;

	fp[0]=(uint64_t)(_ip);fp[1]=_sc;
	fp[2]=(uint64_t)(&fifo[end]);
	fp[4]=(uint64_t)(sz);
	/* mytschedule implementation finishes here. Below there is an optimization to recover empty slots in FIFO.*/
	fp[3]=_sc;
	maxfmem+=(FIRSTFREEOFFSET+_sc+FGUARD)*sizeof(uint64_t);
	++tottsched;
	if (maxdscheddestr<tottsched-tottdestr) maxdscheddestr=tottsched-tottdestr;
	TSETWAIT(fifo[end],(uint64_t)fp); ++totwaiting;
	if (mydtsudebug > 3) {
		printf("    TS: fi=%ld  ip=%p  fp=%p  sc=%ld/%ld  st=%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],TADDR(fifo[end]),fp[1],fp[3],TBITS(fifo[end])); fflush(stdout);
	} else  {
		if (mydtsudebug == 2 || mydtsudebug == 3) {
			printf("    TS: fi=%ld  ip=%p  fp=%p  sc=%ld/%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],TADDR(fifo[end]),fp[1],fp[3]); fflush(stdout);
		}
	}
	end = (end+1) % fsize;
	if (TBITS(fifo[end])) {
		++totscans;
		/* scan to see if there are empty slots ahead */
		end1=end;
		totbusy=0;
		do {
			end1 = (end1+1) % fsize;
			++totbusy;
			if (end1==end) { printf("- FIFO: queue is totally full. (totlaunch=%ld,totscans=%ld,totbusy=%ld)\n",totlaunch,totscans,totbusy); fflush(stdout); exit(3); }
		} while (TBITS(fifo[end1]));
		end = end1;
		if (mydtsudebug) {
			printf("- FIFO busy slot (totlaunch=%ld,totscans=%ld,totbusy=%ld e=%ld)\n",totlaunch,totscans,totbusy,end); fflush(stdout);
		}
	}
	//rdtscll(sch_stop);
	//  if (end==start) { printf("- FIFO ended (totlaunch=%ld) end=%ld fifo[end]=0x%x, totbusy=%ld\n",totlaunch,end,fifo[end],totbusy); fflush(stdout); exit(1); }
	sch_stop = PAPI_get_real_cyc();
	_schedule = (sch_stop - sch_start);
	sum_sch+=_schedule;
	//clock_gettime(CLOCK_REALTIME, &tv);
	//t8= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	//sum_sch+=(t8-t7);
	return((uint64_t)fp);
}

__always_inline static uint64_t mytwrite(afp fp1, uint64_t off1, uint64_t val)
{ 
	write_count++;
	//rdtscll(write_start);
	write_start = PAPI_get_real_cyc();
	/* for df_writeN: */
	//clock_gettime(CLOCK_REALTIME, &tv);
	//t9= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	afp fp; uint64_t off;
	//  printf("     xx: fp1=%p off1=%p val=%p\n",fp1,off1,val); fflush(stdout);
	if (mydtsudebug > 4) {
		printf("     Tw: fp1=%p off1=%p val=0x%lx\n",fp1,off1,val); fflush(stdout);
	}
	if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
		off = ((uint64_t)fp1 & 0xFFUL); 
		fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
	}
	else { 
		fp = fp1; off=off1;
	}
	//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);
	fp[off+FIRSTFREEOFFSET]=val; (fp[1])--; if (fp[1] == 0) { TSETREADY(*((afp)(fp[2]))); ++totready; --totwaiting;} ++tottw; 
	//  if (mydtsudebug > 4) {
	if (mydtsudebug > 2) {
		printf("     TW: fi=%ld  ip=%p  fp=%p/%ld val=0x%lx  sc=%ld/%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,off,val,fp[1],fp[3]); fflush(stdout);
	}
	//rdtscll(write_stop);
	write_stop = PAPI_get_real_cyc();
	_write = (write_stop-write_start);
	sum_write+=_write;
	//clock_gettime(CLOCK_REALTIME, &tv);
	//t10= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
	//sum_write+=(t10-t9);
}

__always_inline void mydropframe()
{
	dest_count++;
	//rdtscll(dest_start);
//	rdtscll(tmp4);
	dest_start = PAPI_get_real_cyc();
	++tottdestr;
	if(!launcherbusy) { launcherbusy=1;
		/* Launcher */
		if(start==end) { printf("- FIFO: attempt to extract from empty queue. \n"); fflush(stdout); exit(2); }
		else {
			uint64_t found,start1,start0,start2;
			uint64_t bits, bits0;
			uint64_t val;
			afp fpnew;
			uint64_t ipnew;
			if (mydtsudebug) {
				printf("- DRT: Starting Dataflow launcher.\n"); fflush(stdout);
			}
			totlaunch = 0;
			launcherlooping = 0;
			do {
				start1 = start; found=0;
				do {
					val = fifo[start1]; fpnew = (afp)(TADDR(val)); bits = TBITS(val);
					ipnew = 0;
					start0 = start1; bits0 = bits;
					start1 = (start1+1) % fsize;
					bits = TBITS(fifo[start1]);
					if (bits0 == TREADY) { 
						TSETEMPTY(fifo[start0]); /* clean fifo slot */
						--totready; ++found;
						corefp = fpnew; ipnew = fpnew[0];
						++totlaunch;
						if (mydtsudebug > 3) {
							printf("  TE: fi=%ld st=%ld  ipnew=%p  fpnew=%p (s=%ld,s1=%ld,e=%ld,ll=%ld  launched=%ld  R=%ld,W=%ld)\n",start0,bits0,ipnew,fpnew,start,start1,end,launcherlooping,totlaunch,totready,totwaiting); fflush(stdout);
						} else {
							if (mydtsudebug == 2 || mydtsudebug == 3) {
								printf("  TE: fi=%ld ipnew=%p  fpnew=%p\n",start0,ipnew,fpnew); fflush(stdout);
							}
						}
						((fpt)(ipnew))();
					}
					//        } while ((bits != 0 || start1!=end) && launcherlooping < MAXLOOPS);
					//          if (bits==TWAITING) printf("  pp: fi=%ld st=%ld  ipnew=%p  fpnew=%p (s=%ld,s1=%ld,e=%ld,ll=%ld  launched=%ld  R=%ld,W=%ld)\n",start0,bits,ipnew,fpnew,start,start1,end,launcherlooping,totlaunch,totready,totwaiting); fflush(stdout);
			} while ((bits == TREADY || start1!=end) && launcherlooping < MAXLOOPS);
				if (! found) ++launcherlooping; 
				if (launcherlooping == MAXLOOPS) { printf("- DRT runtime: cannot find READY threads! looping reached %ld\n", MAXLOOPS); exit(7); }
				//        while (bits == 0 && start!=end) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
				//        while (bits == 0 && start!=start2) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
				//        while (bits != TREADY && start!=start2) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
				//        start2=(start+1) % fsize;
				//        bits=TBITS(fifo[start]);
				//        while (bits != TREADY && start!=start2) { start = (start-1) % fsize; bits=TBITS(fifo[start]); }
				start2=(start-1) % fsize;
				bits=TBITS(fifo[start]);
				//rdtscll(tmp5);
				//tmp6=(tmp5-tmp4);
				//        if (bits != TREADY)
				rdtscll(temp1);
				while (bits != TREADY && start!=start2) { start = (start+1) % fsize; bits=TBITS(fifo[start]); }
				rdtscll(temp2);
				temp3+=(temp2-temp1);
//sum_dest+=(t12-t11);
				//        printf("  qq: fi=%ld st=%ld  ipnew=%p  fpnew=%p (s=%ld,s1=%ld,e=%ld,ll=%ld  launched=%ld  R=%ld,W=%ld)\n",start0,bits,ipnew,fpnew,start,start1,end,launcherlooping,totlaunch,totready,totwaiting); fflush(stdout);
				//        start2=(start+1) % fsize;
				//        bits=TBITS(fifo[start]);
				//        while (bits != 0 && start!=start2) { start = (start-1) % fsize; bits=TBITS(fifo[start]); }
				//      } while (bits != 0);
				//      } while (bits == TREADY || start==start2);
} while (bits == TREADY);
}
if (mydtsudebug) {
	printf("- DRT: Ending DF launcher (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld R=%ld,W=%ld).\n",maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,totready,totwaiting); fflush(stdout);
	fflush(stdout);
}
launcherbusy=0;
free(fifo);
} else {
	maxfmem -= (FIRSTFREEOFFSET+corefp[3]+FGUARD)*sizeof(uint64_t);
	if (mydtsudebug > 3) {
		printf("  TD: fi=%ld  ip=%p  fp=%p  sc=%ld (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld R=%ld,W=%ld)\n",(corefp[2]-(uint64_t)fifo)/sizeof(uint64_t),corefp[0],corefp,corefp[3],maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,totready,totwaiting); fflush(stdout);

		//      printf("- DRT: mydfdestroy (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld,fp=%p,sc=%ld).\n",maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,corefp,corefp[3]); fflush(stdout);
	} else { 
		if (mydtsudebug == 2 || mydtsudebug == 3) {
			printf("  TD: fi=%ld  ip=%p  fp=%p  sc=%ld \n",(corefp[2]-(uint64_t)fifo)/sizeof(uint64_t),corefp[0],corefp,corefp[3]); fflush(stdout);

			//      printf("- DRT: mydfdestroy (mxfmem=%ld MB,ts=%ld,td=%ld,tw=%ld,mxsd=%ld,fp=%p,sc=%ld).\n",maxfmem/1024/1024,tottsched,tottdestr,tottw,maxdscheddestr,corefp,corefp[3]); fflush(stdout);
		}
	}
	//free(corefp);
}
//rdtscll(dest_stop);
dest_stop = PAPI_get_real_cyc();
_dest=dest_stop-dest_start;
sum_dest+=_dest;
}
__always_inline static uint64_t mydfstart(uint64_t df_ts0[])
{
	uint64_t phypz, psize, totcpu, totmem, fszmem, i, t, frame_szmem;
	char *s;
	phypz = sysconf(_SC_PHYS_PAGES);
	psize = sysconf(_SC_PAGE_SIZE);
	totcpu = sysconf(_SC_NPROCESSORS_CONF);
	totmem = phypz * psize /1024 /1024 /1024;
	fsize = ((phypz * psize) / sizeof(uint64_t)) / 10; /* rule of thumb: allocate a 10% of total phy memory */
	s = getenv("DRT_DEBUG"); mydtsudebug = (s) ? atoi(s) : 0;
	s = getenv("DRT_FSIZE"); t = (s) ? atoi(s) : fsize; if (t) fsize = t;
	s = getenv("DRT_MSIZE"); t = (s) ? atoi(s) : fsize; if (t) fmem_sz = t;
	fszmem = fsize /1024 /1024 *sizeof(uint64_t);
	frame_szmem = fmem_sz /1024 /1024 *sizeof(uint64_t);

	if (mydtsudebug) {
		printf("- DRT: you have %ld GB of physical memory and %d processors on this host.\n", totmem, totcpu);
		printf("- DRT: allocating a FRAME-MEM queue of %ld MB (%ld entries).\n", fszmem, fsize);
		printf("- DRT: allocating a FRAME-MEM frame of %ld MB.\n", frame_szmem);
		fflush(stdout);
	}
	fmem_ptr = (afp) calloc(fmem_sz, sizeof(uint64_t));
	fifo = (afp)malloc(fsize*sizeof(uint64_t));
	if(fmem_ptr == NULL) { printf("- FRAME-MEM: cannot allocate frame memory. \n"); fflush(stdout); exit(4); }
	if(fifo == NULL) { printf("- FRAME-MEM: cannot allocate queue. \n"); fflush(stdout); exit(4); }

	for (i =0; i<fsize; ++i) TSETEMPTY(fifo[i]);
	if (mydtsudebug) {
		printf("- DRT: FRAME-MEM allocation+initialization done.\n");
		fflush(stdout);
	}
}

__always_inline static uint64_t mydfexit(void) {
	++totdfx;
	if (totdfx>1) {
		printf("- DRT: mydfexit called more the ONCE !\n");
	}
	if (start!=end && !launcherbusy) {
		printf("- DRT: mydfexit there are pending threads (%ld) start=%ld end=%ld!\n", end-start,start,end);
	}
}
clock_t dfread_start,dfread_stop;
uint64_t sum_df;
__always_inline static uint64_t mydfread(afp fp, uint64_t off) {
	read_count++;
//	rdtscll(dfread_start);
	dfread_start=PAPI_get_real_cyc();
	uint64_t val = fp[(off)+FIRSTFREEOFFSET];
	if (mydtsudebug > 5) {
		printf("     TR: %x/%ld val=0x%lx\n",fp,off,val);fflush(stdout);
	}
	//rdtscll(dfread_stop);
	dfread_stop=PAPI_get_real_cyc();
	_read = dfread_stop-dfread_start;
	sum_df+=_read;
	return val;
}

__always_inline static void mymwrite(afp p, uint64_t val) {
	if (mydtsudebug > 5) {
		printf("     MW: %p  val=0x%lx\n",p,val);fflush(stdout);
	}
	*p=val;
}

__always_inline static uint64_t mymread(afp p) {
	if (mydtsudebug > 5) {
		printf("     xx: %p  \n",p);fflush(stdout);
	}
	uint64_t val = ((uint64_t)(*p));
	if (mydtsudebug > 5) {
		printf("     MR: %p  val=0x%lx\n",p,val);fflush(stdout);
	}
	return val;
}

__always_inline static void mytdecreaseN(afp fp1, uint64_t n)
{ 
	afp fp; uint64_t off;
	if (mydtsudebug > 4) {
		printf("     T.: fp1=%p  n=%ld\n",fp1,n); fflush(stdout);
	}
	if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
		off = ((uint64_t)fp1 & 0xFFUL);
		fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
	}
	else {
		fp = fp1; off=0;
	}
	//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);

	fp[1] -= n; if (fp[1] == 0) { TSETREADY(*((afp)(fp[2]))); ++totready; --totwaiting; } ++totdec; 
	if (mydtsudebug > 4) {
		printf("     T0: fi=%ld  ip=%p  fp=%p  sc=%ld/%ld\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,fp[1],fp[3]); fflush(stdout);
	}
}

__always_inline static void *mytcache(afp fp1)
{ 
	afp rfp, fp; uint64_t off;
	if (mydtsudebug > 4) {
		printf("     Tc: fp1=%p\n",fp1); fflush(stdout);
	}
	if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
		off = ((uint64_t)fp1 & 0xFFUL);
		fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
	}
	else {
		fp = fp1; off=0;
	}
	//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);

	rfp = fp + FIRSTFREEOFFSET; ++tottcache; 
	if (mydtsudebug > 4) {
		printf("     TC: fi=%ld  ip=%p  fp=%p  sc=%ld/%ld rfp=%p\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,fp[1],fp[3], rfp); fflush(stdout);
	}
	return rfp;
}

__always_inline static void *mytsubscribe(afp fp1, uint32_t roff, uint32_t sz, uint8_t rw)
{ //TODO
	afp rfp, fp; uint64_t off;
	if (mydtsudebug > 4) {
		printf("     Tu: fp1=%p\n",fp1); fflush(stdout);
	}
	if (((uint64_t)fp1 & 0x80000000000000UL)>>48 == 0x80) { //small hack to support df_writeN
		off = ((uint64_t)fp1 & 0xFFUL);
		fp = (afp)(((uint64_t)fp1 & 0xFF7FFFFFFFFFFFFFUL) >>8);
	}
	else {
		fp = fp1; off=0;
	}
	//  printf("     yy: fp= %p off= %p val=%p\n",fp,off,val); fflush(stdout);

	uint64_t val = (uint64_t) malloc(sz);
	fp[off+FIRSTFREEOFFSET]=val; ++tottsub;

	if (mydtsudebug > 4) {
		printf("     TU: fi=%ld  ip=%p  fp=%p  p=%p\n",(fp[2]-(uint64_t)fifo)/sizeof(uint64_t),fp[0],fp,val); fflush(stdout);
	}
}

__always_inline static void mytpublish(void* regptr)
{ //TODO
}

/*
   struct timespec tv;
   long long start_time, time_in_nano;

   __always_inline static void xzonestart(int zone)
   {
   printf("\tXZONESTART %d DETECTED\n",zone);
   fflush(stdout);
//
//	   gettimeofday(&tv,NULL);
//	   time_in_mill= (tv.tv_sec) * 1000000 + (tv.tv_usec);
//	   start_time = time_in_mill;

clock_gettime(CLOCK_REALTIME, &tv);
time_in_nano= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
start_time = time_in_nano;

}

__always_inline static void xzonestop(int zone)
{


//   gettimeofday(&tv,NULL);
//  time_in_mill = (tv.tv_sec) * 1000000 + (tv.tv_usec);

clock_gettime(CLOCK_REALTIME, &tv);
time_in_nano= (tv.tv_sec) * 1000000000 + (tv.tv_nsec);
printf("\tXZONESTOP %d DETECTED \n",zone);
printf("\t[INFO]: time elapsed = %lld ns\n",(time_in_nano - start_time));

fflush(stdout);

}*/
clock_t/*long long*/ sum_ld,ld_start,ld_stop;

#define df_tstamp(df_ts0)	mydfstart(df_ts0);
#define df_ldframe(n)		{load_count++;rdtscll(ld_start);}afp fp = corefp;{rdtscll(ld_stop);_load=(ld_stop-ld_start);sum_ld+=_load;}

#define df_destroy()		mydropframe();
//#define df_frame(_off)	(fp[(_off)+FIRSTFREEOFFSET])
#define df_frame(_off)		mydfread(fp,_off)
#define df_read(_off)		(df_frame(_off))
#define df_write(_tid,_off,_val) \
	mytwrite((afp)(_tid), _off, _val)
#define df_tschedule_cond(_ip,_sc,_cond) \
	(mytschedule(_cond,(fpt)(_ip),_sc,(_sc)*sizeof(uint64_t)))
#define df_tscheduleN(_ip,_sc)	(mytschedule(1,(fpt)(_ip),(_sc),(_sc)*sizeof(uint64_t)))
#define df_tschedule(_ip,_sc)	(mytschedule(1,(fpt)(_ip),(_sc),(_sc)*sizeof(uint64_t)))
#define df_talloc(_type, _sz)	(uint64_t)malloc(_sz)
///* df_writeN assumes we do not care about the 3 most #define df_tread_al(_p)		((uint64_t)(*((afp)(_p))))
#define df_tread_al(_p)		mymread((afp)(_p))
//#define df_twrite_al(_p,_val)	*((afp)(_p))=(_val)
#define df_twrite_al(_p,_val)	mymwrite((afp)(_p),_val)
#define df_tfree(_p)		free((void *)(_p))
#define df_exit()		mydfexit()
//#define _TLOC(_tp,_l)		((_tp)+((uint64_t)(_l)<<3))
//#define _TLOC(_tp,_l) ((_tp)+((unsigned long long)(_l)<<3))
#define _TLOC(_tp,_l)		((((uint64_t)_tp)<<8)+(_l)) | 0x0080000000000000UL
#define myL8b(_VL)		(((uint64_t)_VL) & 0xFFUL)
#define myH56b(_VH)		(((uint64_t)_VH) >> 8)
//#define df_writeN(_tid,_val)	mytwrite((afp)(myH56b(_tid)), myL8b(_tid), _val)
#define df_writeN(_tid,_val)	mytwrite((afp)(_tid), 0, _val)
/* df_writeN assumes we do not care about the 3 most sigificant bits of _tid */
#define df_tdecrease(_tid)	mytdecreaseN((afp)(_tid), 1)
#define df_tdecreaseN(_tid,_n)	mytdecreaseN((afp)(_tid), (uint64_t)(_n))
#define df_tschedule64(_ip,_sc) \
	(mytschedule(1,(fpt)(_ip),(_sc),(_sc)*sizeof(uint64_t)))
#define df_tschedulez(_ip,_sc,_sz) \
	(mytschedule(1,(_ip),(_sc),(_sz)))
#define df_tload()		(corefp+FIRSTFREEOFFSET)
#define df_tcache(_tid)		(mytcache((afp)(_tid)))
#define df_tid_t		uint64_t
#define df_constrain(_tid,_xc)	
#define df_printstats(_ts0,_ts1,_t1)	
#define _TLOC_OFF(_tloc)	(myL8b(_tloc))
#define df_subscribe(_tloc,_roff,_sz,_rw) \
	mytsubscribe((afp)(_tloc),_roff,_sz,_rw)
#define df_publish(_regptr)	mytpublish(_regptr)
#define _OWM_DESC(sz,rw)	(((sz)<<8)|((rw)&0xff))
#define _OWM_R(d)		((d)&0x01)
#define _OWM_W(d)		((d)&0x02)
#define _OWM_SZ(d)		((d)>>8)
#define _OWM_MODE_R		(0x01)
#define _OWM_MODE_W		(0x02)
#define _OWM_MODE_RW		(_OWM_MODE_R|_OWM_MODE_W)
typedef void (*df_thread_t)(void);




/*
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#endif // DRT_H

