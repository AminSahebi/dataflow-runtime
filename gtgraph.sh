#!/bin/bash
# This script created by Roberto Giorgi (15-Sep-1995) is version:
VERSION="200417"
###############################################################################
#
# This script extract data from a data file, produce a graphic using gnuplot
# and assmble a latex document to show the graphs
#
# For the 'Graphic Table' EBNF syntax see ghtparser.sh
#
###############################################################################

# Exit error codes:
# -1  invalid command line
# -2  some required program is missing
# -3  data file not found
# -4  incorrect data file syntax
#
# Tool dependencies
MYSHLIB="myshlib.sh"
GTHPARSERSH="gtparser.sh"

mountdport="20048"
nfsport="2049"
portmapperport="111"
# md5sum in ubuntu/fedora is in coreutils
# netcat is needed in the checkconnection function
SUPPORTED_DISTROS="ubuntu fedora suse debian centos"
FEDORA_DEPENDENCIES="redhat-lsb texlive gnuplot netpbm epstool texlive-enumitem"
UBUNTU_DEPENDENCIES="texlive gnuplot netpbm epstool texlive-latex-extra"
SUSE_DEPENDENCIES="texlive gnuplot netpbm epstool"

#
lang="english"
# lang="italian" PSEXT=".eps"
PSEXT=".ps"
PSTMPEXT="-tmp$PSEXT"
HGEXT=".hgl" 
SEPFIG="postscript eps 24 color" #SEPFIG="hpgl"
nan="NaN"
PAPERFMT="a4" #PAPERFMT="letter"
GPBASEFN="/tmp/gtg-gnuplot" #be careful: don't set to empty !!
DEFHISTOSUFF="-hist"
PDFVIEWER="evince"
DEEPDEBUG="0"
#################################
# OLD:
#NEW="1"
#oldt2p="tf_t2p"
#newt2p="tf2_t2p.sh"
#olds2t="csim_s2t"
#news2t="tf2_s2t.sh"
#oldinfodir="/r1/usr/local1/bin"
#if [ "$NEW" = "1" ]; then
#   myt2p="$newt2p"
#   mys2t="$news2t"
#else
#   myt2p="$oldt2p"
#   mys2t="$olds2t"
#fi
PCUSER="$(id -u -n)"
GTG_CMDL="$*"
prog=`echo $0 | awk -F/ '{print $NF}'`

md5sig=`md5sum $0|awk '{print $1}'`
MYSIG="${md5sig:0:2}${md5sig:(-2)}" # reduced-hash: first 2 char and last two char
VERSION1="v${VERSION}-${MYSIG}"
GTG_THISSCRIPT=`basename ${BASH_SOURCE[0]}`
GTG_THISTOOL="${GTG_THISSCRIPT%%.*}"; GTG_THISTOOLCAP="${GTG_THISTOOL^^}"
GTG_T00000=`date +%s%N | cut -b1-13`
#-----------------------------------------------------------------------------
#####################################################
function gtg_usage () {
   echo "Usage: $0 [<options>] <data_file>.txt [<list_of_table_ids>]"
   echo "   where:"
   echo "   <data_file>	is a GTable-format data file"
   echo "   <list_of_table_ids>	is a comma-separated list of ids"
   echo ""
   echo "   and <options> can be:"
   echo "   -v          verbose mode"
   echo "   -d          debug mode (more times: more details)"
   echo "   --deepdebug debug also sourced functions"
   echo "   -q          quiet mode"
   echo "   -p          send output to the default printer"
   echo "   -s          send ouput to the screen"
   echo "   -S          send ouput to the PDF viewer"
   echo "   -g          don't generate gif files"
   echo "   --nolatex   don't generate a latex document (default)"
   echo "   --latex     generate a latex document"
   echo "   --version   print script version"
   echo "   --histogram plot using histogram instead of lines"
   echo "   --keepgp    keep the gnuplot file after execution"
   echo ""
   exit 1
}

#####################################################
function gtg_commandline () {
#echo "cmdl===$*"
   local noinc

   noinc=""
   if [ "$1" = "local" ]; then noinc="1"; fi
   if [ "$1" = "external" ]; then noinc="0"; fi
   if [ "$noinc" = "" ]; then cecho "ERROR: you misused the function 'commandilne' !" "$red"; exit 1; fi
   shift

   debug="0"
   verbose="0"
   quiet="0"
   OPT_Q=""
   version="0"
   tolpr="0"
   df=""
   nogif="0"
   SPEZZA="0"
   NOLATEX="0"
   TOSCREEN="0"
   TOPDFVIEWER="0"
   HISTOGRAM="0"
   KEEPGP="0"
   tablist=""
   while [ $# -gt 0 ]; do case "$1" in
      -v) [ "$noinc" = "0" ] && verbose=`expr $verbose + 1`; strverb="$strverb -v"; shift;;
      -d) [ "$noinc" = "0" ] && debug=`expr $debug + 1`; strdebu="$strdebu -d"; shift;;
      --deepdebug) DEEPDEBUG="1"; shift;;
      -q) quiet="1"; OPT_Q=" -q"; shift;;
      -p) tolpr="1"; shift;;
      -s) TOSCREEN="1"; shift;;
      -S) TOPDFVIEWER="1"; shift;;
      -spezza) SPEZZA="1"; shift;;
      --nolatex) NOLATEX="1"; shift;;
      --latex) NOLATEX="0"; shift;;
      -g) nogif="1"; shift;;
      --version) version="1"; shift;;
      --histogram) HISTOGRAM="1"; HISTOSUFFIX="$DEFHISTOSUFF"; shift;;
      --keepgp) KEEPGP="1"; shift;;
#      -e) noexplo="1";shift;;
      -*) gtg_usage;;
      *)  if [ "$df" = "" ]; then df="$1"; shift; continue; fi
          if [ "$tablist" = "" ]; then tablist="$1"; shift; continue; fi
   esac; done

   if [ "$version" = "1" ]; then
      echo "$0: version $VERSION1"; exit 0;
   fi

   # Check syntax
   if [ "$df" = "" ]; then  gtg_usage; fi
   debug1 "df=$df"
   debug1 "tablist=$tablist"
}
#-----------------------------------------------------------------------------

#####################################################
function gtg_printglobals ()
{
   echo -n "" # empty for now
}

#####################################################
function gtg_genlatex() {
   local doc="$PLOTDOCUMENT"
   local out="$PLOTDOCUMENT$HISTOSUFFIX"

   #------------------------------------------------------------------------------------
   # Apply "SPEZZA"
   if [ "$SPEZZA" = "1" ]; then
      spezzawiwu $doc.txt -nolatex
      rm -f ${doc}f3$PSTMPEXT ${doc}f3$PSEXT
      mv w1f1.ps ${doc}f3$PSEXT
      mv w1f1.tmp.ps ${doc}f3$PSTMPEXT
      mv w2f1.ps ${doc}f5$PSEXT
      mv w2f1.tmp.ps ${doc}f5$PSTMPEXT
   #   rm -f w1.ps w2.ps w1.txt w2.txt w1.tmp.ps w2.tmp.ps
   fi

   #------------------------------------------------------------------------------------
   # Calculate space for comment
   if [ "$PAPERFMT" = "a4" ]; then
#      sheeth="28.2"; pagew=19
      sheeth="28.2"; pagew=18.9
   else # letter is default
      sheeth="27.0"; pagew=19.6
   fi
   hncol="3"     # 3 columns for comments
   hntxh="0.3"   # text height for comments (cm)
   h1txh="0.4"   # text height for header1
   h1boxrows="3" # rows for header1
   h1margin="0.5"
   hnmargin="0.8"

   pagehntxrows=`echo "$sheeth / $hntxh"|bc` # headerN text height in rows
   pageh1txrows=`echo "$sheeth / $h1txh"|bc` # header1 text height in rows
   hntxrows=`echo "$headeN"|awk 'END{printf("%d", NR)}'`
   hnboxrows=`echo "$hntxrows / $hncol + 1"|bc`
   spaceforcomments=`echo "scale=2; $hnboxrows / $pagehntxrows * $sheeth + $hnmargin"|bc`
   spaceforheader1=`echo "scale=2; $h1boxrows / $pageh1txrows * $sheeth + $h1margin"|bc`
   debug1 "pageh1txrows=$pageh1txrows"
   debug1 "hntxrows=$hntxrows"
   debug1 "hnboxrows=$hnboxrows"
   debug1 "spaceforcomments=$spaceforcomments"
   debug1 "pageh1txrows=$pageh1txrows"
   debug1 "spaceforheader1=$spaceforheader1"

   #pageh=`echo "$headeN"|awk -v sheeth=$sheeth '{printf("%d", sheeth - length($0) / 80.0 * 0.85)}'`
   #spaceforcomments="4.5"
   #spaceforcomments=`echo "$headeN"|awk 'END{printf("%d", NR / 114.0 * 0.3)}'`
   pageh=`echo ""| awk -v sheeth=$sheeth '{printf("%f", sheeth - sp1 - spc)}' spc=$spaceforcomments sp1=$spaceforheader1`

   #------------------------------------------------------------------------------------
   # Print the comments on the screen if we are in verbose mode
   if [ "$PLOTDOCCOMM" != "" ]; then
      verbose1 "* COMMENTS:"
      verbose1 "----------------------------------------"
      [ "$verbose" -gt "0" ] && { printf '%s\n' "$PLOTDOCCOMM" | while IFS= read -r line; do echo "* $line"; done; }
      verbose1 "----------------------------------------"
      verbose1 "* Page height for $PLOTNGRAPHS graphs is ${pageh}cm"
      verbose1 "* Space for comments=${spaceforcomments}cm (${hntxrows}/${hncol}=$hnboxrows rows)"
      verbose1 "* Space for header1=${spaceforheader1}cm ($h1boxrows rows)"
   fi

   #------------------------------------------------------------------------------------
   col=2; sep="-0.6"
   if [ $PLOTNGRAPHS -eq 1 ]; then col=1; sep="0"; fi
   if [ $PLOTNGRAPHS -gt 16 ]; then col=3; sep="-1.2"; fi
   row=`echo ""|awk '{r1 = int(tc / col); printf("%g", r1 + (r1 * col < tc))}' tc=$PLOTNGRAPHS col=$col`
   ww=`echo $pagew | awk '{printf("%g", pw / c)}' pw=$pagew c=$col`
   #hh=`echo $PLOTNGRAPHS | awk -v ph=$pageh '{printf("%g", ph / (int($1 / 2 + 0.5)))}'`
   hh=`echo ""|awk '{\
      hh = (ww / 4) * 3; \
      if (row * hh > ph) hh = ph / row; \
      printf("%g", hh)} \
      ' tc=$PLOTNGRAPHS ph=$pageh ww=$ww row=$row`

   #------------------------------------------------------------------------------------
   comment "* Generating LaTeX document ($col columns, $row rows, ww=$ww, hh=$hh)"

   #------------------------------------------------------------------------------------
   # Prepare header FIRST LINE
   local pdocsum=`echo "$PLOTDOCSUMM"|sed 's/_/\\\\_/g'`
   local pdocout=`echo "$doc.txt"|sed 's/_/\\\\_/g'`
#   local pdocsum=`echo "$PLOTDOCSUMM"`
   debug2 "pdocsum=$pdocsum"
   debug2 "pdocout=$pdocout"
   local heade1 headeN=""
   heade1="`date` -- $pdocout \\newline $pdocsum"
   debug2 "heade1='$heade1'"

   #------------------------------------------------------------------------------------
   # Prepare header DETAILED COMMENTS
   local pdoccom=`(echo "$doc.txt"; echo "$PLOTDOCCOMM")|sed 's/_/\\\\_/g'`
   if [ "$pdoccom" != "" ]; then 
      local tmp
#      tmp=`echo "$pdoccom"|awk '!/display/{printf("\\\\item %s\n", $0);}'`
      tmp=`echo "$pdoccom"|awk '{printf("\\\\item %s\n", $0);}'`
#   headeN=`echo "{\tiny\begin{multicols}{3}\begin{itemize}\setlength\itemsep{0em}"`$tmp`echo "\end{itemize}\end{multicols}}"`
      headeN=`echo "{\scriptsize\begin{multicols}{3}\
\setlist[2]{noitemsep}\
\setenumerate{noitemsep}\
\begin{itemize}[leftmargin=*,noitemsep]\
"`$tmp`echo "\end{itemize}\end{multicols}}"`
   fi
   debug2 "headeN='$headeN'"

   #------------------------------------------------------------------------------------
   #
   sidemargin="-1.9" #cm
   sheeth1="$(echo "$sheeth+0.4"|bc)"
   debug1 "sheeth1=$sheeth1"

   rm -f $doc.tex
   cat <<EOF >$doc.tex
\documentclass[10pt,titlepage]{article}
\usepackage{times}
%\usepackage{psfig} % NOTE: 20170202: psfig.sty package has been removed from tetex-base
\usepackage{graphicx} % Use graphicx instead of psfig
\usepackage{multicol}
\usepackage{enumitem}
\usepackage[T1]{fontenc}
%\usepackage{epstopdf}
%\epstopdfsetup{outdir=./}
%\epstopdfsetup{update}
%\DeclareGraphicsExtensions{.ps}
%\epstopdfDeclareGraphicsRule{.ps}{pdf}{.pdf}{ps2pdf -dEPSCrop -dNOSAFER #1 \OutputFile}
%\usepackage{epstopdf}
%\usepackage{auto-pst-pdf}
%\documentstyle[psfig,titlepage,11pt]{article}
%\setcounter{page}{-1}

%\pssilent % RG170202: commented out -- it is neded or not ?

% Margins
\sloppy
\setlength{\textwidth}{${pagew}cm}
%\setlength{\textheight}{11.7in}
\setlength{\textheight}{${sheeth1}cm}
\setlength{\voffset}{-0.8in}
\setlength{\hoffset}{0.22cm}
%\setlength{\topmargin}{-1.4in}
\setlength{\topmargin}{-1.1in}
%\setlength{\oddsidemargin}{-0.5in}
%\setlength{\evensidemargin}{-0.5in}
\setlength{\oddsidemargin}{${sidemargin}cm}
\setlength{\evensidemargin}{${sidemargin}cm}

% see above
\newlength{\fullboxwidth}
\setlength{\fullboxwidth}{\textwidth}
\addtolength{\fullboxwidth}{-0.1in}

%\def\ps@rgheadings{%
%    \let\@oddfoot\@empty\let\@evenfoot\@empty
%    \def\@evenhead{\thepage\hfil\slshape\leftmark}%
%    \def\@oddhead{{\slshape\rightmark}}%
%    \let\@mkboth\@gobbletwo
%    \let\sectionmark\@gobble
%    \let\subsectionmark\@gobble
%    }                                             
\begin{document}
\pagestyle{empty}
%\currentspace % defined in header.tex         
{\noindent\textit{$heade1}}

{\noindent $headeN}
\vspace{0.1cm}
EOF

   #------------------------------------------------------------------------------------
   # setup 'tabular' definition
   echo -n "\begin{tabular}{" >>$doc.tex
   c=1
   while [ $c -le $col ]; do
      echo -n "p{${ww}cm}" >>$doc.tex
      c=`expr $c + 1`
   done
   echo "}" >>$doc.tex

   #------------------------------------------------------------------------------------
   h=1
   read -a TABLEV <<< "$TABLELIST"
   while [ $h -le $PLOTNGRAPHS ]; do
      local i=`echo $TABLELIST | awk -v h=$h '{print $h}'`
      local table=`expr $i - 1`
#      local i=`expr $h - 1`
#      local table="${TABLEV[$i]}"
      local fig="${PLOTPARAM11[$table]}$HISTOSUFFIX"
      debug2 "  - $fig"
      cat <<EOF >>$doc.tex
%\hspace*{${sep}cm}  \raise ${hh}cm
\hspace*{${sep}cm}  \rule{0pt}{${hh}cm}
%\begin{minipage}[t]{0.4\linewidth}
%\begin{figure}
%\psfig{figure=$outseptmpfig,height=${hh}cm,width=${ww}cm,angle=0,rheight=0cm}
%\includegraphics[height=${hh}cm,width=${ww}cm]{$outseptmpfig}
%\includegraphics[keepaspectratio,width=${ww}cm]{${fig}}
\includegraphics[height=${hh}cm,width=${ww}cm]{${fig}}
%\end{figure}
%\end{minipage}
EOF
      if [ $col -ge 2 ]; then echo "&">>$doc.tex; fi
      h=`expr $h + 1`

      #------------------------------------------------------------------------------------
      c=2 
      while [ $c -le $col ]; do
         if [ $h -le $PLOTNGRAPHS ]; then
            local i=`echo $TABLELIST | awk -v h=$h '{print $h}'`
            local table=`expr $i - 1`
#            local i=`expr $h - 1`
#            local table="${TABLEV[$i]}"
            local fig="${PLOTPARAM11[$table]}$HISTOSUFFIX"
            debug2 "  - $fig"
            cat <<EOF >>$doc.tex
%\hspace*{${sep}cm}  \raise ${hh}cm
\hspace*{${sep}cm}  \rule{0pt}{${hh}cm}
%\begin{minipage}[t]{0.4\linewidth}
%\begin{figure}
%\psfig{figure=$outseptmpfig,height=${hh}cm,width=${ww}cm,angle=0,rheight=0cm}
%\includegraphics[height=${hh}cm,width=${ww}cm]{$outseptmpfig}
%\includegraphics[keepaspectratio,width=${ww}cm]{${fig}}
\includegraphics[height=${hh}cm,width=${ww}cm]{${fig}}
%\end{figure}
%\end{minipage}
EOF
            if [ $c -lt $col -a $h -lt $PLOTNGRAPHS ]; then echo "&">>$doc.tex; fi
         fi
         h=`expr $h + 1`
         c=`expr $c + 1`
      done
      cat <<EOF >>$doc.tex
\\\\
EOF
   done

   debug2 "---"
   cat <<EOF >>$doc.tex
\end{tabular}
\end{document}
EOF

   #------------------------------------------------------------------------------------
   #echo "Generating Postscript output '$doc.ps'."
   comment "* Generating PDF and GIF outputs '$out.pdf' and '$out.gif'."
   #latexcmd="latex -interaction nonstopmode -halt-on-error -file-line-error $doc.tex"
   latexcmd="latex $doc.tex"
   latexcmd="latex -interaction nonstopmode -file-line-error $doc.tex"
   debug2 "  - LaTeX: $latexcmd"
   #eeee10=`latex $doc.tex 2>&1`
   eeee10=`$latexcmd 2>&1`
   latexerr="$?"
   if [ "$latexerr" != "0" ]; then cecho "WARNING: latex generated error $latexerr" "$yellow"; fi
   debug4 "$eeee10"
   outputwritten=`echo "$eeee10"|grep "Output written"`
   comment "* LATEX: $outputwritten"
   #pdflatex $doc.tex
   rm -f $out.ps
   rm -f $out.pdf
   debug2 "  - launching dvips ..."
   eeee11=`dvips $doc -t $PAPERFMT -o $out.ps 2>&1`
   debug4 "$eeee11"
   debug2 "  - launching ps2pdf ..."
   eeee12=`ps2pdf $out.ps 2>&1`
   debug4 "$eeee12"
#pdflatex $doc.tex
   rm -f ${doc}f*$PSTMPEXT $doc.tex header.tex $doc.aux $doc.dvi $doc.log
#ls -l $out.ps

   if [ "$nogif" = "0" ]; then
      if [ -s $out.ps -a "$nogif" = "0" ]; then
         comment "* Generating PPM output '$out.ppm'."
#         debug3 "  echo \"\" |gs -sDEVICE=ppm -sOutputFile=$out.ppm -q -r128 $out.ps quit.ps 2> /dev/null"
#         eeee13=`echo "" |gs -sDEVICE=ppm -sOutputFile=$out.ppm -q -r128 $out.ps quit.ps 2> /dev/null`
         debug3 "  echo \"\" |gs -sDEVICE=ppm -sOutputFile=$out.ppm -q -r128 $out.ps 2> /dev/null"
         eeee13=`echo "" |gs -sDEVICE=ppm -sOutputFile=$out.ppm -q -r128 $out.ps 2> /dev/null`
         debug4 "$eeee13"
         if [ -s $out.ppm ]; then
            comment "* Generating GIF output '$out.gif'."
            eeee14=`(cat $doc.ppm | ppmtogif > $out.gif ) 2>&1`
            debug2 "$eeee14"
            rm -f $out.ppm
         fi
      else
         cecho "WARNING: GIF not generated because '$out.ps' is missing." "$yellow"
      fi
   fi
   rm -f $out.ps
   if [ -s "$out.pdf" ]; then
      [ "$TOPDFVIEWER" = "1" ] && $PDFVIEWER $out.pdf || \
         echo "--> You can visualize the PDF with: evince $out.pdf"
   fi

   #------------------------------------------------------------------------------------
   #printext="ps"
   printext="pdf"
   if [ -s $out.$printext -a "$tolpr" = "1" ]; then
      comment "* Printing '$out.$printext'"
      lpr $out.$printext
   fi
}

#####################################################
function isnegative () {
   local checkneg=`echo "\
      define sgn(x) {if (x>0) return(1) else return(-1)} sgn($y0lim) \
      "|bc -l`
   [ "$checkneg" -lt "0" ] && isneg="1" || isneg="0"
   debug2 "isneg=$isneg"
}

#####################################################
function isnumeric () {
   local x="$1"
   if [[ "$x" =~ ^[\+-]*[0-9]*[\.]*[0-9]+([eE][\+-]*[0-9]+)?$ ]]; then isnum="1"; else isnum="0"; fi
}

#####################################################
function lessthanepsilon () {
   local a="$1"
   local b="$2"
   local mantissaepsilon="0.00001"
   local lognum="2"
   debug2 "a=$1 b=$2  mantissaepsilon=$mantissaepsilon"
   lesstheps=`echo "\
      define eps() { return($mantissaepsilon) } \
      define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
      define abs(x) {if (x>0) return(x) else return(-x)} \
      define zero(x) {if (x==0) return(1) else return(0)} \
      define logdelta(x,y) { auto d; d=trunc((l(abs(y-(x)))-l(abs(x)))/l(10)); return (d) } \
      define ltlogeps(x,y) { if (logdelta(x,y) <l(eps())/l(10)) return(1) else return(0) } \
      define lte(x,y) { auto d; d=abs(y-(x)); if (zero(d)) return (1) else return (ltlogeps(x,y)) } \
      lte($a,$b)"\
      |bc -l|awk '{printf("%d",$0)}'`
   debug2 "lesstheps=$lesstheps"
}

#####################################################
function gtg_oneplot () {
   local tidx="$1"
   local outsepfig
   local outseptmpfig
   local gnuplotfile
#   local t="${TABLER[$tidx]}"
   local t=`expr $tidx - 1`
   
   #
   gthp_initgfxparams "$t"

   # Sanity check
   if [ "${PLOTHEADER[$t]}" = "" ]; then
      cecho "WARNING: TABLE[$tidx] (#$t) not available in $MYDATAFILE !" "$yellow"
      return 1
   fi
   # assert: found a table
   FOUNDTABLES=`expr $FOUNDTABLES + 1`

   outsepfig="$fig$HISTOSUFFIX$PSEXT"
   outseptmpfig="$fig$HISTOSUFFIX$PSTMPEXT"

   comment "* Generating $outsepfig PLOT for table #${TABLEV[$t]} ($graphname1) ..."
   gnuplotfile=`mktemp ${GPBASEFN}XXXXXX.gp` # e.g. "gnuplot.tmp"
   verbose1 "* gnuplotfile=$gnuplotfile"
   verbose1 "* datafile=$dat"

   #--------------------------------------------------------------------------
   # START A NEW GNUPLOT FILE
   rm -f $outseptmpfig $fig$HGEXT 2>/dev/null

   echo "#!/usr/bin/gnuplot" >$gnuplotfile
   echo "" >>$gnuplotfile
   if [ "$TOSCREEN" = "0" ]; then
      # Produce ps
      echo "set terminal $SEPFIG" >> $gnuplotfile
      echo "set output \"$outsepfig\"" >> $gnuplotfile
   fi
   #--------------------------------------------------------------------------
   # manage the '_' and ':'
   graphxlabel2=`echo "$graphxlabel1"|sed 's/:/,/g'`
   graphztitle2=`echo "$graphztitle1"|sed 's/:/,/g'`
   graphzvalues2=`echo "$graphzvalues1"|sed 's/:/,/g'`
   debug1 "tit=$tit"
   debug2 "graphzlabel2=$graphzlabel2"
   debug2 "graphztitle2=$graphztitle2"
   debug2 "graphzvalues2=$graphzvalues2"

   #--------------------------------------------------------------------------
   #
   echo "set format xy \"%g\"">>$gnuplotfile
   echo "set xlabel \"$graphxlabel2\"">>$gnuplotfile
   echo "set ylabel \"$tit\"">>$gnuplotfile

#RG
#set grid y 3 #from version 3.6
#  set grid ytics lw 1 # DOESN'T WORK
   echo "set grid ytics lw 1 lt 0">>$gnuplotfile
   echo "set pointsize 1.6 #from version 3.6">>$gnuplotfile

#   local eminx=$(echo "scale=2;$minx" | bc)
#   if ! [[ "$minx" =~ ^[\+-]*[0-9]*[\.]*[0-9]+([eE][\+-]*[0-9]+)?$ ]] ; then
#   if ! [[ "$minx" =~ ^[\+-]*[0-9]*[\.]*[0-9]+([eE][\+-]*[0-9]+)?$ && "$eminx" != "0" ]] ; then
   #
   local eminx="0"
   isnumeric $minx; # sets $isnum
   debug3 "minx=$minx isnum=$isnum"
   [ "$isnum" = "1" ] && eminx=$(echo "scale=2;$minx" | bc)
   if ! [[ "$isnum" = "1" && "$eminx" != "0" ]]; then
      xalpha=":xtic(1)"
      xnum=""
      xrotate="rotate by 30 right"
   else
      if [ "$HISTOGRAM" = "1" ]; then
         xalpha=":xtic(1)"
         xnum=""
         xrotate=""
      else
         xalpha=""
#         xnum="(\$1):"  # in this case I get only actual values not interpolated ones
         xnum="1:"
         xrotate=""
      fi
   fi
#xalpha=":xtic(1)"
#xnum=""
   debug1 "xrec=$xrec  xnum='$xnum'  xalpha='$xalpha'"
   if [ "$xnum" = "" ]; then xlimits="0"; fi #patch
   if [ "$xscal" = "log" -a "$xlimits" != "0" -a "$HISTOGRAM" = "0" -a "$xrec" -gt "1" ]; then
      echo "set logscale x $xlogn" >>$gnuplotfile
   fi

   echo "#set key $kx,$ky">>$gnuplotfile
   echo "#set key left Left reverse title \"$graphztitle2\"">>$gnuplotfile
#   echo "set key left top reverse title \"$graphztitle2\"">>$gnuplotfile
#   echo "set key font \",14\"">>$gnuplotfile
   echo "set key font \",20\"">>$gnuplotfile
   #--------------------------------------------------------------------------

   #--------------------------------------------------------------------------
   # Define the keystring
   local keystring="set nokey"
   local keylen

   # Define the keylenght (including Xlabel)
   debug2 "keys=$keys"
   keylen=`echo $keys | awk '{print NF}'`

   if [ $keylen -ge 2 ]; then # it includes the Xlabel
      # Set keys
      local keypos1a
      local keypos1b
      local keypos1c
      if [ "$keypos1" != "" ]; then
         keypos1a=`echo $keypos1|cut -d" " -f 1`
         keypos1b=`echo $keypos1|cut -d" " -f 2`
         keypos1c=`echo $keypos1|cut -d" " -f 3`
      else
######         keypos1a="left"
#         keypos1a="center"
#         keypos1b="Left"
#         keypos1c="top"
#         keypos1a="right"
         keypos1a="left"
         keypos1b="Left"
         keypos1c="top"
      fi

      keystring="set key $keypos1a $keypos1b $keypos1c reverse"
#      keystring="set key $keypos1a $keypos1b $keypos1c reverse outside"
      # Add Key Title
      if [ "$graphztitle2" != "-" -a "$graphztitle2" != "$graphzvalues2" ]; then
         keystring="$keystring title \"$graphztitle2\""
      fi
   fi
   keystring=`echo "$keystring"|tr '_' ' '`
   echo "$keystring" >>$gnuplotfile
   #--------------------------------------------------------------------------

   #--------------------------------------------------------------------------
##RG
##set grid y 3 #from version 3.6
##  set grid ytics lw 1 # DOESN'T WORK
#   echo "set grid ytics lw 1 lt 0">>$gnuplotfile
#   echo "set pointsize 1.6 #from version 3.6">>$gnuplotfile
#
##   if ! [[ "$minx" =~ ^[\+-]*[0-9]*[\.]*[0-9]+([eE][\+-]*[0-9]+)?$ ]] ; then
#   local eminx=$(echo "scale=2;$minx" | bc)
#   if ! [[ "$minx" =~ ^[\+-]*[0-9]*[\.]*[0-9]+([eE][\+-]*[0-9]+)?$ && "$eminx" != "0" ]] ; then
#      xalpha=":xtic(1)"
#      xnum=""
#   else
#      xalpha=""
##     xnum="(\$1):"  # in this case I get only actual values not interpolated ones
#      xnum="1:"
#   fi
#xalpha=":xtic(1)"
#xnum=""

   isnumeric $x0lim; isnumx0="$isnum"; debug2 "x0lim isnumeirc? $isnumx0"
   isnumeric $x1lim; isnumx1="$isnum"; debug2 "x1lim isnumeirc? $isnumx1"
   if [ "$isnumx0" = "1" -a "$isnumx1" = "1" ]; then

      # check if the mantissas are close
      lessthanepsilon $x0lim $x1lim; # result in $lesstheps
      # if too close: makes a default displacment of -1 and +1
      if [ "$lesstheps" = "1" ]; then
         x0lim=`echo "$x0lim - 1"|bc -l|awk '{printf("%g",$0)}'`
         x1lim=`echo "$x1lim + 1"|bc -l|awk '{printf("%g",$0)}'`
      fi
      debug2 "x0lim(filtered)=$x0lim"
      debug2 "x1lim(filtered)=$x1lim"

      # xstep
      if [ "$xstep" = "auto" -a "$xnum" != "" ]; then
         lognum="2"
         # NOTE: mye has been defined since in bc e(-10000) may be VERY slow
         xsteprange=`echo \
            "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
               define mye(x) {if (x<-100) return(0) else return(e(x))} \
               define abs(x) {if (x>0) return(x) else return(-x)} \
               mye(l($lognum)*(trunc(l(abs($x1lim-($x0lim)))/l($lognum))-1))"\
               |bc -l|awk '{printf("%g",$0)}'`
         debug2 "xsteprange=$xsteprange"
         xstep="$xsteprange"
         graphxtics1="1"
         if [ "$xrec" = "2" ]; then xstep="autofreq"; fi
      fi
   fi
   if [ "$xnum" = "" ]; then xlimits="0"; fi
   debug2 "xlimits=$xlimits"
#xstep="1"
#graphxtics1=""
   debug2 "graphxtics1=$graphxtics1"
   debug2 "xstep=$xstep"

   #
   debug3 "ylogn=$ylogn"
   if [ "$ylogn" = "auto" ]; then
      ylogn="10"
   fi
   debug2 "new ylogn=$ylogn"

   #
   local yvalid="1"
   debug3 "y0lim=$y0lim"
   debug3 "y1lim=$y1lim"
   isnumeric $y0lim; isnumy0="$isnum"; debug2 "y0lim isnumeirc? $isnumy0"
   isnumeric $y1lim; isnumy1="$isnum"; debug2 "y1lim isnumeirc? $isnumy1"

   if [ "$y0lim" = "-" -o "$y1lim" = "-" -o "$y0lim" = "" -o "$y1lim" = "" ]; then
#      y0lim="-1"; y1lim="1"; yvalid="0"
      y0lim="-0.5"; y1lim="0.5"; yvalid="0"; ystep="0"; yscal="lin"
      debug2 "yvalid=$yvalid"
      debug2 "ystep=$ystep"
      debug2 "yscal=$yscal"
   else
      y0lim=`echo "$y0lim"|sed -e 's/[eE]+*/\\*10\\^/'`
      y1lim=`echo "$y1lim"|sed -e 's/[eE]+*/\\*10\\^/'`
      debug2 "yvalid=$yvalid"
      debug2 "y0lim(filtered)=$y0lim"
      debug2 "y1lim(filtered)=$y1lim"

      # check if the mantissas are close
      lessthanepsilon $y0lim $y1lim; # result in $lesstheps
### TO BE REVIEWED: maybe it's not necessary (may cause problems with a positive single number)
#      # if too close: makes a default displacment of -1 and +-l
#      if [ "$lesstheps" = "1" ]; then
#         y0lim=`echo "$y0lim - 1"|bc -l`
#         y1lim=`echo "$y1lim + 1"|bc -l`
#      fi
      debug2 "y0lim(filtered2)=$y0lim"
      debug2 "y1lim(filtered2)=$y1lim"

      if [ "$lesstheps" = "1" ]; then
         ystep="0"
         yscal="lin"
      else
         # ystep
         if [ "$ystep" = "auto" ]; then
            lognum="2"
            ysteprange=`echo \
               "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
                  define mye(x) {if (x<-100) return(0) else return(e(x))} \
                  mye(l($lognum)*(trunc(l($y1lim-($y0lim))/l($lognum))-1))"\
                  |bc -l|awk '{printf("%g",$0)}'`
            debug2 "ysteprange=$ysteprange"
            ystep="$ysteprange"
         fi
         debug2 "ystep=$ystep"
   
         # ylogdeltarange
         if [ "$yscal" = "auto" ]; then
            lognum="2"
            debug2 "y0lim=$y0lim"
            debug2 "y1lim=$y1lim"
            debug2 "ylogn=$ylogn"
            ylogdeltarange=`echo \
               "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
                  define abs(x) {if (x>0) return(x) else return(-x)} \
                  trunc((l((abs($y1lim-($y0lim))))-l(abs($y0lim)))/l($lognum)+1)"\
                  |bc -l|awk '{printf("%d",$0)}'`
            debug2 "ylogdeltarange=$ylogdeltarange"

            #
            if [ "$ylogdeltarange" -gt "1" -o "$ylogdeltarange" -lt "-1" ]; then
               yscal="log"
            else
               yscal="lin"
            fi
         fi
      fi
   fi
   
   # Check if y0 or y1 are negative --> no log scale!
   isnegative $y0lim; isnegy0="$isneg"
   isnegative $y1lim; isnegy1="$isneg"
   if [ "$isnegy0" = "1" -o "$isnegy1" = "1" ]; then
      cecho "WARNING: switching to a linear scale (negative y0lim or y1lim)" "$yellow"
      yscal="lin"
   fi
   if [ "$yscal" = "log" ]; then echo "set logscale y $ylogn" >>$gnuplotfile; fi
   debug2 "yscal=$yscal"

   #
   ystpadd=""
   if [ "$yscal" = "log" ]; then
      lognum="$ylogn"
      debug3 "lognum=$lognum y0lim=$y0lim y1lim=$y1lim"
#            sgn=1; if (l($y0lim)<0) sgn=-1;\
#            ($lognum)^(trunc(l($y1lim)/l($lognum)+sgn*0.9999999))"\
#            e((l($y0lim)/l($lognum)-1.05)*l($lognum))"\
#
#            sgn=1; if (l($y0lim)<0) sgn=-1;\
#
#            sgn=1; if (l($y1lim)<0) sgn=-1;\
#
#            e(trunc(l($y1lim)/l($lognum)+1)*l($lognum))"\
#            sgn($y1lim)*e(trunc(l(abs($y1lim))/l($lognum)-1)*l($lognum))"\
#            trunc(sgn($y1lim)*e(trunc(l(abs($y1lim))/l($lognum)+0.999999999)*l($lognum))+sgn($y1lim)*0.000000001)\
#            trunc(sgn($y1lim)*e(trunc(l(abs($y1lim))/l($lognum)+0.999999999)*l($lognum))+sgn($y1lim)*0.000000001)\
#            sgn($y0lim)*mye(trunc(l(abs($y0lim))/l($lognum)-0.999999999)*l($lognum)-($y0lim/1000))\
#            trunc(sgn($y1lim)*mye(trunc(l(abs($y1lim))/l($lognum)+0.999999999)*l($lognum))+($y1lim/1000))\
      # Sanity checks
      local checkneg=`echo \
         "define sgn(x) {if (x>0) return(1) else return(-1)} \
         sgn($y0lim) \
         "|bc -l`
      debug3 "y0lim checkneg=$checkneg"
      if [ "$checkneg" -lt "0" ]; then
         cecho "WARNING: negative value on a log scale! (y0lim=$y0lim)" "$yellow"
      fi
      checkneg=`echo \
         "define sgn(x) {if (x>0) return(1) else return(-1)} \
         sgn($y1lim) \
         "|bc -l`
      debug3 "y1lim checkneg=$checkneg"
      if [ "$checkneg" -lt "0" ]; then
         cecho "WARNING: negative value on a log scale! (y0lim=$y1lim)" "$yellow"
      fi
      # Calculating an approximation of the nearest integer power of $lognum
#      out0=`echo "\
#         define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
#         define mye(x) {if (x<-100) return(0) else return(e(x))} \
#         define sgn(x) {if (x>0) return(1) else return(-1)} \
#         define abs(x) {if (x>0) return(x) else return(-x)} \
#         sgn($y0lim)*mye(trunc(l(abs($y0lim))/l($lognum)-0.999999999)*l($lognum))*0.999\
#         "|bc -l|awk '{printf("%g",$0)}'`
#      debug3 "out0=$out0"
#      out1=`echo \
#         "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
#         define mye(x) {if (x<-100) return(0) else return(e(x))} \
#         define sgn(x) {if (x>0) return(1) else return(-1)} \
#         define abs(x) {if (x>0) return(x) else return(-x)} \
#         sgn($y1lim)*mye(trunc(l(abs($y1lim))/l($lognum)+0.999999999)*l($lognum))*1.001\
#         "|bc -l|awk '{printf("%g",$0)}'`
#      debug3 "out1=$out1"
      out0=`echo "\
         define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define mye(x) {if (x<-100) return(0) else return(x)} \
         define sgn(x) {if (x>0) return(1) else return(-1)} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         define exp(x) {y=e(x*l(10)); return y} \
         define log(x) {y=l(x)/l(10); return y} \
         define round(x,n) {auto s,y;s=scale;y=(((10^n)*x)+0.5);scale=0;y=y/1;scale=s;y=y/(10^n); return y} \
         s=trunc(log(abs($y0lim))+(1+sgn(log(abs($y0lim))))/2)-1; \
         sgn($y0lim)*round(trunc(abs($y0lim)*exp(-s))*exp(s),-s); \
         " |bc -l|awk '{printf("%g",$0)}'`
      out1=`echo "\
         define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define mye(x) {if (x<-100) return(0) else return(x)} \
         define sgn(x) {if (x>0) return(1) else return(-1)} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         define exp(x) {y=e(x*l(10)); return y} \
         define log(x) {y=l(x)/l(10); return y} \
         define round(x,n) {auto s,y;s=scale;y=(((10^n)*x)+0.5);scale=0;y=y/1;scale=s;y=y/(10^n); return y} \
         s=trunc(log(abs($y0lim))+(1+sgn(log(abs($y0lim))))/2)-1; \
         yyd=round(exp(s),-s); \
         yy1=sgn($y1lim)*round(trunc(abs($y1lim)*exp(-s))*exp(s),-s); \
         sgn($y1lim)*round(abs(yy1)+yyd,-s) \
         " |bc -l|awk '{printf("%g",$0)}'`
      out0f=`echo "$out0"|sed -e 's/[eE]+*/\\*10\\^/'`
      out1f=`echo "$out1"|sed -e 's/[eE]+*/\\*10\\^/'`
      debug3 "out0=$out0  out1=$out1  out0f=$out0f  out1f=$out1f"
      ystep=`echo \
         "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         abs($out1f-($out0f))/5 "\
         |bc -l|awk '{printf("%g",$0)}'`
      ystpf=`echo "$ystep"|sed -e 's/[eE]+*/\\*10\\^/'`
      ystprange2=`echo \
         "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         define log(x) {y=l(x)/l(10); return y} \
         define round(x,n) {auto s,y;s=scale;y=(((10^n)*x)+0.5);scale=0;y=y/1;scale=s;y=y/(10^n); return y} \
         abs(round(log(abs($y1lim))-log(abs($y0lim)),0)) "\
         |bc -l|awk '{printf("%g",$0)}'`
      debug3 "ystep=$ystep  ystprange2=$ystprange2"
      if [ "$ystprange2" -le "1" ]; then
         for i in 0 1 2 3 4 5; do
            ystp1=`echo \
               "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
               $out0f+($i)*($ystpf) "\
               |bc -l|awk '{printf("%g",$0)}'|sed 's/^[ \t]*//'`
            [ -z "$ystpadd" ] && ystpadd="set ytics ($ystp1" || ystpadd="$ystpadd,$ystp1"
         done
         ystpadd="$ystpadd)"
         debug2 "ystpadd=$ystpadd"
         yrng1=`echo \
            "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
            if ($y0lim >=1 && $y1lim <=10) 1 else 0 "\
            |bc -l`
         debug2 "yrng1=$yrng1"
         if [ "$yrng1" = "1" ]; then
            echo "set format y \"%2.1t\"">>$gnuplotfile
         else
#            echo "set format y \"%2.1t{/Symbol \\327}$lognum^{%L}\"">>$gnuplotfile
#            echo "set format y \"%2.1l{/Symbol \\327}$lognum^{%L}\"">>$gnuplotfile
            echo "set format y \"%2.1l×$lognum^{%L}\"">>$gnuplotfile
         fi
      else
         echo "set format y '$lognum^{%L}'">>$gnuplotfile
      fi
   elif [ "$yscal" = "lin" ]; then
      lognum="10"
#      ydeltarange2=`echo \
#         "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
#         define mye(x) {if (x<-100) return(0) else return(x)} \
#         mye((l($y1lim-($y0lim))/l($lognum)*0.05)*l($lognum))"\
#         |bc -l|awk '{printf("%g",$0)}'`
#      debug2 "ydeltarange2=$ydeltarange2"
#      out0=`echo \
#         "$y0lim - $ydeltarange2"\
#            |bc -l|awk '{printf("%g",$0)}'`
#      out1=`echo \
#         "$y1lim + $ydeltarange2"\
#            |bc -l|awk '{printf("%g",$0)}'`
      out0=`echo "\
         define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define mye(x) {if (x<-100) return(0) else return(x)} \
         define sgn(x) {if (x>0) return(1) else return(-1)} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         define exp(x) {y=e(x*l(10)); return y} \
         define log(x) {y=l(x)/l(10); return y} \
         define round(x,n) {auto s,y;s=scale;y=(((10^n)*x)+0.5);scale=0;y=y/1;scale=s;y=y/(10^n); return y} \
         s=trunc(log(abs($y0lim))+(1+sgn(log(abs($y0lim))))/2)-1; \
         sgn($y0lim)*round(trunc(abs($y0lim)*exp(-s))*exp(s),-s); \
         " |bc -l|awk '{printf("%g",$0)}'`
      out1=`echo "\
         define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define mye(x) {if (x<-100) return(0) else return(x)} \
         define sgn(x) {if (x>0) return(1) else return(-1)} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         define exp(x) {y=e(x*l(10)); return y} \
         define log(x) {y=l(x)/l(10); return y} \
         define round(x,n) {auto s,y;s=scale;y=(((10^n)*x)+0.5);scale=0;y=y/1;scale=s;y=y/(10^n); return y} \
         s=trunc(log(abs($y0lim))+(1+sgn(log(abs($y0lim))))/2)-1; \
         yyd=round(exp(s),-s); \
         yy1=sgn($y1lim)*round(trunc(abs($y1lim)*exp(-s))*exp(s),-s); \
         sgn($y1lim)*round(abs(yy1)+yyd,-s) \
         " |bc -l|awk '{printf("%g",$0)}'`
      out0f=`echo "$out0"|sed -e 's/[eE]+*/\\*10\\^/'`
      out1f=`echo "$out1"|sed -e 's/[eE]+*/\\*10\\^/'`
      ystep=`echo \
         "define trunc(x) {auto s;s=scale;scale=0;x=x/1;scale=s;return x} \
         define abs(x) {if (x>0) return(x) else return(-x)} \
         abs($out1f-($out0f))/5 "\
         |bc -l|awk '{printf("%g",$0)}'`
   else
      out0="$y0lim"
      out1="$y1lim"
      ystep="$ystep"
   fi
   debug2 "out0=$out0"
   debug2 "out1=$out1"
   debug2 "ystep=$ystep"

   [ "$xlimits" != "0" ] && echo "set xrange [$x0lim:$x1lim]" >>$gnuplotfile
#   [ "$ylimits" != "0" ] && echo "set yrange [$out0:$out1]" >>$gnuplotfile
   echo "set yrange [$out0:$out1]" >>$gnuplotfile

   if [ "$xlimits" != "0" ]; then
      if [ "$graphxtics1" != "" -a "$yscal" != "log" ]; then
         if [ "$xstep" = "autofreq" ]; then
            echo "set xtics $xstep">>$gnuplotfile
         else
            echo "set xtics $minx,$xstep,$maxx">>$gnuplotfile
         fi
      fi
   else
      echo "set xtics $xrotate" >>$gnuplotfile
   fi
   if [ "$ylimits" != "0" ]; then
      if [ "$graphytics1" != "" -a "$yscal" != "log" ]; then
         echo "set ytics $out0,$ystep,$out1">>$gnuplotfile
      fi
   fi
   [ "$ystpadd" != "" ] && echo "$ystpadd" >>$gnuplotfile
#   [ "$ystpadd" != "" ] && echo "set mytics 10" >>$gnuplotfile

   #--------------------------------------------------------------------------
   if [ "$HISTOGRAM" = "1" ]; then
      echo "set style data histogram">>$gnuplotfile
      echo "set style histogram clustered">>$gnuplotfile
      echo "set style histogram gap 1">>$gnuplotfile # to center the bars around an x-value
      echo "set style fill solid border">>$gnuplotfile
   fi

   #--------------------------------------------------------------------------
   debug1 "plot command - keylen=$keylen"
   echo "plot \\">>$gnuplotfile
   #--------------------------------------------------------------------------
   # Associating Hashmarks
   local j1=1
   local titlestr
   while [ $j1 -le $keylen ]; do
#      t1=`echo $keys | awk -v j=$j '{print $j}'|tr '_' ' '`
#      t=`csim_s2t $t1 $lang`
#      t2=`echo $keys | awk -v j=$j '{print $j}'`
      local j2=`expr $j1 + 1`
      local t2=`echo $graphzvalues2 | awk -v j=$j1 '{print $j}'`
      local tt=`echo $t2 |tr '_' ' '`
      local jf="1"
      local ji="1"
      local jj
      for jj in $graphzvalues2; do 
         if [ "$jj" = "$t2" ]; then jf=$ji; break; fi
         ji=`expr $ji + 1`
      done
      local l
#      l=`echo $genmarkers | awk -v j=$j1 '{print $j}'`
      l=`echo $genmarkers | cut -d' ' -f $jf` 
      debug1 "marker[$jf] ($t2) =$l"
      local trailer=""
      [ $j1 -lt  $keylen ] && trailer=", \\"
#         echo "\"$dat\" using $xnum(\$$j)$xalpha title \"$tt\" "\
      if [ "$yvalid" = "1" ]; then
         echo -n "\"$dat\" using $xnum$j2$xalpha title \"$tt\" ">>$gnuplotfile
         if [ "$HISTOGRAM" = "1" ]; then
            echo "$trailer">>$gnuplotfile
         else
            echo "w linespoint lw 1 pt $l $trailer">>$gnuplotfile
         fi
      else
#         echo "0 title \"$tt\" $trailer">>$gnuplotfile
         sed -i 's/\([ \t]\+\)-/\1NaN/g' $dat #RG190510: patch to avoid '-': use 'NaN'

         [ "$j1" = "1" ] && titlestr="" || titlestr=" title \"$tt\""
         echo "\"$dat\" using $xnum$j2$xalpha$titlestr $trailer">>$gnuplotfile
      fi

      #
      if [ $j1 = $keylen ]; then # last iteration of the while
         if [ "$TOSCREEN" = "1" ]; then
            gnuplotscreenfile=`mktemp ${GPBASEFN}-screen-XXXXXX.gp` # e.g. "gnuplot.tmp"
            verbose1 "* gnuplotscreenfile=$gnuplotscreenfile"
            cat $gnuplotfile > $gnuplotscreenfile
            echo "pause -1" >> $gnuplotscreenfile
         else
            echo "set terminal $SEPFIG" >> $gnuplotfile
            echo "set output \"$outsepfig\"" >> $gnuplotfile
            echo "replot" >> $gnuplotfile
         fi
      fi
      j1=`expr $j1 + 1`

   done

   #--------------------------------------------------------------------------
   if [ "$TOSCREEN" = "0" ]; then
      debug1 "  - GNUPLOT: producing '$outseptmpfig and $outsepfig' ..."
   else
      comment "GNUPLOT: displaying output on screen..."
      cecho "--> PRESS <ENTER> to end/continue" "$blue"
      gnuplot $gnuplotscreenfile
   fi
   gnuplot $gnuplotfile
#ls -l $outseptmpfig $outsepfig

   if [ "$SEPFIG" = "hpgl" ]; then
      sed "{
         s/SP2;/SP1;/g
         s/SP3;/SP1;/g
         s/SP4;/SP1;/g
         s/SP5;/SP1;/g
         s/SP6;/SP1;/g
      }" $fig$HGEXT > $HGEXT.tmp
      mv -f $HGEXT.tmp $fig$HGEXT
   fi

   #--------------------------------------------------------------------------
   debug1 "  - EPSTOOL: adjusting EPS ..."
   # Adjusting eps frame
   rm -f tmp.epstool 2> /dev/null
#RG
#   epstool -b -t4 -otmp.epstool $outsepfig 2>/dev/null
   local eee1=`epstool -b -t4 $outsepfig tmp.epstool 2>/dev/null`
   debug4 "$eee1"
   rm -f $outsepfig 2>/dev/null
#RG
#   epstool -p -o$outsepfig tmp.epstool 2>/dev/null
   local eee2=`epstool -p tmp.epstool $outsepfig 2>/dev/null`
   debug4 "$eee2"
#RG
#   epstool -b -t4 -otmp.epstool $out$PSTMPEXT 2>/dev/null
   local eee3=`epstool -b -t4 $outseptmpfig tmp.epstool 2>/dev/null`
   debug4 "$eee3"
   rm -f $outseptmpfig 2>/dev/null
#RG
#   epstool -p -o$outseptmpfig tmp.epstool 2>/dev/null
   local eee4=`epstool -p tmp.epstool $outseptmpfig 2>/dev/null`
   debug4 "$eee4"
   rm -f tmp.epstool 2> /dev/null
   #--------------------------------------------------------------------------
}

###############################################################
function gtg_genplot () {
   local i
   local t

   # Eliminate old gnuplot files
   if [ "$KEEPGP" = "0" ]; then
      rm -f ${GPBASEFN}* 2>/dev/null
   fi

   # Loop on tables
   debug1 "TABLELIST=$TABLELIST"
   if [ "$TABLELIST" = "" ]; then
      return;
   fi
   debug1 "TABLEV=${TABLEV[@]}"
   # Setup a Reverse Lookup table
   for((t=0; t<${#TABLEV[@]};++t)); do
      TABLER[${TABLEV[$t]}]="$t";
   done

   # Process the TABLELIST
   for i in $TABLELIST; do
      comment "------------------------------------------------------------"
      gtg_oneplot $i
   done
   comment "------------------------------------------------------------"
}

########################################################
# function detectdistro (new algorithm 171007)
#   - this algorithm does not try to install lsb_release
function detectdistro {
   local ich0=`which lsb_release 2>&1`
   local ich1=`echo "$ich0"|grep "not found"`
   local ich2=`echo "$ich0"|grep "no lsb_release"`
   local ich3=`which yum 2>&1|grep yum`
   local ich4=`which apt-get 2>&1|grep apt-get`
   local ich5=`which dnf 2>&1|grep "dnf$"`
   local ich6=`which zypper 2>&1|grep zypper`
   if [[ "$ich1" != "" || "$ich2" != "" ]]; then
      # lsb_release not available - try to use /etc/os-release
      if [ -s "/etc/os-release" ]; then
         DIST=`cat /etc/os-release|awk -F= '/^NAME/{print $2}'|tr -d '"'|awk '{print $1}'`
         DNICKN=""
         DVERNO=`cat /etc/os-release|awk -F= '/^VERSION_ID/{print $2}'|tr -d '"'|awk '{print $1}'`
         DVN1=${DVERNO%.*}
      else # try some guess
         if [ "$ich3" != "" -o "$ich5" != "" ]; then
            DIST="Fedora"
         fi
         #try
         if [ "$ich4" != "" ]; then
            local un=`uname -a 2>/dev/null|grep "Ubuntu"`
            [ "$un" != "" ] && DIST="Ubuntu" || DIST="Debian"
         fi
         DNICKN=""
         DVERNO=""
         DVN1=""
      fi
   else # lsb_release is present
      DIST=`lsb_release -i -s|cut -d " " -f 1`
      DNICKN=`lsb_release -c -s`
      DVERNO=`lsb_release -r -s`
      DVN1=${DVERNO%.*}
   fi

   DISTROORIG=""
   DNICKORIG=""
   #patch to use CentOS
   if [ "$DIST" = "CentOS" -o "$DIST" = "RedHatEnterpriseServer" ]; then
      DISTROORIG="$DIST"
      DVERNO="$DVERNO"
      DIST="Fedora"
      if [ "$DVN1" = "6" ]; then DVERNO="14"; fi
   fi
   if [ "$DIST" = "n/a" ]; then
      #try to guess then...
      DISTROALL=`lsb_release -a|grep "Description"`
      DIST=`echo "$DISTROALL"| awk '{print $2}'`
      DNICKN=`echo "$DISTROALL"| awk '{print $3}'`
   fi

   # define the package manager
   case $DIST in
      Fedora)
         [ "$ich5" != "" ] && PKGINST="dnf" || PKGINST="yum"
         PKGIOPT="-y" ;;
      Ubuntu|Debian)
         PKGINST="apt-get"; PKGIOPT="-q -q" ;;
      SUSE)
         PKGINST="zypper"; PKGIOPT="-n" ;;
      *)
         PKGINST=""; PKGIOPT="" ;;
   esac

   # at exit:
   # echo "Distribution '$DIST' - Version '$DVERNO' - PackageInstaller '$PKGINST"
}

##################################################
function checkapackage() {
   case $1 in
      texlive-enumitem|texlive-latex-extra) exeftest="/usr/share/texlive/texmf-dist/tex/latex/enumitem/enumitem.sty";;
      texlive)          exeftest="/usr/bin/latex";;
      epstool)          exeftest="/usr/bin/epstool";;
      netpbm)           exeftest="/usr/bin/ppmtogif";;
      gnuplot)          exeftest="/usr/bin/gnuplot";;
      lsb|redhat-lsb)   exeftest="lsb_release" ;;
      coreutils)        exeftest="md5sum" ;;
      rpcbind)          exeftest="/usr/bin/rpcinfo /sbin/rpcinfo /usr/sbin/rpcinfo" ;;
      netcat|nmap-ncat) exefest="nc" ;;
      *)                exeftest="" ;;
   esac
}

#####################################################
function checkdependencies() {
   detectdistro
   case $DIST in
      Fedora)
         DEPPKGLIST="$FEDORA_DEPENDENCIES" ;;
      Ubuntu|Debian)
         DEPPKGLIST="$UBUNTU_DEPENDENCIES" ;;
      SUSE)
         DEPPKGLIST="$SUSE_DEPENDENCIES" ;;
      *)
         DEPPKGLIST="" ;;
   esac

   #
   MISSINGPKGS=""
   if [ "$DEPPKGLIST" != "" ]; then
      local p
      local ret
      for p in $DEPPKGLIST; do
         checkapackage $p # returns exeftest
         if [ "$exeftest" != "" ]; then
            local exe
            for exe in $exeftest; do
#               ret=`which $exe 2>/dev/null|grep $exe`
#               [ "$ret" != "" ] && break
               [ -s $exe ] && break
            done
         fi
         if [ "$ret" = "" ]; then
            [ -z "$MISSINGPKGS" ] && MISSINGPKGS="$p" || MISSINGPKGS="$MISSINGPKGS $p"
         fi
      done
   fi

   #
   if [ "$MISSINGPKGS" != "" ]; then
      echo "* MISSING_PACKAGES: $MISSINGPKGS"
      # check if sudo is available
      local suok=`timeout -k 1 2 sudo id 2>/dev/null >/dev/null && echo 1 || echo 0`
      if [ "$suok" = "0"  ]; then
         echo "ERROR: missing package dependencies"
         echo "  - add user '$MYUSER' to sudoers (with 'NOPASSWD:' option) and relaunch this script OR"
         echo "    manually install the missing packages, in such case you (or your sysadmin) should issue:"
         echo "    sudo $PKGINST $PKGIOPT install $MISSINGPKGS"
         exit 1
      else
         echo "Installing missing dependencies:"
         sudo $PKGINST $PKGIOPT install $MISSINGPKGS
      fi
   fi
   return 0
}

###############################################################


# Setup infodir
#infodir=`$myt2p info`
#infodir="."

#-----------------------------------------------------------------------------
#SCRIPT STARTS HERE
#-----------------------------------------------------------------------------
[ -s "$HOME/bin/$MYSHLIB" ] && MYSHLIB="$HOME/bin/$MYSHLIB"
if [ ! -s "$MYSHLIB" ]; then
   cecho "ERROR: cannot find '$MYSHLIB' in current directory or ~/bin" "$red"
   exit 1
fi
source "$MYSHLIB"
FOUNDTABLES="0"
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Manage Command line
gtg_commandline external $GTG_CMDL

# Set local path
SAVEDPATH="`pwd`"
debug1 "SAVEDPATH=$SAVEDPATH"
###############################################################
[ "$debug" -gt "0" ] && gtg_printglobals

getdepfile "$GTHPARSERSH"  # Sets DEPFNAME (and cd to dir where script is)
GTHLOC="$DEPFNAME"
verbose1 "* GTHLOC=$GTHLOC"
debug1 "* PWD=`pwd`"

#-----------------------------------------------------------------------------
# Look for the existance of the programs used by this script
export PATH=".:$PATH"
checkdependencies
##latex --> texlive
#programs="gnuplot latex ppmtogif epstool"
#for p in $programs; do
#   which $p > /dev/null
#   if [ "$?" != "0" ]; then
#      cecho "$0: '$p' program is needed." "$red"
#      exit -2
#   fi
#   if [ "$debug" = "1" ]; then comment "Found '$p' program."; fi
#done

# Look for the existance of the data file
if [ ! -f $df ]; then
   cecho "$0: file '$df' not found." "$red"
   exit -3
fi

#--------------------
debug1 "---------- START sourcing external file '$GTHLOC'"
debug1 "source $GTHLOC --norun --data $df $tablist ${OPT_Q}$strverb$strdebu"
[ "$DEEPDEBUG" = "0" ] && { gtg_savedebug="$debug"; debug="0"; gtg_savestrdebu="$strdebu"; gtg_savestrverb="$strverb"; }
source $GTHLOC --norun --data $df $tablist ${OPT_Q}$strverb$strdebu
# Info from gtparser.sh
#ITPARAMS="metric input nodes cores hd l2c"
#PLOTHEADER="#-Graph1,xyplot,CACHEMISSRATE,,min 2 max log 2,,min 2 max log 2,CACHESIZEKIB 4,NODES"
[ "$DEEPDEBUG" = "0" ] && { debug="$gtg_savedebug"; strdebu="$gtg_savestrdebu"; strverb="$gtg_savestrverb"; }
debug1 "---------- END sourcing external file '$GTHLOC'"

#-----------------------------------------------------------------------------

#gthp_parse

gtg_genplot


#####################################################3

#rm -f data*.tmp tito*.tmp keys*.tmp maxv*.tmp $gnuplotfile
#
GTG_T00001=`date +%s%N | cut -b1-13`
gtg_exptimems=`expr $GTG_T00001 - $GTG_T00000`
gtg_exptimehh=`echo ""|awk 'END{v=exptimems/3600000;printf("%7.2f",v)}' exptimems=$gtg_exptimems`
gtg_tots=`expr $gtg_exptimems / 1000`

if [ "$FOUNDTABLES" -gt "0" ]; then
   [ "$NOLATEX" = "0" ] && gtg_genlatex
#   cecho "GTGRAPH Goodbye." "$green"
   echo "Goodbye. "`cecho -f -n "$GTG_THISTOOLCAP" "$green"`" $gtg_tots secs."
else
   cecho "NO TABLES IN [$TABLELIST] FOUND." "$red"
fi
exit 0

