#!/bin/bash
user=$USER
host=`hostname -s`
release=`lsb_release -r | awk 'NR==1{printf $2}' | cut -c -2`
distro=`lsb_release -d  | awk 'NR==1{print $2.$3}' | cut -c -1`
OS=`lsb_release -d | awk 'NR==1{print $2.$3}'`
date=`date +%Y%m%d%H%M | cut -c 3-`
DATE=`date +%Y/%m/%d/%H:%M`
#core=`lscpu | awk 'NR==4{print "C"$2}'`
core=`getconf _NPROCESSORS_ONLN`
#echo "$distro"
#echo "$date"
#wdir="$HOME/XOCR/ocr" # working dir
wdir=`pwd`
toolname=`pwd | awk -F/ '{print $NF}'`
cpath="fib-papi.sh" #command path
irange="5 10 15 20 25" # input range
inum=9
threshold=1
#irange="3 4 5"
#mstring="EXECUTION TIME" # matching string
#mstring="time elapsed" # matching string
#mcolumn="5" # matching column
#nrep="10" #number of repetition of the test
#-----------------------------------
if [ ! -d "$wdir" ]; then
    echo "ERROR: cannot find '$wdir'"; exit 1
fi

if [ ! -s "$wdir/$cpath" ]; then
    echo "ERROR: cannot find '$cpath' in '$wdir'"; exit 1
fi

cd $wdir
FILENAME=$toolname-$host$distro$release"C"$core-$date
re='^[0-9]+$'
echo "pwd=`pwd`"
printf %s'\n' "++ Fibonacci experiment on $host machine using PAPI v.6.0" > $FILENAME.txt
printf %s'\n' "++ DATE: $DATE" >> $FILENAME.txt
printf %s'\n' "++ USER: $user" >> $FILENAME.txt
printf %s'\n' "++ " >> $FILENAME.txt
printf %s'\n' "++ Working directory: $wdir " >> $FILENAME.txt
printf %s'\n' "++ Machine: $host " >> $FILENAME.txt
printf %s'\n' "++ OS distro: $OS " >> $FILENAME.txt
printf %s'\n' "++ OS release: $release " >> $FILENAME.txt
printf %s'\n' "++ Total numebr of cores: $core " >> $FILENAME.txt
printf %s'\n' "++" >> $FILENAME.txt
printf %s'\n' "++" >> $FILENAME.txt
printf %s'\n' "++" >> $FILENAME.txt

echo "create log and plot file name: $FILENAME.csv"
echo "logs and plots will store into $wdir/logs/"
echo ""
printf %s' ' "idx" "sch_cnt" "wrt_cnt" "rd_cnt" "ld_cnt" "dest_cnt" "sch_cyc" "wrt_cyc" "rd_cyc" "ld_cyc" "dest_cyc">> $FILENAME.txt
printf %s'\n' "fib index,schedule_count,write_count,read_count,load_count,destroy_count" > $FILENAME.csv
for i in $irange; do  
	printf %s'\n' "" >> ${FILENAME}.txt
	#a="0"; min=""; max="0"
	schedule_count=`./$cpath $i $threshold 2>error.log | awk 'NR==5{printf $2}'`
	write_count=`./$cpath $i $threshold 2>error.log | awk 'NR==6{printf $2}'`
	read_count=`./$cpath $i $threshold 2>error.log | awk 'NR==7{printf $2}'`
	load_count=`./$cpath $i $threshold 2>error.log | awk 'NR==8{printf $2}'`
	destroy_count=`./$cpath $i $threshold 2>error.log | awk 'NR==9{printf $2}'`
	schedule_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==11{printf $2}'`
	write_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==12{printf $2}'`
	read_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==13{printf $2}'`
	load_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==14{printf $2}'`
	destroy_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==15{printf $2}'`
	tot_schedule_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==17{printf $2}'`
	tot_write_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==18{printf $2}'`
	tot_read_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==19{printf $2}'`
	tot_load_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==20{printf $2}'`
	tot_destroy_cyc=`./$cpath $i $threshold 2>error.log | awk 'NR==21{printf $2}'`
#	val=`echo "$xschedule_count"`
#	echo "fib($i,$threshold)  $schedule_count $write_count $read_count $load_count $destroy_count $schedule_cyc $write_cyc $read_cyc $load_cyc $destroy_cyc" |  awk '$1=$1' OFS="    "
#  	for r in `seq $nrep`; do

      #output=`./$cpath $i 2>error.log`
      #echo "$output"
	#val=`echo "$output"|awk "/$mstring/{print \\$5}" c=$mcolumn`
      #if ! [[ $val =~ $re ]] ; then echo "ERROR: The output is not a number! (input=$i,output='$val')" >&2; exit 1; fi
      #if [ 1 = `echo "$val > $max"|bc` ]; then max="$val"; fi
      #if [ "$min" = "" ]; then 
      #   min="$val"; a="$val"
      #else
      #   if [ 1 = `echo "$val < $min"|bc` ]; then min="$val"; fi
      #fi
      #i1=`expr $i - 1`
      #a=`echo "define trunc(x) { auto s; s=scale; scale=0; x=x/1; scale=s; return x } trunc($a * $i1 / $i + $val / $i)"|bc -l`
#      echo "$i -- $val -- $a -- $min -- $max"

#	done
	printf %s'\n' "$i,$schedule_count,$write_count,$read_count,$load_count,$destroy_count,$schedule_cyc,$write_cyc,$read_cyc,$load_cyc,$destroy_cyc,$tot_schedule_cyc,$tot_write_cyc,$tot_read_cyc,$tot_load_cyc,$tot_destroy_cyc" >> $FILENAME.csv
	printf %d'\t ' "$i" "$schedule_count" "$write_count" "$read_count" "$load_count" "$destroy_count" "$schedule_cyc" "$write_cyc" "$read_cyc" "$load_cyc" "$destroy_cyc" >> ${FILENAME}.txt
done


for FILE in ${FILENAME}.csv; do
    gnuplot <<- EOF
reset
set terminal pdf font ",8" 
set output "${FILENAME}.pdf"
set style data histograms
set style histogram cluster gap 1.5
set offset -0.5,-0.5,0,0
set datafile separator ","
set style fill solid border -1
set boxwidth 0.5
set multiplot layout 2,2 \
margins 0.1,0.98,0.1,0.98 \
spacing 0.08,0.08
set key left
set key font ",6"
set grid
set format y '10^{%L}' 
set key autotitle columnhead
set logscale y
set yrange [1:]
set xlabel "Fib Index" font ",8" offset 1
set ylabel "DF-Threads call" font ",8" 
plot "${FILENAME}.csv" using 2:xtic(1) title "SCHEDULE",\
	"" using 3 title "WRITE" lt rgb "#21908d",\
	"" using 4 title "READ" lt rgb "#4f127b",\
	"" using 5 title "LOAD" lt rgb "#e55964",\
	"" using 6 title "DESTROY" lt rgb "#b5367a"
unset ylabel
set ylabel "cycles" font",8"
plot "${FILENAME}.csv" using 7:xtic(1) title "SCHEDULE",\
	"" using 8 title "WRITE" lt rgb "#21908d",\
	"" using 9 title "READ" lt rgb "#4f127b",\
	"" using 10 title "LOAD" lt rgb "#e55964",\
	"" using 11 title "DESTROY" lt rgb "#b5367a"
plot "${FILENAME}.csv" using 7:xtic(1) title "schedule"
unset multiplot
set multiplot layout 2,2 \
margins 0.1,0.98,0.1,0.98 \
spacing 0.08,0.08
set title "title"
plot "${FILENAME}.csv" using 3:xtic(1) title "write" 
unset multiplot
unset output
	EOF
done
awk '{
     printf("%d, %d, %.1lf, %.1lf,%.1lf\n", $1,$2,$3,$4,$5); 
 }'  ${FILENAME}.csv > output.txt
#if [ -d "$wdir/logs" ] 
#then
#mkdir -p "$wdir/logs"
#fi
echo "creating directory ${FILENAME}"
mkdir ${FILENAME}
cp ${FILENAME}.csv $wdir/${FILENAME}/
cp ${FILENAME}.txt $wdir/${FILENAME}/
cp ${FILENAME}.pdf $wdir/${FILENAME}/

rm -f *.csv
rm -f *.pdf
rm -f *.txt

#	plot "${FILENAME}.csv" using 1:3 title 'write_count' 
#margins 0.08,0.85,0.08,0.08 \
#spacing 0.05,0.05
#plot for [COL=2] '${FILENAME}.csv' using COL:xtic(1) ti col

#layout 2,2 \
#margins 0.1,0.98,0.1,0.98 \
#spacing 0.08,0.08
