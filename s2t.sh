#!/bin/bash
#THIS IS VERSION 1.0.0 (29-Sep-1995)
VERSION="170602"
###############################################################################
#
# This script transforms a symbol in a PLAIN-TEXT-string
# (english or italian) using the table whose name is in $3 variable
#
###############################################################################

# Exit error codes:
# -1  invalid command line
#

md5sig=`md5sum $0|awk '{print $1}'`
MYSIG="${md5sig:0:2}${md5sig:(-2)}" # reduced-hash: first 2 char and last two char
VERSION1="v${VERSION}-${MYSIG}"

#-----------------------------------------------------------------------------
#####################################################
function usage () {
   echo "This script transform a symbol into text."
   echo "Usage: $0 [<options>] <symbol_name> <language> <conversion_table>"
   echo "   where:"
   echo "   <language>=english means english-language translation"
   echo "   <language>=italian means italian-language translation"
   echo "   <Conversion_table> is the file containing the conversion table"
   echo ""
   echo "   and <options> can be:"
   echo "   -v          verbose mode"
   echo "   -d          debug mode (more times: more details)"
   echo "   -q          quiet mode"
   echo "   -r          raw format"
   echo "   -i          input parameter format"
   echo "   --version   print script version"
   echo ""
   exit 1
}
#####################################################
function commandline () {
   debug="0"
   version="0"
   quiet="0"
   raw="0"
   sym=""
   lan=""
   table=""
   inp="0"
   eopts=""
   while [ $# -gt 0 ]; do case "$1" in
      -v) verbose=`expr $verbose + 1`; shift;;
      -d) debug=`expr $debug + 1`; shift;;
      -q) quiet="1"; shift;;
      -r) raw="1"; shift;;
      --version) version="1"; shift;;
      -[^-]*) usage;;
      *|-)  if [ "$sym" = "" ]; then sym="$1"; shift; continue; fi
          if [ "$lan" = "" ]; then lan="$1"; shift; continue; fi
          if [ "$tab" = "" ]; then tab="$1"; shift; continue; fi
   esac; done

   if [ "$version" = "1" ]; then
      echo "$0: version $VERSION1"; exit 0;
   fi

if [ "$debug" = "1" ]; then echo "sym='$sym' lan='$lan' tab='$tab'"; fi

   # Check syntax
   if [ "$sym" = "" ]; then  usage; fi
   if [ "$lan" = "" ]; then  usage; fi
   if [ "$tab" = "" ]; then  usage; fi

   Lan=""
   if [ "$lan" = "english" ]; then Lan="0"; fi
   if [ "$lan" = "italian" ]; then Lan="1"; fi
   if [ "$Lan" = "" ]; then
      echo "ERROR: <language> has to be either 'english' or 'italian'."
   fi
}

#-----------------------------------------------------------------------------
commandline $*

echo `awk -v s=$sym -v p=$Lan -v raw=$raw -v inp=$inp '\
   {if ($1 == s) {\
      if (raw == 0) {\
         if (inp > 0) { p = 2; } \
         split($0, a, "\"");\
         print a[(p + 1) * 2];\
         exit;\
      }\
      else {\
         print $1; exit;\
      }\
   }\
}' $tab`
