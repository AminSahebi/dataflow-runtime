#!/bin/bash

host=`hostname -s`
release=`lsb_release -r | awk 'NR==1{printf $2}' | cut -c -2`
distro=`lsb_release -d  | awk 'NR==1{print $2.$3}' | cut -c -1`
date=`date +%Y%m%d%H%M | cut -c 3-`
#core=`lscpu | awk 'NR==4{print "C"$2}'`
core=`getconf _NPROCESSORS_ONLN`
#echo "$distro"
#echo "$date"
#wdir="$HOME/XOCR/ocr" # working dir
wdir=`pwd`
toolname=`pwd | awk -F/ '{print $NF}'`
cpath="fib-papi.sh" #command path
irange="10 15 20 25 30" # input range
#irange="3 4 5"
#mstring="EXECUTION TIME" # matching string
#mstring="time elapsed" # matching string
mcolumn="5" # matching column
#nrep="10" #number of repetition of the test
#-----------------------------------
if [ ! -d "$wdir" ]; then
    echo "ERROR: cannot find '$wdir'"; exit 1
fi

if [ ! -s "$wdir/$cpath" ]; then
    echo "ERROR: cannot find '$cpath' in '$wdir'"; exit 1
fi

cd $wdir
FILENAME=$toolname-$host$distro$release"C"$core-$date
re='^[0-9]+$'
echo "pwd=`pwd`"
echo "create log and plot file name: $FILENAME.csv"
echo "logs and plots will store into $wdir/logs/"
echo ""
#printf %s'\n' "n,avg,min,max" > $FILENAME.csv
for i in $irange; do  
	a="0"; min=""; max="0"
#  	for r in `seq $nrep`; do

      output=`./$cpath $i 2>error.log`
      echo "$output"
	val=`echo "$output"|awk "/$mstring/{print \\$5}" c=$mcolumn`
      #if ! [[ $val =~ $re ]] ; then echo "ERROR: The output is not a number! (input=$i,output='$val')" >&2; exit 1; fi
      #if [ 1 = `echo "$val > $max"|bc` ]; then max="$val"; fi
      #if [ "$min" = "" ]; then 
      #   min="$val"; a="$val"
      #else
      #   if [ 1 = `echo "$val < $min"|bc` ]; then min="$val"; fi
done
